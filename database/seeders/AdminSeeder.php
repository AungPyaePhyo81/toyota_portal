<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $firstRecord = User::first();
        if ($firstRecord) {
            echo "Aborted with message 'User table exists data.' : ";
            return false;
        }

        User::query()->create([
            'name' => 'System Admin',
            'email' => 'toyota@mail.com',
            'password' => bcrypt('admin@toyota'), // admin@toyota
        ]);
    }
}
