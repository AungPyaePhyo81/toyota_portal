<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_bookings', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('type_of_service_id');
            $table->unsignedBigInteger('province_id');
            $table->unsignedBigInteger('dealer_info_id');
            $table->date('date');
            $table->time('time')->nullable();
            $table->string('name');
            $table->string('email');
            $table->string('phone');
            $table->string('dial_code');
            $table->tinyInteger('notification')->default(0);
            $table->tinyInteger('is_confirm')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_bookings');
    }
}
