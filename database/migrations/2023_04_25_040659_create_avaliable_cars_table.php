<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvaliableCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('avaliable_cars', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('color_id');
            $table->unsignedBigInteger('car_model_id');
            $table->unsignedBigInteger('dealer_id')->nullable();
            $table->Integer('status')->default(0);
            $table->string('engine')->nullable();
            $table->string('transmission')->nullable();
            $table->string('fuel')->nullable();
            $table->string('car_no')->nullable();
            $table->string('steering_pos')->nullable();
            $table->string('trim_grade')->nullable();
            $table->string('driven_kilo')->nullable();
            $table->string('model_year')->nullable();
            $table->string('contact_no')->nullable();
            $table->longText('thumbnail');
            $table->longText('image')->nullable();
            $table->string('price')->nullable();
            $table->foreign('color_id')->references('id')->on('colors')->onDelete('cascade');
            $table->foreign('car_model_id')->references('id')->on('car_models')->onDelete('cascade');
            $table->foreign('dealer_id')->references('id')->on('dealers')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('avaliable_cars');
    }
}
