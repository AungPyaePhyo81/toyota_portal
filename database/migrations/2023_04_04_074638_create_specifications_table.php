<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpecificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('specifications', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('car_model_id');
            $table->string('horse_power')->nullable();
            $table->string('torque')->nullable();
            $table->string('engine_name')->nullable();
            $table->string('grade')->nullable();
            $table->string('driven_train')->nullable();
            $table->longText('dimensions_weight')->nullable();
            $table->longText('engine')->nullable();
            $table->longText('chassi')->nullable();
            $table->string('price')->nullable();
            $table->longText('standard_equipment_exterior')->nullable();
            $table->longText('standard_equipment_interior')->nullable();
            $table->longText('safety_system')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('car_model_id')->references('id')->on('car_models')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('specifications');
    }
}
