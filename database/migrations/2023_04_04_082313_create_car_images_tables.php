<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateCarImagesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_images', function (Blueprint $table) {
            $table->id();
            $table->longText('image');
            $table->unsignedBigInteger('car_models_id');
            $table->unsignedBigInteger('colors_id');          
            $table->timestamps();
            $table->foreign('car_models_id')->references('id')->on('car_models')->onDelete('cascade');
            $table->foreign('colors_id')->references('id')->on('colors')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_images_tables');
    }
}
