<?php

namespace App\Http\Controllers;

use App\Models\AvaliableCar;
use App\Models\Map;
use App\Models\Blog;
use App\Models\City;
use App\Models\Dealer;
use App\Models\CarModel;
use App\Models\DealersInfo;
use App\Models\SubCategory;
use App\Models\Color;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class AjaxController extends Controller
{
    public function getDealersByCity(Request $request)
    {
        $city = $request->city_id;
        $dealers = Dealer::with('dealerInfos.map.city')
            ->whereHas('dealerInfos.map.city', function ($query) use ($city) {
                $query->where('id', $city);
            })->get();

        return response()->json($dealers);
    }


    public function getModelsByCategory(Request $request)
    {
        $category_id = $request->category_id;

        $models = CarModel::whereHas('subCategory', function ($q) use ($category_id) {
            $q->where('category_id', $category_id);
        })->get();

        return response()->json($models);
    }

    public function getDealerById(Request $request)
    {
        $dealer = Dealer::where('id', $request->id)->with('dealerInfos.map')->first();
        return response()->json($dealer);
    }

    public function getDealersByCityAndDealer(Request $request)
    {
        $query = Dealer::query();

        if (isset($request->city_id)) {
            $query = $query->whereHas('dealerInfos.map.city', function ($query) use ($request) {
                $query->where('id', $request->city_id);
            });
        }

        if (isset($request->dealer_id)) {
            $query = $query->where('id', $request->dealer_id);
        }

        $dealers = $query->with('dealerInfos.map.city')->get();
        return response()->json($dealers);
    }

    public function getSubCategoryByModel(Request $request)
    {
        $model = $request->model_id;

        $sub = SubCategory::whereHas('carModel', function ($q) use ($model) {
            $q->where('id', $model);
        })->with('carModel')->first();

        return response()->json($sub);
    }

    public function getServiceCentersByProvince(Request $request)
    {
        $data      = $request->all(); //type_of_service_id, province_id
        $city_ids  = City::where('province_id', $data['province_id'])->pluck('id')->toArray();

        $map_ids = [];
        foreach ($city_ids as $city_id) {
            $map = Map::select('id')->where('city_id', $city_id)->first();
            $map_ids[] = $map->id;
        }

        $dealers_info_arr = DealersInfo::with('map')->whereIn('map_id', $map_ids)->get()->toArray();

        $dealers_infos = [];
        foreach ($dealers_info_arr as $dealers_info) {
            if (in_array($data['type_of_service_id'], json_decode($dealers_info['service']))) {
                $dealers_infos[] = $dealers_info;
            }
        }

        // logger($dealers_infos);
        return response()->json($dealers_infos);
    }

    public function inventoryFilter(Request $request)
    {
        $exact = [];
        if (!empty($request->dealerIds) || !empty($request->modelIds) || !empty($request->colorIds)) {
            $query = AvaliableCar::query();
            if (!empty($request->dealerIds)) {
                $query = $query->whereIn('dealer_id', $request->dealerIds);
            }

            if (!empty($request->modelIds)) {
                $query = $query->whereIn('car_model_id', $request->modelIds);
            }

            if (!empty($request->colorIds)) {
                $query = $query->whereIn('color_id', $request->colorIds);
            }

            $exact = $query->where('status', 0)->with('dealer', 'carModel')->get();
        }

        $dealerIds = $request->dealerIds ?? [];
        $modelIds = $request->modelIds ?? [];
        $colorIds = $request->colorIds ?? [];
        $similar = AvaliableCar::where(function ($query) use ($dealerIds, $modelIds, $colorIds) {
            $query->whereIn('dealer_id', $dealerIds)->orWhereIn('car_model_id', $modelIds)->orWhereIn('color_id', $colorIds);
        })->where(function ($query) {
            $query->where('status', 0);
        })->with('dealer', 'carModel')->get();

        if (!empty($exact)) {
            $similar = $similar->reject(function ($car) use ($exact) {
                return $exact->contains($car);
            });
        }
        return response()->json(['exact' => $exact, 'similar' => $similar]);
    }

    public function getModelById(Request $request)
    {
        $id = $request->model_id;
        $model = CarModel::where('id', $id)->with('subCategory')->first();
        return response()->json($model);
    }

    public function getColorsByModel(Request $request)
    {
        $colors = Color::where('car_models_id', $request->model_id)->get();
        return response()->json($colors);
    }
}
