<?php

namespace App\Http\Controllers;

use App\Mail\EnquiryMail;
use App\Mail\OnlineInquiryMail;
use App\Models\Blog;
use App\Models\SubCategoryInformation;
use App\Models\Category;
use App\Models\Enquiry;
use App\Models\SubCategory;
use App\Models\CarModel;
use App\Models\Color;
use App\Models\Specification;
use App\Models\Dealer;
use App\Models\AvaliableCar;
use Illuminate\Http\Request;
use App\Models\Promotion;

use App\Models\City;
use App\Models\DealersInfo;
use App\Models\Province;
use App\Models\ServiceBooking;
use App\Models\StayInformed;
use App\Models\TypeOfService;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Response;
use App\Models\Link;
use App\Models\OnlineInquiry;

use function Ramsey\Uuid\v1;

class FrontEndController extends Controller
{
    public function home()
    {
        $blogs = Blog::paginate(10);
        $categories = Category::with('subCategories')->get();
        $subCategories = SubCategory::get();
        $usedCars = Category::with(['subCategories.carModel.avaliableCar'])
            ->get()->map(function ($category) {
                return [
                    'category_id' => $category->id,
                    'category_name' => $category->name,
                    'available_cars' => $category->subCategories->flatMap(function ($subCategory) {
                        return $subCategory->carModel->flatMap(function ($model) {
                            return $model->avaliableCar->where('status', 1);
                        });
                    })
                ];
            })->toArray();
        $usedCounts = AvaliableCar::where('status', 1)->count();
        // dd($usedCars);

        return view('home', compact('blogs', 'categories', 'subCategories', 'usedCars', 'usedCounts'));
    }


    /* ================================================== Promotion ================================================== */

    public function promotion()
    {
        $promotions = Promotion::whereDate('promotion_end_date', '>=', now()->format('Y-m-d'))->orderBy('promotion_start_date', 'desc')->get();

        $dealers = Dealer::all();

        return view('promotion.index', compact('promotions', 'dealers'));
    }

    public function promotionFilter(Request $request)
    {
        $dealer = $request->dealer_id ?? null;
        $sort = $request->sort;
        $start_date = $request->start_date ?? null;
        $end_date = $request->end_date ?? null;
        $query = Promotion::orderBy('promotion_start_date', $sort);
        // $shareButtons = \Share::page(url()->current())
        //     ->facebook()
        //     ->linkedin();

        // Input 2 dates
        if (isset($request->start_date) && isset($request->end_date)) {
            $start_date = $request->start_date;
            $end_date = $request->end_date;
            $query = $query->whereBetween('promotion_end_date', [$start_date, $end_date])
                ->whereBetween('promotion_start_date', [$start_date, $end_date]);
        }
        // Input only one date
        elseif (isset($request->start_date) || isset($request->end_date)) {
            $date = $start_date ?? $end_date;
            $query = $query->where('promotion_start_date', '<=', $date)
                ->where('promotion_end_date', '>=', $date);
        }
        // No date input filled
        else {
            $query = $query->whereDate('promotion_end_date', '>=', now()->format('Y-m-d'));
        }

        // Dealer Input fill
        if (isset($dealer)) {
            $query = $query->where('dealer_id', 'like', '%"' . $dealer . '"%');
            $dealer = Dealer::where('id', $dealer)->first()->name;
        }

        $promotions = $query->orderBy('created_at', $sort)->get();
        $dealers = Dealer::all();
        return view('promotion.index', compact('promotions', 'dealers', 'sort', 'start_date', 'end_date', 'dealer'));
    }

    public function promotionShare($id)
    {
        $promotion = Promotion::findOrFail($id);
        return view('promotion.share', compact('promotion'));
    }




    /* ================================================== Blog ================================================== */

    public function blog()
    {
        $blogs = Blog::orderBy('date', 'desc')->orderBy('id', 'desc')->paginate(3);
        $years = Blog::selectRaw('YEAR(date) AS year, COUNT(*) AS count')->groupBy('year')->orderBy('year', 'desc')->get()->toArray();

        foreach ($years as $idx => $year) {
            $years[$idx]['blogs'] = Blog::whereYear('date', $year['year'])->orderBy('id', 'desc')->get();
        }

        return view('blog.index', compact('blogs', 'years'));
    }

    public function blogDetail($id)
    {
        $blog = Blog::findOrFail($id);
        return view('blog.show', compact('blog'));
    }

    public function blogSearch(Request $request)
    {
        $topic = $request->blog_topic;
        $search = $request->blog_text;

        // Filter apply on text search and topic search
        if (!is_null($search) && $topic != null) {
            $blogs = Blog::where('status', $topic)
                ->where(function ($q) use ($search) {
                    $q->where('title', 'LIKE', "%{$search}%")
                        ->orWhere('long_description', 'LIKE', "%{$search}%")
                        ->orWhere('slug', 'LIKE', "%{$search}%");
                })->orderBy('date', 'desc')->orderBy('id', 'desc')->paginate(3);
        }
        // Filter apply only on text search
        elseif (!is_null($search) && $topic == null) {
            $blogs = Blog::where(function ($q) use ($search) {
                $q->where('title', 'LIKE', "%{$search}%")
                    ->orWhere('long_description', 'LIKE', "%{$search}%")
                    ->orWhere('slug', 'LIKE', "%{$search}%");
            })->orderBy('date', 'desc')->orderBy('id', 'desc')->paginate(3);
        }
        // Filter apply only on topic search
        elseif (is_null($search) && $topic != null) {
            $blogs = Blog::where('status', $topic)->orderBy('date', 'desc')->orderBy('id', 'desc')->paginate(3);
        }
        // No Filter Applied
        else {
            $blogs = Blog::orderBy('date', 'desc')->orderBy('id', 'desc')->paginate(3);
        }

        $years = Blog::selectRaw('YEAR(date) AS year, COUNT(*) AS count')->groupBy('year')->get()->toArray();

        foreach ($years as $idx => $year) {
            $years[$idx]['blogs'] = Blog::whereYear('date', $year['year'])->orderBy('date', 'desc')->get();
        }
        return view('blog.index', compact('blogs', 'years', 'topic', 'search'));
    }

    public function blogShare($id)
    {
        $blog = Blog::findOrFail($id);
        return view('blog.share', compact('blog'));
    }


    /* ================================================== Dealer ================================================== */

    public function dealer()
    {
        $cities = City::all();
        $dealers = Dealer::all();
        return view('dealer.index', compact('cities', 'dealers'));
    }

    public function dealerDetail($id)
    {
        // dd($id);
        $dealer = Dealer::findOrFail($id);
        return view('dealer.detail', compact('dealer'));
    }


    /* ================================================== Inventory ================================================== */

    public function inventory()
    {
        $categories = Category::all();
        $subCategories = SubCategory::all();
        return view('search_inventory.index', compact('categories', 'subCategories'));
    }

    public function inventoryDetail($id)
    {
        $sub_cate = SubCategory::findOrFail($id);
        // $dealers = Dealer::with('dealerInfos.map.city')->get();
        $dealers = Dealer::select('dealers.*')
            ->leftJoin('avaliable_cars', 'dealers.id', '=', 'avaliable_cars.dealer_id')
            ->leftJoin('car_models', 'car_models.id', '=', 'avaliable_cars.car_model_id')
            ->where('car_models.sub_category_id', '=', $sub_cate->id)->get();
        $colors = Color::get()->unique('code');
        $models = CarModel::where('sub_category_id', $id)->get();

        return view('search_inventory.detail', compact('dealers', 'sub_cate', 'colors', 'models'));
    }
    public function carDetail($id)
    {
        $sub_cate = SubCategory::findorFail($id);

        $infos = SubCategoryInformation::where('sub_category_id', $id)->get();
        $carModels = CarModel::where('sub_category_id', $id)
            ->with('subCategory')
            ->get();

        $specifications = collect(); // initialize an empty collection
        foreach ($carModels as $carModel) {
            $specs = Specification::where('car_model_id', $carModel->id)
                ->with('carModel')
                ->get();
            $specifications = $specifications->merge($specs);
        }
        return view('model.car_detail', compact('sub_cate', 'carModels', 'specifications', 'infos'));
    }

    /* ================================================== Used Car ================================================== */

    public function usedCarDetail($id)
    {
        $usedCar = AvaliableCar::findOrFail($id);
        return view('used_car.show', compact('usedCar'));
    }

    public function usedCar()
    {
        $usedCars = Category::with(['subCategories.carModel.avaliableCar'])
            ->get()->map(function ($category) {
                return [
                    'category_id' => $category->id,
                    'category_name' => $category->name,
                    'available_cars' => $category->subCategories->flatMap(function ($subCategory) {
                        return $subCategory->carModel->flatMap(function ($model) {
                            return $model->avaliableCar->where('status', 1);
                        });
                    })
                ];
            })->toArray();

        return view('used_car.index', compact('usedCars'));
    }

    public function usedCarSearch(Request $request)
    {
        $search = $request->term;
        $searchResult = AvaliableCar::where('status', 1)
            ->where(function ($q) use ($search) {
                $q->where('engine', 'LIKE', "%$search%")
                    ->orWhere('transmission', 'LIKE', "%$search%")
                    ->orWhere('fuel', 'LIKE', "%$search%")
                    ->orWhere('price', 'LIKE', "%$search%")
                    ->orWhereHas('carModel', function ($q) use ($search) {
                        $q->where('name', 'LIKE', "%$search%");
                    });
            })->get();
        $usedCars = Category::with(['subCategories.carModel.avaliableCar'])
            ->get()->map(function ($category) {
                return [
                    'category_id' => $category->id,
                    'category_name' => $category->name,
                    'available_cars' => $category->subCategories->flatMap(function ($subCategory) {
                        return $subCategory->carModel->flatMap(function ($model) {
                            return $model->avaliableCar->where('status', 1);
                        });
                    })
                ];
            })->toArray();



        return view('used_car.index', compact('search', 'searchResult', 'usedCars'));
    }

    /* ================================================== Inquiry (Change all enquire to inquiry) ================================================== */

    public function enquiryStore(Request $request)
    {
        $validate = $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'address' => 'required'
        ]);
        $enquiry = Enquiry::create($validate);
        if ($enquiry) {
            $emails = ['hlanansoe@toyota-myanmar-sales.com', 'heinminhtike@toyota-myanmar-sales.com', 'onlineenquiry@toyota-myanmar-sales.com'];
            // $emails = ['zinmgmg44868@gmail.com', 'aungpyaephyoe.dev@gmail.com', 'zinmgmg@myansis.com'];
            foreach ($emails as $email) {
                try {
                    Mail::to($email)->send(new EnquiryMail($enquiry));
                    Log::info("email send to " . $email);
                } catch (\Exception $e) {
                    Log::error("email send fail mss =>" . $e);
                }
            }
        }
        if ($enquiry) {
            return redirect()->back()->with('success_message', 'Successfully Saved!');
        }

        return redirect()->back()->with('error_message', 'Something Went Wrong!');
    }


    /* ================================================== Car Model ================================================== */

    public function model()
    {
        $categories = Category::all();
        $subCategories = SubCategory::all();

        return view('model.compare_select', compact('categories', 'subCategories'));
    }

    public function compareModel(Request $request)
    {
        // dd($request);
        for ($i = 1; $i < 3; $i++) {
            $model_id = $i == 1 ? $request->first_model_id : $request->sec_model_id;
            $models[$i] = CarModel::where('id', $model_id)->with('avaliableCar')->first();

            $specifications[$i] = [
                'price' =>   $models[$i]->specification?->price ?? '',

                'overall' => $this->formatArray($models[$i]->specification?->dimensions_weight, 'overall') ?? '',
                'wheel_base' => $this->formatArray($models[$i]->specification?->dimensions_weight, 'wheel_base') ?? '',
                'tread' => $this->formatArray($models[$i]->specification?->dimensions_weight, 'tread') ?? '',
                'minimum_running_ground_clearance' => $this->formatArray($models[$i]->specification?->dimensions_weight, 'minimum_running_ground_clearance') ?? '',
                'minimum_turning_radius' => $this->formatArray($models[$i]->specification?->dimensions_weight, 'minimum_turning_radius') ?? '',
                'gross_vehicles_weight' => $this->formatArray($models[$i]->specification?->dimensions_weight, 'gross_vehicles_weight') ?? '',
                'total_min_options' => $this->formatArray($models[$i]->specification?->dimensions_weight, 'total_min_options') ?? '',
                'total_max_options' => $this->formatArray($models[$i]->specification?->dimensions_weight, 'total_max_options') ?? '',
                'seating_capacity' => $this->formatArray($models[$i]->specification?->dimensions_weight, 'seating_capacity') ?? '',
                'deck_size' => $this->formatArray($models[$i]->specification?->dimensions_weight, 'deck_size') ?? '',


                'engine_model_code' => $this->formatArray($models[$i]->specification?->engine, 'engine_model_code') ?? '',
                'engine_type' => $this->formatArray($models[$i]->specification?->engine, 'engine_type') ?? '',
                'displacement' => $this->formatArray($models[$i]->specification?->engine, 'displacement') ?? '',
                'bore_x_stoke' => $this->formatArray($models[$i]->specification?->engine, 'bore_x_stoke') ?? '',
                'compression_ratio' => $this->formatArray($models[$i]->specification?->engine, 'compression_ratio') ?? '',
                'max_output' => $this->formatArray($models[$i]->specification?->engine, 'max_output') ?? '',
                'max_torque' => $this->formatArray($models[$i]->specification?->engine, 'max_torque') ?? '',
                'fuel_system' => $this->formatArray($models[$i]->specification?->engine, 'fuel_system') ?? '',
                'fuel_type' => $this->formatArray($models[$i]->specification?->engine, 'fuel_type') ?? '',
                'fuel_tank_capacity' => $this->formatArray($models[$i]->specification?->engine, 'fuel_tank_capacity') ?? '',
                'fuel_consumption' => $this->formatArray($models[$i]->specification?->engine, 'fuel_consumption') ?? '',


                'transmission_type' => $this->formatArray($models[$i]->specification?->chassi, 'transmission_type') ?? '',
                'suspension' => $this->formatArray($models[$i]->specification?->chassi, 'suspension') ?? '',
                'brake_type' => $this->formatArray($models[$i]->specification?->chassi, 'brake_type') ?? '',
                'rear_combination_lamps' => $this->formatArray($models[$i]->specification?->chassi, 'rear_combination_lamps') ?? '',
                'tires' => $this->formatArray($models[$i]->specification?->chassi, 'tires') ?? '',
                'wheels' => $this->formatArray($models[$i]->specification?->chassi, 'wheels') ?? '',
                'steering_gear_type' => $this->formatArray($models[$i]->specification?->chassi, 'steering_gear_type') ?? '',


                'headlamps' => $this->formatArray($models[$i]->specification?->standard_equipment_exterior, 'headlamps') ?? '',
                'front_fog_driving_lamp' => $this->formatArray($models[$i]->specification?->standard_equipment_exterior, 'front_fog_driving_lamp') ?? '',
                'daytime_running_light_system' => $this->formatArray($models[$i]->specification?->standard_equipment_exterior, 'daytime_running_light_system') ?? '',
                'rear_combination_lamps' => $this->formatArray($models[$i]->specification?->standard_equipment_exterior, 'rear_combination_lamps') ?? '',
                'outside_rear_view_mirror' => $this->formatArray($models[$i]->specification?->standard_equipment_exterior, 'outside_rear_view_mirror') ?? '',
                'mudguard' => $this->formatArray($models[$i]->specification?->standard_equipment_exterior, 'mudguard') ?? '',
                'deck_bar' => $this->formatArray($models[$i]->specification?->standard_equipment_exterior, 'deck_bar') ?? '',
                'bed_liner' => $this->formatArray($models[$i]->specification?->standard_equipment_exterior, 'bed_liner') ?? '',

                'steering_switch' => $this->formatArray($models[$i]->specification?->standard_equipment_interior, 'steering_switch') ?? '',
                'steering_column' => $this->formatArray($models[$i]->specification?->standard_equipment_interior, 'steering_column') ?? '',
                'part_time_transfer_switch' => $this->formatArray($models[$i]->specification?->standard_equipment_interior, 'part_time_transfer_switch') ?? '',
                'drive_mode_switch' => $this->formatArray($models[$i]->specification?->standard_equipment_interior, 'drive_mode_switch') ?? '',
                'cruise_control' => $this->formatArray($models[$i]->specification?->standard_equipment_interior, 'cruise_control') ?? '',
                'multi_information_display' => $this->formatArray($models[$i]->specification?->standard_equipment_interior, 'multi_information_display') ?? '',
                'economy_meter' => $this->formatArray($models[$i]->specification?->standard_equipment_interior, 'economy_meter') ?? '',
                'front_seat' => $this->formatArray($models[$i]->specification?->standard_equipment_interior, 'front_seat') ?? '',
                'rear_seat' => $this->formatArray($models[$i]->specification?->standard_equipment_interior, 'rear_seat') ?? '',
                'seat_cover_material' => $this->formatArray($models[$i]->specification?->standard_equipment_interior, 'seat_cover_material') ?? '',
                'audio_system' => $this->formatArray($models[$i]->specification?->standard_equipment_interior, 'audio_system') ?? '',
                'speaker_system' => $this->formatArray($models[$i]->specification?->standard_equipment_interior, 'speaker_system') ?? '',
                'air_conditioning' => $this->formatArray($models[$i]->specification?->standard_equipment_interior, 'air_conditioning') ?? '',
                'starting_system' => $this->formatArray($models[$i]->specification?->standard_equipment_interior, 'starting_system') ?? '',
                'wireless_door_lock' => $this->formatArray($models[$i]->specification?->standard_equipment_interior, 'wireless_door_lock') ?? '',
                'accessory_connector' => $this->formatArray($models[$i]->specification?->standard_equipment_interior, 'accessory_connector') ?? '',

                'pre_collision_system' => $this->formatArray($models[$i]->specification?->safety_system, 'pre_collision_system') ?? '',
                'dynamic_radar_cruise_control' => $this->formatArray($models[$i]->specification?->safety_system, 'dynamic_radar_cruise_control') ?? '',
                'lane_departure_alert' => $this->formatArray($models[$i]->specification?->safety_system, 'lane_departure_alert') ?? '',
                'srs_airbags' => $this->formatArray($models[$i]->specification?->safety_system, 'srs_airbags') ?? '',
                'anti_lock_brake_system' => $this->formatArray($models[$i]->specification?->safety_system, 'anti_lock_brake_system') ?? '',
                'vehicles_stability_control' => $this->formatArray($models[$i]->specification?->safety_system, 'vehicles_stability_control') ?? '',
                'hill_start_assist_control' => $this->formatArray($models[$i]->specification?->safety_system, 'hill_start_assist_control') ?? '',
                'trailer_sway_control' => $this->formatArray($models[$i]->specification?->safety_system, 'trailer_sway_control') ?? '',
                'emergency_brake_signal' => $this->formatArray($models[$i]->specification?->safety_system, 'emergency_brake_signal') ?? '',
                'clearance_back_sonar' => $this->formatArray($models[$i]->specification?->safety_system, 'clearance_back_sonar') ?? '',
                'back_monitor' => $this->formatArray($models[$i]->specification?->safety_system, 'back_monitor') ?? '',
                'child_restraint_system' => $this->formatArray($models[$i]->specification?->safety_system, 'child_restraint_system') ?? '',
            ];
        }
        $spec1 = array_map(function ($value) {
            return strtolower(str_replace(' ', '', $value));
        }, $specifications[1]);
        $spec2 = array_map(function ($value) {
            return strtolower(str_replace(' ', '', $value));
        }, $specifications[2]);
        // dd($spec1, $spec2);
        $spec1 = array_filter($spec1);
        $spec2 = array_filter($spec2);
        $same = array_intersect($spec1, $spec2);
        $sameArrKeys = array_keys($same);
        // dd($sameArrKeys);
        $categories = Category::all();
        $subCategories = SubCategory::all();

        return view('model.compare_model', compact('models', 'categories', 'subCategories', 'specifications',  'sameArrKeys'));
    }

    private function formatArray($parentArr, $key)
    {
        $result = '';
        if ($parentArr) {
            $decoded = json_decode($parentArr, true);
            if (is_array($decoded) && array_key_exists(0, $decoded) && array_key_exists($key, $decoded[0])) {
                $result = $decoded[0][$key];
            }
        }
        return $result;
    }


    /* ================================================== Download Brochure================================================== */

    public function carSpecification($id)
    {
        $specification = Specification::findOrFail($id);
        return view('model.car_spec', compact('specification'));
    }

    public function brochure()
    {
        $categories = Category::all();
        return view('brochure.index', compact('categories'));
    }

    /* ================================================== Service Support================================================== */

    public function bookService()
    {
        $typeOfServices = TypeOfService::get();
        $provinces = Province::get();
        $dealerInfos = DealersInfo::get();
        return view('service_support.book_service', compact('typeOfServices', 'provinces', 'dealerInfos'));
    }

    public function serviceBookingStore(Request $request)
    {
        $validate = $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'type_of_service_id' => 'required',
            'province_id' => 'required',
            'dealer_info_id' => 'required',
            'date' => 'required|date',
            'time' => 'nullable|date_format:H:i',
            'phone' => 'required',
            'dial_code' => 'required',
            'notification' => 'nullable'
        ]);
        $serviceBooking = ServiceBooking::create($validate);

        if ($serviceBooking) {
            return redirect()->back()->with('success_message', 'Successfully Saved!');
        }

        return redirect()->back()->with('error_message', 'Something Went Wrong!');
    }

    /* ================================================== Online inquiry ================================================== */

    public function onlineInquiryStore(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'type_of_inquiries' => 'required',
            'phone' => 'required',
            'dial_code' => 'required',
            'description' => 'nullable'
        ]);
        $onlineInquiry = OnlineInquiry::create($validated);
        if ($onlineInquiry) {
            $emails = ['hlanansoe@toyota-myanmar-sales.com', 'heinminhtike@toyota-myanmar-sales.com', 'onlineenquiry@toyota-myanmar-sales.com'];
            // $emails = ['zinmgmg44868@gmail.com', 'aungpyaephyoe.dev@gmail.com', 'zinmgmg@myansis.com'];
            foreach ($emails as $email) {
                try {
                    Mail::to($email)->send(new OnlineInquiryMail($onlineInquiry));
                    Log::info("email send to " . $email);
                } catch (\Exception $e) {
                    Log::error("email send fail mss =>" . $e);
                }
            }
        }
        if ($onlineInquiry) {
            return redirect()->back()->with('success_message', 'Successfully Saved!');
        }

        return redirect()->back()->with('error_message', 'Something Went Wrong!');
    }

    /* ================================================== Stay Informed ================================================== */

    public function stayInformedStore(Request $request)
    {
        $validate = $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'privacy_confirm' => 'required',
            'notification' => 'nullable',
            'dial_code' => 'nullable',
        ]);
        $stayInformed = StayInformed::create($validate);

        if ($stayInformed) {
            return redirect()->back()->with('success_message', 'Successfully Saved!');
        }

        return redirect()->back()->with('error_message', 'Something Went Wrong!');
    }
}
