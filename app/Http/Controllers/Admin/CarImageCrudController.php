<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CarImageRequest;
use App\Http\Requests\CarModelRequest;
use App\Http\Requests\ColorRequest;
use App\Models\Color;
use App\Models\CarModel;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class CarImageCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CarImageCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\CarImage');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/carimage');
        $this->crud->setEntityNameStrings('carimage', 'car_images');
    }

    protected function setupListOperation(){
                    $this->crud->addColumn([
                        'name' => 'color',
                        'label' => 'Color',

                    ]);
                    $this->crud->addColumn([
                        'name' => 'carModel',
                        'label' => 'Car Model',

                    ]);
                // $this->crud->removeColumn('image');
                $this->crud->addColumn([
                    'name' => 'image',
                    'label'=>'Image',
                    ]);


                $this->crud->addFilter([
                    'type' => 'select2',
                    'name' => 'carModel',
                    'label' => 'Car Model',
                ],
                    function () {
                    return CarModel::select('name')->distinct()->get()->pluck('name','name')->toArray();
                    },

                    function ($value) {
                        $this->crud->addClause('whereHas', 'carModel', function($query) use ($value){
                            $query->where('name', '=', $value);
                        } );
                    });
                $this->crud->denyAccess('show');

                $this->crud->addFilter([
                    'type' => 'select2',
                    'name' => 'color',
                    'label' => 'Color',
                                        ],

                    function () {
                    return CarModel::select('name')->distinct()->get()->pluck('name','name')->toArray();
                    },
                    function ($value) {
                        $this->crud->addClause('whereHas', 'color', function($query) use ($value){
                            $query->where('name', '=', $value);
                        } );
                    });
                $this->crud->denyAccess('show');
    }

    protected function setupCreateOperation()
    {
         $this->crud->setValidation(CarImageRequest::class);
         $this->crud->addField([
            'type' => 'select2',
            'name' => 'color',
            'label' => 'Color',

        ]);
         $this->crud->addField([
            'type' => 'select2',
            'name' => 'carModel',
            'label' => 'Car Model',

        ]);

        $this->crud->addField([
            'name' =>'image',
            'type'  => 'repeatable',
            'fields' => [[
            'name'    => 'image',
            'type'    => 'image',
            'label'   => 'Image',
        ]],
        ]);

    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
