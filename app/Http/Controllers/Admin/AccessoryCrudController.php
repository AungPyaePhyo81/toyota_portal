<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AccessoryRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class AccessoryCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AccessoryCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Accessory');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/accessory');
        $this->crud->setEntityNameStrings('accessory', 'accessories');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        // $this->crud->setFromDb();
        $this->crud->addColumn([
            'name' => 'name',
            'label'=>'Car Model Name',

        ]);
        $this->crud->addColumn([
            'name' => 'images',
            'label'=>'Images',

        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(AccessoryRequest::class);

        $this->crud->addField([
            'type' => 'text',
            'name' => 'name',
            'label' => 'Car Model Name',
        ]);

        $this->crud->addField([
            'name' => 'images',
            'label' => 'Images',
            'type'  => 'repeatable',
            'fields' => [[
                'name'    => 'image',
                'label'    => 'images',
                'type'    => 'browse'
            ]],
            'new_item_label' => 'Add Photo',
            'init_rows' => 1,
            'min_rows' => 1,
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
