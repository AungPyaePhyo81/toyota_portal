<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SubCategoryRequest;
use Illuminate\Support\Facades\Validator;
use App\Models\Category;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\InlineCreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;



/**
 * Class SubCategoryCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class SubCategoryCrudController extends CrudController
{
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation;
    use InlineCreateOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\SubCategory');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/subcategory');
        $this->crud->setEntityNameStrings('subcategory', 'sub_categories');
    }

    protected function setupListOperation()
    {
        $this->crud->addColumn([
            'name' => 'name',
            'type' => 'text',
            'label' => 'Name',
        ]);

        $this->crud->addColumn([
            'name' => 'category',
            'label' => 'Category',
        ]);

        $this->crud->addColumn([
            'name' => 'image',
            'label' => 'Image',
        ]);
        $this->crud->addColumn([
            'name' => 'status',
            'label' => 'Detail Page',
            'type' => 'radio',
            'options' => [
                '0' => 'No',
                '1' => 'Yes'
            ]
        ]);

        $this->crud->addFilter(
            [
                'type' => 'select2',
                'name' => 'category',
                'label' => 'Category',
            ],
            function () {
                return Category::select('name')->distinct()->get()->pluck('name', 'name')->toArray();
            },
            function ($value) {
                $this->crud->addClause('whereHas', 'category', function ($query) use ($value) {
                    $query->where('name', '=', $value);
                });
            }
        );
        $this->crud->denyAccess('show');
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(SubCategoryRequest::class);
        $this->crud->addField([
            'type' => 'text',
            'name' => 'name',
        ]);

        $this->crud->addField([
            'type' => 'select2',
            'name' => 'category',
            'label' => 'Category',

        ]);

        $this->crud->addField([
            'type' => 'browse',
            'name' => 'image',
        ]);

        $this->crud->addField([
            'name' => 'status', // the name of the db column
            'label' => 'Detail Page Available', // the input label
            'type' => 'checkbox',
            'options'     => [
                // the key will be stored in the db, the value will be shown as label;
                0 => "No",
                1 => "Yes"
            ],
        ]);

        $this->crud->addField([
            'name' => 'banner',
            'type' => 'browse',
            'label' => 'Banner',

        ]);

        $this->crud->addField([
            'type' => 'repeatable',
            'name' => 'brochures',
            'fields' => [[
                'name'    => 'brochures',
                'type'    => 'browse',
                'label'   => 'Brochures'
            ]]
        ]);

        $this->crud->addField([
            'type' => 'repeatable',
            'name' => 'owner_manual',
            'fields' => [[
                'name'    => 'owner_manual',
                'type'    => 'browse',
                'label'   => 'Owner Maual'
            ]]

        ]);

        $this->crud->addField([
            'name' => 'video',
            'label' => 'Video',
            'type' => 'browse',
            'upload' => true,
            'disk' => 'public',
            'prefix' => 'videos',
            'hint' => 'Allowed types: mp4, avi, wmv, mov',
            'setValidation' => [
                'required',
                'file',
                'mimes:mp4,avi,wmv,mov',
                'max:' . (50 * 1024), // 50 MB
            ],
            'allowed_extensions' => ['mp4', 'avi', 'wmv', 'mov'],
        ]);









    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }


}
