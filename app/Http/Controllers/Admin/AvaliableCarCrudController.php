<?php

namespace App\Http\Controllers\Admin;
use App\Models\CarModel;
use App\Models\Dealer;
use App\Http\Requests\AvaliableCarRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class AvaliableCarCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AvaliableCarCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\AvaliableCar');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/avaliablecar');
        $this->crud->setEntityNameStrings('avaliablecar', 'avaliable_cars');
    }

    protected function setupListOperation()
    {
        // $this->crud->addColumns([
        //      'engine','transmission','fuel','car_no','steering_pos','trim_grade','driven_kilo',
        //     'model_year','contact_no','thumbnail','price',

        // ]);
        $this->crud->addColumn([
            'name'        => 'status', // the name of the db column
            'label'       => 'Type', // the input label
            'type'        => 'radio',
            'options'     => [
                // the key will be stored in the db, the value will be shown as label;
                0 => "Inventory",
                1 => "Used Car"
            ],
            ]);



        $this->crud->addColumn([
            'name' => 'dealer',
            'label'=>'Dealer',

        ]);
        $this->crud->addColumn([
            'name' => 'carModel',
            'label'=>'Car Model',

        ]);
        $this->crud->addColumn([
            'name' => 'color',
            'label'=>'Color',

        ]);
        $this->crud->addColumns([
            'model_year','fuel','car_no','steering_pos',
            'contact_no','price'
        ]);
// -----------------------------------------Start Filter Method--------------------------------------------------
        $this->crud->addFilter(
            [
                'type' => 'select2',
                'name' => 'carModel',
                'label' => 'CarModel',
            ],
            function () {
                return CarModel::select('name')->distinct()->get()->pluck('name', 'name')->toArray();
            },
            function ($value) {
                $this->crud->addClause('whereHas', 'carModel', function ($query) use ($value) {
                    $query->where('name', '=', $value);
                });
            }
        );

        $this->crud->addFilter(
            [
                'type' => 'select2',
                'name' => 'dealer',
                'label' => 'Dealer',
            ],
            function () {
                return Dealer::select('name')->distinct()->get()->pluck('name', 'name')->toArray();
            },
            function ($value) {
                $this->crud->addClause('whereHas', 'dealer', function ($query) use ($value) {
                    $query->where('name', '=', $value);
                });
            }
        );
    // --------------------------------------------------End--------------------------------------
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(AvaliableCarRequest::class);
        $this->crud->addField([
            'type' => 'select2',
            'name' => 'dealer',
            'label' => 'Dealer',
        ]);
        $this->crud->addField([
            'type' => 'select2',
            'name' => 'carModel',
            'label' => 'Car Model',
        ]);
        $this->crud->addField([
            'type' => 'select2',
            'name' => 'color',
            'label' => 'Color',
        ]);
        $this->crud->addField([
            'type' => 'select2',
            'name' => 'dealer',
            'label' => 'Dealer',
        ]);
        $this->crud->addField([
        'name'        => 'status', // the name of the db column
        'label'       => 'Status', // the input label
        'type'        => 'radio',
        'options'     => [
            // the key will be stored in the db, the value will be shown as label;
            0 => "Inventory",
            1 => "Used Car"
        ],
        ]);

        $this->crud->addField([
            'type' => 'text',
            'name' => 'engine',
            'label' => 'Engine',
        ]);
        $this->crud->addField([
            'type' => 'select2',
            'name' => 'dealer',
            'label' => 'Dealer',
        ]);
        $this->crud->addField([
            'type' => 'text',
            'name' => 'transmission',
            'label' => 'Transmission',
        ]);
        $this->crud->addField([
            'type' => 'text',
            'name' => 'fuel',
            'label' => 'Fuel',
        ]);
        $this->crud->addField([
            'type' => 'text',
            'name' => 'car_no',
            'label' => 'Car No:',
        ]);
        $this->crud->addField([
            'type' => 'text',
            'name' => 'steering_pos',
            'label' => 'Steering POS',
        ]);
        $this->crud->addField([
            'type' => 'text',
            'name' => 'trim_grade',
            'label' => 'Trim grade',
        ]);
        $this->crud->addField([
            'type' => 'text',
            'name' => 'driven_kilo',
            'label' => 'Driven Kilo',
        ]);
        $this->crud->addField([
            'type' => 'text',
            'name' => 'model_year',
            'label' => 'Model Year',
        ]);
        $this->crud->addField([
            'type' => 'text',
            'name' => 'contact_no',
            'label' => 'Contact No:',
        ]);
        $this->crud->addField([
            'type' => 'browse',
            'name' => 'thumbnail',
            'label' => 'Thumbnail',
        ]);

        $this->crud->addField([
            'type' => 'repeatable',
            'name' => 'image',
            'fields' => [[
                'name'    => 'image',
                'type'    => 'browse',
                'label'   => 'Images'
                ]]

        ]);
        $this->crud->addField([
            'type' => 'text',
            'name' => 'price',
            'label' => 'Price',


        ]);


    }


    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

}
