<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PromotionRequest;
use App\Mail\PromotionMail;
use App\Models\Promotion;
use App\Models\Dealer;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Models\StayInformed;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

/**
 * Class PromotionCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PromotionCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Promotion');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/promotion');
        $this->crud->setEntityNameStrings('promotion', 'promotions');
    }

    protected function setupListOperation()
    {


        $this->crud->addColumn([
            'name' => 'promotion_title',
            'type' => 'text',
            'label' => 'Promotion Title',
        ]);

        $this->crud->addColumn(
            [
                'name'     => 'dealer',
                'label'    => 'Dealers',
                'type'     => 'closure',
                'function' => function ($entry) {
                    $dealers = Dealer::whereIn('id', json_decode($entry->dealer_id))->get();

                    $content = '';
                    foreach ($dealers as $dealer) {
                        $content .=  $dealer->name . '<br>';
                    }
                    return $content;
                }
            ],
        );

        $this->crud->addColumn([
            'name' => 'promotion_thumbnail',
            'type' => 'browse',
            'label' => 'Promotion Thumbnail',
        ]);


        $this->crud->addColumn([
            'name' => 'promotion_description',
            'type' => 'textarea',
            'label' => 'Promotion Description',
        ]);

        $this->crud->addColumn([
            'name' => 'promotion_start_date',
            'type' => 'date',
            'label' => 'Promotion Start Date',
        ]);


        $this->crud->addColumn([
            'name' => 'promotion_end_date',
            'type' => 'date',
            'label' => 'Promotion End Date',
        ]);


    }


    private function formatDealer()
    {
        return Dealer::pluck('name', 'id');
    }
    protected function setupCreateOperation()
    {
        $this->crud->setValidation(PromotionRequest::class);
        $entry = $this->crud->getCurrentEntry();
        $value = [];

        if ($entry && is_object($entry)) {
            $value = json_decode($entry->dealer_id) ?? [];
        }

        $this->crud->addField(
            [
                'name' => 'dealer_id',
                'label' => "Dealer",
                'type' => 'select2_from_array',
                'options' => $this->formatDealer(),
                'allows_null' => false,
                'allows_multiple' => true,
                'value' => $value,
            ],
        );

        $this->crud->addField([
            'name' => 'promotion_title',
            'type' => 'text',
            'label' => 'Title'
        ]);

        $this->crud->addField([
            'name' => 'promotion_thumbnail',
            'type' => 'browse',
            'label' => 'Promotion Thumbnail'
        ]);

        $this->crud->addField([
            'type' => 'ckeditor',
            'name' => 'promotion_description',
            'Label' => 'Description',
            'options'       => [
                'autoGrow_minHeight'   => 200,
                'autoGrow_bottomSpace' => 50,
                'removePlugins'        => 'resize,maximize',
            ]

        ]);

        $this->crud->addField([
            'name' => 'promotion_start_date',
            'type' => 'date',
            'label' => 'Promotion Start Date',
        ]);


        $this->crud->addField([
            'name' => 'promotion_end_date',
            'type' => 'date',
            'label' => 'Promotion End Date',
        ]);



    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function store(PromotionRequest $request)
    {

        $request->validate([
            'promotion_title' => 'required',
            'promotion_thumbnail' => 'required',
            'promotion_description' => 'required',
            'promotion_start_date' => 'required',
            'promotion_end_date' => 'required',
            'dealer_id' => 'nullable|array',
        ]);


        // Convert the "service" array to a JSON string


        $dealer = json_encode($request->input('dealer_id', []));

        // Create a new DealerInfo object
        $promotion = new Promotion([
            'promotion_title' => $request->input('promotion_title'),
            'promotion_thumbnail' => $request->input('promotion_thumbnail'),
            'promotion_description' => $request->input('promotion_description'),
            'promotion_start_date' => $request->input('promotion_start_date'),
            'dealer_id' => $dealer,
            'promotion_end_date' => $request->input('promotion_end_date'),

        ]);
        $promotion->save();


        return redirect(backpack_url('/promotion'))->with('success_message', 'Successfully add a new promotion!');
    }



    public function update(PromotionRequest $request)
    {
        // Validate the input data
        $request->validate([
            'promotion_title' => 'required',
            'promotion_thumbnail' => 'required',
            'promotion_description' => 'required',
            'promotion_start_date' => 'required',
            'promotion_end_date' => 'required',
            // 'dealer_id' => 'nullable|array',
        ]);

        // Convert the "service" array to a JSON string
        $dealer = json_encode($request->input('dealer_id', []));

        $promotion = Promotion::findOrFail($request->input('id'));

        // Update the DealerInfo object
        $promotion->promotion_title = $request->input('promotion_title');
        $promotion->promotion_thumbnail = $request->input('promotion_thumbnail');
        $promotion->promotion_description = $request->input('promotion_description');
        $promotion->promotion_start_date = $request->input('promotion_start_date');
        $promotion->dealer_id = json_encode($request->input('dealer_id', []));
        $promotion->promotion_end_date = $request->input('promotion_end_date');

        $promotion->save();

        // Redirect the user to the previous page with a success message
        return redirect(backpack_url('/promotion'))->with('success_message', 'Successfully add a new promotion!');
    }

    public function setupShowOperation()
    {
        $this->crud->addColumn(
            [
                'name'     => 'dealer',
                'label'    => 'Dealers',
                'type'     => 'closure',
                'function' => function ($entry) {
                    $dealers = Dealer::whereIn('id', json_decode($entry->dealer_id))->get();
                    $content = '';
                    foreach ($dealers as $dealer) {
                        $content .=  $dealer->name . '<br>';
                    }
                    return $content;
                }
            ],
        );
    }
}
