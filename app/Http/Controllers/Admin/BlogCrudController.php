<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\BlogRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class BlogCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class BlogCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Blog');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/blog');
        $this->crud->setEntityNameStrings('blog', 'blogs');
    }

    protected function setupListOperation()
    {

        // $this->crud->setFromDb();
        $this->crud->addColumn([
            'name' => 'blog_thumbnail',
            'label' => 'Blog Thumbnail ',
        ]);
        $this->crud->addColumn([
            'name' => 'title',
            'label' => 'Title ',
        ]);
        $this->crud->addColumn([
            'name' => 'date',
            'label' => 'Date ',
        ]);

    }

    protected function setupCreateOperation()


    {  $this->crud->setValidation(BlogRequest::class);
       $this->crud->addField([
            'type' => 'browse',
            'name' => 'blog_thumbnail',
            'label'=>'Blog Thumbnail',
            'mime_types' => ['uploads/image'],
        ]);

        $this->crud->addField([
            'type' => 'text',
            'name' => 'title',
        ]);

         $this->crud->addField([
            'type' => 'text',
            'name' => 'slug',
            'label' => 'Short Description',
        ]);

         $this->crud->addField([
            'type' => 'date',
            'name' => 'date',
        ]);

        $this->crud->addField([
            'type' => 'ckeditor',
            'name' => 'long_description',
            'Label' => 'Long Description',
            'options'       => [
            'autoGrow_minHeight'   => 200,
            'autoGrow_bottomSpace' => 50,
            'removePlugins'        => 'resize,maximize',],
        ]);

         $this->crud->addField([
            'name' => 'images',
            'label' => 'Images',
            'type'  => 'repeatable',
            'fields' => [[
                'name'    => 'image',
                'label'    => 'image',
                'type'    => 'browse'
            ]],
            'new_item_label' => 'Add Photo',
            'init_rows' => 1,
            'min_rows' => 1,
            'max_rows' => 5
        ]);



        $this->crud->addField(
            [
                'name'        => 'status',
                'label'       => 'Type',
                'type'        => 'radio',
                'options'     => [
                    'sale' => "Sale",
                    'service' => "Service",
                    'sparepart' => "Spare Part"
                ]
            ]);
    }





    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
