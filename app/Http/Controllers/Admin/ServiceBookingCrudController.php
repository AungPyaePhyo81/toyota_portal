<?php

namespace App\Http\Controllers\Admin;

use App\Models\TypeOfService;

use App\Http\Requests\ServiceBookingRequest;
use App\Mail\ServiceBookingConfirmMail;
use App\Models\DealersInfo;

use App\Models\Province;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

/**
 * Class ServiceBookingCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ServiceBookingCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    // In your CrudController

    public function setup()
    {
        $this->crud->setModel('App\Models\ServiceBooking');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/servicebooking');
        $this->crud->setEntityNameStrings('service booking', 'service bookings');
        $this->crud->denyAccess('create');
    }



    protected function setupListOperation()
    {
        $this->crud->addColumn([
            'name' => 'is_confirm',
            'label' => 'Is Confirm',
            'type' => 'closure',
            'function' => function ($entry) {
                if ($entry->is_confirm == 1) {
                    return '<a href="' . backpack_url('servicebooking/' . $entry->id . '/unconfirm') . '"><button class="btn btn-sm btn-success">Yes</button></a>';
                } else {
                    return '<a href="' . backpack_url('servicebooking/' . $entry->id . '/confirm') . '"><button class="btn btn-sm btn-danger">No</button></a>';
                }
            }
        ]);

        // -------------------------FilterByTypeOfService----------------------------------------------------

        $this->crud->addFilter(
            [
                'type' => 'select2',
                'name' => 'typeOfService',
                'label' => 'Type Of Service',
            ],
            function () {
                return TypeOfService::select('name')->distinct()->get()->pluck('name', 'name')->toArray();
            },
            function ($value) {
                $this->crud->addClause('whereHas', 'typeOfService', function ($query) use ($value) {
                    $query->where('name', '=', $value);
                });
            }
        );

        // ----------------------------FilterByProvice----------------------------------------------------
        $this->crud->addFilter(
            [
                'name'        => 'province',
                'label'       => "Proviance",
                'type' => 'select2',

            ],
            function () {
                return Province::select('name')->distinct()->get()->pluck('name', 'name')->toArray();
            },
            function ($value) {
                $this->crud->addClause('whereHas', 'province', function ($query) use ($value) {
                    $query->where('name', '=', $value);
                });
            }
        );
        // --------------------------------------------FilterByDealerinfo---------------------------------------
        // $this->crud->addFilter([
        //     'name'        => 'dealer_info_id',
        //     'label'       => "Dealer Info",
        //     'type'        => 'select_from_array',
        //     'options'     => $this->formatDealerInfo(),
        // ],
        // function () {
        //     return DealersInfo::select('dealer_info_id')->distinct()->get()->pluck('dealer_info_id', 'dealer_info_id')->toArray();
        // },
        // function ($value) {
        //     $this->crud->addClause('whereHas', 'province', function ($query) use ($value) {
        //         $query->where('dealer_info_id', '=', $value);
        //     });
        // });


        $this->crud->denyAccess('show');

        $this->crud->setFromDb();
    }

    public function confirm($id)
    {
        $this->crud->hasAccessOrFail('update');
        $entry = $this->crud->getEntry($id);
        $entry->is_confirm = true;
        $entry->save();

        if ($entry->notification == 1) {

            try {
                Mail::to($entry->email)->queue(new ServiceBookingConfirmMail($entry));
                Log::info("email send to " . $entry->email);
            } catch (\Exception $e) {
                Log::error("email send fail mss =>" . $e);
            }
        }

        return redirect(backpack_url('servicebooking'));
    }

    public function unconfirm($id)
    {
        $this->crud->hasAccessOrFail('update');
        $entry = $this->crud->getEntry($id);
        $entry->is_confirm = false;
        $entry->save();

        return redirect(backpack_url('servicebooking'));
    }


    private function formatDealerInfo()
    {
        $dealerInfos = DealersInfo::with('map')->get();
        $arr = [];

        foreach ($dealerInfos as $dealerInfo) {
            $arr[$dealerInfo->id] = $dealerInfo->map->name;
        }

        return $arr;
    }
    protected function setupUpdateOperation()
    {
        $this->crud->setFromDb();

        $this->crud->removeField('type_of_service_id');
        $this->crud->removeField('dealer_info_id');
        $this->crud->removeField('province_id');


        $this->crud->addField([
            'name' => 'typeOfService',
            'label' => 'Type Of Service'
        ]);

        $this->crud->addField([
            'name'        => 'dealer_info_id',
            'label'       => "Dealer Info",
            'type'        => 'select_from_array',
            'options'     => $this->formatDealerInfo(),
        ]);

        $this->crud->addField([
            'name' => 'province',
            'label' => 'Province'
        ]);
    }

    public function setupShowOperation()
    {
        $this->crud->set('show.setFromDb', false);

        $this->crud->addColumns(['name', 'email', 'phone', 'dial_code', 'date', 'time']);

        $this->crud->addColumn([
            'name'  => 'notification',
            'label' => 'Notification',
            'type'  => 'boolean',
        ]);
        $this->crud->addColumn([
            'name' => 'typeOfService',
            'label' => 'Type Of Service'
        ]);

        $this->crud->addColumn([
            'name' => 'province',
            'label' => 'Province'
        ]);

        // $this->crud->addColumn([
        //     'name' => 'dealer_info_id',
        //     'label' => 'Dealer Info'
        // ]);
        $this->crud->addColumn([
            'name'        => 'dealer_info_id',
            'label'       => "Dealer Info",
            'type'        => 'select_from_array',
            'options'     => $this->formatDealerInfo(),
        ]);

        // $this->crud->addColumn([
        //     'name'  => 'type_of_service_id',
        //     'label' => 'Type Of Service',
        //     'type'  => 'select',
        //     'entity'    => 'typeOfServices', // the method that defines the relationship in your Model
        //     'attribute' => 'name', // foreign key attribute that is shown to user
        //     'model'     => "App\Models\TypeOfService",
        // ]);
    }
}
