<?php

namespace App\Http\Controllers\Admin;

use App\Models\DealersInfo;

use App\Models\TypeOfService;
use App\Models\Map;
use App\Models\Dealer;
use Illuminate\Support\Collection;
use App\Http\Requests\DealersInfoRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class DealersInfoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class DealersInfoCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\DealersInfo');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/info');
        $this->crud->setEntityNameStrings('dealersinfo', 'dealers_infos');
    }

    protected function setupListOperation()
    {

        $this->crud->addColumn([
            'name' => 'dealer',
            'label' => 'Dealer ',
        ]);
        $this->crud->addColumn([
            'name' => 'map',
            'label' => 'Map ',
        ]);



        $this->crud->addFilter(
            [
                'type' => 'select2',
                'name' => 'map',
                'label' => 'map',
            ],
            function () {
               return Map::select('name')->distinct()->get()->pluck('name', 'name')->toArray();
            },
            function ($value) {
                $this->crud->addClause('whereHas', 'map', function ($query) use ($value) {
                    $query->where('name', '=', $value);
                });
            }
        );

        $this->crud->addFilter(
            [
                'type' => 'select2',
                'name' => 'dealer',
                'label' => 'dealer',
            ],
            function () {
                return Dealer::select('name')->distinct()->get()->pluck('name', 'name')->toArray();
            },
            function ($value) {
                $this->crud->addClause('whereHas', 'dealer', function ($query) use ($value) {
                    $query->where('name', '=', $value);
                });
            }
        );

    }

    public function setupShowOperation()

    {
        $this->crud->addColumn([
            'name' => 'dealer',
            'label' => 'Dealer ',
        ]);
        $this->crud->addColumn([
            'name' => 'map',
            'label' => 'Map ',
        ]);

        $this->crud->addColumn([
            'name' => 'address',
            'label' => 'address ',
            'type' => 'textarea'
        ]);
        $this->crud->addColumn(
            [
                'name'     => 'service',
                'label'    => 'Type Of Service',
                'type'     => 'closure',
                'function' => function ($entry) {
                    $services = TypeOfService::whereIn('id', json_decode($entry->service))->get();
                    $content = '';
                    foreach ($services as $service) {
                        $content .=  $service->name . '<br>';
                    }
                    return  $content;
                }
            ],
        );

        $this->crud->addColumn(
            [
                'name'     => 'phone_no',
                'label'    => 'Phone',
                'type'     => 'closure',
                'function' => function ($entry) {
                    $content = '';
                    foreach (json_decode($entry->phone_no) as $phone) {

                        $content .=  $phone->phone_no . '<br>';
                    }
                    return  $content;
                }
            ],
        );
    }

    private function formatTypeOfServices()
    {
        return TypeOfService::pluck('name', 'id');
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(DealersInfoRequest::class);
        $this->crud->addField([
            'type' => 'select2',
            'name' => 'dealer',
        ]);

        $this->crud->addField([
            'type' => 'select2',
            'name' => 'map',
        ]);

        $this->crud->addField([
            'type' => 'text',
            'name' => 'address',
        ]);


        $this->crud->addField([
            'type' => 'repeatable',
            'name' => 'phone_no',
            'fields' => [
                [
                    'name' => 'phone_no',
                    'type'    => 'text',
                    'label'   => 'Phone No:',
                    // 'wrapper' => ['class' => 'form-group col-6'],
                ]
            ],
        ]);

        $this->crud->addField([
            'type' => 'text',
            'name' => 'opening_hour',
        ]);

        $this->crud->addField([
            'type' => 'text',
            'name' => 'opening_day',
        ]);


        //        $this->crud->addField([
        //            'name' => 'service',
        //            'label' => 'Type of Service',
        //            'type' => 'select2_multiple',
        //            'entity' => 'typeOfServices',
        //            'attribute' => 'name',
        //            'model' => "App\Models\TypeOfService",
        //            'placeholder' => 'Select a type of service',
        //            'multiple' => 'true',
        //        ]);

        $entry = $this->crud->getCurrentEntry();
        $value = [];

        if ($entry && is_object($entry)) {
            $value = json_decode($entry->service) ?? [];
        }

        $this->crud->addField(
            [
                'name' => 'service',
                'label' => "Type of Service",
                'type' => 'select2_from_array',
                'options' => $this->formatTypeOfServices(),
                'allows_null' => false,
                'allows_multiple' => true,
                'value' => $value,
            ],
        );

        $this->crud->addField([
            'name' => 'image',
            'label' => 'Image',
            'type'  => 'repeatable',
            'fields' => [[
                'name'    => 'image',
                'label'    => 'images',
                'type'    => 'browse'
            ]]
        ]);
    }



    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }


    // ---------------------------------custom for select2_multiple------------------------------------------
    public function store(DealersInfoRequest $request)
    {
        // Validate the input data
        $request->validate([
            'address' => 'required',
            'phone_no' => 'required',
            'opening_day' => 'required',
            'opening_hour' => 'required',
            // 'image' => 'nullable|image|max:2048',
            'service' => 'nullable|array',

        ]);

        // Convert the "service" array to a JSON string
        $service = json_encode($request->input('service', []));


        // Create a new DealerInfo object
        $dealerInfo = new DealersInfo([
            'address' => $request->input('address'),
            'phone_no' => $request->input('phone_no'),
            'opening_day' => $request->input('opening_day'),
            'opening_hour' => $request->input('opening_hour'),
            'image' => $request->input('image'),
            'service' => $service,
            'dealer_id' => $request->input('dealer'),
            'map_id' => $request->input('map'),
        ]);

        $dealerInfo->save();


        return redirect(backpack_url('/info'))->with('success', 'Dealer info added successfully.');
    }



    public function update(DealersInfoRequest $request)
    {
        // Validate the input data
        $request->validate([
            'address' => 'required',
            'phone_no' => 'required',
            'opening_day' => 'required',
            'opening_hour' => 'required',
            'service' => 'nullable|array',

        ]);

        // Convert the "service" array to a JSON string
        $service = json_encode($request->input('service', []));

        // Find the DealerInfo object by ID
        $dealerInfo = DealersInfo::findOrFail($request->input('id'));

        // Update the DealerInfo object
        $dealerInfo->address = $request->input('address');
        $dealerInfo->phone_no = $request->input('phone_no');
        $dealerInfo->opening_day = $request->input('opening_day');
        $dealerInfo->opening_hour = $request->input('opening_hour');
        $dealerInfo->service = $service;
        $dealerInfo->image = $request->input('image');
        $dealerInfo->dealer_id = $request->input('dealer');
        $dealerInfo->map_id = $request->input('map');

        // Save the DealerInfo object to the database
        $dealerInfo->save();

        // Redirect the user to the previous page with a success message
        return redirect(backpack_url('/info'))->with('success', 'Dealer info added successfully.');
    }
    // Note that this is just an example code and may need to be adjusted to fit your specific needs. Make sure to test it thoroughly before using it in production.

}
