<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\EnquiryRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class EnquiryCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class EnquiryCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Enquiry');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/enquiry');
        $this->crud->setEntityNameStrings('enquiry', 'enquiries');
        $this->crud->denyAccess('create');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        $this->crud->setFromDb();
    }

    // protected function setupCreateOperation()
    // {
    //     $this->crud->setValidation(EnquiryRequest::class);

    //     // TODO: remove setFromDb() and manually define Fields
    //     $this->crud->setFromDb();
    // }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
