<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\Validator;

use App\Http\Requests\CarModelRequest;
use App\Models\SubCategory;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
use Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanel;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class CarModelCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class CarModelCrudController extends CrudController
{
    use ListOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use ShowOperation {
        show as traitShow;
    }

    public function setup()
    {
        $this->crud->setModel('App\Models\CarModel');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/carmodel');
        $this->crud->setEntityNameStrings('carmodel', 'car_models');
    }

    protected function setupListOperation()
    {

        $this->crud->addColumn([
            'name' => 'name',
            'type' => 'text',
            'label'=>'Name',

        ]);


        $this->crud->removeColumn('subCategory');
        $this->crud->addColumn([
            'name' => 'subCategory',

            'label'=>'SubCategory',

        ]);

        $this->crud->addFilter([
            'type' => 'select2',
            'name' => 'subCategory',
            'label' => 'Sub Category',
        ],
            function () {
                return SubCategory::select('name')->distinct()->get()->pluck('name', 'name')->toArray();
            },
            function ($value) {
                $this->crud->addClause('whereHas', 'subCategory', function ($query) use ($value) {
                    $query->where('name', '=', $value);
                } );
            }

        );
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(CarModelRequest::class);

        $this->crud->addField([
            'type' => 'select2',
            'name' => 'subCategory',
        ]);
        $this->crud->addField([
            'type' => 'text',
            'name' => 'name',
        ]);
















    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function show($id)
    {
        $content = $this->traitShow($id);
        $this->crud->addColumn([
            'name' => 'subCategory'
        ]);
        $this->crud->removeColumn('subCategory');
        return $content;
    }
}
