<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\DealerRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class DealerCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class DealerCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Dealer');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/dealer');
        $this->crud->setEntityNameStrings('dealer', 'dealers');
    }

    protected function setupListOperation()
    {
        $this->crud->addColumns([
            'name', 'description', 'website_link', 'social_link'
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(DealerRequest::class);

        $this->crud->addField([
            'type' => 'text',
            'name' => 'name',
        ]);

       $this->crud->addField([
            'type' => 'ckeditor',
            'name' => 'description',
            'Label' => 'Description',
            'options'       => [
             'autoGrow_minHeight'   => 200,
             'autoGrow_bottomSpace' => 50,
            'removePlugins'        => 'resize,maximize',
    ]

        ]);

        $this->crud->addField([
            'type' => 'text',
            'name' => 'website_link',
        ]);

        $this->crud->addField([
            'type' => 'repeatable',
            'name' => 'social_link',
            'fields' => [
                [
                    'name' => 'social_link',
                    'type'    => 'text',
                    'label'   => 'Social Link',
                    'wrapper' => ['class' => 'form-group col-12'],
                    'attributes' => [
                        'placeholder' => 'https://example.com',
                    ],
                ],
        ],
    ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
