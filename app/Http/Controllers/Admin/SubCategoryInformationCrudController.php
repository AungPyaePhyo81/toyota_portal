<?php

namespace App\Http\Controllers\Admin;
use App\Models\SubCategory;
use App\Http\Requests\SubCategoryInformationRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class SubCategoryInformationCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class SubCategoryInformationCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\SubCategoryInformation');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/sub-info');
        $this->crud->setEntityNameStrings('subcategoryinformation', 'sub_category_informations');
    }

    protected function setupListOperation()
    {    $this->crud->addColumns([
        'title', 'description',
    ]);

        $this->crud->addColumn([
            'name' => 'SubCategory',
            'label'=>'SubCategory',

        ]);
        $this->crud->addColumn([
            'name' => 'images',
            'label'=>'Image',
        ]);




        $this->crud->addFilter([
            'type' => 'select2',
            'name' => 'subCategory',
            'label' => 'SubCategory',
        ],
            function () {
                return SubCategory::select('name')->distinct()->get()->pluck('name', 'name')->toArray();
            },
            function ($value) {
                $this->crud->addClause('whereHas', 'subCategory', function ($query) use ($value) {
                    $query->where('name', '=', $value);
                });
            });
        $this->crud->denyAccess('show');

    }

    protected function setupCreateOperation()
    {
       $this->crud->setValidation(SubCategoryInformationRequest::class);

       $this->crud->addField([
            'type' => 'select2',
            'name' => 'subCategory',
            'label'=>'SubCategory',
        ]);

       $this->crud->addField([
            'type' => 'text',
            'name' => 'title',
            'label'=>'Title',
        ]);

        $this->crud->addField([
            'name' => 'images',
            'type' => 'repeatable',
            'fields' => [[
                'name'    => 'images',
                'type'    => 'browse',
                'label'   => 'Image'
                ]]
        ]);

        $this->crud->addField([
            'name' => 'description',
            'type' => 'repeatable',
            'fields' => [[
                'type' => 'ckeditor',
                'name' => 'description',
                'label' => ' Description',
                'options'       => [
                        'autoGrow_minHeight'   => 200,
                        'autoGrow_bottomSpace' => 50,
                        'removePlugins'        => 'resize,maximize']
                ]
            ]
        ]);

    //    $this->crud->addField([
    //     'type' => 'ckeditor',
    //     'name' => 'description',
    //     'Label' => ' Description',
    //     'options'       => [
    //     'autoGrow_minHeight'   => 200,
    //     'autoGrow_bottomSpace' => 50,
    //     'removePlugins'        => 'resize,maximize'],
    //     ]);

    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
