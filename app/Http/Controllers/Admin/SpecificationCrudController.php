<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SpecificationRequest;
use App\Models\CarModel;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class SpecificationCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class SpecificationCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    public function setup()
    {
        $this->crud->setModel('App\Models\Specification');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/specification');
        $this->crud->setEntityNameStrings('specification', 'specifications');
    }

    protected function setupListOperation()
    {
        $this->crud->addColumn([
            'name' => 'carModel',
            'label' => 'Model',
        ]);

        $this->crud->addColumns([
            'horse_power', 'torque', 'engine_name', 'grade', 'driven_train'
        ]);

        $this->crud->addFilter(
            [
                'type' => 'select2',
                'name' => 'carModel',
                'label' => 'Model',
            ],
            function () {
                return CarModel::select('name')->distinct()->get()->pluck('name', 'name')->toArray();
            },
            function ($value) {
                $this->crud->addClause('whereHas', 'carModel', function ($query) use ($value) {
                    $query->where('name', '=', $value);
                });
            }
        );
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(SpecificationRequest::class);
        $this->crud->addField([
            'type' => 'select2',
            'name' => 'carModel',
        ]);

        $this->crud->addField([
            'type' => 'text',
            'name' => 'horse_power',
        ]);
        $this->crud->addField([
            'type' => 'text',
            'name' => 'torque',
        ]);
        $this->crud->addField([
            'type' => 'text',
            'name' => 'engine_name',
        ]);
        $this->crud->addField([
            'type' => 'text',
            'name' => 'price',
        ]);
        $this->crud->addField([
            'type' => 'text',
            'name' => 'grade',
        ]);
        $this->crud->addField([
            'type' => 'text',
            'name' => 'driven_train',
        ]);

        $this->crud->addField([
            'type'  => 'repeatable',
            'name' => 'dimensions_weight',
            'fields' => [
                [
                    'name' => 'overall',
                    'type'    => 'text',
                    'label'   => 'Overall',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
                [
                    'name' => 'wheel_base',
                    'type'    => 'text',
                    'label'   => 'Wheelbase',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
                [
                    'name' => 'tread',
                    'type'    => 'text',
                    'label'   => 'Tread',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
                [
                    'name' => 'minimum_running_ground_clearance',
                    'type'    => 'text',
                    'label'   => 'Minimum running ground clearance',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
                [
                    'name' => 'minimum_turning_radius',
                    'type'    => 'text',
                    'label'   => 'Minimum turning radius',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],

                [
                    'name' => 'gross_vehicles_weight',
                    'type'    => 'text',
                    'label'   => 'Gross vehicles weight',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],

                [
                    'name' => 'total_min_options',
                    'type'    => 'text',
                    'label'   => 'Total min options',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],

                [
                    'name' => 'total_max_options',
                    'type'    => 'text',
                    'label'   => 'Total max options',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
                [
                    'name' => 'seating_capacity',
                    'type'    => 'text',
                    'label'   => 'Seating capacity	',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
                [
                    'name' => 'deck_size',
                    'type'    => 'text',
                    'label'   => 'Deck size',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
            ],
            'max_rows' => 1,
        ]); //overall

        $this->crud->addField([
            'type'  => 'repeatable',
            'name' => 'engine',
            'fields' => [
                [
                    'name' => 'engine_model_code',
                    'type'    => 'text',
                    'label'   => 'Engine Model Code',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
                [
                    'name' => 'engine_type',
                    'type'    => 'text',
                    'label'   => 'Engine Type',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
                [
                    'name' => 'displacement',
                    'type'    => 'text',
                    'label'   => 'Displacement',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
                [
                    'name' => 'bore_x_stoke',
                    'type'    => 'text',
                    'label'   => 'Bore x stoke',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],

                [
                    'name' => 'compression_ratio',
                    'type'    => 'text',
                    'label'   => 'Compression ratio',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],

                [
                    'name' => 'max_output',
                    'type'    => 'text',
                    'label'   => 'Max.output (SAE net)',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],

                [
                    'name' => 'max_torque',
                    'type'    => 'text',
                    'label'   => 'Max.torque (SAE net)',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
                [
                    'name' => 'fuel_system',
                    'type'    => 'text',
                    'label'   => 'Fuel System',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
                [
                    'name' => 'fuel_type',
                    'type'    => 'text',
                    'label'   => 'Fuel Type',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
                [
                    'name' => 'fuel_tank_capacity',
                    'type'    => 'text',
                    'label'   => 'Fuel Tank Capacity',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
                [
                    'name' => 'fuel_consumption',
                    'type'    => 'text',
                    'label'   => 'Fuel Consumption',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
            ],
            'max_rows' => 1,
        ]); //engine
        $this->crud->addField([
            'type'  => 'repeatable',
            'name' => 'chassi',
            'fields' => [
                [
                    'name' => 'transmission_type',
                    'type'    => 'text',
                    'label'   => 'Transmission Type',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
                [
                    'name' => 'suspension',
                    'type'    => 'text',
                    'label'   => 'Suspension ( Front/Rear)',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
                [
                    'name' => 'brake_type',
                    'type'    => 'text',
                    'label'   => 'Brake Type (Front/Rear)',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
                [
                    'name' => 'rear_combination_lamps',
                    'type'    => 'text',
                    'label'   => 'Rear combination lamps',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],

                [
                    'name' => 'tires',
                    'type'    => 'text',
                    'label'   => 'Tires',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],

                [
                    'name' => 'wheels',
                    'type'    => 'text',
                    'label'   => 'Wheels',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],

                [
                    'name' => 'steering_gear_type',
                    'type'    => 'text',
                    'label'   => 'Steering gear type',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
            ],
            'max_rows' => 1,
        ]); //chassi

        $this->crud->addField([
            'type'  => 'repeatable',
            'name' => 'standard_equipment_exterior',
            'fields' => [
                [
                    'name' => 'headlamps',
                    'type'    => 'text',
                    'label'   => 'Headlamps',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
                [
                    'name' => 'front_fog_driving_lamp',
                    'type'    => 'text',
                    'label'   => 'Front fog & driving lamp',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
                [
                    'name' => 'daytime_running_light_system',
                    'type'    => 'text',
                    'label'   => 'Daytime running light system',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
                [
                    'name' => 'rear_combination_lamps',
                    'type'    => 'text',
                    'label'   => 'Rear combination lamps',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],

                [
                    'name' => 'outside_rear_view_mirror',
                    'type'    => 'text',
                    'label'   => 'Outside rear view mirror',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],

                [
                    'name' => 'mudguard',
                    'type'    => 'text',
                    'label'   => 'Mudguard',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
                [
                    'name' => 'deck_bar',
                    'type'    => 'text',
                    'label'   => 'Deck bar',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
                [
                    'name' => 'bed_liner',
                    'type'    => 'text',
                    'label'   => 'Bed liner',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
            ],
            'max_rows' => 1,
        ]); //exterior

        $this->crud->addField([
            'type'  => 'repeatable',
            'name' => 'standard_equipment_interior',
            'fields' => [
                [
                    'name' => 'steering_switch',
                    'type'    => 'text',
                    'label'   => 'Steering switch',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
                [
                    'name' => 'steering_column',
                    'type'    => 'text',
                    'label'   => 'Steering column',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
                [
                    'name' => 'part_time_transfer_switch',
                    'type'    => 'text',
                    'label'   => 'Part-time transfer switch',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
                [
                    'name' => 'drive_mode_switch',
                    'type'    => 'text',
                    'label'   => 'Drive mode switch',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],

                [
                    'name' => 'cruise_control',
                    'type'    => 'text',
                    'label'   => 'Cruise control',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],

                [
                    'name' => 'multi_information_display',
                    'type'    => 'text',
                    'label'   => 'Multi-information display	',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],

                [
                    'name' => 'economy_meter',
                    'type'    => 'text',
                    'label'   => 'Economy meter',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],

                [
                    'name' => 'front_seat',
                    'type'    => 'text',
                    'label'   => 'Front seat',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],

                [
                    'name' => 'rear_seat',
                    'type'    => 'text',
                    'label'   => 'Rear seat',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],

                [
                    'name' => 'seat_cover_material',
                    'type'    => 'text',
                    'label'   => 'Seat cover material',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],

                [
                    'name' => 'audio_system',
                    'type'    => 'text',
                    'label'   => 'Audio system',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],

                [
                    'name' => 'speaker_system',
                    'type'    => 'text',
                    'label'   => 'Speaker system',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],

                [
                    'name' => 'air_conditioning',
                    'type'    => 'text',
                    'label'   => 'Air conditioning',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],

                [
                    'name' => 'starting_system',
                    'type'    => 'text',
                    'label'   => 'Starting system',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],

                [
                    'name' => 'wireless_door_lock',
                    'type'    => 'text',
                    'label'   => 'Wireless door lock',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],

                [
                    'name' => 'accessory_connector',
                    'type'    => 'text',
                    'label'   => 'Accessory connector',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
            ],
            'max_rows' => 1,
        ]); //standard_equipment_interior

        $this->crud->addField([
            'type'  => 'repeatable',
            'name' => 'safety_system',
            'fields' => [
                [
                    'name' => 'pre_collision_system',
                    'type'    => 'text',
                    'label'   => 'Pre-collision System (PCS)',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
                [
                    'name' => 'dynamic_radar_cruise_control',
                    'type'    => 'text',
                    'label'   => 'Dynamic Radar Cruise Control (DRCC)',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
                [
                    'name' => 'lane_departure_alert',
                    'type'    => 'text',
                    'label'   => 'Lane Departure Alert (LDA)',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
                [
                    'name' => 'downhill_assist_control',
                    'type'    => 'text',
                    'label'   => 'Downhill Assist Control (DAC)',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
                [
                    'name' => 'limited_slip_differential',
                    'type'    => 'text',
                    'label'   => 'Limited Slip Differential (LSD)',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
                [
                    'name' => 'srs_airbags',
                    'type'    => 'text',
                    'label'   => 'SRS airbags',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
                [
                    'name' => 'anti_lock_brake_system',
                    'type'    => 'text',
                    'label'   => 'Anti-lock brake system (ABS)',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
                [
                    'name' => 'vehicles_stability_control',
                    'type'    => 'text',
                    'label'   => 'Vehicles Stability Control (VSC)',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
                [
                    'name' => 'hill_start_assist_control',
                    'type'    => 'text',
                    'label'   => 'Hill start assist control (HAC)',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],

                [
                    'name' => 'trailer_sway_control',
                    'type'    => 'text',
                    'label'   => 'Trailer Sway Control (TSC)',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],

                [
                    'name' => 'emergency_brake_signal',
                    'type'    => 'text',
                    'label'   => 'Emergency brake signal',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],

                [
                    'name' => 'clearance_back_sonar',
                    'type'    => 'text',
                    'label'   => 'Clearance & back sonar',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],

                [
                    'name' => 'back_monitor',
                    'type'    => 'text',
                    'label'   => 'Back monitor',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
                [
                    'name' => 'child_restraint_system',
                    'type'    => 'text',
                    'label'   => 'Child Restraint System',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
            ],
            'max_rows' => 1,
        ]); //safety_system

    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
