<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ColorRequest;
use App\Http\Requests\CarModelRequest;
use App\Models\CarModel;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ColorCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ColorCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Color');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/color');
        $this->crud->setEntityNameStrings('color', 'colors');
    }

    protected function setupListOperation()
    {
        $this->crud->addColumn([
            'type' => 'text',
            'name' => 'name',
        ]);

        $this->crud->addColumn([
            'type' => 'color',
            'name' => 'code',
        ]);
        $this->crud->addColumn([
            'type' => 'browse',
            'name' => 'image',
        ]);

        $this->crud->removeColumn('carModel');
        $this->crud->addColumn([
            'name' => 'carModel',
            'label' => 'Car Model',


        ]);

        $this->crud->addFilter([
            'type' => 'select2',
            'name' => 'carModel',
            'label' => 'Car Model',
        ],
            function () {
                return CarModel::select('name')->distinct()->get()->pluck('name', 'name')->toArray();
            },
            function ($value) {
                $this->crud->addClause('whereHas', 'carModel', function ($query) use ($value) {
                    $query->where('name', '=', $value);
                });
            });
        $this->crud->denyAccess('show');



    }


    protected function setupCreateOperation()
    {
       $this->crud->setValidation(ColorRequest::class);

         $this->crud->addField([
            'type'    => 'color',
            'default' => '#000000',
            'name' => 'code',
        ]);

        $this->crud->addField([
            'type' => 'text',
            'name' => 'name',
        ]);

       $this->crud->addField([
            'type' => 'select2',
            'name' => 'carModel',

        ]);

        $this->crud->addField([
            'type' => 'browse',
            'name' => 'image',
        ]);



    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
