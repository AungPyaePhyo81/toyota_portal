<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\MapRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class MapCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class MapCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Map');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/map');
        $this->crud->setEntityNameStrings('map', 'maps');
    }

    protected function setupListOperation()
    {
        $this->crud->addColumn([
            'type' => 'text',
            'name' => 'name',
        ]);
       $this->crud->addColumn([
            'lable' => 'City',
            'name' => 'city',

        ]);

        $this->crud->addColumn([
            'type' => 'text',
            'name' => 'latitude',
        ]);
         $this->crud->addColumn([
            'type' => 'text',
            'name' => 'longitude',
        ]);
    }

    protected function setupCreateOperation()
    {
       $this->crud->setValidation(MapRequest::class);
       $this->crud->addField([
            'type' => 'text',
            'name' => 'name',
        ]);
       $this->crud->addField([
            'type' => 'select2',
            'label'=>'City',
            'name' => 'city',

        ]);

        $this->crud->addField([
            'type' => 'text',
            'name' => 'latitude',
        ]);
         $this->crud->addField([
            'type' => 'text',
            'name' => 'longitude',
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
