<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\OnlineInquiryRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class OnlineInquiryCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class OnlineInquiryCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\OnlineInquiry');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/onlineinquiry');
        $this->crud->setEntityNameStrings('onlineinquiry', 'online_inquiries');
        $this->crud->denyAccess('create');;
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        $this->crud->setFromDb();
        $this->crud->removeColumn('type_of_inquiries'); // remove a column from the table

    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(OnlineInquiryRequest::class);

        $this->crud->addField([
            'name' => 'name',
            'label' => 'Name',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'email',
            'label' => 'Email',
            'type' => 'email'
        ]);

        $this->crud->addField([
            'name' => 'phone',
            'label' => 'Phone',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'dial_code',
            'label' => 'Dial Code',
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'type_of_inquiries',
            'label' => 'Type of Inquiries',
            'type' => 'radio',
            'options' => [
                'Sales' => 'Sales',
                'Service' => 'Service',
                'Spare Parts' => 'Spare Parts'
            ]
        ]);

        $this->crud->addField([
            'name' => 'description',
            'label' => 'Description',
            'type' => 'textarea'
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
