<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CityRequest;
use App\Models\Province;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class CityCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CityCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\City');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/city');
        $this->crud->setEntityNameStrings('city', 'cities');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        // $this->crud->setFromDb();

        $this->crud->addColumn([
            'name' => 'city_name',
            'type' => 'text',
            'label' => 'Name',
        ]);



        $this->crud->addColumn([
            'name' => 'province',
            'label' => 'Province',

        ]);

        $this->crud->addFilter(
            [
                'type' => 'select2',
                'name' => 'province',
                'label' => 'Province',
            ],
            function () {
                return Province::select('name')->distinct()->get()->pluck('name', 'name')->toArray();
            },
            function ($value) {
                $this->crud->addClause('whereHas', 'province', function ($query) use ($value) {
                    $query->where('name', '=', $value);
                });
            }
        );
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(CityRequest::class);
        // $this->crud->setFromDb();
        $this->crud->addField([
            'type' => 'select2',
            'name' => 'province',
        ]);
        $this->crud->addField([
            'type' => 'text',
            'name' => 'city_name',
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
