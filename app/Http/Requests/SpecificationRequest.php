<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class SpecificationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'torque' => 'required',
            // 'engine_name' => 'required',
            // 'grade' => 'required',
            // 'driven_train' => 'required',
            // 'dimensions_weight' => 'required',
            // 'engine' => 'required',
            // 'chassi' => 'required',
            // 'standard_equipment_exterior' => 'required',
            // 'standard_equipment_interior' => 'required',
            // 'safety_system' => 'required',

            'carModel' => 'required'
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
