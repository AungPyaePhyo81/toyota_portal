<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class PromotionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             'promotion_title' => 'required',
             'promotion_thumbnail' => 'required',
             'promotion_description' => 'required',
             'promotion_start_date' => 'required',
             'promotion_end_date' => 'required',
             'dealer_id' => 'required|exists:dealers,id',
        ];
    }


    public function withValidator($validator)
{
    $validator->after(function ($validator) {
        if ($this->input('promotion_start_date') > $this->input('promotion_start_date')) {
            $validator->errors()->add('promotion_start_date', 'The start date must be before or equal to the end date.');
            $validator->errors()->add('promotion_end_date', 'The end date must be after or equal to the start date.');
        }
    });
}
    public function messages()
    {
        return [
            'promotion_start_date.before_or_equal' => 'The start date must be before or equal to the end date.',
            'promotion_end_date.after_or_equal' => 'The end date must be after or equal to the start date.',
            // other custom error messages
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'promotion_start_date.before_or_equal' => 'The start date must be before or equal to the end date.',
            'promotion_end_date.after_or_equal' => 'The end date must be after or equal to the start date.',
            // other custom error messages
        ];

    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */

}
