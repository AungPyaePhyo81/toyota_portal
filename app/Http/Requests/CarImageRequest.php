<?php

namespace App\Http\Requests;
use Illuminate\Validation\Rule;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class CarImageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'car_models_id' => 'required',
            // 'colors_id' => 'required',
            // 'colors_id' => [
            //     'required',
            //     Rule::exists('colors', 'id'),
            // ],
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
