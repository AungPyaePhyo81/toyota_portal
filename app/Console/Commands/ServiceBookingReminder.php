<?php

namespace App\Console\Commands;

use App\Mail\ServiceBookingReminderMail;
use App\Models\ServiceBooking;
use Illuminate\Console\Command;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class ServiceBookingReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:service-booking';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sending email to service booking user when near booking date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::info("Schedule working fine");
        $nearDate = Carbon::now()->addDays(3);
        $serviceBookings = ServiceBooking::whereDate('date', '=', $nearDate)->where('is_confirm', 1)->get();
        foreach ($serviceBookings as $serviceBooking) {
            try {
                Mail::to($serviceBooking->email)->queue(new ServiceBookingReminderMail($serviceBooking));
                Log::info("email send to " . $serviceBooking->email);
            } catch (\Exception $e) {
                Log::error("email send fail mss =>" . $e);
            }
        }
    }
}
