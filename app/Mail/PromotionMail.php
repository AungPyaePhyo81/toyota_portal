<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PromotionMail extends Mailable
{
    use Queueable, SerializesModels;

    public $stayInformedMail;

    public $data;

    public $url;
    public function __construct($stayInformedMail, $data, $url)
    {
        $this->stayInformedMail = $stayInformedMail;
        $this->data = $data;
        $this->url = $url;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('New TOYOTA Promotion!')->view('promotion.mail');
    }
}
