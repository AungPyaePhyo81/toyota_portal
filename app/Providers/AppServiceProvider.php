<?php

namespace App\Providers;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Models\Category;
use Illuminate\Support\ServiceProvider;
use Illuminate\Validation\Rule;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('layouts.nav', function ($view) {
            $categories = Category::with('subCategories')->get();
            $view->with([
                'categories' => $categories
            ]);
        });

}
}
