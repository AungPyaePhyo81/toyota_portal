<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class DealersInfo extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'dealers_info';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = ['address', 'dealer_id', 'map_id', 'service'];
    // protected $hidden = [];
    // protected $dates = [];




    // public function dealer(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    // {

    // }
    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function dealer(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Dealer::class, 'dealer_id', 'id');
    }



    public function map(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {

        return $this->belongsTo(Map::class, 'map_id', 'id');
    }

    public function typeOfServices(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {

        return $this->belongsTo(TypeOfService::class, 'type_of_service_id', 'id');
    }

    // public function typeOfServices()
    // {
    //     return $this->belongsToMany(TypeOfService::class);
    // }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
