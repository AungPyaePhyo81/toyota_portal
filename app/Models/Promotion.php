<?php

namespace App\Models;

use App\Mail\PromotionMail;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class Promotion extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'promotions';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = ['dealer_id'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    public function dealer(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Dealer::class,'dealer_id','id');

    }

    protected static function boot()
    {
        parent::boot();

        static::created(function ($promotion) {
            $stayInformedMails = StayInformed::where('notification', 1)->pluck('email');
            $url = route('promotion');
            $data = Promotion::orderBy('id','DESC')->first();
            foreach ($stayInformedMails as $stayInformedMail) {
                try {
                    Mail::to($stayInformedMail)->queue(new PromotionMail($stayInformedMail, $data, $url));
                    Log::info("email send to " . $stayInformedMail);
                }catch (\Exception $e){
                    Log::error("email send fail mss =>".$e);
                }
            }
        });
    }


    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
