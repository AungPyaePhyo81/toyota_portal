const observer = new IntersectionObserver(entries => {
    entries.forEach(entry => {
      if (entry.isIntersecting) {
        entry.target.classList.add('animate');
      } else {
        entry.target.classList.remove('animate');
      }
    });
  });

  const items = document.querySelectorAll('.animation');

  items.forEach(e => {
    observer.observe(e);
  });
