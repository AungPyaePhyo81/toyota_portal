<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="{!! $blog->long_description !!}">
    <meta property="og:title" content="{{ $blog->title }}">
    <meta property="og:description" content="{!! $blog->long_description !!}">
    <meta property="og:image" content="{{ asset('storage/' . $blog->blog_thumbnail) }}">
    <title>Toyota Myanmar</title>
</head>

<body>
    <h1>{{ $blog->title }}</h1>
    <p>{!! $blog->long_description !!}</p>
    <img src="{{ asset('storage/' . $blog->blog_thumbnail) }}" alt="blog_thumbnail" width="100%">
    <a href="{{ route('blog.detail', $blog->id) }}"></a>
</body>

</html>
