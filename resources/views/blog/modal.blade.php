  <!-- Modal -->
  <div class="modal fade overflow-hidden" id="imgViewerModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="imgViewerModalLabel" aria-hidden="true">
    <div class="img-viewer-container position-fixed">
        {{-- <i class="fa-solid fa-xmark text-light img-viewer-icon icon-xxl" data-bs-dismiss="modal" aria-label="Close" id="close"></i> --}}
        {{-- <i class="fa-solid fa-magnifying-glass-plus text-light img-viewer-icon icon-xxl" id="zoom_in"></i>
        <i class="fa-solid fa-magnifying-glass-minus text-light img-viewer-icon icon-xxl" id="zoom_out"></i> --}}
        <i class="las la-times text-light img-viewer-icon icon-xxl" data-bs-dismiss="modal" aria-label="Close" id="close"></i>
        <i class="las la-search-plus text-light img-viewer-icon icon-xxl" id="zoom_in" style="transform:scaleX(-100%)"></i>
        <i class="las la-search-minus text-light img-viewer-icon icon-xxl" id="zoom_out" style="transform:scaleX(-100%)"></i>
    </div>
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content" id="img_container">
            <img src="" alt="" id="view_img">
        </div>
    </div>
  </div>

@section('js')
    <script>
        $(document).ready(function() {
            // image viewer
            var scale = 1.0;
            var image = $("#img_container");

            $("#zoom_in").click(function() {
                scale += 0.1;
                image.css({
                    "transform": "scale(" + scale + ")"
                });
            });

            $("#zoom_out").click(function() {
                scale -= 0.1;
                if (scale > 0.1) {
                    image.css({
                        "transform": "scale(" + scale + ")"
                    });
                } else {
                    scale += 0.1;
                }
            });

            $('#close').click(function() {
                scale = 1.0;
                image.css({
                    "transform": "scale(" + scale + ")"
                });
            })

            if ($('#imgViewerModal')) {
                $('#imgViewerModal').on('show.bs.modal', function(event) {
                    btn = event.relatedTarget;
                    img = $(btn).attr('data-bs-img');
                    // console.log(img)
                    $('#view_img').attr('src', '/storage/' + img)
                });
            }
        });
    </script>
@endsection
