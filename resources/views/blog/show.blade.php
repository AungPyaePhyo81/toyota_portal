@extends('layouts.main')

@section('title', "$blog->title | Blogs")

@section('content')
    <main class="container-fluid bg-white px-sm-5 px-3 pb-4">
        @include('components.breadcrumb', ['data' => [['route' => 'blog', 'name' => 'Blog']]])
        <article class="container py-5">
            <h1 class="py-2">{{ $blog->id }}. {{ $blog->title }}</h1>
            <div class="py-4 d-flex align-items-center text-secondary" style="--icon-color: #333; --icon-size: 1.7em">
                <span class="me-4">{{ date('d / M / Y', strtotime($blog->date)) }}</span>
                @php
                    $shareButtons = \Share::page(route('blog.share', $blog->id))
                        ->facebook()
                        ->linkedin();
                @endphp
                {!! $shareButtons !!}
            </div>
            <button type="button" data-bs-img="{{ $blog->blog_thumbnail }}" class="btn-img-viewer overflow-hidden"
                data-bs-toggle="modal" data-bs-target="#imgViewerModal">
                <img src="{{ asset('storage/' . $blog->blog_thumbnail) }}" alt="" width="100%">
            </button>
            @include('blog.modal')
            <p class="toyota-paragraph">{!! $blog->long_description !!}</p>
        </article>
        <section>
            <div class="container">
                <div class="owl-carousel owl-theme owl-loaded position-relative pt-2 pb-5 blog-carousel" id="blog_img">
                    <div class="owl-stage-outer">
                        <div class="owl-stage d-flex justify-content-center align-items-center">
                            @php
                                $images = json_decode($blog->images, true);
                            @endphp
                            @foreach ($images as $img)
                                <input type="hidden">
                                <div class="owl-item">
                                    <button type="button" class="btn-img-viewer overflow-hidden rounded"
                                        data-bs-img="{{ $img['image'] }}" data-bs-toggle="modal"
                                        data-bs-target="#imgViewerModal">
                                        <img src="{{ asset('storage/' . $img['image']) }}" alt="" width="100%">
                                    </button>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            var owl = $('.owl-carousel');
            owl.owlCarousel({
                center: true,
                loop: false,
                margin: 20,
                nav: true,
                dots: false,
                responsive: {
                    0: {
                        items: 1,
                    },
                    600: {
                        items: 2,
                    },
                    1200: {
                        items: 3,
                    }
                }
            });
            owl.on('mousewheel', '.owl-stage', function(e) {
                if (e.originalEvent.deltaY > 0) {
                    owl.trigger('next.owl');
                } else {
                    owl.trigger('prev.owl');
                }
                e.preventDefault();
            });
        })
    </script>
@endsection
