@extends('layouts.main')

@section('title', 'Blogs')

@section('content')
    <main class="px-sm-5 px-3">
        @include('components.breadcrumb')
        <section class="container pt-4">
            <h2 class="fw-bold">Blogs</h2>

            {{-- Search --}}
            <form class="row" action="{{ route('blog.search') }}" method="GET">
                @csrf
                <div class="col-12 col-lg-6 d-flex flex-column flex-sm-row">
                    <div class="position-relative  w-100">
                        <i class="fa-solid fa-magnifying-glass position-absolute fa-lg text-toyota-danger search-icon"></i>
                        <input type="search" name="blog_text" id="blog_search" value="{{ $search ?? null }}"
                            class="form-control toyota-input ps-5" placeholder="Type here to search">
                    </div>
                    <input type="submit" value="Search" id="text_search"
                        class="btn btn-toyota-danger my-2 px-4 ms-0 ms-sm-4 rounded fw-bold d-none d-sm-block">
                </div>
                <div class="col-12 col-lg-6 d-flex flex-column flex-sm-row">
                    <select name="blog_topic" id="topic_select" class="form-select toyota-input">
                        <option value="" {{ !isset($topic) ? 'selected' : '' }}>All topics</option>
                        <option value="sale" {{ isset($topic) ? ($topic == 'sale' ? 'selected' : '') : '' }}>Sales
                        </option>
                        <option value="service" {{ isset($topic) ? ($topic == 'service' ? 'selected' : '') : '' }}>Service
                        </option>
                        <option value="sparepart" {{ isset($topic) ? ($topic == 'sparepart' ? 'selected' : '') : '' }}>Spare
                            Parts</option>
                    </select>
                    <input type="submit" value="Search" id="topic_search"
                        class="btn btn-toyota-danger my-2 px-4 ms-0 ms-sm-4 rounded fw-bold">
                </div>
            </form>


            {{-- List View --}}
            <div class="row">
                @foreach ($blogs as $blog)
                    <div class="col-12 col-lg-6 d-flex align-items-center py-3">
                        <a href="{{ route('blog.detail', [$blog->id]) }}"
                            class="position-relative blog-img-container w-100 h-100">
                            <img src="{{ asset('storage/' . $blog->blog_thumbnail) }}" alt="" width="100%"
                                height="100%" class="blog-img">
                        </a>
                    </div>
                    <div class="col-12 col-lg-6 d-flex flex-column justify-content-center py-3 border-bottom">
                        <div>
                            <h3 class="fw-bold">{{ $blog->id }}. {{ $blog->title }}</h3>
                            <p class="text-secondary py-2">
                                {{ date('d / m / Y', strtotime($blog->date)) }}
                                @if ($blog->status == 'sale')
                                    <span class="mx-2 badge rounded-pill bg-danger">Sales</span>
                                @elseif ($blog->status == 'service')
                                    <span class="mx-2 badge rounded-pill bg-dark">Service</span>
                                @else
                                    <span class="mx-2 badge rounded-pill bg-light text-danger border border-danger">Spare
                                        Parts</span>
                                @endif
                            </p>
                            <p>
                                {{ $blog->slug }}
                            </p>
                        </div>
                        <div class="d-flex justify-content-between align-items-center">
                            <a href="{{ route('blog.detail', [$blog->id]) }}"
                                class="btn btn-toyota-outline-danger px-4">Read More</a>
                            <div class="py-1 py-sm-5">
                                <u>Share With Friends</u>
                                <br>
                                <div class="text-start d-flex justify-content-center">
                                    @php
                                        $shareButtons = \Share::page(route('blog.share', $blog->id))
                                            ->facebook()
                                            ->linkedin();
                                    @endphp
                                    {!! $shareButtons !!}
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </section>
    </main>
    <div class="py-3 d-flex justify-content-center">
        {{-- {{ $blogs->links('vendor.pagination.simple-custom') }} --}}
        {{ $blogs->appends(['blog_text' => request('blog_text'), 'blog_topic' => request('blog_topic')])->links('vendor.pagination.simple-custom') }}
    </div>

    @if (count($years) > 0)
        <article class="bg-white py-4">
            <div class="accordion container" id="blog_accordion">
                @foreach ($years as $i => $year)
                    <div class="accordion-item border-bottom">
                        <h2 class="accordion-header" id="heading{{ $i }}">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapse{{ $i }}" aria-expanded="false">
                                {{ $year['year'] }} ({{ $year['count'] }} article{{ $year['count'] > 1 ? 's' : '' }})
                            </button>
                        </h2>
                        <div id="collapse{{ $i }}" class="accordion-collapse collapse"
                            data-bs-parent="#blog_accordion">
                            <div class="accordion-body row"">
                                @foreach ($year['blogs'] as $idx => $blog)
                                    <div class="col-xl-4 col-md-6 col-12 py-3">
                                        @include('blog.card', [
                                            'no' => true,
                                            'id' => $blog->id,
                                            'blog_thumbnail' => $blog->blog_thumbnail,
                                            'title' => $blog->title,
                                            'description' => $blog->description,
                                            'date' => date('d/m/Y', strtotime($blog->date)),
                                            'share' => true,
                                        ])
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
            <div class="text-end p-4 m-4">
                <a href="#" class="animated-link-reverse" style="--line-color: #000">Back To Top</a>
            </div>
        </article>
    @endif

@endsection
