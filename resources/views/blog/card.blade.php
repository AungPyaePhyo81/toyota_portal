<a href="{{ route('blog.detail', [$id]) }}" class="blog-img-container">
    <img src='{{ asset('storage/' . $blog_thumbnail) }}' alt="" height="100%" width="100%">
</a>
<div class="blog-text-container text-light p-4">
    <h5 class="blog-title">
        <span>{{ !is_null($no) ? $id . '.' : '' }}</span>
        {{ $title }}
    </h5>
    @if (isset($description))
        <p class="blog-description">{{ $description }}</p>
    @endif

    @if (isset($date))
        <span class="text-secondary">{{ $date }}</span>
    @endif
    <div class="row">
        <div class="col d-flex align-items-center">
            <a href="{{ route('blog.detail', [$blog->id]) }}" class="btn btn-toyota-outline-danger px-4">Read More</a>
        </div>
        @if (isset($share))
            <div class="col">
                <u>Share With Friends</u>
                <div class="text-start d-flex gap-3" style="--icon-color: #fff; --icon-size: 25px">
                    @php
                        $shareButtons = \Share::page(route('blog.share', $blog->id))
                            ->facebook()
                            ->linkedin();
                    @endphp
                    {!! $shareButtons !!}
                </div>
            </div>
        @endif
    </div>
</div>
