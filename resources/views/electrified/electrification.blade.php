@extends('layouts.main')

@section('head')
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="pragma" content="no-cache">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=5.0">
    <meta name="theme-color" media="(prefers-color-scheme: light)" content="#ffffff">
    <meta name="theme-color" media="(prefers-color-scheme: dark)" content="#eb0a1e">
    <meta name="title" content="Toyota Electrification | Toyota Myanmar">
    <meta name="description" content="Discover how we power future mobility by investing in and innovating electrification solutions for you.">
    <title>Toyota Electrification | Toyota Myanmar</title>
    <meta name="theme-color" content="#eb0a1e" /><link rel="manifest" href="electrified/manifest.json" /><link rel="shortcut icon" href="favicon.svg">
    <link href="electrified/assets/css/global.css" rel="stylesheet">
    <link href="electrified/assets/css/toyota-electrification.css" rel="stylesheet"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/Noto-Sans-Devanagari-Black.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/Noto-Sans-Devanagari-Black.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/Noto-Sans-Devanagari-Bold.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/Noto-Sans-Devanagari-Bold.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/Noto-Sans-Devanagari-ExtraLight.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/Noto-Sans-Devanagari-ExtraLight.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/Noto-Sans-Devanagari-Light.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/Noto-Sans-Devanagari-Light.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/Noto-Sans-Devanagari-Regular.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/Noto-Sans-Devanagari-Regular.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/Noto-Sans-Devanagari-SemiBold.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/Noto-Sans-Devanagari-SemiBold.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansKhmer-Black.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansKhmer-Black.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansKhmer-Bold.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansKhmer-Bold.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansKhmer-ExtraLight.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansKhmer-ExtraLight.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansKhmer-Light.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansKhmer-Light.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansKhmer-Regular.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansKhmer-Regular.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansKhmer-SemiBold.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansKhmer-SemiBold.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansLao-Black.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansLao-Black.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansLao-Bold.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansLao-Bold.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansLao-ExtraLight.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansLao-ExtraLight.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansLao-Light.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansLao-Light.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansLao-Regular.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansLao-Regular.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansLao-SemiBold.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansLao-SemiBold.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/ToyotaType-Black.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/ToyotaType-Black.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/ToyotaType-Bold.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/ToyotaType-Bold.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/ToyotaType-Book.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/ToyotaType-Book.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/ToyotaType-Light.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/ToyotaType-Light.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/ToyotaType-Regular.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/ToyotaType-Regular.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/ToyotaType-Semibold.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/ToyotaType-Semibold.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/lg.ttf" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/lg.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/lg.woff2" rel="preload">
@endsection

@section('content')
  <body class="cpl-pg--brand">
    <div id="cpl--wrapper">
      <main class="p-0">
        <!-- Article -->
        <article>
          <!-- JSON data -->
    <script type="text/javascript">
    // Masthead
    window.data_brand_masthead = {
        "media": {
        "imgBig_webp": "",
        "imgSml_webp": "",
        "imgBig": "electrified/dam/electrification/masthead-og-explore/toyota-xev-masthead-d.jpg",
        "imgSml": "electrified/dam/electrification/masthead-og-explore/toyota-xev-masthead-m.jpg",
        "videoBig": "electrified/dam/electrification/masthead-xev.mp4",
        "videoSml": "electrified/dam/electrification/masthead-xev.mp4",
        "imgAlt": "TOYOTA ELECTRIFICATION "
        },
        "content": {
        "header": ["TOYOTA", "ELECTRIFICATION"],
        "tagline": "",
        "logoSrc": "",
        "isLightTheme": false,
        "isStretchOut": false,
        "isAlignToBottom": true
        },
        "intro": {
        "header": "CHANGE IS IN MOTION",
        "desc": "<p>At Toyota, we are on a mission to become a company that contributes to a better global environment. To date, we have saved more than 160 million metric tonnes of CO<sub>2</sub> emissions – and we are taking action to do more. This includes powering future mobility with multiple solutions for everyone while aiming for carbon neutrality.<br /><br />Our journey began nearly 30 years ago with the introduction of the world’s first hybrid electric vehicle. Since then, we have been investing in and innovating various electrification solutions to address our customers’ lifestyle and mobility needs. </p>"
        }
    };

    // Timeline
    window.data_brand_timeline = {
        "id": "redefining-whats-possible-today",
        "bgColor": "white", // off-gold, off-gold-v2, black, white
        "header": "OUR ELECTRIFICATION JOURNEY",
        "timestamps": [
        {
            "date": "1997",
            "desc": "Launch of Toyota Prius – the world’s first mass-produced hybrid electric vehicle",
        },
        {
            "date": "2014",
            "desc": "Launch of Mirai – Toyota’s first hydrogen fuel cell electric vehicle ",
        },
        {
            "date": "2020",
            "desc": "Expansion of our electrified vehicle lineup to 55 models – and counting",
        },
        {
            "date": "2022",
            "desc": "Launch of bZ4X, cumulative global sales of Toyota EVs reach 20 million units",
        },
        {
            "date": "2030",
            "desc": "Aim to introduce a full lineup of 30 battery electric models",
        },
        {
            "date": "2050",
            "desc": "Toyota’s goal: to achieve carbon neutrality",
        }
        ]

    }

    // REDEFINING WHAT'S POSSIBLE TODAY
    window.data_brand_cards_single__1 = {
        "id": "redefining-whats-possible-today",
        "bgColor": "black", // off-gold, off-gold-v2, black, white
        "bgBig": "electrified/dam/electrification/toyota-xEV-video-bg-1920x1080.jpg",
        "bgSml": "electrified/dam/electrification/toyota-xEV-video-bg-1920x1080.jpg",
        "header": "REDEFINING WHAT’S POSSIBLE TODAY",
        "desc": "<p>In our pursuit over the decades to create ever-better cars, one thing has remained constant: driving towards a sustainable future that makes way for every lifestyle.​</p>",
        "cards": [
        {
            "isSideBySide": false,
            "img": "electrified/dam/electrification/toyota-xev-vid-thumbnail.jpg",
            "imgAlt": "",
            "videoUrl": "https://www.youtube.com/watch?v=FvZrpBiqD8U&ab_channel=ToyotaMotorAsiaPacific",
            "header": "",
            "desc": "",
            "cta": {
            "label": "",
            "url": ""
            }
        }
        ],
        "ctaBtn": {
        "label": "",
        "url": ""
        }
    };

    // MULTIPLE PATHS TO SUSTAINABLE MOBILITY
    window.data_cards_mobility__1 = {
        "id": "",
        "layoutClass": "cpl-grid-cell-4",
        "bgColor": "off-white", // white, off-white, grey-mid, grey-dark, black
        "header": "SUSTAINABLE FUTURE FOR EVERY LIFESTYLE",
        "desc": "Guided by our ambition to achieve carbon neutrality by 2050, we are developing our lineup of electrified vehicles through multiple pathways. This means you can choose the car that best fits your lifestyle.",
        "cards": [
        {
            "img": "electrified/dam/electrification/toyota-xev-hev.jpg",
            "imgAlt": "",
            "header": "Hybrid Electric Vehicles",
            "cta": {
            "url": "hybrid-electric-vehicles"
            },
            "contentImg": "electrified/dam/electrification/toyota-xev-hev.jpg",
            "contentImgAlt": "",
            "contentHeader": "Hybrid Electric Vehicles",
            "contentDesc": "Hybrid electrics offer power without compromise and charge on the go, while helping you reduce emissions and fuel consumption.<br /><br />For city journeys, Toyota hybrid electrics can run purely on the electric motor. And when power is needed, both the petrol engine and the electric motor can combine and optimise for an exhilarating yet fuel-efficient drive. ",
            "contentList": "<li>Improved fuel economy</li><li>Charge while driving</li><li>Convenient refuelling</li>",
            "contentLabel": "Learn more",
            "contentUrl": "{{asset('hybrid-electric')}}"
        },
        {
            "img": "electrified/dam/electrification/toyota-xev-phev.jpg",
            "imgAlt": "",
            "header": "Plug-In Hybrid Electric Vehicles",
            "cta": {
            "url": "plug-in-hybrid-electric-vehicles"
            },
            "contentImg": "electrified/dam/electrification/toyota-xev-phev.jpg",
            "contentImgAlt": "",
            "contentHeader": "Plug-In Hybrid Electric<br />Vehicles",
            "contentDesc": "Like hybrid electrics, plug-in hybrid electrics come with two power sources – an electric motor for completing shorter trips on electric energy, and a secondary petrol engine for longer distance drives.<br /><br />Their bigger battery enables them to go further on a full charge, and they can run solely on electric power for short journeys such that they do not produce emissions. You enjoy all the advantages of hybrids, with the added option of plug-in charging on the go to travel further on pure electric power. ",
            "contentList": "<li>Flexibility between powertrains</li><li>Home or public charging</li><li>Convenient refuelling</li>",
            "contentLabel": "",
            "contentUrl": ""
        },
        {
            "img": "electrified/dam/electrification/toyota-xev-bev.jpg",
            "imgAlt": "",
            "header": "Battery Electric Vehicles",
            "cta": {
            "url": "battery-electric-vehicles"
            },
            "contentImg": "electrified/dam/electrification/toyota-xev-bev.jpg",
            "contentImgAlt": "",
            "contentHeader": "Battery Electric Vehicles",
            "contentDesc": "These vehicles run solely on electric power and recharge when plugged in. Battery electric vehicles enable you to eliminate exhaust emissions from your journeys.<br /><br />Instant torque and acceleration also give you maximum power without the need to shift gears. Experience a smooth and entertaining drive.",
            "contentList": "<li>No exhaust emissions</li><li>Near-silent drive</li><li>Instant acceleration</li>",
            "contentLabel": "Learn more",
            "contentUrl": ""
        },
        {
            "img": "electrified/dam/electrification/toyota-xev-fuel-cell.jpg",
            "imgAlt": "",
            "header": "Fuel Cell Electric Vehicles",
            "cta": {
            "url": "fuel-cell-electric-vehicles"
            },
            "contentImg": "electrified/dam/electrification/toyota-xev-fuel-cell.jpg",
            "contentImgAlt": "",
            "contentHeader": "Fuel Cell Electric Vehicles",
            "contentDesc": "Touted for their clean driving performance, fuel cell electric vehicles (FCEVs) emit only water vapour.<br /><br />They run on electric power generated by the chemical reaction between oxygen from the air and hydrogen stored in tanks onboard. A compact battery pack stores extra energy, including any from regenerative braking. ",
            "contentList": "<li>Zero exhaust emissions</li><li>Remarkable range</li><li>Rapid refuelling</li>",
            "contentLabel": "",
            "contentUrl": ""
        }
        ]
    };

    // EXPLORE OUR ROAD CARS
    window.data_explore_models = {
        "id": "explore-our-road-cars",
        "bgColor": "white", // white, off-white, grey-mid, grey-dark, black
        "header": "EXPLORE OUR VEHICLES​"
    };

    window.data_global_nav_models = {
        "label": "Models",
        "viewModelsLabel": "View grades",
        "currentDateTimeAPI": "https://toyota-aem.convertium.net:4502/bin/services/v1/getCurrentTime",
        "tabs": [
        {
            "id": "hev",
            "label": "HYBRID ELECTRIC",
            "subTabs": [
            {
                "id": "all",
                "label": "",
                "modelLists": [
                {
                    "id": "corolla-cross-hybrid-electric",
                    "header": "COROLLA CROSS HYBRID ELECTRIC",
                    "desc": "Cross-over",
                    "priceLabel": "From $XX,XXX",
                    "promoLabel": "",
                    "imgLazy": "electrified/dam/cars/lazy-angle-left.png",
                    "imgThumb": "electrified/dam/cars/toyota-xev-corolla-cross-hev.png",
                    "url": "#",
                    "selectGrades": []
                },
                {
                    "id": "camry-hybrid-electric",
                    "header": "CAMRY HYBRID ELECTRIC",
                    "desc": "Sedan",
                    "priceLabel": "From $XX,XXX",
                    "promoLabel": "",
                    "imgLazy": "electrified/dam/cars/lazy-angle-left.png",
                    "imgThumb": "electrified/dam/cars/toyota-xev-camry-hybrid.png",
                    "url": "#",
                    "selectGrades": []
                },
                {
                    "id": "rav4-hybrid-electric",
                    "header": "RAV4 HYBRID ELECTRIC",
                    "desc": "SUV",
                    "priceLabel": "From $XX,XXX",
                    "promoLabel": "",
                    "imgLazy": "electrified/dam/cars/lazy-angle-left.png",
                    "imgThumb": "electrified/dam/cars/toyota-xev-rav4-hybrid.png",
                    "url": "#",
                    "selectGrades": []
                }
                ]
            }
            ]
        },
        {
            "id": "bev",
            "label": "BATTERY ELECTRIC",
            "subTabs": [
            {
                "id": "all",
                "label": "",
                "modelLists": [
                {
                    "id": "corolla-cross-hybrid-electric",
                    "header": "BZ4X",
                    "desc": "All-Electric",
                    "priceLabel": "From $XX,XXX",
                    "promoLabel": "",
                    "imgLazy": "electrified/dam/cars/lazy-angle-left.png",
                    "imgThumb": "electrified/dam/cars/toyota-xev-bz4x.png",
                    "url": "#",
                    "selectGrades": []
                }
                ]
            }
            ]
        }
        ]
    };

    </script>

            <!--#end of JSON data -->

            <!-- Section: Masthead -->
            <section class="cpl-masthead cpl_brand_masthead" :class="masthead.intro ? 'cpl-align-top' : ''" id="">
                <div class="cpl-masthead-sec cpl-el-anim" :class="masthead.content.isLightTheme ? 'cpl-sec-bg-white' : 'cpl-sec-bg-black'">
                <!-- Section: Media -->
                <section class="cpl-masthead-media" :class="{'cpl-masthead-has-video': masthead.media.videoBig}">
                    <!-- Images -->
                    <picture>
                    <source type="image/webp" media="(orientation: landscape)" v-if="masthead.media.imgBig_webp" :data-srcset="masthead.media.imgBig_webp">
                    <source type="image/webp" media="(orientation: portrait)" v-if="masthead.media.imgSml_webp" :data-srcset="masthead.media.imgSml_webp">
                    <source media="(orientation: landscape)" :data-srcset="masthead.media.imgBig">
                    <source media="(orientation: portrait)" :data-srcset="masthead.media.imgSml">
                    <img
                        class="cpl-lazy"
                        data-src="../../electrified/dam/img-placeholder.svg"
                        :alt="masthead.media.imgAlt"
                    >
                    </picture>

                    <!-- Videos -->
                    <div class="cpl-videos-container" v-if="masthead.media.videoBig">
                    <div class="cpl-video-wrapper show_on_sml_up">
                        <video class="cpl-lazy" autoplay="false" preload="metadata" loop muted playsinline>
                        <source type="video/mp4" src="" :data-src="masthead.media.videoBig">
                        </video>
                    </div>
                    <div class="cpl-video-wrapper show_on_sml_down">
                        <video class="cpl-lazy" autoplay="false" preload="metadata" loop muted playsinline>
                        <source type="video/mp4" src="" :data-src="masthead.media.videoSml">
                        </video>
                    </div>
                    </div>
                </section>
                <!--#end of Section: Media -->

                <!-- Section: Content -->
                <section
                    class="cpl-masthead-content"
                    :class="masthead.content.isStretchOut ? 'cpl-cont-stretch' : '' || masthead.content.isAlignToBottom ? 'cpl-cont-bottom' : ''"
                >
                    <div class="cpl-sec-cont">
                    <div class="cpl-grid no-pad">
                        <div class="cpl-grid-inner">
                        <section class="cpl-grid-cell-6">
                            <div v-if="masthead.content.parentHdr">
                            <h1 class="h4" v-html="masthead.content.parentHdr"></h1>
                            <h2>
                                <div v-for="line in masthead.content.header" v-html="line"></div>
                            </h2>
                            </div>
                            <h1 class="h2" v-else>
                            <div v-for="line in masthead.content.header" v-html="line"></div>
                            </h1>
                            <p v-if="masthead.content.tagline" v-html="masthead.content.tagline"></p>
                            <div v-if="!masthead.content.logoOnRight">
                            <div v-if="masthead.content.logoSrc">
                                <img class="cpl-masthead-logo" :src="masthead.content.logoSrc" alt="">
                            </div>
                            </div>
                        </section>

                        <section class="cpl-grid-cell-6" v-if="masthead.content.logoOnRight">
                            <div class="cpl-masthead-logo-wrp" v-if="masthead.content.logoSrc">
                            <img class="cpl-masthead-logo" :src="masthead.content.logoSrc" alt="">
                            </div>
                        </section>
                        </div>
                    </div>
                    </div>
                </section>
                <!--#end of Section: Content -->

                <!-- Section: Intro -->
                <section
                    class="cpl-masthead-intro"
                    v-if="masthead.intro"
                >
                <div class="cpl-sec-wrapper">
                    <div class="cpl-grid">
                    <div class="cpl-grid-inner">
                        <section class="cpl-grid-cell-9">
                        <header>
                            <h2 class="h3 cpl-parallax" data-modifier="5" v-html="masthead.intro.header"></h2>
                            <div class="cpl-parallax" data-modifier="5" v-html="masthead.intro.desc"></div>
                        </header>
                        </section>
                    </div>
                    </div>
                </div>
                </section>
                <!-- #end of Section: Intro -->

                <!-- Scrolling Down -->
                <a class="cpl-cta--scroll cpl-unstyled" href="javascript:;">
                    <svg class="show_on_desktop" version="1.1" width="28.2" height="48" viewBox="0 0 28.2 48" preserveAspectRatio="xMidYMid meet" aria-labelledby="scrollDownBtnTitle" role="img">
                    <title id="scrollDownBtnTitle">Scroll down</title>
                    <g fill-rule="evenodd">
                        <path d="M14.1,48C6.3,48,0,41.6,0,33.7V14.3C0,6.4,6.3,0,14.1,0s14.1,6.4,14.1,14.3v19.4C28.2,41.6,21.9,48,14.1,48z M14.1,1.2 C7,1.2,1.2,7.1,1.2,14.3v19.4c0,7.2,5.8,13.1,12.9,13.1c7.1,0,12.9-5.9,12.9-13.1V14.3C27,7.1,21.2,1.2,14.1,1.2z" fill="currentColor"></path>
                        <circle class="cpl-anim--mouse" cx="14.3" cy="12.3" r="3.2" fill="currentColor"></circle>
                    </g>
                    </svg>
                </a>
                <!--#end of Scrolling Down -->
                </div>
            </section>
            <!--#end of Section: Masthead -->

            <!-- Section: Timeline -->
            <section class="cpl_brand_timeline" :class="'cpl-sec-bg-'+ timeline.bgColor" :id="timeline.id">
                <div class="cpl-sec-wrapper">
                <!-- Section Header -->
                <div class="cpl-grid">
                    <div class="cpl-grid-inner">
                    <section class="cpl-grid-cell-10">
                        <header>
                        <h1 class="h3" v-html="timeline.header"></h1>
                        </header>
                    </section>
                    </div>
                </div>
                <!--#end of Section Header -->
                </div>

                <div class="cpl-sec-wrapper timeline-wrapper">
                <div class="cpl-grid horizontal-scroll-wrapper">
                    <div class="cpl-grid-inner">
                    <section class="cpl-grid-cell-12">
                        <ul class="timeline" id="timeline">
                        <li class="li cpl-sec--anim" v-for="timestamp in timeline.timestamps">
                            <div class="status">
                            <h5 v-html="timestamp.date"></h5>
                            <p v-html="timestamp.desc"></p>
                            </div>
                        </li>
                        </ul>
                    </section>
                    </div>
                </div>
                </div>
            </section>
            <!--#end of Section: Timeline -->

            <!-- Section: REDEFINING WHAT'S POSSIBLE TODAY -->
            <section class="cpl_brand_cards_single__1" :class="'cpl-sec-bg-'+ section.bgColor" :id="section.id"
            :style="{ 'background-image': 'url(' + section.bgBig + ')'}"
            >
                <!-- <div v-if="section.bgBig" class="cpl-brand-bg cpl-el-anim">
                <picture>
                    <source media="(min-width: 600px)" :data-srcset="section.bgBig">
                    <source media="(max-width: 599px)" :data-srcset="section.bgSml">
                    <img
                    class="cpl-lazy"
                    src="electrified/dam/img-placeholder.svg"
                    :data-src="section.bgSml"
                    alt=""
                    >
                </picture>
                </div> -->
                <div class="cpl-sec-wrapper">
                <div class="cpl-grid" v-if="section.header">
                    <div class="cpl-grid-inner">
                    <section class="cpl-grid-cell-9">
                        <header>
                        <h2 class="h3 cpl-parallax" data-modifier="5" v-html="section.header"></h2>
                        <div class="cpl-parallax" data-modifier="5" v-if="section.desc" v-html="section.desc"></div>
                        </header>
                    </section>
                    </div>
                </div>
                <div class="cpl-grid no-pad-top">
                    <div class="cpl-grid-inner">
                    <section class="cpl-grid-cell-12" v-for="card in section.cards">
                        <div class="cpl-card" :class="card.isSideBySide ? 'cpl-card--side' : ''">
                        <div class="cpl-card--img cpl-lightbox--media cpl-parallax" data-modifier="5" v-if="card.videoUrl">
                            <div
                            class="cpl-link-lightbox cpl-i-play"
                            data-lg-size="1280-720"
                            data-poster="../../electrified/dam/spacer.gif"
                            :data-src="card.videoUrl"
                            >
                            <picture class="cpl-ar cpl-ar--16-9">
                                <img
                                class="cpl-lazy"
                                src="../../electrified/dam/img-placeholder.svg"
                                :data-src="card.img"
                                :alt="card.imgAlt"
                                >
                            </picture>
                            </div>
                        </div>
                        <div class="cpl-card--img cpl-parallax" data-modifier="5" v-else>
                            <picture class="cpl-ar cpl-ar--16-9">
                            <img
                                class="cpl-lazy"
                                src="../../electrified/dam/img-placeholder.svg"
                                :data-src="card.img"
                                :alt="card.imgAlt"
                            >
                            </picture>
                        </div>
                        <div class="cpl-card--cont cpl-freehtml">
                            <h5 class="cpl-parallax" data-modifier="5" v-if="card.header" v-html="card.header"></h5>
                            <div class="cpl-parallax" data-modifier="5" v-html="card.desc"></div>
                            <p class="cpl-parallax" data-modifier="5" v-if="card.cta.url">
                            <a class="mdc-button mdc-button--text" :href="card.cta.url">
                                <span class="mdc-button__label" v-html="card.cta.label"></span>
                            </a>
                            </p>
                        </div>
                        </div>
                    </section>
                    </div>
                </div>
                <div class="cpl-sec-cont no-pad-top" v-if="section.ctaBtn.url">
                    <div class="cpl-cta-wrapper cpl-align-center">
                    <a class="mdc-button mdc-button--outlined" :href="section.ctaBtn.url">
                        <span class="mdc-button__ripple"></span>
                        <span class="mdc-button__label" v-html="section.ctaBtn.label"></span>
                    </a>
                    </div>
                </div>
                </div>
            </section>
            <!--#end of Section: REDEFINING WHAT'S POSSIBLE TODAY -->

            <!-- Mobility -->
            <section class="cpl-cards-mobility__1" :class="'cpl-sec-bg-'+ mobility.bgColor">
                <div class="cpl-sec-wrapper">
                <!-- Section Header -->
                <div class="cpl-grid">
                    <div class="cpl-grid-inner">
                    <section class="cpl-grid-cell-10">
                        <header>
                        <h1 class="h3" v-html="mobility.header"></h1>
                        <p v-if="mobility.desc" v-html="mobility.desc"></p>
                        </header>
                    </section>
                    </div>
                </div>
                <!--#end of Section Header -->


                <div class="cpl-grid">
                    <div class="cpl-grid-inner mobility-card">
                    <div class="cpl-grid-cell-6" v-for="card in mobility.cards">
                        <a class="cpl-unstyled cpl-card--block cpl-cta--offcanvas" :href="'#'+card.cta.url">
                        <div class="cpl-card">
                            <div class="cpl-card--img cpl-parallax" data-modifier="5">
                            <picture class="cpl-ar cpl-ar--16-9">
                                <img
                                class="cpl-lazy"
                                src="../../electrified/dam/img-placeholder.svg"
                                :data-src="card.img"
                                :alt="card.imgAlt"
                                >
                            </picture>
                            <h5 v-html="card.header"></h5>
                            </div>
                        </div>
                        </a>
                    </div>


                    <!-- Off-canvas: Mobility -->
                    <section class="cpl-offcanvases mobility-offcanvas">
                        <aside class="cpl-offcanvas" v-for="card in mobility.cards" :id="card.cta.url">
                        <section class="cpl-sec-wrapper">
                            <!-- Off-canvas Header -->
                            <div class="cpl-grid">
                            <div class="cpl-grid-inner">
                                <div class="cpl-grid-cell-8 img-wrapper">
                                <picture class="cpl-ar cpl-ar--16-9 cpl-mobile-only">
                                    <img
                                    class="cpl-lazy"
                                    src="../../electrified/dam/img-placeholder.svg"
                                    :data-src="card.contentImg"
                                    :alt="card.contentImgAlt"
                                    >
                                </picture>
                                </div>
                                <div class="cpl-grid-cell-4">
                                <div class="off-canvas-content">
                                    <h5 v-html="card.contentHeader"></h5>
                                    <div v-html="card.contentDesc"></div>
                                    <p>
                                    <div class="cpl-parallax cpl-freehtml">
                                        <ul class="cpl-ul-red" v-html="card.contentList">
                                        <li>
                                            test
                                        </li>
                                        </ul>
                                    </div>
                                    </p>
                                    <p v-if="card.contentUrl">
                                    <a class="mdc-button mdc-button--text" :href="card.contentUrl">
                                        <span class="mdc-button__label" v-html="card.contentLabel"></span>
                                    </a>
                                    </p>
                                </div>
                                </div>
                            </div>
                            </div>
                            <!--#end of Off-canvas Header -->
                        </section>
                        </aside>
                        <a class="cpl-cta--close cpl-unstyled cpl-cta-offcanvas--close" href="javascript:;">
                        <div class="mdc-ripple-surface"></div>
                        </a>
                        <div class="cpl-offcanvas--backdrop"></div>
                    </section>
                    <!--#end of Off-canvas: Mobility -->
                    </div>
                </div>
                </div>
            </section>
            <!--#end of Mobility -->

            <!-- Section: Explore GR Models -->
            <section class="cpl-explore-models" :id="section.id" :class="'cpl-sec-bg-'+ section.bgColor">
                <div class="cpl-sec-wrapper">
                <div class="cpl-grid" v-if="section.header">
                    <div class="cpl-grid-inner">
                    <section class="cpl-grid-cell-9">
                        <header>
                        <h2 class="h3 cpl-parallax" data-modifier="5" v-html="section.header"></h2>
                        </header>
                    </section>
                    </div>
                </div>
                <div class="cpl-sec-cont">
                    <section class="cpl-model-selector cpl-sec--explore">
                    <!-- Tab -->
                    <div class="mdc-tab-bar cpl-tab--main" role="tablist">
                        <div class="mdc-tab-scroller">
                        <div class="mdc-tab-scroller__scroll-area">
                            <div class="mdc-tab-scroller__scroll-content">
                            <button
                                class="mdc-tab" role="tab" aria-selected="false" tabindex="-1" :data-id="tab.id"
                                v-for="tab in models.tabs.filter(function(x){return x.id=='hev'|| x.id=='bev'})">
                                <span class="mdc-tab__content">
                                <span class="mdc-tab__text-label" v-html="tab.label"></span>
                                </span>
                                <span class="mdc-tab-indicator">
                                <span class="mdc-tab-indicator__content mdc-tab-indicator__content--underline"></span>
                                </span>
                                <span class="mdc-tab__ripple"></span>
                            </button>
                            </div>
                        </div>
                        </div>
                    </div>
                    <!--#end of Tab -->

                    <!-- Tab Panels -->
                    <div class="cpl-tabpanels--main">
                        <!-- Tab Panel -->
                        <div class="cpl-tabpanel--main" v-for="tab in models.tabs.filter(function(x){return x.id=='hev'|| x.id=='bev'})">
                        <!-- Sub-tab -->
                        <div class="mdc-tab-bar cpl-tab--sub" role="tablist">
                            <div class="mdc-tab-scroller">
                            <div class="mdc-tab-scroller__scroll-area">
                                <div class="mdc-tab-scroller__scroll-content">
                                <button class="mdc-tab" role="tab" aria-selected="false" tabindex="-1" :data-id="subTab.id" v-for="subTab in tab.subTabs">
                                    <span class="mdc-tab__content">
                                    <span class="mdc-tab__text-label" v-html="subTab.label"></span>
                                    </span>
                                    <span class="mdc-tab-indicator">
                                    <span class="mdc-tab-indicator__content mdc-tab-indicator__content--underline"></span>
                                    </span>
                                    <span class="mdc-tab__ripple"></span>
                                </button>
                                </div>
                            </div>
                            </div>
                        </div>
                        <!--#end of Sub-tab -->
                        <!-- Sub-tab Panels -->
                        <div class="cpl-tabpanels--sub">
                            <div class="cpl-tabpanel--sub" v-for="subTab in tab.subTabs">
                            <ul class="cpl-tab-item--models">
                                <li v-for="list in subTab.modelLists">
                                <a class="cpl-tab-item--model cpl-unstyled" :href="list.url">
                                    <picture class="cpl-tab-item--thumb">
                                    <img class="cpl-lazy" :src="list.imgLazy" :data-src="list.imgThumb" :alt="list.header">
                                    </picture>
                                    <div class="cpl-tab-item--txt">
                                    <h5 v-if="subTab.id=='all'" v-html="list.header"></h5>
                                    <h6 v-else class="cpl-txt-bold" v-html="list.header"></h6>
                                    <p v-html="list.desc"></p>
                                    </div>
                                </a>
                                </li>
                            </ul>
                            </div>
                        </div>
                        <!--#end of Sub-tab Panels -->
                        </div>
                        <!--end of Tab Panel -->
                    </div>
                    <!--#end of Tab Panels -->

                    <!-- CTA Wrapper -->
                    <!-- <div class="cpl-cta-wrapper cpl-align-center" v-if="models.compareModelsCta.url">
                        <a class="mdc-button mdc-button--outlined" :href="models.compareModelsCta.url">
                        <span class="mdc-button__ripple"></span>
                        <span class="mdc-button__label" v-html="models.compareModelsCta.label"></span>
                        </a>
                    </div> -->
                    <!--#end of CTA Wrapper -->
                    </section>
                </div>
                </div>
            </section>
            <!--#end of Section: Explore GR Models -->

            </article>
            <!--#end of Article -->
        </main>
        </div>
    <script type="text/javascript" src="electrified/assets/js/vendors.js"></script><script type="text/javascript" src="electrified/assets/js/global.js"></script><script type="text/javascript" src="electrified/assets/js/toyota-electrification.js"></script>
    </body>
@endsection
