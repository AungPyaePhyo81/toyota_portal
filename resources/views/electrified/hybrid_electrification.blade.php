@extends('layouts.main')

@section('head')
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="pragma" content="no-cache">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=5.0">
    <meta name="theme-color" media="(prefers-color-scheme: light)" content="#ffffff">
    <meta name="theme-color" media="(prefers-color-scheme: dark)" content="#eb0a1e">
    <meta name="description" content="Discover how we power future mobility by investing in and innovating electrification solutions for you.">
    <title>Hybrid Electric | Toyota Electrification | Toyota Myanmar</title>
    <meta name="theme-color" content="#eb0a1e" /><link rel="manifest" href="electrified/manifest.json" /><link rel="shortcut icon" href="favicon.svg">
    <link href="electrified/assets/css/global.css" rel="stylesheet">
    <link href="electrified/assets/css/toyota-hybrid-electric.css" rel="stylesheet"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/Noto-Sans-Devanagari-Black.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/Noto-Sans-Devanagari-Black.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/Noto-Sans-Devanagari-Bold.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/Noto-Sans-Devanagari-Bold.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/Noto-Sans-Devanagari-ExtraLight.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/Noto-Sans-Devanagari-ExtraLight.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/Noto-Sans-Devanagari-Light.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/Noto-Sans-Devanagari-Light.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/Noto-Sans-Devanagari-Regular.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/Noto-Sans-Devanagari-Regular.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/Noto-Sans-Devanagari-SemiBold.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/Noto-Sans-Devanagari-SemiBold.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansKhmer-Black.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansKhmer-Black.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansKhmer-Bold.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansKhmer-Bold.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansKhmer-ExtraLight.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansKhmer-ExtraLight.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansKhmer-Light.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansKhmer-Light.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansKhmer-Regular.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansKhmer-Regular.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansKhmer-SemiBold.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansKhmer-SemiBold.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansLao-Black.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansLao-Black.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansLao-Bold.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansLao-Bold.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansLao-ExtraLight.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansLao-ExtraLight.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansLao-Light.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansLao-Light.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansLao-Regular.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansLao-Regular.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansLao-SemiBold.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/NotoSansLao-SemiBold.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/ToyotaType-Black.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/ToyotaType-Black.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/ToyotaType-Bold.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/ToyotaType-Bold.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/ToyotaType-Book.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/ToyotaType-Book.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/ToyotaType-Light.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/ToyotaType-Light.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/ToyotaType-Regular.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/ToyotaType-Regular.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/ToyotaType-Semibold.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/ToyotaType-Semibold.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/lg.ttf" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/lg.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/lg.woff2" rel="preload">
@endsection

@section('content')
  <body class="cpl-pg--brand">
    <div id="cpl--wrapper">
      <main class="p-0">
        <!-- Article -->
        <article>
          <!-- JSON data -->
    <script type="text/javascript">
    // Masthead
    window.data_brand_masthead = {
        "media": {
        "imgBig_webp": "",
        "imgSml_webp": "",
        "imgBig": "electrified/dam/hybrid-electric/masthead-og-explore/toyota-hev-masthead-d.jpg",
        "imgSml": "electrified/dam/hybrid-electric/masthead-og-explore/toyota-hev-masthead-m.jpg",
        "videoBig": "electrified/dam/hybrid-electric/masthead-hev.mp4",
        "videoSml": "electrified/dam/hybrid-electric/masthead-hev.mp4",
        "imgAlt": "TOYOTA HYBRID ELECTRIC"
        },
        "content": {
        "header": ["TOYOTA", "HYBRID ELECTRIC"],
        "tagline": "",
        "logoSrc": "",
        "isLightTheme": false,
        "isStretchOut": false,
        "isAlignToBottom": true
        },
        "intro": {
        "header": "",
        "desc": "<p>For nearly a quarter of a century, Toyota has been pioneering hybrid electric technology. Our constant innovation has led to more improvements in later generations of Toyotas, making them more efficient than ever.<br /><br />Designed to transform your driving experience, our cutting-edge hybrid electric technology derives power from two sources to deliver smooth, efficient drives.</p>"
        }
    };

    // REDUCING ENVIRONMENTAL IMPACT
    window.data_brand_cards_multiple__1 = {
        "id": "reducing-environmental-impact",
        "bgColor": "off-white", // off-gold, off-gold-v2, black, white
        "bgBig": "",
        "bgSml": "",
        "header": "REDUCING ENVIRONMENTAL IMPACT",
        "desc": "",
        "cards": [
        {
            "isSideBySide": false,
            "img": "electrified/dam/hybrid-electric/toyota-HEV-reducing-environmental-1200x675.jpg",
            "imgAlt": "",
            "videoUrl": "",
            "header": "",
            "desc": "<p>Most of your urban journeys can be done using just the electric motor, meaning you will emit nothing from the tailpipe during those shorter drives.<br /><br />With lower petrol consumption comes lower exhaust emissions. This is why hybrid electrics are some of the most energy-efficient vehicles on the road. </p><h5>20+ million</h5><p>Toyota hybrid electric vehicles sold worldwide</p>",
            "cta": {
            "label": "",
            "url": ""
            }
        }
        ],
        "ctaBtn": {
        "label": "",
        "url": ""
        }
    };

    // HOW HYBRID WORKS
    window.data_brand_cards_multiple__2 = {
        "id": "how-hybrid-works",
        "bgColor": "white", // off-gold, off-gold-v2, black, white
        "bgBig": "",
        "bgSml": "",
        "header": "HOW HYBRID ELECTRIC WORKS",
        "desc": "",
        "cards": [
        {
            "isSideBySide": true,
            "img": "electrified/dam/hybrid-electric/toyota-hev-how-hev-works.jpg",
            "imgAlt": "",
            "videoUrl": "",
            "header": "",
            "desc": "<p>Hybrid electric vehicles use two separate sources of power – a petrol engine and an electric motor. They are more fuel-efficient than conventional vehicles, yet do not require plugging in to charge as the engine charges the battery automatically. <br /><br />This makes hybrids especially practical as you only need to fill up on petrol to enjoy the benefits of both power sources. The engine and motor in Toyota’s hybrid electrics are also able to run independent of each other – or together – according to conditions. </p>",
            "cta": {
            "label": "",
            "url": ""
            }
        }
        ],
        "ctaBtn": {
        "label": "",
        "url": ""
        }
    };

    // HEV
    window.data_brand_hev = {
        "id": "",
        "layoutClass": "cpl-grid-cell-4",
        "bgColor": "off-white", // white, off-white, grey-mid, grey-dark, black
        "svgCode": "<svg id='Layer_1' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 1736.201 161.533'> <defs> <style>.cls-1{stroke: #231f20;}.cls-1, .cls-2{fill: #fff;}.cls-1, .cls-3, .cls-4{stroke-miterlimit: 10;}.cls-1, .cls-4{stroke-width: 2px;}.cls-3{stroke: #e72128; stroke-width: 4px;}.cls-3, .cls-4{fill: none;}.cls-4{stroke: #010101;}.cpl-animated .car-1{animation-name: move-right; animation-duration: 0.3s; animation-timing-function: ease-in;}.cpl-animated .car-1 .line{animation-name: fade-in; animation-timing-function: ease-in; animation-duration: 0.8s;}.cpl-animated .car-2{animation-name: fade-in, move-right-2; animation-duration: 1.8s; animation-timing-function: ease-in;}.cpl-animated .car-2 .line{animation-name: fade-in; animation-timing-function: ease-in; animation-duration: 2.3s;}.cpl-animated .car-3{animation-name: fade-in, move-upwards; animation-duration: 3.5s; animation-timing-function: ease-in;}.cpl-animated .car-3 .line{animation-name: fade-in; animation-timing-function: ease-in; animation-duration: 4s;}.cpl-animated .car-4{animation-name: fade-in, move-downwards; animation-duration: 4.3s; animation-timing-function: ease-in;}.cpl-animated .car-4 .line{animation-name: fade-in; animation-timing-function: ease-in; animation-duration: 5s;}@keyframes fade-in{0%{opacity: 0;}50%{opacity: 0;}70%{opacity: 0;}100%{opacity: 1;}}@keyframes move-right{0%{opacity: 0; transform: translateX(-20px);}100%{opacity: 1; transform: translateX(0);}}@keyframes move-right-2{0%{opacity: 0; transform: translateX(-50px);}50%{opacity: 0; transform: translateX(-40px);}70%{opacity: 0; transform: translateX(-20px);}100%{opacity: 1; transform: translateX(0);}}@keyframes move-upwards{0%{transform: translate(-40px, 10px);}50%{transform: translate(-20px, 5px);}70%{transform: translate(-10px, 3px);}100%{transform: translate(0);}}@keyframes move-downwards{0%{transform: translate(-50px, -7px);}50%{transform: translate(-25px, -5px);}70%{transform: translate(-15px, -3px);}100%{transform: translate(0);}}</style> </defs> <path class='cls-3' d='M4.424,136.039H696.614c38.793,0,77.254-7.038,113.55-20.732,60.82-22.947,161.842-57.17,231.367-60.582,105.683-5.187,320.836,51.159,399.384,72.936,20.267,5.619,41.026,9.232,61.999,10.793l224.915,16.735'/> <g class='car-1'> <g> <path class='cls-4' d='M103.696,123.037l-28.864-4.81v-14.432c0-4.811,4.811-4.811,4.811-9.621v-9.622h19.242s24.054-19.242,43.298-19.242h24.053c14.432,0,33.675,19.242,33.675,19.242,0,0,24.053,4.811,33.674,9.622,5.993,2.998,9.622,9.621,9.622,9.621v19.243h-24.054'/> <path class='cls-4' d='M190.286,118.227c0,7.97,6.462,14.432,14.432,14.432s14.434-6.462,14.434-14.432-6.462-14.434-14.434-14.434-14.432,6.462-14.432,14.434Z'/> <path class='cls-4' d='M103.696,118.227c0,7.97,6.462,14.432,14.432,14.432s14.434-6.462,14.434-14.432-6.462-14.434-14.434-14.434-14.432,6.462-14.432,14.434Z'/> <line class='cls-4' x1='190.288' y1='123.039' x2='132.56' y2='123.037'/> </g> <line class='cls-4 line' x1='65.398' y1='94.175' x2='42.419' y2='94.175'/> <line class='cls-4 line' x1='65.398' y1='103.794' x2='49.345' y2='103.794'/> </g> <g class='car-2'> <rect class='cls-2' x='522.722' y='77.987' width='20.631' height='18.797'/> <g> <path class='cls-1' d='M444.557,123.037l-28.864-4.81v-14.432c0-4.811,4.811-4.811,4.811-9.621v-9.622h19.242s24.054-19.242,43.298-19.242h24.053c14.432,0,33.675,19.242,33.675,19.242,0,0,24.053,4.811,33.674,9.622,5.993,2.998,9.622,9.621,9.622,9.621v19.243h-24.054'/> <path class='cls-1' d='M531.147,118.227c0,7.97,6.462,14.432,14.432,14.432s14.434-6.462,14.434-14.432-6.462-14.434-14.434-14.434-14.432,6.462-14.432,14.434Z'/> <path class='cls-1' d='M444.557,118.227c0,7.97,6.462,14.432,14.432,14.432s14.434-6.462,14.434-14.432-6.462-14.434-14.434-14.434-14.432,6.462-14.432,14.434Z'/> <line class='cls-1' x1='531.149' y1='123.039' x2='473.421' y2='123.037'/> </g> <line class='cls-1 line' x1='406.259' y1='91.222' x2='358.297' y2='91.222'/> <line class='cls-1 line' x1='406.259' y1='100.525' x2='378.864' y2='100.525'/> <line class='cls-1 line' x1='406.259' y1='109.828' x2='364.93' y2='109.828'/> </g> <g class='car-3'> <g> <path class='cls-1' d='M804.992,101.51l-28.917,4.481-4.524-13.705c-1.508-4.569,3.061-6.077,1.553-10.644l-3.016-9.137,18.272-6.032s16.81-25.812,35.083-31.845l22.84-7.54c13.705-4.524,38.01,7.715,38.01,7.715,0,0,24.349-2.972,34.993-1.419,6.631,.968,12.153,6.119,12.153,6.119l6.032,18.273-22.842,7.541'/> <path class='cls-1' d='M885.71,69.799c2.498,7.568,10.66,11.679,18.229,9.181s11.68-10.661,9.182-18.229c-2.499-7.57-10.661-11.68-18.231-9.181s-11.679,10.66-9.18,18.23Z'/> <path class='cls-1' d='M803.484,96.943c2.498,7.568,10.66,11.679,18.229,9.181s11.68-10.661,9.182-18.229c-2.499-7.57-10.661-11.68-18.231-9.181-7.568,2.498-11.679,10.66-9.18,18.23Z'/> <line class='cls-1' x1='887.22' y1='74.367' x2='832.401' y2='92.462'/> </g> <line class='cls-1 line' x1='758.651' y1='83.305' x2='689.388' y2='106.17'/> <line class='cls-1 line' x1='761.567' y1='92.139' x2='722.006' y2='105.199'/> <line class='cls-1 line' x1='764.484' y1='100.973' x2='704.8' y2='120.676'/> </g> <g class='car-4'> <g> <path class='cls-1' d='M1229.889,62.077l-27.018-11.238,3.277-14.055c1.092-4.685,5.778-3.593,6.87-8.277l2.185-9.371,18.739,4.369s27.795-13.277,46.536-8.908l23.424,5.462c14.055,3.277,28.427,26.386,28.427,26.386,0,0,22.332,10.147,30.609,17.017,5.156,4.281,7.187,11.554,7.187,11.554l-4.37,18.741-23.426-5.462'/> <path class='cls-1' d='M1315.309,77.054c-1.81,7.762,3.016,15.522,10.778,17.332,7.764,1.81,15.524-3.016,17.334-10.778,1.81-7.763-3.016-15.524-10.779-17.334-7.762-1.81-15.522,3.016-17.332,10.779Z'/> <path class='cls-1' d='M1230.981,57.393c-1.81,7.762,3.016,15.522,10.778,17.332s15.524-3.016,17.334-10.778c1.81-7.763-3.016-15.524-10.779-17.334-7.762-1.81-15.522,3.016-17.332,10.779Z'/> <line class='cls-1' x1='1314.218' y1='81.74' x2='1257.999' y2='68.631'/> </g> <line class='cls-1 line' x1='1199.815' y1='22.397' x2='1128.781' y2='5.835'/> <line class='cls-1 line' x1='1197.703' y1='31.457' x2='1157.129' y2='21.997'/> <line class='cls-1 line' x1='1195.59' y1='40.517' x2='1134.38' y2='26.245'/> </g></svg>",
        "imgAlt": "",
        "contents": [
        {
            "hevNumber": "1",
            "hevTitle": "Starting off",
            "hevDesc": "<em>Electric motor only</em><br />The electric motor is used to propel the vehicle at slower speeds."
        },
        {
            "hevNumber": "2",
            "hevTitle": "Regular driving",
            "hevDesc": "<em>Engine + electric motor</em><br />The engine takes over and does most of the work with support from the electric motor."
        },
        {
            "hevNumber": "3",
            "hevTitle": "Full acceleration",
            "hevDesc": "<em>Engine + electric motor</em><br />Both the engine and electric motor are called on to provide maximum power."
        },
        {
            "hevNumber": "4",
            "hevTitle": "Deceleration/ Braking",
            "hevDesc": "When you lift off the accelerator, the engine turns off and regenerative braking begins, both slowing the vehicle and charging the battery."
        }
        ]
    };

    //WHY CHOOSE TOYOTA HYBRIDS?
    window.data_cards_mobility__1 = {
        "id": "",
        "layoutClass": "cpl-grid-cell-4",
        "bgColor": "off-white", // white, off-white, grey-mid, grey-dark, black
        "header": "WHY CHOOSE TOYOTA HYBRID ELECTRIC?",
        "desc": "",
        "cards": [
        {
            "img": "electrified/dam/hybrid-electric/toyota-HEV-why-batteries-1200x675.jpg",
            "imgAlt": "",
            "header": "Batteries that last",
            "desc": "Our hybrid batteries are designed to be safe and long-lasting, built with the same dedication to quality as you have come to expect from a Toyota. <br /><br />All hybrid electric components, including the battery, also come with a warranty to give you greater peace of mind.",
            "cta": {
            "url": "batteries-that-last"
            }
        },
        {
            "img": "electrified/dam/hybrid-electric/toyota-HEV-why-fun-1200x675.jpg",
            "imgAlt": "",
            "header": "Fun to drive",
            "desc": "Hybrids are surprisingly responsive and spirited due to the instant availability of torque from the electric motor and the higher speeds the petrol engine can deliver. <br /><br />These dual power sources perfectly complement each other to optimise power and performance at any speed. ",
            "cta": {
            "url": "fun-to-drive"
            }
        },
        {
            "img": "electrified/dam/hybrid-electric/toyota-hev-why-choose-fuel-economy.jpg",
            "imgAlt": "",
            "header": "Better fuel economy",
            "desc": "With two power sources, hybrids can drive purely on electricity at lower speeds and when the engine is idling or starting up. With the engine operating only at higher speeds, overall fuel efficiency is also improved. <br /><br />These come together to create better fuel economy without compromising performance.",
            "cta": {
            "url": "better-fuel-economy"
            }
        },
        {
            "img": "electrified/dam/hybrid-electric/toyota-hev-why-choose-low-maintenance.jpg",
            "imgAlt": "",
            "header": "Low maintenance costs",
            "desc": "Overall, the cost of ownership of a hybrid electric is surprisingly low, both in terms of fuelling and maintenance.<br /><br />Maintenance costs for hybrids are lower thanks to the removal of many parts and the higher reliability of electric and battery components. For example, our regenerative braking means there is less wear on brake pads.",
            "cta": {
            "url": "low-maintenance-costs"
            }
        }
        ]
    };

    // EXPLORE OUR ROAD CARS
    window.data_explore_models = {
        "id": "explore-our-road-cars",
        "bgColor": "white", // white, off-white, grey-mid, grey-dark, black
        "header": "EXPLORE OUR VEHICLES"
    };

    // Generic Cards Layout - Related Pages
    window.data_cards_related__1 = {
        "id": "",
        "layoutClass": "cpl-grid-cell-4",
        "bgColor": "off-white", // white, off-white, grey-mid, grey-dark, black
        "header": "DISCOVER MORE TOYOTA TECHNOLOGY",
        "cards": [
        {
            "img": "electrified/dam/hybrid-electric/toyota-xev-explore.jpg",
            "imgAlt": "",
            "header": "TOYOTA ELECTRIFICATION ",
            "desc": "<p>We are committed to developing an electrified lineup that is both exciting to drive and meets your diverse lifestyle needs.</p>",
            "cta": {
            "url": "{{route('electrification')}}",
            }
        },
        //   {
        //     "img": "electrified/dam/hybrid-electric/toyota-bev-explore.jpg",
        //     "imgAlt": "",
        //     "header": "TOYOTA BATTERY ELECTRIC",
        //     "desc": "<p>Driven purely by electric power, our battery electric vehicles deliver an exhilarating driving experience with zero emissions.</p>",
        //     "cta": {
        //       "url": "#",
        //     }
        //   },
        {
            "img": "electrified/dam/hybrid-electric/xev-explore-tnga.jpg",
            "imgAlt": "",
            "header": "TOYOTA NEW GLOBAL ARCHITECTURE",
            "desc": "<p>Our new car-making philosophy is paving the way for us to create Toyotas that have even better driving dynamics and appeal.</p>",
            "cta": {
            "url": "{{route('discover.global-architecture')}}",
            }
        }
        ]
    };

    window.data_global_nav_models = {
        "label": "Models",
        "viewModelsLabel": "View grades",
        "currentDateTimeAPI": "https://toyota-aem.convertium.net:4502/bin/services/v1/getCurrentTime",
        "tabs": [
        {
            "id": "hev",
            "label": "HYBRID ELECTRIC",
            "subTabs": [
            {
                "id": "all",
                "label": "",
                "modelLists": [
                {
                    "id": "corolla-cross-hybrid-electric",
                    "header": "COROLLA CROSS HYBRID ELECTRIC",
                    "desc": "Cross-over",
                    "priceLabel": "From $XX,XXX",
                    "promoLabel": "",
                    "imgLazy": "electrified/dam/cars/lazy-angle-left.png",
                    "imgThumb": "electrified/dam/cars/toyota-xev-corolla-cross-hev.png",
                    "url": "#",
                    "selectGrades": []
                },
                {
                    "id": "camry-hybrid-electric",
                    "header": "CAMRY HYBRID ELECTRIC",
                    "desc": "Sedan",
                    "priceLabel": "From $XX,XXX",
                    "promoLabel": "",
                    "imgLazy": "electrified/dam/cars/lazy-angle-left.png",
                    "imgThumb": "electrified/dam/cars/toyota-xev-camry-hybrid.png",
                    "url": "#",
                    "selectGrades": []
                },
                {
                    "id": "rav4-hybrid-electric",
                    "header": "RAV4 HYBRID ELECTRIC",
                    "desc": "SUV",
                    "priceLabel": "From $XX,XXX",
                    "promoLabel": "",
                    "imgLazy": "electrified/dam/cars/lazy-angle-left.png",
                    "imgThumb": "electrified/dam/cars/toyota-xev-rav4-hybrid.png",
                    "url": "#",
                    "selectGrades": []
                }
                ]
            }
            ]
        },
        {
            "id": "bev",
            "label": "BATTERY ELECTRIC",
            "subTabs": [
            {
                "id": "all",
                "label": "",
                "modelLists": [
                {
                    "id": "corolla-cross-hybrid-electric",
                    "header": "BZ4X",
                    "desc": "All-Electric",
                    "priceLabel": "From $XX,XXX",
                    "promoLabel": "",
                    "imgLazy": "electrified/dam/cars/lazy-angle-left.png",
                    "imgThumb": "electrified/dam/cars/toyota-xev-bz4x.png",
                    "url": "#",
                    "selectGrades": []
                }
                ]
            }
            ]
        }
        ]
    };

    </script>

            <!--#end of JSON data -->

            <!-- Section: Masthead -->
            <section class="cpl-masthead cpl_brand_masthead" :class="masthead.intro ? 'cpl-align-top' : ''" id="">
                <div class="cpl-masthead-sec cpl-el-anim" :class="masthead.content.isLightTheme ? 'cpl-sec-bg-white' : 'cpl-sec-bg-black'">
                <!-- Section: Media -->
                <section class="cpl-masthead-media" :class="{'cpl-masthead-has-video': masthead.media.videoBig}">
                    <!-- Images -->
                    <picture>
                    <source type="image/webp" media="(orientation: landscape)" v-if="masthead.media.imgBig_webp" :data-srcset="masthead.media.imgBig_webp">
                    <source type="image/webp" media="(orientation: portrait)" v-if="masthead.media.imgSml_webp" :data-srcset="masthead.media.imgSml_webp">
                    <source media="(orientation: landscape)" :data-srcset="masthead.media.imgBig">
                    <source media="(orientation: portrait)" :data-srcset="masthead.media.imgSml">
                    <img
                        class="cpl-lazy"
                        data-src="../../electrified/dam/img-placeholder.svg"
                        :alt="masthead.media.imgAlt"
                    >
                    </picture>

                    <!-- Videos -->
                    <div class="cpl-videos-container" v-if="masthead.media.videoBig">
                    <div class="cpl-video-wrapper show_on_sml_up">
                        <video class="cpl-lazy" autoplay="false" preload="metadata" loop muted playsinline>
                        <source type="video/mp4" src="" :data-src="masthead.media.videoBig">
                        </video>
                    </div>
                    <div class="cpl-video-wrapper show_on_sml_down">
                        <video class="cpl-lazy" autoplay="false" preload="metadata" loop muted playsinline>
                        <source type="video/mp4" src="" :data-src="masthead.media.videoSml">
                        </video>
                    </div>
                    </div>
                </section>
                <!--#end of Section: Media -->

                <!-- Section: Content -->
                <section
                    class="cpl-masthead-content"
                    :class="masthead.content.isStretchOut ? 'cpl-cont-stretch' : '' || masthead.content.isAlignToBottom ? 'cpl-cont-bottom' : ''"
                >
                    <div class="cpl-sec-cont">
                    <div class="cpl-grid no-pad">
                        <div class="cpl-grid-inner">
                        <section class="cpl-grid-cell-6">
                            <div v-if="masthead.content.parentHdr">
                            <h1 class="h4" v-html="masthead.content.parentHdr"></h1>
                            <h2>
                                <div v-for="line in masthead.content.header" v-html="line"></div>
                            </h2>
                            </div>
                            <h1 class="h2" v-else>
                            <div v-for="line in masthead.content.header" v-html="line"></div>
                            </h1>
                            <p v-if="masthead.content.tagline" v-html="masthead.content.tagline"></p>
                            <div v-if="!masthead.content.logoOnRight">
                            <div v-if="masthead.content.logoSrc">
                                <img class="cpl-masthead-logo" :src="masthead.content.logoSrc" alt="">
                            </div>
                            </div>
                        </section>

                        <section class="cpl-grid-cell-6" v-if="masthead.content.logoOnRight">
                            <div class="cpl-masthead-logo-wrp" v-if="masthead.content.logoSrc">
                            <img class="cpl-masthead-logo" :src="masthead.content.logoSrc" alt="">
                            </div>
                        </section>
                        </div>
                    </div>
                    </div>
                </section>
                <!--#end of Section: Content -->

                <!-- Section: Intro -->
                <section
                    class="cpl-masthead-intro"
                    v-if="masthead.intro"
                >
                <div class="cpl-sec-wrapper">
                    <div class="cpl-grid">
                    <div class="cpl-grid-inner">
                        <section class="cpl-grid-cell-9">
                        <header>
                            <h2 class="h3 cpl-parallax" data-modifier="5" v-html="masthead.intro.header"></h2>
                            <div class="cpl-parallax" data-modifier="5" v-html="masthead.intro.desc"></div>
                        </header>
                        </section>
                    </div>
                    </div>
                </div>
                </section>
                <!-- #end of Section: Intro -->

                <!-- Scrolling Down -->
                <a class="cpl-cta--scroll cpl-unstyled" href="javascript:;">
                    <svg class="show_on_desktop" version="1.1" width="28.2" height="48" viewBox="0 0 28.2 48" preserveAspectRatio="xMidYMid meet" aria-labelledby="scrollDownBtnTitle" role="img">
                    <title id="scrollDownBtnTitle">Scroll down</title>
                    <g fill-rule="evenodd">
                        <path d="M14.1,48C6.3,48,0,41.6,0,33.7V14.3C0,6.4,6.3,0,14.1,0s14.1,6.4,14.1,14.3v19.4C28.2,41.6,21.9,48,14.1,48z M14.1,1.2 C7,1.2,1.2,7.1,1.2,14.3v19.4c0,7.2,5.8,13.1,12.9,13.1c7.1,0,12.9-5.9,12.9-13.1V14.3C27,7.1,21.2,1.2,14.1,1.2z" fill="currentColor"></path>
                        <circle class="cpl-anim--mouse" cx="14.3" cy="12.3" r="3.2" fill="currentColor"></circle>
                    </g>
                    </svg>
                </a>
                <!--#end of Scrolling Down -->
                </div>
            </section>
            <!--#end of Section: Masthead -->

            <!-- Section: REDUCING ENVIRONMENTAL IMPACT -->
            <section class="cpl_brand_cards_multiple__1" :class="'cpl-sec-bg-'+ section.bgColor" :id="section.id">
                <div v-if="section.bgBig" class="cpl-brand-bg cpl-el-anim">
                <picture>
                    <source media="(min-width: 600px)" :data-srcset="section.bgBig">
                    <source media="(max-width: 599px)" :data-srcset="section.bgSml">
                    <img
                    class="cpl-lazy"
                    src="electrified/dam/img-placeholder.svg"
                    :data-src="section.bgSml"
                    alt=""
                    >
                </picture>
                </div>
                <div class="cpl-sec-wrapper">
                <div class="cpl-grid" v-if="section.header">
                    <div class="cpl-grid-inner">
                    <section class="cpl-grid-cell-9">
                        <header>
                        <h2 class="h3 cpl-parallax" data-modifier="5" v-html="section.header"></h2>
                        <div class="cpl-parallax" data-modifier="5" v-if="section.desc" v-html="section.desc"></div>
                        </header>
                    </section>
                    </div>
                </div>
                <div class="cpl-grid no-pad-top">
                    <div class="cpl-grid-inner" v-for="card in section.cards">
                    <div class="cpl-grid-cell-8">
                        <div class="cpl-card" :class="card.isSideBySide ? 'cpl-card--side' : ''">
                        <div class="cpl-card--img cpl-lightbox--media cpl-parallax" data-modifier="5" v-if="card.videoUrl">
                            <div
                            class="cpl-link-lightbox cpl-i-play"
                            data-lg-size="1280-720"
                            data-poster="../../electrified/dam/spacer.gif"
                            :data-src="card.videoUrl"
                            >
                            <picture class="cpl-ar cpl-ar--16-9">
                                <img
                                class="cpl-lazy"
                                src="../../electrified/dam/img-placeholder.svg"
                                :data-src="card.img"
                                :alt="card.imgAlt"
                                >
                            </picture>
                            </div>
                        </div>
                        <div class="cpl-card--img cpl-parallax" data-modifier="5" v-else>
                            <picture class="cpl-ar cpl-ar--16-9">
                            <img
                                class="cpl-lazy"
                                src="../../electrified/dam/img-placeholder.svg"
                                :data-src="card.img"
                                :alt="card.imgAlt"
                            >
                            </picture>
                        </div>
                        </div>
                    </div>
                    <div class="cpl-grid-cell-4">
                        <div class="cpl-card--cont cpl-freehtml">
                        <h5 class="cpl-parallax" data-modifier="5" v-if="card.header" v-html="card.header"></h5>
                        <div class="cpl-parallax" data-modifier="5" v-html="card.desc"></div>
                        <p class="cpl-parallax" data-modifier="5" v-if="card.cta.url">
                            <a class="mdc-button mdc-button--text" :href="card.cta.url">
                            <span class="mdc-button__label" v-html="card.cta.label"></span>
                            </a>
                        </p>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="cpl-sec-cont no-pad-top" v-if="section.ctaBtn.url">
                    <div class="cpl-cta-wrapper cpl-align-center">
                    <a class="mdc-button mdc-button--outlined" :href="section.ctaBtn.url">
                        <span class="mdc-button__ripple"></span>
                        <span class="mdc-button__label" v-html="section.ctaBtn.label"></span>
                    </a>
                    </div>
                </div>
                </div>
            </section>
            <!--#end of Section: REDUCING ENVIRONMENTAL IMPACT -->

            <!-- Section: HOW HYBRID WORKS -->
            <section class="cpl_brand_cards_multiple__2" :class="'cpl-sec-bg-'+ section.bgColor" :id="section.id">
                <div v-if="section.bgBig" class="cpl-brand-bg cpl-el-anim">
                <picture>
                    <source media="(min-width: 600px)" :data-srcset="section.bgBig">
                    <source media="(max-width: 599px)" :data-srcset="section.bgSml">
                    <img
                    class="cpl-lazy"
                    src="electrified/dam/img-placeholder.svg"
                    :data-src="section.bgSml"
                    alt=""
                    >
                </picture>
                </div>
                <div class="cpl-sec-wrapper">
                <div class="cpl-grid" v-if="section.header">
                    <div class="cpl-grid-inner">
                    <section class="cpl-grid-cell-9">
                        <header>
                        <h2 class="h3 cpl-parallax" data-modifier="5" v-html="section.header"></h2>
                        <div class="cpl-parallax" data-modifier="5" v-if="section.desc" v-html="section.desc"></div>
                        </header>
                    </section>
                    </div>
                </div>
                <div class="cpl-grid no-pad-top">
                    <div class="cpl-grid-inner">
                    <section class="cpl-grid-cell-12" v-for="card in section.cards">
                        <div class="cpl-card" :class="card.isSideBySide ? 'cpl-card--side' : ''">
                        <div class="cpl-card--img cpl-lightbox--media cpl-parallax" data-modifier="5" v-if="card.videoUrl">
                            <div
                            class="cpl-link-lightbox cpl-i-play"
                            data-lg-size="1280-720"
                            data-poster="../../electrified/dam/spacer.gif"
                            :data-src="card.videoUrl"
                            >
                            <picture class="cpl-ar cpl-ar--16-9">
                                <img
                                class="cpl-lazy"
                                src="../../electrified/dam/img-placeholder.svg"
                                :data-src="card.img"
                                :alt="card.imgAlt"
                                >
                            </picture>
                            </div>
                        </div>
                        <div class="cpl-card--img cpl-parallax" data-modifier="5" v-else>
                            <picture class="cpl-ar cpl-ar--16-9">
                            <img
                                class="cpl-lazy"
                                src="../../electrified/dam/img-placeholder.svg"
                                :data-src="card.img"
                                :alt="card.imgAlt"
                            >
                            </picture>
                        </div>
                        <div class="cpl-card--cont cpl-freehtml">
                            <h5 class="cpl-parallax" data-modifier="5" v-if="card.header" v-html="card.header"></h5>
                            <div class="cpl-parallax" data-modifier="5" v-html="card.desc"></div>
                            <p class="cpl-parallax" data-modifier="5" v-if="card.cta.url">
                            <a class="mdc-button mdc-button--text" :href="card.cta.url">
                                <span class="mdc-button__label" v-html="card.cta.label"></span>
                            </a>
                            </p>
                        </div>
                        </div>
                    </section>
                    </div>
                </div>
                <div class="cpl-sec-cont no-pad-top" v-if="section.ctaBtn.url">
                    <div class="cpl-cta-wrapper cpl-align-center">
                    <a class="mdc-button mdc-button--outlined" :href="section.ctaBtn.url">
                        <span class="mdc-button__ripple"></span>
                        <span class="mdc-button__label" v-html="section.ctaBtn.label"></span>
                    </a>
                    </div>
                </div>
                </div>
            </section>
            <!--#end of Section: HOW HYBRID WORKS -->

            <!-- HEV -->
            <section class="cpl-brand-hev cpl-sec-bg-white">
                <div class="cpl-sec-wrapper horizontal-scroll-wrapper">
                <div class="cpl-grid">
                    <div class="cpl-grid-inner cpl-sec--anim cpl-hev-container">
                    <div class="cpl-grid-cell-12">
                        <div v-html="hev.svgCode"></div>
                    </div>
                    <div class="cpl-grid-cell-12">
                        <div class="hev-content cpl-grid-cell-4" v-for="content in hev.contents">
                        <div class="hev-number" v-html="content.hevNumber"></div>
                        <h5 class="hev-title" v-html="content.hevTitle"></h5>
                        <div class="hev-desc" v-html="content.hevDesc"></div>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
            </section>
            <!--#end of HEV -->

            <!-- WHY CHOOSE TOYOTA HYBRIDS? -->
            <section class="cpl-cards-mobility__1" :class="'cpl-sec-bg-'+ mobility.bgColor" >
                <div class="cpl-sec-wrapper">


                <!-- Section Header -->
                <div class="cpl-grid">
                    <div class="cpl-grid-inner">
                    <section class="cpl-grid-cell-10">
                        <header>
                        <h1 class="h3" v-html="mobility.header"></h1>
                        <p v-if="mobility.desc" v-html="mobility.desc"></p>
                        </header>
                    </section>
                    </div>
                </div>
                <!--#end of Section Header -->


                <div class="cpl-grid">
                    <div class="cpl-grid-inner mobility-card">
                    <div class="cpl-grid-cell-6" v-for="card in mobility.cards">
                        <div class="cpl-card">
                        <div class="cpl-card--img cpl-parallax" data-modifier="5">
                            <picture class="cpl-ar cpl-ar--16-9">
                            <img
                                class="cpl-lazy"
                                src="../../electrified/dam/img-placeholder.svg"
                                :data-src="card.img"
                                :alt="card.imgAlt"
                            >
                            </picture>
                            <div class="cpl-card--cont">
                            <h5 class="cpl-parallax" data-modifier="5" v-html="card.header"></h5>
                            <p class="cpl-parallax" data-modifier="5" v-html="card.desc"></p>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>

                </div>
            </section>
            <!--#end of WHY CHOOSE TOYOTA HYBRIDS? -->

            <!-- Section: Explore GR Models -->
            <section class="cpl-explore-models" :id="section.id" :class="'cpl-sec-bg-'+ section.bgColor">
                <div class="cpl-sec-wrapper">
                <div class="cpl-grid" v-if="section.header">
                    <div class="cpl-grid-inner">
                    <section class="cpl-grid-cell-9">
                        <header>
                        <h2 class="h3 cpl-parallax" data-modifier="5" v-html="section.header"></h2>
                        </header>
                    </section>
                    </div>
                </div>
                <div class="cpl-sec-cont">
                    <section class="cpl-model-selector cpl-sec--explore">
                    <!-- Tab -->
                    <div class="mdc-tab-bar cpl-tab--main" role="tablist">
                        <div class="mdc-tab-scroller">
                        <div class="mdc-tab-scroller__scroll-area">
                            <div class="mdc-tab-scroller__scroll-content">
                            <button
                                class="mdc-tab" role="tab" aria-selected="false" tabindex="-1" :data-id="tab.id"
                                v-for="tab in models.tabs.filter(function(x){return x.id=='hev'|| x.id=='bev'})"
                            >
                                <span class="mdc-tab__content">
                                <span class="mdc-tab__text-label" v-html="tab.label"></span>
                                </span>
                                <span class="mdc-tab-indicator">
                                <span class="mdc-tab-indicator__content mdc-tab-indicator__content--underline"></span>
                                </span>
                                <span class="mdc-tab__ripple"></span>
                            </button>
                            </div>
                        </div>
                        </div>
                    </div>
                    <!--#end of Tab -->

                    <!-- Tab Panels -->
                    <div class="cpl-tabpanels--main">
                        <!-- Tab Panel -->
                        <div class="cpl-tabpanel--main" v-for="tab in models.tabs.filter(function(x){return x.id=='hev'|| x.id=='bev'})">
                        <!-- Sub-tab -->
                        <div class="mdc-tab-bar cpl-tab--sub" role="tablist">
                            <div class="mdc-tab-scroller">
                            <div class="mdc-tab-scroller__scroll-area">
                                <div class="mdc-tab-scroller__scroll-content">
                                <button class="mdc-tab" role="tab" aria-selected="false" tabindex="-1" :data-id="subTab.id" v-for="subTab in tab.subTabs">
                                    <span class="mdc-tab__content">
                                    <span class="mdc-tab__text-label" v-html="subTab.label"></span>
                                    </span>
                                    <span class="mdc-tab-indicator">
                                    <span class="mdc-tab-indicator__content mdc-tab-indicator__content--underline"></span>
                                    </span>
                                    <span class="mdc-tab__ripple"></span>
                                </button>
                                </div>
                            </div>
                            </div>
                        </div>
                        <!--#end of Sub-tab -->
                        <!-- Sub-tab Panels -->
                        <div class="cpl-tabpanels--sub">
                            <div class="cpl-tabpanel--sub" v-for="subTab in tab.subTabs">
                            <ul class="cpl-tab-item--models">
                                <li v-for="list in subTab.modelLists">
                                <a class="cpl-tab-item--model cpl-unstyled" :href="list.url">
                                    <picture class="cpl-tab-item--thumb">
                                    <img class="cpl-lazy" :src="list.imgLazy" :data-src="list.imgThumb" :alt="list.header">
                                    </picture>
                                    <div class="cpl-tab-item--txt">
                                    <h5 v-if="subTab.id=='all'" v-html="list.header"></h5>
                                    <h6 v-else class="cpl-txt-bold" v-html="list.header"></h6>
                                    <p v-html="list.desc"></p>
                                    </div>
                                </a>
                                </li>
                            </ul>
                            </div>
                        </div>
                        <!--#end of Sub-tab Panels -->
                        </div>
                        <!--end of Tab Panel -->
                    </div>
                    <!--#end of Tab Panels -->

                    <!-- CTA Wrapper -->
                    <!-- <div class="cpl-cta-wrapper cpl-align-center" v-if="models.compareModelsCta.url">
                        <a class="mdc-button mdc-button--outlined" :href="models.compareModelsCta.url">
                        <span class="mdc-button__ripple"></span>
                        <span class="mdc-button__label" v-html="models.compareModelsCta.label"></span>
                        </a>
                    </div> -->
                    <!--#end of CTA Wrapper -->
                    </section>
                </div>
                </div>
            </section>
            <!--#end of Section: Explore GR Models -->

            <!-- Section: Generic Cards Layout - Related Pages -->
            <section class="cpl-cards-related__1" :class="'cpl-sec-bg-'+ section.bgColor" :id="section.id">
                <div class="cpl-sec-wrapper">
                <div class="cpl-grid">
                    <div class="cpl-grid-inner">
                    <section class="cpl-grid-cell-9">
                        <header>
                        <h2 class="h3 cpl-parallax" data-modifier="5" v-html="section.header"></h2>
                        </header>
                    </section>
                    </div>
                </div>
                <div class="cpl-grid no-pad-top">
                    <div class="cpl-grid-inner">
                    <section :class="section.layoutClass" v-for="card in section.cards">
                        <a class="cpl-unstyled cpl-card--block" :href="card.cta.url">
                        <div class="cpl-card">
                            <div class="cpl-card--img cpl-parallax" data-modifier="5">
                            <picture class="cpl-ar cpl-ar--16-9">
                                <img
                                class="cpl-lazy"
                                src="../../electrified/dam/img-placeholder.svg"
                                :data-src="card.img"
                                :alt="card.imgAlt"
                                >
                            </picture>
                            </div>
                            <div class="cpl-card--cont cpl-freehtml">
                            <h5 class="cpl-parallax" data-modifier="5">
                                <span v-html="card.header"></span>
                            </h5>
                            <div class="cpl-parallax" data-modifier="5" v-html="card.desc"></div>
                            </div>
                        </div>
                        </a>
                    </section>
                    </div>
                </div>
                </div>
            </section>
            <!--#end of Section: Generic Cards Layout - Related Pages -->

            </article>
            <!--#end of Article -->
        </main>
        </div>
    <script type="text/javascript" src="electrified/assets/js/vendors.js"></script><script type="text/javascript" src="electrified/assets/js/global.js"></script><script type="text/javascript" src="electrified/assets/js/toyota-hybrid-electric.js"></script>
@endsection
