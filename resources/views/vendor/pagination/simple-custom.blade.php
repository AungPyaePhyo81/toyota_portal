@if ($paginator->hasPages())
    <nav class="container">
        <ul class="pagination d-flex justify-content-between h5">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                {{-- <li class="disabled" aria-disabled="true"><span>@lang('pagination.previous')</span></li> --}}
                <li class="disabled" aria-disabled="true"></li>
            @else
                <li><a href="{{ $paginator->previousPageUrl() }}" class="pagination-danger" rel="prev">@lang('pagination.previous')</a></li>
            @endif

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li><a href="{{ $paginator->nextPageUrl() }}" class="pagination-danger" rel="next">@lang('pagination.next')</a></li>
            @else
                {{-- <li class="disabled" aria-disabled="true"><span>@lang('pagination.next')</span></li> --}}
                <li class="disabled" aria-disabled="true"></li>
            @endif
        </ul>
    </nav>
@endif
