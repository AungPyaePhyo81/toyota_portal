<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
@php
    function collapseItems($children, $active) {
        return in_array(request()->segment(2), $children) ? $active : '';
    }
    // child routes of dealer and car collapse
    $dealers = ['province', 'city', 'map', 'dealer', 'typeofservice', 'info'];
    $cars = ['category', 'subcategory', 'sub-info', 'carmodel', 'color', 'carimage', 'specification', 'avaliablecar','accessory'];
@endphp

<ul class="nav nav-pills">
    <li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}">
        <i class="la la-home nav-icon"></i>{{ trans('backpack::base.dashboard') }}</a>
    </li>

    <li class="nav-item"><a class="nav-link bold" href="{{ backpack_url('elfinder') }}">
        <i class="las la-folder-plus nav-icon"></i>{{ trans('backpack::crud.file_manager') }}</a>
    </li>

    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('blog') }}'>
        <i class='la la-book-open nav-icon'></i>Blogs</a>
    </li>

    <li class="nav-item collapsed" data-toggle="collapse" data-target="#dealer">
        <a href="#" class="nav-link {{collapseItems($dealers, 'active')}} d-flex justify-content-between">
            <b><i class="nav-icon la la-store-alt"></i> DEALER</b> <i class="las la-angle-down"></i>
        </a>

        <ul id="dealer" class="list-unstyled collapse {{collapseItems($dealers, 'show')}}">
            <li class="nav-item"><a class='nav-link' href='{{ backpack_url('province') }}'>
                <i class='nav-icon la la-road'></i> Provinces</a>
            </li>

            <li class="nav-item"><a class='nav-link' href='{{ backpack_url('city') }}'>
                <i class='nav-icon la la-city'></i> Cities</a>
            </li>

            <li class="nav-item"><a class='nav-link' href='{{ backpack_url('map') }}'>
                <i class="las la-street-view nav-icon"></i>Maps</a>
            </li>

            <li class="nav-item"><a class='nav-link' href='{{ backpack_url('dealer') }}'>
                <i class="las la-store nav-icon"></i>Dealers</a>
            </li>

            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('typeofservice') }}'>
                <i class='nav-icon la la-star'></i> Type Of Services</a>
            </li>

            <li class="nav-item"><a class='nav-link' href='{{ backpack_url('info') }}'>
                <i class='nav-icon la la-info-circle'></i> Dealers Infos</a>
            </li>
        </ul>
    </li>

    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('promotion') }}'>
        <i class="las la-percent nav-icon"></i>Promotions</a>
    </li>

    <li class="nav-item collapsed" data-toggle="collapse" data-target="#car">
        <a href="#" class="nav-link {{collapseItems($cars, 'active')}}  d-flex justify-content-between">
            <b><i class="nav-icon la la-car-alt"></i> CAR</b> <i class="las la-angle-down"></i>
        </a>

        <ul id="car" class="list-unstyled collapse {{collapseItems($cars, 'show')}}">
            <li class='nav-item'>
                <a class='nav-link' href='{{ backpack_url('category') }}'>
                    <i class="las la-tag nav-icon"></i>Categories
                </a>
            </li>

            <li class='nav-item'>
                <a class='nav-link' href='{{ backpack_url('subcategory') }}'>
                    <i class="las la-tags nav-icon"></i>Sub Categories
                </a>
            </li>

            <li class='nav-item'>
                <a class='nav-link' href='{{ backpack_url('sub-info') }}'>
                    <i class='la la-info-circle nav-icon'></i> Sub Category Info
                </a>
            </li>

            <li class='nav-item'>
                <a class='nav-link' href='{{ backpack_url('carmodel') }}'>
                    <i class="las la-car nav-icon"></i>Car Models
                </a>
            </li>

            <li class='nav-item'>
                <a class='nav-link' href='{{ backpack_url('color') }}'>
                    <i class='la la-palette nav-icon'></i>Colors
                </a>
            </li>

            {{-- <li class='nav-item'>
                <a class='nav-link' href='{{ backpack_url('carimage') }}'>
                    <i class='la la-image nav-icon'></i>Car Images
                </a>
            </li> --}}

            <li class='nav-item'>
                <a class='nav-link' href='{{ backpack_url('specification') }}'>
                    <i class="las la-car-side nav-icon"></i>Specifications
                </a>
            </li>

            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('avaliablecar') }}'>
                <i class='nav-icon la la-truck-pickup'></i> Availavable Cars</a>
            </li>

            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('accessory') }}'>
                <i class="nav-icon las la-cog"></i> Accessories</a>
            </li>
        </ul>
    </li>

</ul>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('enquiry') }}'>
    <i class='nav-icon la la-mail-bulk'></i> Enquiry (Spare Parts)</a>
</li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('stayinformed') }}'>
    <i class='nav-icon la la-envelope-square'></i> Stay Informed</a>
</li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('servicebooking') }}'><i class='nav-icon la la-book-medical'>
    </i> Service Bookings</a>
</li>

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('onlineinquiry') }}'>
    <i class='nav-icon la la-envelope-open-text'></i> Online Inquiries</a>
</li>
