@extends('layouts.main')

@section('content')
<main class="container-fluid px-sm-5 px-3 pb-4">
    @include('components.breadcrumb',
    ['data' => [['route' => 'discover.dream-car',
                'name' => 'Toyota Dream Car Art Contest']]])
    <header class="py-4">
        <h2 class="fw-bold" data-aos="zoom-in" data-aos-duration="1000">2016 Toyota Dream Car Art Contest</h2>
    </header>

<div class="row my-5" data-aos="fade-up" data-aos-duration="1000">
    <div class="col-6 col-sm-2 p-1">
      <img src="{{asset('img/temp/2016dreamcart/dreamcart1.jpg')}}" alt="" width="100%">
    </div>
    <div class="col-6 col-sm-2 p-1">
      <img src="{{asset('img/temp/2016dreamcart/dreamcart2.jpg')}}" alt="" width="100%">
    </div>
    <div class="col-6 col-sm-2 p-1">
      <img src="{{asset('img/temp/2016dreamcart/dreamcart3.jpg')}}" alt="" width="100%">
    </div>
    <div class="col-6 col-sm-2 p-1">
      <img src="{{asset('img/temp/2016dreamcart/dreamcart4.jpg')}}" alt="" width="100%">
    </div>
    <div class="col-6 col-sm-2 p-1">
      <img src="{{asset('img/temp/2016dreamcart/dreamcart5.jpg')}}" alt="" width="100%">
    </div>
    <div class="col-6 col-sm-2 p-1">
      <img src="{{asset('img/temp/2016dreamcart/dreamcart6.jpg')}}" alt="" width="100%">
    </div>
    <div class="col-6 col-sm-2 p-1">
      <img src="{{asset('img/temp/2016dreamcart/dreamcart7.jpg')}}" alt="" width="100%">
    </div>
    <div class="col-6 col-sm-2 p-1">
      <img src="{{asset('img/temp/2016dreamcart/dreamcart8.jpg')}}" alt="" width="100%">
    </div>
    <div class="col-6 col-sm-2 p-1">
      <img src="{{asset('img/temp/2016dreamcart/dreamcart9.jpg')}}" alt="" width="100%">
    </div>

  </div>
</div>
</main>
@endsection
