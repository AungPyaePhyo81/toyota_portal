@extends('layouts.main')

@section('head')
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="pragma" content="no-cache">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=5.0">
    <meta name="theme-color" media="(prefers-color-scheme: light)" content="#ffffff">
    <meta name="theme-color" media="(prefers-color-scheme: dark)" content="#eb0a1e">
    <meta name="description" content="Discover how we power future mobility by investing in and innovating electrification solutions for you.">
    <title>Toyota Safety Sense (TSS) | Toyota Myanmar</title>
    <meta name="description" content="Toyota description here.">
    <meta name="theme-color" content="#eb0a1e" /><link rel="manifest" href="./manifest.json" /><link rel="shortcut icon" href="favicon.svg"><link href="safety_sense/assets/css/global.css" rel="stylesheet"><link href="safety_sense/assets/css/safety-sense.css" rel="stylesheet"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/ToyotaType-Black.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/ToyotaType-Black.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/ToyotaType-Bold.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/ToyotaType-Bold.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/ToyotaType-Book.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/ToyotaType-Book.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/ToyotaType-Light.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/ToyotaType-Light.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/ToyotaType-Regular.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/ToyotaType-Regular.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/ToyotaType-Semibold.woff" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/ToyotaType-Semibold.woff2" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/lg.ttf" rel="preload"><link as="font" crossorigin="anonymous" href="etc.clientlibs/omotenashi/clientlibs/clientlib-base/fonts/resources/lg.woff" rel="preload">
@endsection

@section('content')
  <body class="cpl-pg--mlp">
    <div id="cpl--wrapper">
      <main class="p-0">
        <!-- Article -->
        <article>
          <!-- JSON data -->
          <script type="text/javascript">
            window.data_mlp_masthead = {
              "media": {
                "imgSml_webp": "safety_sense/dam/masthead/toyota-safety-sense-masthead-m.jpg",
                "imgBig_webp": "safety_sense/dam/masthead/toyota-safety-sense-masthead-d.jpg",
                "imgSml": "safety_sense/dam/masthead/toyota-safety-sense-masthead-m.jpg",
                "imgBig": "safety_sense/dam/masthead/toyota-safety-sense-masthead-d.jpg",
                "imgAlt": "Toyota Safety Sense"
              },
              "content": {
                "header": "",
                "tagline": "TOYOTA <br/>SAFETY SENSE",
                "paragraph": "",
                "boxStyle": "default" // default, keyline, black, white
              }
            };

            window.data_an_eye_on_safety = {
              "id": "eye-on-safety",
              "title": "AN EYE ON SAFETY",
              "paragraphs": ["Toyota has a long history of creating advancements and innovations, striving to produce vehicles that are safe and reliable. We are devoted to innovating and creating new technology that can help to protect you and your loved ones on your journeys."],
              "img": "safety_sense/dam/logo/toyota-safety-sense-logo.png",
              "imgAlt": ""
            };

            window.data_generic_separator__1 = {
              // "imgBig": "https://picsum.photos/id/67/1920/1080.webp",
              // "imgSml": "https://picsum.photos/id/67/1200/1600.jpg",
              "imgBig": "safety_sense/dam/toyota-safety-sense-raize-d.jpg",
              "imgSml": "safety_sense/dam/toyota-safety-sense-raize-m.jpg",
              "imgAlt": ""
            };

            // Timeline
            window.data_brand_safe_journey = {
              "id": "safe-journeys",
              "title": "SAFE JOURNEYS FOR ALL",
              "paragraphs": [
                {
                  "line": "Toyota Safety Sense (TSS) are advanced safety features designed to help protect drivers, passengers, and others on the road. Using camera or radar technology, they enable better hazard detection and support driver performance."
                }
              ],
              "helpTxt": "Disclaimer: <br />Drivers are responsible for their own safety, and must always drive safely, obey traffic speed limits and laws and focus on the road while driving. Refer to the Owner’s Manual for your vehicle to read more information on TSS precautions and limitations.<br /><br />Images may differ from local specifications.",
              "timelineItems": [
                {
                  "header": "WHILE ON THE ROAD",
                  "tiles": [
                    {
                      "title": "Dynamic Radar Cruise Control (DRCC) or Adaptive Cruise Control (ACC)",
                      "desc": [
                        "DRCC/ACC helps make driving safer and easier by automatically adjusting your vehicle’s speed to match speed changes of the vehicle ahead and keep a constant preset distance."
                      ],
                      "desc2": [
                        "For instance, when the system detects that the vehicle in front is travelling slower than the set speed, it will slow your vehicle accordingly to maintain the preset following distance without you having to apply the brakes."
                      ],
                      "imgLazy": "safety_sense/src/dam/img-placeholder.svg",
                      "imgSrc": "safety_sense/dam/toyota-safety-sense-dynamic-radar-cruise-control.jpg"
                    },
                    {
                      "title": "Lane Tracing Assist (LTA) or Lane Keeping Control (LKC)",
                      "desc": [
                        "LTA/LKC is designed to work with DRCC/ACC to help you in keeping your vehicle centred in its lane and preemptively avoid unintended lane departures, while your hands remain on the steering wheel."
                      ],
                      "desc2": [
                        "The system monitors lane markings and the path of the vehicle ahead, if needed, and can automatically make constant steering inputs to help keep your vehicle centred in its lane. Simultaneously, DRCC/ACC helps to maintain a preset distance from the vehicle ahead, if there is one."
                      ],
                      "imgLazy": "safety_sense/src/dam/img-placeholder.svg",
                      "imgSrc": "safety_sense/dam/toyota-safety-sense-lane-tracing-assist.jpg"
                    },
                    {
                      "title": "Lane Departure Alert (LDA) or Lane Departure Warning (LDW)",
                      "desc": [
                        "LDA/LDW is designed to reduce the possibility of an accident due to your vehicle unintentionally drifting out of a clearly marked lane."
                      ],
                      "desc2": [
                        "The system recognises visible lane markings, such as on highways and freeways. If it detects the possibility of lane departure, LDA/LDW will use a warning buzzer and visual notification to alert you and urge you to steer back into the lane."
                      ],
                      "imgLazy": "safety_sense/src/dam/img-placeholder.svg",
                      "imgSrc": "safety_sense/dam/toyota-safety-sense-lane-departure-alert.jpg"
                    }
                  ]
                },
                {
                  "header": "DRIVING AT NIGHT",
                  "tiles": [
                    {
                      "title": "Automatic High Beam (AHB) or Auto High Beam (AHB)",
                      "desc": [
                        "Navigate the road with greater confidence at night with AHB. This system is designed to help you see more clearly at night, potentially enabling earlier detection of pedestrians and obstacles, while reducing glare for oncoming traffic."
                      ],
                      "desc2": [
                        "When activated, AHB uses the forward-facing camera to help detect the headlights of oncoming vehicles and the tail lights of preceding vehicles. It automatically turns high beam off when it detects another vehicle, and switches back to high beam when no other vehicles are detected."
                      ],
                      "imgLazy": "safety_sense/src/dam/img-placeholder.svg",
                      "imgSrc": "safety_sense/dam/toyota-safety-sense-automatic-high-beam.jpg"
                    }
                  ]
                },
                {
                  "header": "IF A COLLISION IS IMMINENT",
                  "tiles": [
                    {
                      "title": "Pre-collision System (PCS) or Pre-collision Warning (PCW)",
                      "desc": [
                        "PCS/PCW determines whether a frontal collision with another vehicle is imminent. If it detects the possibility of a collision, it will warn you to take evasive action by showing an alert on the multi-information display and sound a buzzer."
                      ],
                      "desc2": [
                        "It then prepares the brake assist system to provide additional assistance. If you do not brake within a set time, the system may also automatically apply the brakes to help you avoid the collision or reduce the speed of the impact."
                      ],
                      "imgLazy": "safety_sense/src/dam/img-placeholder.svg",
                      "imgSrc": "safety_sense/dam/toyota-safety-sense-pre-collision-system.jpg"
                    }
                  ]
                }
              ]
            };

            window.data_cards_related__1 = {
              "id": "explore-more",
              "layoutClass": "cpl-grid-cell-4",
              "bgColor": "white", // white, off-white, grey-mid, grey-dark, black
              "header": "EXPLORE MORE ABOUT TOYOTA",
              "desc": "",
              "cards": [
                {
                  "img": "safety_sense/dam/explore/tss-explore-tnga.jpg",
                  "header": "TOYOTA NEW GLOBAL ARCHITECTURE",
                  "desc": "<p>Our new car-making philosophy is paving the way for us to create Toyotas that have even better driving dynamics and appeal.</p>",
                  "cta": {
                    "url": "{{route('discover.global-architecture')}}"
                  }
                },
                // {
                //   "img": "https://via.placeholder.com/1200x675/7fbfbf/000",
                //   "header": "TOYOTA HYBRID ELECTRIC",
                //   "desc": "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis aute irure dolor in reprehenderit in voluptate.</p>",
                //   "cta": {
                //     "url": ""
                //   }
                // }
              ],
              "ctaBtn": {
                "label": "",
                "url": ""
              },
            }

            window.data_global_nav_models = {
              "label": "Models",
              "viewModelsLabel": "View grades",
              "tabs": [
                {
                  "id": "suv",
                  "label": "SUV",
                  "subTabs": [
                    {
                      "id": "all",
                      "label": "All",
                      "modelLists": [
                        {
                          "header": "RAIZE",
                          "desc": "Compact SUV",
                          "priceLabel": "From $XXX,XXX",
                          "promoLabel": "PROMOTION AVAILABLE",
                          "imgLazy": "safety_sense/dam/cars/lazy-angle-left.png",
                          "imgThumb": "safety_sense/dam/cars/suv/raize/toyota-raize-thumbnail-nav-new.png",
                          "url": "#",
                          "selectGrades": [
                            {
                              "label": "LE",
                              "value": "model.html"
                            },
                            {
                              "label": "XLE",
                              "value": "model.html"
                            },
                            {
                              "label": "Limited",
                              "value": "model.html"
                            }
                          ]
                        },
                        {
                          "header": "RUSH",
                          "desc": "Compact SUV",
                          "priceLabel": "From $XXX,XXX",
                          "promoLabel": "",
                          "imgLazy": "safety_sense/dam/cars/lazy-angle-left.png",
                          "imgThumb": "safety_sense/dam/cars/suv/rush/toyota-rush-thumbnail-nav.png",
                          "url": "#",
                          "selectGrades": [
                            {
                              "label": "Grade #1",
                              "value": "model.html"
                            },
                            {
                              "label": "Grade #2",
                              "value": "model.html"
                            },
                            {
                              "label": "Grade #3",
                              "value": "model.html"
                            },
                            {
                              "label": "Grade #4",
                              "value": "model.html"
                            },
                            {
                              "label": "Grade #5",
                              "value": "model.html"
                            },
                            {
                              "label": "Grade #6",
                              "value": "model.html"
                            },
                            {
                              "label": "Limited",
                              "value": "model.html"
                            }
                          ]
                        },
                        {
                          "header": "COROLLA CROSS",
                          "desc": "Compact crossover",
                          "priceLabel": "From $XXX,XXX",
                          "promoLabel": "",
                          "imgLazy": "safety_sense/dam/cars/lazy-angle-left.png",
                          "imgThumb": "safety_sense/dam/cars/suv/corolla-cross/toyota-corolla-cross-thumbnail-nav.png",
                          "url": "#",
                          "selectGrades": [
                            {
                              "label": "LE",
                              "value": "model.html"
                            },
                            {
                              "label": "XLE",
                              "value": "model.html"
                            },
                            {
                              "label": "Limited",
                              "value": "model.html"
                            }
                          ]
                        },
                        {
                          "header": "RAV4",
                          "desc": "Compact crossover SUV",
                          "priceLabel": "From $XXX,XXX",
                          "promoLabel": "",
                          "imgLazy": "safety_sense/dam/cars/lazy-angle-left.png",
                          "imgThumb": "safety_sense/dam/cars/suv/rav4/toyota-rav4-thumbnail-nav.png",
                          "url": "#",
                          "selectGrades": [
                            {
                              "label": "LE",
                              "value": "model.html"
                            },
                            {
                              "label": "XLE",
                              "value": "model.html"
                            },
                            {
                              "label": "Limited",
                              "value": "model.html"
                            }
                          ]
                        },
                        {
                          "header": "FORTUNER",
                          "desc": "7-seater luxury SUV",
                          "priceLabel": "From $XXX,XXX",
                          "promoLabel": "",
                          "imgLazy": "safety_sense/dam/cars/lazy-angle-left.png",
                          "imgThumb": "safety_sense/dam/cars/suv/fortuner/toyota-fortuner-thumbnail-nav.png",
                          "url": "#",
                          "selectGrades": [
                            {
                              "label": "LE",
                              "value": "model.html"
                            },
                            {
                              "label": "XLE",
                              "value": "model.html"
                            },
                            {
                              "label": "Limited",
                              "value": "model.html"
                            }
                          ]
                        },
                        {
                          "header": "LAND CRUISER 70",
                          "desc": "SUV",
                          "priceLabel": "From $XXX,XXX",
                          "promoLabel": "",
                          "imgLazy": "safety_sense/dam/cars/lazy-angle-left.png",
                          "imgThumb": "safety_sense/dam/cars/suv/lc70/toyota-lc70-thumbnail-nav.png",
                          "url": "#",
                          "selectGrades": [
                            {
                              "label": "LE",
                              "value": "model.html"
                            },
                            {
                              "label": "XLE",
                              "value": "model.html"
                            },
                            {
                              "label": "Limited",
                              "value": "model.html"
                            }
                          ]
                        },
                        {
                          "header": "LAND CRUISER PRADO",
                          "desc": "7-seater luxury SUV",
                          "priceLabel": "From $XXX,XXX",
                          "promoLabel": "",
                          "imgLazy": "safety_sense/dam/cars/lazy-angle-left.png",
                          "imgThumb": "safety_sense/dam/cars/suv/prado/toyota-prado-mt-thumbnail-nav.png",
                          "url": "#",
                          "selectGrades": [
                            {
                              "label": "LE",
                              "value": "model.html"
                            },
                            {
                              "label": "XLE",
                              "value": "model.html"
                            },
                            {
                              "label": "Limited",
                              "value": "model.html"
                            }
                          ]
                        },
                        {
                          "header": "LAND CRUISER",
                          "desc": "Flagship luxury SUV",
                          "priceLabel": "From $XXX,XXX",
                          "promoLabel": "",
                          "imgLazy": "safety_sense/dam/cars/lazy-angle-left.png",
                          "imgThumb": "safety_sense/dam/cars/suv/lc300/toyota-lc300-thumbnail-nav.png",
                          "url": "#",
                          "selectGrades": [
                            {
                              "label": "LE",
                              "value": "model.html"
                            },
                            {
                              "label": "XLE",
                              "value": "model.html"
                            },
                            {
                              "label": "Limited",
                              "value": "model.html"
                            }
                          ]
                        }
                      ]
                    // },
                    // {
                    //   "id": "hybrid",
                    //   "label": "Hybrid",
                    //   "modelLists": [
                    //     {
                    //       "header": "XXX",
                    //       "desc": "Lorem SUV",
                    //       "priceLabel": "From $XXX,XXX",
                    //       "promoLabel": "",
                    //       "imgLazy": "dam/cars/lazy-angle-left.png",
                    //       "imgThumb": "dam/cars/thumb-angle-left.png",
                    //       "url": "#",
                    //       "selectGrades": [
                    //         {
                    //           "label": "XXX",
                    //           "value": "model.html"
                    //         },
                    //         {
                    //           "label": "XXX",
                    //           "value": "model.html"
                    //         },
                    //         {
                    //           "label": "Xxxxxxx",
                    //           "value": "model.html"
                    //         }
                    //       ]
                    //     }
                    //   ]
                    }
                  ]
                },
                {
                  "id": "mpv",
                  "label": "MPV",
                  "subTabs": [
                    {
                      "id": "all",
                      "label": "All",
                      "modelLists": [
                        {
                          "header": "INNOVA",
                          "desc": "",
                          "priceLabel": "From $XXX,XXX",
                          "promoLabel": "",
                          "imgLazy": "safety_sense/dam/cars/lazy-angle-left.png",
                          "imgThumb": "safety_sense/dam/cars/mpv/innova/toyota-innova-thumbnail-nav.png",
                          "url": "#",
                          "selectGrades": [
                            {
                              "label": "Grade #1",
                              "value": "model.html"
                            },
                            {
                              "label": "Grade #2",
                              "value": "model.html"
                            },
                            {
                              "label": "Grade #3",
                              "value": "model.html"
                            }
                          ]
                        }
                      ]
                    }
                  ]
                },
                {
                  "id": "sedan",
                  "label": "SEDAN",
                  "subTabs": [
                    {
                      "id": "all",
                      "label": "All",
                      "modelLists": [
                        {
                          "header": "CAMRY",
                          "desc": "Lorem Ipsum",
                          "priceLabel": "From $XXX,XXX",
                          "promoLabel": "",
                          "imgLazy": "safety_sense/dam/cars/lazy-angle-left.png",
                          "imgThumb": "safety_sense/dam/cars/sedan/camry/toyota-camry-thumbnail-nav.png",
                          "url": "#",
                          "selectGrades": [
                            {
                              "label": "Grade #1",
                              "value": "model.html"
                            },
                            {
                              "label": "Grade #2",
                              "value": "model.html"
                            },
                            {
                              "label": "Grade #3",
                              "value": "model.html"
                            }
                          ]
                        }
                      ]
                    }
                  ]
                },
                {
                  "id": "pickup",
                  "label": "PICKUP",
                  "subTabs": [
                    {
                      "id": "all",
                      "label": "All",
                      "modelLists": [
                        {
                          "header": "ACE",
                          "desc": "Bus",
                          "priceLabel": "From $XXX,XXX",
                          "promoLabel": "",
                          "imgLazy": "safety_sense/dam/cars/lazy-angle-left.png",
                          "imgThumb": "safety_sense/dam/cars/pickup/ace/toyota-ace-thumbnail-nav.png",
                          "url": "#",
                          "selectGrades": [
                            {
                              "label": "Grade #1",
                              "value": "model.html"
                            },
                            {
                              "label": "Grade #2",
                              "value": "model.html"
                            },
                            {
                              "label": "Grade #3",
                              "value": "model.html"
                            }
                          ]
                        },
                        {
                          "header": "HILUX",
                          "desc": "Pickup",
                          "priceLabel": "From $XXX,XXX",
                          "promoLabel": "",
                          "imgLazy": "safety_sense/dam/cars/lazy-angle-left.png",
                          "imgThumb": "safety_sense/dam/cars/pickup/hilux/toyota-hilux-thumbnail-nav.png",
                          "url": "#",
                          "selectGrades": [
                            {
                              "label": "Grade #1",
                              "value": "model.html"
                            },
                            {
                              "label": "Grade #2",
                              "value": "model.html"
                            },
                            {
                              "label": "Grade #3",
                              "value": "model.html"
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ],
              "hvas": [
                {
                  "label": "Book a Test Drive",
                  "url": "form.html",
                  "svgCode": "<svg version='1.1' xmlns='http://www.w3.org/2000/svg' width='30' height='30' viewBox='0 0 30 30'><path d='M15,0C6.7,0,0,6.7,0,15c0,8.3,6.7,15,15,15c8.3,0,15-6.7,15-15C30,6.7,23.3,0,15,0z M15,1.1c5.6,0,10.5,3.4,12.7,8.2H23 c-1.8-2.5-4.7-4.1-8-4.1c-3.3,0-6.2,1.6-8,4.1H2.3C4.5,4.5,9.4,1.1,15,1.1z M23.7,15c0,0.7-0.1,1.4-0.2,2c-0.9,0-3.7,0.1-5.5,1.9 c-1.1,1.1-1.6,2.7-1.5,4.7c-0.5,0.1-1,0.1-1.5,0.1c-0.5,0-1-0.1-1.5-0.1c0.1-2-0.4-3.5-1.5-4.7C10.2,17.1,7.4,17,6.5,17 c-0.2-0.7-0.2-1.3-0.2-2c0-0.1,0-0.1,0-0.2c1-0.5,4.5-2.3,8.7-2.3c4.2,0,7.8,1.8,8.7,2.3C23.7,14.9,23.7,14.9,23.7,15z M23.2,18.1 c-1,2.5-3,4.4-5.5,5.2c0-1.5,0.3-2.8,1.2-3.6C20.1,18.4,22.2,18.2,23.2,18.1z M12.4,23.3c-2.6-0.8-4.6-2.7-5.5-5.2 c1,0,3,0.3,4.3,1.6C12,20.6,12.4,21.8,12.4,23.3z M15,11.4c-3.8,0-7.1,1.4-8.6,2.1c0.7-4.1,4.3-7.2,8.6-7.2c4.3,0,7.9,3.1,8.6,7.2 C22.1,12.7,18.8,11.4,15,11.4z M15,28.9C7.3,28.9,1.1,22.7,1.1,15c0-1.6,0.3-3.2,0.8-4.6h4.4c-0.7,1.4-1.2,2.9-1.2,4.6 c0,5.4,4.4,9.8,9.8,9.8c5.4,0,9.8-4.4,9.8-9.8c0-1.7-0.4-3.2-1.2-4.6h4.4c0.5,1.4,0.8,3,0.8,4.6C28.9,22.7,22.7,28.9,15,28.9z' fill='currentColor'></path></svg>"
                },
                {
                  "label": "Download Brochures",
                  "url": "model-brochures.html",
                  "svgCode": "<svg version='1.1' xmlns='http://www.w3.org/2000/svg' width='20.2' height='30' viewBox='0 0 20.2 30'><path d='M9.1,7.4L3.7,10v6.5l5.4-2.7V7.4z M7.9,13.2l-3.1,1.6v-4l3.1-1.6V13.2z M12.8,5.6V0L0,5.8V30h1.6h1.1h17.5V5.6H12.8z M1.1,28.6v-5.4l0.2,0.4l10.3-5.2v5.3L1.1,28.6z M11.6,1.7v3.9v12.1l-0.2-0.4 L1.1,22.4V6.5L11.6,1.7z M19,28.9H3.1l9.6-4.4V6.7H19V28.9z' fill='currentColor'></path></svg>"
                },
                {
                  "label": "Compare Models",
                  "url": "model-comparison.html",
                  "svgCode": "<svg version='1.1' xmlns='http://www.w3.org/2000/svg' width='35.4' height='30' viewBox='0 0 35.4 30'><path d='M10.3,18.6v-5.3H0v-8h10.3V0l10.6,9.3L10.3,18.6z M1.2,12.1h10.3v3.8l7.6-6.6l-7.6-6.6v3.8H1.2V12.1z M25.1,30l-10.6-9.3l10.6-9.3v5.3h10.3v8H25.1V30z M16.3,20.7l7.6,6.6v-3.8h10.3v-5.6H23.9v-3.8L16.3,20.7z' fill='currentColor'></path></svg>"
                }
              ],
              "compareModelsCta": {
                "label": "COMPARE MODELS",
                "url": "model-comparison.html"
              }
            };

            window.data_explore_models = {
              "id": "explore-models",
              "bgColor": "off-white", // white, off-white, grey-mid, grey-dark, black
              "header": "EXPLORE THE VEHICLES WITH TSS"
            };
          </script>
          <!--#end of JSON data -->

          <!-- MLP: Masthead -->
          <section class="cpl-mlp-masthead" id="">
            <div class="cpl-masthead-sec cpl-el-anim">
              <!-- Section: Media -->
              <section class="cpl-masthead-media">
                <!-- Images -->
                <picture>
                  <source type="image/webp" media="(orientation: landscape)" v-if="masthead.media.imgBig_webp" :data-srcset="masthead.media.imgBig_webp">
                  <source type="image/webp" media="(orientation: portrait)" v-if="masthead.media.imgSml_webp" :data-srcset="masthead.media.imgSml_webp">
                  <source media="(orientation: landscape)" :data-srcset="masthead.media.imgBig">
                  <source media="(orientation: portrait)" :data-srcset="masthead.media.imgSml">
                  <img
                    class="cpl-lazy"
                    data-src="safety_sense/dam/img-placeholder.svg"
                    :alt="masthead.media.imgAlt"
                  >
                </picture>
              </section>
              <!--#end of Section: Media -->

              <!-- Section: Content -->
              <section class="cpl-masthead-content" :class="'cpl-box-'+ masthead.content.boxStyle">
                <div class="cpl-sec-cont" :class="masthead.content.boxStyle=='white' ? 'cpl-sec-bg-white' : 'cpl-sec-bg-black'">
                  <h1 class="h4" v-html="masthead.content.header"></h1>
                  <h2 v-html="masthead.content.tagline"></h2>
                  <p v-if="masthead.content.paragraph" v-html="masthead.content.paragraph"></p>
              </section>
              <!--#end of Section: Content -->
              <!-- Scrolling Down -->
              <a class="cpl-cta--scroll cpl-unstyled" href="javascript:;">
                <svg class="show_on_desktop" version="1.1" width="28.2" height="48" viewBox="0 0 28.2 48" preserveAspectRatio="xMidYMid meet" aria-labelledby="scrollDownBtnTitle" role="img">
                  <title id="scrollDownBtnTitle">Scroll down</title>
                  <g fill-rule="evenodd">
                    <path d="M14.1,48C6.3,48,0,41.6,0,33.7V14.3C0,6.4,6.3,0,14.1,0s14.1,6.4,14.1,14.3v19.4C28.2,41.6,21.9,48,14.1,48z M14.1,1.2 C7,1.2,1.2,7.1,1.2,14.3v19.4c0,7.2,5.8,13.1,12.9,13.1c7.1,0,12.9-5.9,12.9-13.1V14.3C27,7.1,21.2,1.2,14.1,1.2z" fill="currentColor"></path>
                    <circle class="cpl-anim--mouse" cx="14.3" cy="12.3" r="3.2" fill="currentColor"></circle>
                  </g>
                </svg>
              </a>
              <!--#end of Scrolling Down -->

            </div>
          </section>
          <!--#end of MLP: Masthead -->

          <!-- Section: An Eye On Safety -->
          <section class="cpl-sec-bg-white cpl-an-eye-on-safety" v-if="eye_safety.id" :id="eye_safety.id">
            <div class="cpl-sec-wrapper">
              <div class="cpl-grid">
                <div class="cpl-grid-inner">
                  <div class="cpl-grid-cell-8">
                    <header>
                      <h2 class="h3 cpl-parallax" data-modifier="5" v-html="eye_safety.title"></h2>
                      <p class="cpl-parallax" data-modifier="5" v-for="p in eye_safety.paragraphs" v-html="p"></p>
                    </header>
                  </div>
                  <div class="cpl-grid-cell-4 cpl-sm-float-top">
                    <picture class="cpl-ar cpl-ar--16-9 cpl-parallax" data-modifier="5">
                      <img
                        class="cpl-lazy"
                        src="safety_sense/dam/img-placeholder.svg"
                        :data-src="eye_safety.img"
                        :alt="eye_safety.imgAlt"
                      >
                    </picture>
                  </div>
                </div>
                </div>
              </div>
            </div>
          </section>
          <!--#end of Section: An Eye On Safety -->

          <!-- Section: Generic Separator -->
          <footer class="cpl-separator cpl-generic-separator__1 cpl-sec-bg-black" id="">
            <picture class="cpl-parallax-bg">
              <source media="(min-width: 600px)" :data-srcset="separator.imgBig">
              <source media="(max-width: 599px)" :data-srcset="separator.imgSml">
              <img
                class="cpl-lazy"
                src="safety_sense/dam/img-placeholder.svg"
                :data-src="separator.imgSml"
                alt="separator.imgAlt"
              >
            </picture>
          </footer>
          <!--#end of Section: Generic Separator -->

          <!-- Section: Timeline Layout -->
          <section class="brand__section_timeline cpl-sec-bg-white" :id="section.id">
            <div class="cpl-sec-wrapper">
              <div class="cpl-grid" v-if="section.title">
                <div class="cpl-grid-inner">
                  <div class="cpl-grid-cell-10">
                    <header>
                      <h2 class="h3 cpl-parallax" data-modifier="5" v-html="section.title"></h2>
                    </header>
                    <p class="cpl-parallax" data-modifier="5" v-for="paragraph in section.paragraphs" v-html="paragraph.line"></p>
                  </div>
                </div>
              </div>
              <div class="sec__cont no_padding_top">
                <ul class="brand__layout brand__layout_timeline">
                  <!-- Timeline v1 -->
                  <li v-for="item in section.timelineItems" class="brand__timeline_item">
                    <div class="tile_timeline">
                      <h5 class="tile_timeline_header" v-html="item.header"></h5>
                      <div class="tile_timeline_body">
                        <ul class="brand__layout" :class="item.tiles.length > 1 ? 'brand__layout_2_col': 'brand__layout_1_col'">
                          <li v-for="tile in item.tiles">
                            <div class="tile__block">
                              <div class="tile__block_img">
                                <picture class="cpl-ar cpl-ar--16-9 cpl-parallax" data-modifier="5">
                                  <img
                                    class="cpl-lazy"
                                    src="safety_sense/dam/img-placeholder.svg"
                                    :data-src="tile.imgSrc"
                                    :alt="tile.title"
                                  >
                                </picture>
                              </div>
                              <div class="tile__block_cont">
                                <h5 data-modifier="5" class="cpl-parallax" v-html="tile.title"></h5>
                                <p v-for="desc in tile.desc" v-html="desc" class="cpl-parallax" data-modifier="5"></p>
                                <p class="cpl-parallax cpl-expandable-toggle cpl-hide" v-for="desc2 in tile.desc2" v-html="desc2" data-modifier="5">
                                </p>
                                <p class="cpl-parallax" data-modifier="5">
                                  <a class="cpl-unstyled cpl-link-expandable" href="javascript:;">
                                    <span class="expandable-show active">Read more</span>
                                    <span class="expandable-hide">Read less</span>
                                  </a>
                                </p>
                              </div>
                            </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </li>
                </ul>

                <p class="section_help_text cpl-grid" v-if="section.helpTxt">
                  <em v-html="section.helpTxt"></em>
                </p>
              </div>
            </div>
          </section>
          <!--#end of Section: Timeline Layout -->

          <!-- Section: Generic Cards Layout - Related Pages -->
          <section class="cpl-cards-related__1" :class="'cpl-sec-bg-'+ section.bgColor" :id="section.id">
            <div class="cpl-sec-wrapper">
              <div class="cpl-grid">
                <div class="cpl-grid-inner">
                  <section class="cpl-grid-cell-9">
                    <header>
                      <h2 class="h3 cpl-parallax" data-modifier="5" v-html="section.header"></h2>
                    </header>
                  </section>
                </div>
              </div>
              <div class="cpl-grid no-pad-top">
                <div class="cpl-grid-inner">
                  <section :class="section.layoutClass" v-for="card in section.cards">
                    <a class="cpl-unstyled cpl-card--block" :href="card.cta.url">
                      <div class="cpl-card">
                        <div class="cpl-card--img cpl-parallax" data-modifier="5">
                          <picture class="cpl-ar cpl-ar--16-9">
                            <img
                              class="cpl-lazy"
                              src="safety_sense/dam/img-placeholder.svg"
                              :data-src="card.img"
                              :alt="card.imgAlt"
                            >
                          </picture>
                        </div>
                        <div class="cpl-card--cont cpl-freehtml">
                          <h5 class="cpl-parallax" data-modifier="5">
                            <span v-html="card.header"></span>
                          </h5>
                          <div class="cpl-parallax" data-modifier="5" v-html="card.desc"></div>
                        </div>
                      </div>
                    </a>
                  </section>
                </div>
              </div>
            </div>
          </section>
          <!--#end of Section: Generic Cards Layout - Related Pages -->

          <!-- Section: Model Selector -->
          <section class="cpl-explore-models" :id="section.id" :class="'cpl-sec-bg-'+ section.bgColor">
            <div class="cpl-sec-wrapper">
              <div class="cpl-grid" v-if="section.header">
                <div class="cpl-grid-inner">
                  <section class="cpl-grid-cell-9">
                    <header>
                      <h2 class="h3 cpl-parallax no-margin-bot" data-modifier="5" v-html="section.header"></h2>
                    </header>
                  </section>
                </div>
              </div>
              <div class="cpl-sec-cont no-pad-top">
                <section class="cpl-model-selector cpl-sec--explore">
                  <!-- Tab -->
                  <div class="mdc-tab-bar cpl-tab--main" role="tablist">
                    <div class="mdc-tab-scroller">
                      <div class="mdc-tab-scroller__scroll-area">
                        <div class="mdc-tab-scroller__scroll-content">
                          <button class="mdc-tab" role="tab" aria-selected="false" tabindex="-1" v-for="tab in models.tabs" :data-id="tab.id">
                            <span class="mdc-tab__content">
                              <span class="mdc-tab__text-label" v-html="tab.label"></span>
                            </span>
                            <span class="mdc-tab-indicator">
                              <span class="mdc-tab-indicator__content mdc-tab-indicator__content--underline"></span>
                            </span>
                            <span class="mdc-tab__ripple"></span>
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!--#end of Tab -->

                  <!-- Tab Panels -->
                  <div class="cpl-tabpanels--main">
                    <!-- Tab Panel -->
                    <div class="cpl-tabpanel--main" v-for="tab in models.tabs">
                      <!-- Sub-tab -->
                      <div class="mdc-tab-bar cpl-tab--sub" role="tablist">
                        <div class="mdc-tab-scroller">
                          <div class="mdc-tab-scroller__scroll-area">
                            <div class="mdc-tab-scroller__scroll-content">
                              <button class="mdc-tab" role="tab" aria-selected="false" tabindex="-1" v-for="subTab in tab.subTabs" :data-id="subTab.id">
                                <span class="mdc-tab__content">
                                  <span class="mdc-tab__text-label" v-html="subTab.label"></span>
                                </span>
                                <span class="mdc-tab-indicator">
                                  <span class="mdc-tab-indicator__content mdc-tab-indicator__content--underline"></span>
                                </span>
                                <span class="mdc-tab__ripple"></span>
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!--#end of Sub-tab -->
                      <!-- Sub-tab Panels -->
                      <div class="cpl-tabpanels--sub">
                        <div class="cpl-tabpanel--sub" v-for="subTab in tab.subTabs">
                          <ul class="cpl-tab-item--models">
                            <li v-for="list in subTab.modelLists">
                              <a class="cpl-tab-item--model cpl-unstyled" :href="list.url">
                                <picture class="cpl-tab-item--thumb">
                                  <img class="cpl-lazy" :src="list.imgLazy" :data-src="list.imgThumb" :alt="list.header">
                                </picture>
                                <div class="cpl-tab-item--txt">
                                  <h5 v-if="subTab.id=='all'" v-html="list.header"></h5>
                                  <h6 v-else class="cpl-txt-bold" v-html="list.header"></h6>
                                  <p v-html="list.priceLabel"></p>
                                </div>
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <!--#end of Sub-tab Panels -->
                    </div>
                    <!--end of Tab Panel -->
                  </div>
                  <!--#end of Tab Panels -->

                  <!-- CTA Wrapper -->
                  <!-- <div class="cpl-cta-wrapper cpl-align-center" v-if="models.compareModelsCta.url">
                    <a class="mdc-button mdc-button--outlined" :href="models.compareModelsCta.url">
                      <span class="mdc-button__ripple"></span>
                      <span class="mdc-button__label" v-html="models.compareModelsCta.label"></span>
                    </a>
                  </div> -->
                  <!--#end of CTA Wrapper -->
                </section>
              </div>
            </div>
          </section>
          <!--#end of Section: Model Selector -->

        </article>
        <!--#end of Article -->
        <!-- JSON data -->
        <script type="text/javascript">
          window.data_global_hva = [
            {
              "label": "Book a Service",
              "url": "form.html",
              "svgCode": "<svg version='1.1' xmlns='http://www.w3.org/2000/svg' width='29.7' height='30' viewBox='0 0 29.7 30'><path d='M5.5,30c-1.1,0-2.5-0.4-3.8-1.7c-2-2-1.9-4.1-1.5-5.4c0.5-1.4,1.4-2.4,1.5-2.4l10.9-9.9c-0.1-0.6-0.2-1.1-0.2-1.7 c0-4.8,3.9-8.8,8.8-8.8c3.6,0,6.9,2.3,8.2,5.6l0.3,0.8h-6.2L22,8.8l1.5,2.3h6.2l-0.3,0.8c-1.3,3.4-4.6,5.6-8.2,5.6 c-0.6,0-1.1-0.1-1.7-0.2L9.6,28.2c-0.1,0.1-1,1-2.4,1.5C6.7,29.9,6.1,30,5.5,30z M21.2,1.2c-4.2,0-7.5,3.4-7.5,7.5 c0,0.6,0.1,1.2,0.2,1.8l0.1,0.4L2.6,21.3c-0.1,0.1-3,3.1,0,6.1c3,3,6,0.1,6.1,0L19.1,16l0.4,0.1c0.6,0.1,1.2,0.2,1.8,0.2 c2.8,0,5.4-1.6,6.7-4h-5l-2.4-3.5l2.4-3.5h5C26.6,2.8,24,1.2,21.2,1.2z' fill='currentColor'></path></svg>"
            },
            {
              "label": "Book a Test Drive",
              "url": "form.html",
              "svgCode": "<svg version='1.1' xmlns='http://www.w3.org/2000/svg' width='30' height='30' viewBox='0 0 30 30'><path d='M15,0C6.7,0,0,6.7,0,15c0,8.3,6.7,15,15,15c8.3,0,15-6.7,15-15C30,6.7,23.3,0,15,0z M15,1.1c5.6,0,10.5,3.4,12.7,8.2H23 c-1.8-2.5-4.7-4.1-8-4.1c-3.3,0-6.2,1.6-8,4.1H2.3C4.5,4.5,9.4,1.1,15,1.1z M23.7,15c0,0.7-0.1,1.4-0.2,2c-0.9,0-3.7,0.1-5.5,1.9 c-1.1,1.1-1.6,2.7-1.5,4.7c-0.5,0.1-1,0.1-1.5,0.1c-0.5,0-1-0.1-1.5-0.1c0.1-2-0.4-3.5-1.5-4.7C10.2,17.1,7.4,17,6.5,17 c-0.2-0.7-0.2-1.3-0.2-2c0-0.1,0-0.1,0-0.2c1-0.5,4.5-2.3,8.7-2.3c4.2,0,7.8,1.8,8.7,2.3C23.7,14.9,23.7,14.9,23.7,15z M23.2,18.1 c-1,2.5-3,4.4-5.5,5.2c0-1.5,0.3-2.8,1.2-3.6C20.1,18.4,22.2,18.2,23.2,18.1z M12.4,23.3c-2.6-0.8-4.6-2.7-5.5-5.2 c1,0,3,0.3,4.3,1.6C12,20.6,12.4,21.8,12.4,23.3z M15,11.4c-3.8,0-7.1,1.4-8.6,2.1c0.7-4.1,4.3-7.2,8.6-7.2c4.3,0,7.9,3.1,8.6,7.2 C22.1,12.7,18.8,11.4,15,11.4z M15,28.9C7.3,28.9,1.1,22.7,1.1,15c0-1.6,0.3-3.2,0.8-4.6h4.4c-0.7,1.4-1.2,2.9-1.2,4.6 c0,5.4,4.4,9.8,9.8,9.8c5.4,0,9.8-4.4,9.8-9.8c0-1.7-0.4-3.2-1.2-4.6h4.4c0.5,1.4,0.8,3,0.8,4.6C28.9,22.7,22.7,28.9,15,28.9z' fill='currentColor'></path></svg>"
            },
            {
              "label": "Calculate Payments",
              "url": "javascript:;",
              "svgCode": "<svg version='1.1' xmlns='http://www.w3.org/2000/svg' width='23.4' height='30' viewBox='0 0 23.4 30'><path d='M18.3,0H5c-4,0-5,3.3-5,5V25c0,1.7,1.1,5,5,5h13.3c4,0,5-3.3,5-5V5C23.4,3.3,22.3,0,18.3,0z M22.2,25c0,0.2-0.1,3.8-3.8,3.8 H5c-3.8,0-3.8-3.7-3.8-3.8V5c0-0.2,0.1-3.8,3.8-3.8h13.3c3.8,0,3.8,3.7,3.8,3.8V25z M4.4,9h14.5V4.4H4.4V9z M5.6,5.6h12.1v2.1H5.6 V5.6z M5.3,13.1H7v1.7H5.3V13.1z M10.8,13.1h1.7v1.7h-1.7V13.1z M16.4,13.1H18v1.7h-1.7V13.1z M5.3,17.5H7v1.7H5.3V17.5z M10.8,17.5 h1.7v1.7h-1.7V17.5z M16.4,17.5H18v1.7h-1.7V17.5z M5.3,21.9H7v1.7H5.3V21.9z M10.8,21.9h1.7v1.7h-1.7V21.9z M16.4,21.9H18v1.7h-1.7 V21.9z' fill='currentColor'></path></svg>"
            },
            {
              "label": "Compare Models",
              "url": "model-comparison.html",
              "svgCode": "<svg version='1.1' xmlns='http://www.w3.org/2000/svg' width='35.4' height='30' viewBox='0 0 35.4 30'><path d='M10.3,18.6v-5.3H0v-8h10.3V0l10.6,9.3L10.3,18.6z M1.2,12.1h10.3v3.8l7.6-6.6l-7.6-6.6v3.8H1.2V12.1z M25.1,30l-10.6-9.3l10.6-9.3v5.3h10.3v8H25.1V30z M16.3,20.7l7.6,6.6v-3.8h10.3v-5.6H23.9v-3.8L16.3,20.7z' fill='currentColor'></path></svg>"
            },
            {
              "label": "Configurator",
              "url": "javascript:;",
              "svgCode": "<svg version='1.1' xmlns='http://www.w3.org/2000/svg' width='38.1' height='30' viewBox='0 0 38.1 30'><path d='M36.3,26.5h-6.4c-0.3-1.3-1.4-2.3-2.8-2.3s-2.6,1-2.8,2.3H1.8v1.2h22.5c0.3,1.3,1.4,2.3,2.8,2.3s2.6-1,2.8-2.3h6.4V26.5z M27.1,28.8c-0.9,0-1.7-0.8-1.7-1.7s0.8-1.7,1.7-1.7s1.7,0.8,1.7,1.7S28,28.8,27.1,28.8z M36.3,20.7H21.9c-0.3-1.3-1.4-2.3-2.8-2.3s-2.6,1-2.8,2.3H1.8v1.2h14.4c0.3,1.3,1.4,2.3,2.8,2.3s2.6-1,2.8-2.3h14.4V20.7z M19,23c-0.9,0-1.7-0.8-1.7-1.7c0-0.9,0.8-1.7,1.7-1.7s1.7,0.8,1.7,1.7C20.7,22.3,20,23,19,23z M38.1,12.7l0-4.1c0-0.2-0.1-1.6-2.6-2.8c-1.8-0.9-3.5-1.2-4.9-1.4c-1.2-0.2-2.2-0.4-3.2-0.9c-0.2-0.1-0.5-0.3-0.8-0.6 C25.3,1.9,22.8,0,19,0h-4.6c-2.9,0-4.9,1.3-6.5,2.3C6.9,2.9,6,3.5,5.2,3.5H1.2v2.9c0,0.3-0.1,0.4-0.4,0.7C0.4,7.4,0,7.8,0,8.7l0,2.8 l7.1,1.2C7.6,14,8.9,15,10.4,15c1.5,0,2.8-1,3.3-2.3h10.7c0.5,1.3,1.8,2.3,3.3,2.3c1.5,0,2.8-1,3.3-2.3L38.1,12.7z M10.4,13.8 c-1.3,0-2.3-1-2.3-2.3s1-2.3,2.3-2.3c1.3,0,2.3,1,2.3,2.3S11.6,13.8,10.4,13.8z M27.7,13.8c-1.3,0-2.3-1-2.3-2.3s1-2.3,2.3-2.3 c1.3,0,2.3,1,2.3,2.3S28.9,13.8,27.7,13.8z M27.7,8.1c-1.9,0-3.5,1.5-3.5,3.5H13.9c0-1.9-1.6-3.5-3.5-3.5c-1.9,0-3.4,1.5-3.5,3.3 l-5.7-1l0-1.8c0-0.3,0.1-0.4,0.4-0.7c0.3-0.3,0.8-0.8,0.8-1.6V4.7h2.8c1.1,0,2.2-0.6,3.3-1.4c1.6-1,3.3-2.1,5.9-2.1H19 c3.4,0,5.5,1.7,6.8,2.7c0.4,0.3,0.7,0.6,1,0.7c1.1,0.6,2.3,0.8,3.5,1c1.4,0.2,2.9,0.5,4.6,1.3c1.8,0.9,2,1.8,2,1.8c0,0,0,0,0,0 l0,2.8l-5.7,0C31.1,9.6,29.6,8.1,27.7,8.1z' fill='currentColor'></path></svg>"
            },
            {
              "label": "Download Brochures",
              "url": "model-brochures.html",
              "svgCode": "<svg version='1.1' xmlns='http://www.w3.org/2000/svg' width='20.2' height='30' viewBox='0 0 20.2 30'><path d='M9.1,7.4L3.7,10v6.5l5.4-2.7V7.4z M7.9,13.2l-3.1,1.6v-4l3.1-1.6V13.2z M12.8,5.6V0L0,5.8V30h1.6h1.1h17.5V5.6H12.8z M1.1,28.6v-5.4l0.2,0.4l10.3-5.2v5.3L1.1,28.6z M11.6,1.7v3.9v12.1l-0.2-0.4 L1.1,22.4V6.5L11.6,1.7z M19,28.9H3.1l9.6-4.4V6.7H19V28.9z' fill='currentColor'></path></svg>"
            },
            {
              "label": "Find a Dealer",
              "url": "javascript:;",
              "svgCode": "<svg version='1.1' xmlns='http://www.w3.org/2000/svg' width='19.1' height='30' viewBox='0 0 19.1 30'><path d='M9.5,30l-0.6-1.1c-0.7-1.2-1.4-2.4-2.1-3.6C2.9,18.7,0,13.9,0,9.5C0,4.3,4.3,0,9.5,0s9.5,4.3,9.5,9.5 c0,4.4-2.9,9.2-6.8,15.8c-0.7,1.1-1.4,2.3-2.1,3.6L9.5,30z M9.5,1.5c-4.5,0-8.1,3.6-8.1,8.1c0,3.9,2.8,8.6,6.6,15.1 c0.5,0.8,1,1.6,1.5,2.5c0.5-0.9,1-1.7,1.5-2.5c3.8-6.5,6.6-11.1,6.6-15.1C17.6,5.1,14,1.5,9.5,1.5z M9.5,13.2 c-2.1,0-3.9-1.7-3.9-3.9s1.7-3.9,3.9-3.9s3.9,1.7,3.9,3.9S11.7,13.2,9.5,13.2z M9.5,6.9C8.2,6.9,7.1,8,7.1,9.3s1.1,2.4,2.4,2.4 S12,10.7,12,9.3S10.9,6.9,9.5,6.9z' fill='currentColor'></path></svg>"
            },
            {
              "label": "Live Chat",
              "url": "javascript:;",
              "svgCode": "<svg version='1.1' xmlns='http://www.w3.org/2000/svg' width='29.1' height='30' viewBox='0 0 29.1 30'><path d='M0,0v22.2h14V30l7.8-7.8h7.3V0H0z M27.9,20.9h-6.6L15.2,27v-6.1h-14V1.2h26.7V20.9z M17.4,15.2H7.6V14h9.8V15.2z M21.5,9.6 h-14V8.4h14V9.6z' fill='currentColor'></path></svg>"
            },
            {
              "label": "Online Booking Payment",
              "url": "javascript:;",
              "svgCode": "<svg version='1.1' xmlns='http://www.w3.org/2000/svg' width='31.6' height='30' viewBox='0 0 31.6 30'><path d='M31.6,7V5.8h-2.7l-0.8,1.7c-0.2-0.2-0.3-0.3-0.4-0.4l-2.2-4.4C25.3,2.2,24.1,0,22.7,0H9.2C7.7,0,6.5,2.2,6.3,2.6L4.1,7 C3.9,7.1,3.7,7.3,3.5,7.5L2.7,5.8H0V7h1.9l0.6,1.3c-1,0.9-2,2.3-2,3.7v9.7h1.2v4.5h5.7v-4.5h8.3v-1.2H9.1l2.8-3.8l-1-2.9h9.7 l-0.9,2.6l1.1,0.4l1.4-4.2H9.3l1.3,3.9l-3,4H7.5H1.8V13l3.6,1.9L6,13.8l-4.1-2.1c0.2-1.4,1.8-2.8,2.8-3.5H27 c1.3,1.3,2.4,2.7,2.8,3.5l-4.2,2.1l0.6,1.1l3.8-1.9V21h1.2V12c0-0.9-1.2-2.3-2.2-3.5L29.7,7H31.6z M6.2,21.7V25H2.9v-3.3H6.2z M7.4,3.2c0.4-0.8,1.3-2,1.8-2h13.5c0.5,0,1.4,1.2,1.8,2L26.3,7H5.5L7.4,3.2z M14.6,15.3L21.1,30l2.7-2.7l2.5,2.5l2.8-2.8l-2.5-2.4 l2.7-2.8L14.6,15.3z M27.4,27.1l-1,1l-2.5-2.5l-2.3,2.3L17,17.7l10.2,4.5l-2.3,2.4L27.4,27.1z' fill='currentColor'></path></svg>"
            },
            {
              "label": "Online Enquiry",
              "url": "form.html",
              "svgCode": "<svg version='1.1' xmlns='http://www.w3.org/2000/svg' width='30.3' height='30' viewBox='0 0 30.3 30'><path d='M30.3,4.6L26.7,1l-3.2,3.2V0H0v30h13.7c0.1,0,9.8-0.1,9.8-9.8l0-8.8L30.3,4.6z M28.3,4.6l-8.5,8.5l-2.4,0.8l0.8-2.4L26.7,3 L28.3,4.6z M15.9,28.3c0.6-0.7,1-1.7,1-2.9v-1.9h1.9c0,0,0,0,0,0c1.3,0,2.3-0.5,3-1.1C21.1,26.3,18.1,27.8,15.9,28.3z M22.1,20 c-0.3,0.5-1.4,2.1-3.2,2.1h0h-3.3v3.3c0,1.8-1.6,2.9-2.1,3.2H1.4V1.4h20.7v4.2L17,10.7l-1.8,5.4l5.4-1.8l1.5-1.5L22.1,20z M13.7,15.7H4.6v-1.4h9.1V15.7z M13.7,11.8H4.6v-1.4h9.1V11.8z M4.6,18.2h11.7v1.4H4.6V18.2z' fill='currentColor'></path></svg>"
            },
            {
              "label": "Price List",
              "url": "price-list.html",
              "svgCode": "<svg version='1.1' xmlns='http://www.w3.org/2000/svg' width='34.3' height='30' viewBox='0 0 34.3 30'><path d='M8.1,12.8h8.6V14H8.1V12.8z M16.6,18.2H8.1v-1.1h8.6V18.2z M8.1,21.4h8.6v1.1H8.1V21.4z M3.8,12.8h2.1V14H3.8V12.8z M3.8,17.1h2.1v1.1H3.8V17.1z M3.8,21.4h2.1v1.1H3.8V21.4z M16.1,3.2h-3.2V2.7c0-0.9-0.6-2.7-2.7-2.7C8,0,7.5,1.8,7.5,2.7v0.5H4.3v1.1L0,4.3V30h20.4V4.3l-4.3,0V3.2z M5.4,4.3h3.2V2.7 c0-0.3,0.1-1.6,1.6-1.6c1.5,0,1.6,1.3,1.6,1.6v1.6H15v3.1H5.4V4.3z M19.2,5.4v23.5H1.1V5.4l3.1,0v3.2h11.8V5.4L19.2,5.4z M30.5,15h-1V9.7h3.7V8.6h-3.7V5.9h-1.1v2.6h-1c-2.1,0-3.8,1.7-3.8,3.8s1.7,3.8,3.8,3.8h1v5.3h-3.7v1.1h3.7v2.6h1.1v-2.6h1 c2.1,0,3.8-1.7,3.8-3.8S32.6,15,30.5,15z M27.3,15c-1.5,0-2.6-1.2-2.6-2.6c0-1.5,1.2-2.6,2.6-2.6h1V15H27.3z M30.5,21.4h-1v-5.3h1 c1.5,0,2.6,1.2,2.6,2.6S32,21.4,30.5,21.4z' fill='currentColor'></path></svg>"
            },
            {
              "label": "Purchase",
              "url": "javascript:;",
              "svgCode": "<svg version='1.1' xmlns='http://www.w3.org/2000/svg' width='30' height='30' viewBox='0 0 30 30'><path d='M27.3,11l0.1-0.3c1,1.1,2,2.5,2,3.3v9.1h-1.1v-1.1h-0.1v-7.1l0.8-0.4l-0.5-1l-0.4,0.2c-0.3-0.7-1.5-2.1-2.8-3.4h0.3V9.1 h-0.9l-1.7-3.5c-0.4-0.8-1.2-1.8-1.6-1.8H8.6c-0.5,0-1.3,1.1-1.6,1.8L5.2,9.1H4.3v1.1h0.2c-0.8,0.6-2.5,2-2.8,3.4l-0.4-0.2l-0.5,1 l0.8,0.4v7.1H1.6v1.1H0.5v-9.1c0-1.3,1-2.6,1.9-3.5L2.7,11l1-0.5L3.3,9.7c0.2-0.2,0.4-0.3,0.5-0.4l2.1-4.2c0.2-0.4,1.3-2.5,2.7-2.5 h12.9c1.4,0,2.4,2,2.7,2.5l2.1,4.2c0.1,0.1,0.3,0.3,0.5,0.5l-0.3,0.7L27.3,11z M7,22.2l0.2-0.3H7V22.2z M23,22.2v-0.3h-0.2L23,22.2z M6.4,23.1H7v4.3H1.6v-4.3v-1.1h0.1H7v0.3L6.4,23.1z M5.9,23.1H2.7v3.2h3.2V23.1z M28.4,21.9v1.1v4.3H23v-4.3h0.6L23,22.2v-0.3H28.4 L28.4,21.9z M27.3,23.1h-3.2v3.2h3.2V23.1z M23.6,23.1H23H7H6.4L7,22.2l0.2-0.3l2.9-3.8l-1.2-3.7h12.3l-1.2,3.7l2.9,3.8l0.2,0.3 L23.6,23.1z M21.4,21.9l-2.7-3.6l0.9-2.8h-9.1l0.9,2.8l-2.7,3.6H21.4z M25.7,10.2V9.1h-0.9H5.2H4.3v1.1h0.2h21H25.7z M24.9,16.6 l3.5-1.7l0.8-0.4l-0.5-1l-0.4,0.2l-3.9,1.9L24.9,16.6z M5.6,15.6l-3.9-2l-0.4-0.2l-0.5,1l0.8,0.4l3.5,1.7L5.6,15.6z M30,9.1V8h-2.5 l-0.9,1.8l-0.3,0.7l1,0.5l0.1-0.3l0.8-1.5H30z M3.7,10.5L3.3,9.7L2.5,8H0v1.1h1.8l0.6,1.3L2.7,11L3.7,10.5z' fill='currentColor'></path></svg>"
            },
            {
              "label": "Roadside Assistance",
              "url": "javascript:;",
              "svgCode": "<svg version='1.1' xmlns='http://www.w3.org/2000/svg' width='19.6' height='30' viewBox='0 0 19.6 30'><path d='M9.8,24.2c-0.7,0-1.2,0.5-1.2,1.2s0.5,1.2,1.2,1.2c0.7,0,1.2-0.5,1.2-1.2S10.5,24.2,9.8,24.2z M9.8,25.4 C9.8,25.3,9.8,25.3,9.8,25.4C9.8,25.3,9.9,25.3,9.8,25.4C9.9,25.4,9.8,25.4,9.8,25.4z M16.7,0H2.9C0.6,0,0,1.9,0,2.9v24.2c0,1,0.6,2.9,2.9,2.9h13.8c2.3,0,2.9-1.9,2.9-2.9V2.9C19.6,1.9,19,0,16.7,0z M18.4,27.1 c0,0.3-0.1,1.7-1.7,1.7H2.9c-1.6,0-1.7-1.4-1.7-1.7V2.9c0-0.3,0.1-1.7,1.7-1.7h13.8c1.6,0,1.7,1.4,1.7,1.7V27.1z M3.5,21.9h12.7v-15H3.5V21.9z M4.7,8.1H15v12.6H4.7V8.1z M14.4,4.7H5.2V3.5h9.2V4.7z' fill='currentColor'></path></svg>"
            },
            {
              "label": "Service Centres",
              "url": "javascript:;",
              "svgCode": "<svg version='1.1' xmlns='http://www.w3.org/2000/svg' width='19.1' height='30' viewBox='0 0 19.1 30'><path d='M9.5,30l-0.5-0.8c-0.7-1.3-1.5-2.5-2.1-3.6C2.9,18.8,0,13.9,0,9.5C0,4.3,4.3,0,9.5,0s9.5,4.3,9.5,9.5 c0,4.4-2.9,9.3-6.9,16.1c-0.7,1.2-1.4,2.4-2.1,3.6L9.5,30z M9.5,1.1c-4.7,0-8.5,3.8-8.5,8.5c0,4.1,2.8,8.9,6.8,15.5 C8.4,26,9,26.9,9.5,27.9c0.6-1,1.1-1.9,1.7-2.9C15.2,18.4,18,13.6,18,9.5C18,4.9,14.2,1.1,9.5,1.1z M9.5,13.1 c-2.1,0-3.8-1.7-3.8-3.8s1.7-3.8,3.8-3.8s3.8,1.7,3.8,3.8S11.6,13.1,9.5,13.1z M9.5,6.6C8,6.6,6.8,7.8,6.8,9.3S8,12,9.5,12 s2.7-1.2,2.7-2.7S11,6.6,9.5,6.6z' fill='currentColor'></path></svg>"
            },
            {
              "label": "Stay Updated",
              "url": "javascript:;",
              "svgCode": "<svg version='1.1' xmlns='http://www.w3.org/2000/svg' width='25.6' height='30' viewBox='0 0 25.6 30'><path d='M25.6,22.5l-3.3-3.3v-6.4c0-0.1-0.1-7.5-6.7-9.1V2.8c0-1-0.6-2.8-2.8-2.8S10,1.8,10,2.8v0.9c-6.6,1.7-6.7,9-6.7,9.1v6.4 L0,22.5v3h7.8c0.2,1.8,1.4,4.4,5,4.4c2.6,0,3.8-1.4,4.4-2.5c0.4-0.7,0.5-1.4,0.6-1.9h7.8V22.5z M11.2,3.3V2.8c0-0.2,0-1.6,1.6-1.6 c1.5,0,1.6,1.4,1.6,1.6v0.5h-1.6c0,0,0,0,0,0s0,0,0,0H11.2z M12.8,28.8c-2.9,0-3.6-2.2-3.8-3.3h7.6C16.4,26.6,15.7,28.8,12.8,28.8z M24.4,24.4h-6.6h-10H1.2V23l3.3-3.3v-6.9c0-0.3,0.1-8.3,8.3-8.3h0c8.2,0,8.3,7.9,8.3,8.3v6.9l3.3,3.3V24.4z' fill='currentColor'></path></svg>"
            },
            {
              "label": "Trade-in",
              "url": "javascript:;",
              "svgCode": "<svg version='1.1' xmlns='http://www.w3.org/2000/svg' width='30' y='30' viewBox='0 0 30 30'><path d='M23.5,6.5l3.6-3.6l1.2,4.8L23.5,6.5z M1.7,22.3l1.2,4.8l3.6-3.6L1.7,22.3z M5,5c2.7-2.7,6.2-4.1,10-4.1 c3.8,0,7.3,1.5,10,4.1l0.6-0.6C22.8,1.6,19,0,15,0S7.2,1.6,4.4,4.4c-3.4,3.4-5,8.2-4.2,12.9l0.9-0.1C0.4,12.8,1.9,8.2,5,5z M29.8,12.7l-0.9,0.1c0.7,4.4-0.8,9-4,12.2c-5.5,5.5-14.4,5.5-19.9,0l-0.6,0.6C7.3,28.5,11.2,30,15,30c3.8,0,7.7-1.5,10.6-4.4 C29,22.2,30.6,17.4,29.8,12.7z M22,12.8l0.1-0.3c0.5,0.6,0.9,1.3,0.9,2v4.7H22v-1h-0.1v-3l0.6-0.2l-0.3-0.9l-0.4,0.1c-0.2-0.8-1-1.6-1.5-2h0.2v-1h-0.8 L19.2,10c0,0-0.4-1.1-1.4-1.1h-5.6c-1,0-1.4,1-1.4,1.1l0,0.1l-0.6,1.1H9.4v1h0.2c-0.5,0.4-1.3,1.2-1.5,2l-0.4-0.1L7.4,15l0.6,0.2v3 H8v1H7.1v-4.7c0-0.7,0.4-1.5,0.9-2L8,12.8L9,12.5l-0.3-0.8C8.8,11.6,9,11.5,9,11.4l0.9-1.7C10,9.4,10.6,8,12.2,8h5.6 c1.6,0,2.2,1.4,2.3,1.7l0.9,1.7c0.1,0.1,0.2,0.2,0.3,0.3L21,12.5L22,12.8z M10.3,21H9v-1.8h0.9l0.7-1H8.1H8v1V22h3.3v-2.8h-1V21z M21.9,18.2h-2.5l0.7,1H21V21h-1.3v-1.8h-1V22H22v-2.8L21.9,18.2L21.9,18.2z M19.7,19.2h-1h-7.4h-1H9.9l0.7-1l1.6-2.3l-1.2-2.3H19 l-1.2,2.3l1.6,2.3l0.7,1H19.7z M18.3,18.2L16.8,16l0.7-1.4h-4.9l0.7,1.4l-1.5,2.3H18.3z M20.6,12.2v-1h-0.8h-9.6H9.4v1h0.2h10.8 H20.6z M23.4,11.8v-1h-1.7l-0.3,0.9L21,12.5l0.9,0.3l0.1-0.3l0.3-0.8H23.4z M9,12.5l-0.3-0.8l-0.3-0.9H6.6v1h1l0.3,0.8L8,12.8 L9,12.5z M10.5,15l-2.4-0.8l-0.4-0.1L7.4,15l0.6,0.2l2.1,0.7L10.5,15z M19.8,15.9l2.1-0.7l0.6-0.2l-0.3-0.9l-0.4,0.1L19.5,15 L19.8,15.9z' fill='currentColor'></path></svg>"
            },
            {
              "label": "Email",
              "url": "javascript:;",
              "svgCode": "<svg version='1.1' xmlns='http://www.w3.org/2000/svg' width='30' height='30' viewBox='0 0 30 30'><path d='M0,4v4.7V26h30V8.6V4H0z M0.9,4.9h28.1v3.3L15,19.7L0.9,8.2V4.9z M29.1,25.1H0.9V9.4L15,20.9L29.1,9.4V25.1z' fill='currentColor'></path></svg>"
            },
            {
              "label": "Print",
              "url": "javascript:;",
              "svgCode": "<svg version='1.1' xmlns='http://www.w3.org/2000/svg' width='27.6' height='30' viewBox='0 0 27.6 30'><path d='M27.6,10.1c0-1-0.6-2.9-2.9-2.9h-2V0H4.9v7.3h-2C0.6,7.3,0,9.2,0,10.1l0,11.4h4.9V30h17.9v-8.5h4.9L27.6,10.1z M5.7,0.9 h16.1v6.4H5.7V0.9z M21.9,29.1H5.7V16.6h16.1V29.1z M26.7,20.6h-4v-4.9H4.9v4.9h-4l0-10.5c0-0.2,0.1-2,2-2h2v0h17.9v0h2 c1.9,0,2,1.8,2,2L26.7,20.6z' fill='currentColor'></path><rect x='8.9' y='20.6' width='9.7' height='0.9' fill='currentColor'></rect><rect x='8.9' y='24.3' width='6.1' height='0.9' fill='currentColor'></rect><rect x='21.9' y='10.1' width='0.9' height='2.4' fill='currentColor'></rect></svg>"
            },
            {
              "label": "Share",
              "url": "javascript:;",
              "svgCode": "<svg version='1.1' xmlns='http://www.w3.org/2000/svg' width='28.7' height='30' viewBox='0 0 28.7 30'><path d='M8.9,13.5c-0.2-0.4-0.4-0.8-0.6-1.2l11.5-6.2C20,6.6,20.2,7,20.4,7.3L8.9,13.5z M8.9,16.5c-0.2,0.4-0.4,0.8-0.6,1.2 l11.5,6.2c0.2-0.4,0.4-0.8,0.6-1.2L8.9,16.5z M8.9,16.5c-0.2,0.4-0.4,0.8-0.6,1.2c-0.8,1.1-2.2,1.9-3.7,1.9C2.1,19.6,0,17.5,0,15 s2.1-4.6,4.6-4.6c1.5,0,2.8,0.7,3.7,1.9c0.3,0.4,0.5,0.8,0.6,1.2c0.2,0.5,0.3,1,0.3,1.5S9.1,16,8.9,16.5z M4.6,18.2 c1.8,0,3.2-1.4,3.2-3.2s-1.4-3.2-3.2-3.2S1.4,13.2,1.4,15S2.8,18.2,4.6,18.2z M20.4,7.3C20.2,7,20,6.6,19.8,6.1 c-0.2-0.5-0.3-1-0.3-1.5c0-2.5,2.1-4.6,4.6-4.6c2.5,0,4.6,2.1,4.6,4.6s-2.1,4.6-4.6,4.6C22.6,9.2,21.3,8.4,20.4,7.3z M24.1,7.8 c1.8,0,3.2-1.4,3.2-3.2s-1.4-3.2-3.2-3.2c-1.8,0-3.2,1.4-3.2,3.2S22.3,7.8,24.1,7.8z M28.7,25.4c0,2.5-2.1,4.6-4.6,4.6 c-2.5,0-4.6-2.1-4.6-4.6c0-0.5,0.1-1,0.3-1.5c0.2-0.4,0.4-0.8,0.6-1.2c0.8-1.1,2.2-1.9,3.7-1.9C26.6,20.8,28.7,22.9,28.7,25.4z M27.3,25.4c0-1.8-1.4-3.2-3.2-3.2c-1.8,0-3.2,1.4-3.2,3.2c0,1.8,1.4,3.2,3.2,3.2C25.9,28.6,27.3,27.2,27.3,25.4z' fill='currentColor'></path></svg>"
            }
          ];
        </script>
        <!--#end of JSON data -->

        <!-- High Value Action -->
        <nav id="cpl--hva" v-if="hvas.length">
          <ul class="cpl-hva">
            <li v-for="hva in hvas.slice(0, 3)">
              <a class="cpl-unstyled" :href="hva.url">
                <div v-html="hva.svgCode"></div>
                <div>
                  <h6>
                    <span v-html="hva.label"></span>
                  </h6>
                </div>
              </a>
            </li>
          </ul>
        </nav>
        <!--#end of High Value Action -->

      </main>
      <script type="text/javascript">
        if (document.querySelector('.cloudimage-360')) {
          window.CI360 = { notInitOnLoad: true };
        }
      </script>
    </div>
    <script type="text/javascript" src="safety_sense/assets/js/vendors.js"></script><script type="text/javascript" src="safety_sense/assets/js/global.js"></script><script type="text/javascript" src="safety_sense/assets/js/safety-sense.js"></script>
@endsection
