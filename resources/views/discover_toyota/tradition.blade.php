@extends('layouts.main')

@section('title', "TOYOTA Tradition | About Toyota Myanmar")

@section('content')
    <div class="bg-toyota-light">
        <main class="container-fluid bg-toyota-light px-sm-5 px-3 pb-4">
            @include('components.breadcrumb', ['data' => [['route' => 'discover.about', 'name' => 'About Toyota Myanmar']]])
                <header class="container-fluid py-4">
                    <h2 class="fw-bold" data-aos="zoom-out" data-aos-duration="1000">TOYOTA Tradition</h2>
                </header>

            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8">
                        <img src="{{asset('img/discover-toyota/toyota_tradition.png')}}" alt="" width="100%" height="90%" data-aos="zoom-out" data-aos-duration="1500">
                    </div>
                    <div class="col-md-4 py-4">
                        <h4 class="text-dark fw-bold">Quality from day one</h4>
                        <span class="container py-2"></span>
                        <p class="text-justify">Throughout our almost 100 years of existence – even from the days of the original Toyoda family weaving business and before we began making vehicles – we have built a global reputation for delivering high-quality products and services. It’s something we are committed to maintaining and extending. Moreover, from day one, Toyota has steadfastly carried out quality assurance activities, resulting in top ratings from our customers. We constantly improve our knowledge, processes, products and services to deliver ever-higher quality levels in everything we do.</p>
                    </div>
                </div>

            </div>
            <div class="container-fluid">
                <div class="row dir-rtl">
                    <div class="col-md-8">
                        <img src="{{asset('img/discover-toyota/tradition_quality.png')}}" alt="" class="w-100" height="90%" data-aos="zoom-out" data-aos-duration="1500">
                    </div>
                    <div class="col-md-4 py-4">
                        <h4 class="text-dark fw-bold text-start text-md-end">Quality assurance principles</h4>
                        <p class="mt-3" style="direction: ltr !important">The core principles behind our quality assurance system, including ‘Customer First’, ‘Quality First’ and ‘Genchi Genbutsu’ (Go & See at the scene), were established when the company was founded. Since then, these principles have been passed on and inform every organisational level of Toyota today, from the shop floor to executive management. We are proud of these traditions, which we continue to develop and improve to meet new challenges. In recent times our Total Quality Control process has helped us continuously improve the quality and reliability of our vehicles.</p>
                    </div>
                </div>
            </div>
            <div class="container-fluid mt-n5">
                <div class="row">
                    <div class="col-md-8">
                        <img src="{{asset('img/discover-toyota/tradition_customer.png')}}" alt="" width="100%" height="90%" data-aos="zoom-out" data-aos-duration="1500">
                    </div>

                    <div class="col-md-4 py-4">
                        <h4 class="text-dark fw-bold">Quality means being customer-oriented</h4>
                        <span class="container"></span>
                        <p>Back in 1969, Toyota in Japan had already introduced a vehicle recall system. At that time we used a series of high-profile newspaper advertisement features to show how customers are our first concern in ensuring quality in every part of our business. This commitment is just as relevant today as it was then. The intelligence gathering and analysis of customer feedback helps us with planning new products, raising quality standards and improving the way we work and the services we provide.</p>
                    </div>
                </div>
            </div>
        </main>
    </div>
@endsection
