@extends('layouts.main')

@section('title', "Emblem | About Toyota Myanmar")

@section('content')
    <main class="container-fluid bg-toyota-light px-sm-5 px-3 pb-4">
        @include('components.breadcrumb',
                ['data' => [['route' => 'discover.about',
                            'name' => 'About Toyota Myanmar']]])
        <div class="container">
            {{-- Emblem --}}
            <header class="py-sm-4 py-3">
                <h2 class="fw-bold" data-aos="fade-right" data-aos-duration="1000">Emblem</h2>
            </header>

            <p class="toyota-paragraph" data-aos="fade-up" data-aos-duration="1500">Trace the evolution of both the Toyota name and logos in vehicles, from the original 1936 Toyoda mark to the 1989 ovals steeped in symbolism you see today.</p>

            {{-- Idas and Meaning --}}
            <h3 class="fw-bold py-sm-4 py-2" data-aos="fade-right" data-aos-duration="1500">Ideas and Meanings</h3>

            <p class="toyota-paragraph" data-aos="fade-up" data-aos-duration="2000">The current Toyota mark debuted in October 1989 to commemorate the 50th anniversary of the company. The development of the current logo took about five years, as it was necessary to develop a suitable logo to fit the rising profile of Toyota in foreign countries. There were two motivating factors in designing the logo: recognition from a distance announcing the "arrival of Toyota," and a logo with a strong visual impact that stands out from other automobiles.</p>

            <div class="emblem-img m-2">
                <img src="{{asset('img/discover-toyota/toyota_emblem.png')}}" alt="" width="100%" data-aos="fade-up" data-aos-duration="2000">
            </div>

            <p class="toyota-paragraph py-sm-4 py-2" data-aos="fade-up" data-aos-duration="2500">
                There are three ovals in the new logo that are combined in a horizontally symmetrical configuration. The two perpendicular ovals inside the larger oval represent the heart of the customer and the heart of the company. They are overlapped to represent a mutually beneficial relationship and trust between each other.
            </p>

            <p class="toyota-paragraph py-sm-4 py-2" data-aos="fade-up" data-aos-duration="2500">
                The overlapping of the two perpendicular ovals inside the outer oval symbolize "T" for Toyota, as well as a steering wheel representing the vehicle itself. The outer oval symbolizes the world that embraces Toyota. Each oval is contoured with different stroke thicknesses, similar to the brush art in Japanese culture.
            </p>

            <p class="toyota-paragraph" data-aos="fade-up" data-aos-duration="1000">The space in the background within the logo exhibits the infinite values that Toyota wishes to convey to its customers: superb quality, value beyond expectation, joy of driving, innovation, and integrity in safety, the environment, and social responsibility.</p>

            {{-- Power Of Brand --}}
            <h3 class="fw-bold py-sm-4 py-2" data-aos="fade-right" data-aos-duration="1000">Power of the Brand</h3>

            <div class="emblem-img d-flex d-md-none">
                <img src="{{asset('img/discover-toyota/power_of_brand.png')}}" alt="" width="100%" data-aos="fade-up" data-aos-duration="2000">
            </div>

            <div class="row">
                <p class="toyota-paragraph py-2 col-12 col-md-7 col-lg-8 col-xl-9 d-flex align-items-center" data-aos="fade-up" data-aos-duration="1500">
                    This newly designed Toyota logo made its debut on the Celsior luxury model in October 1989, and soon after, more models were proudly displaying the new emblem. An unprecedented logo of its time and clearly perceptible both head-on and in a rear view mirror, it has quickly become widely recognized as the symbol of Toyota.
                </p>

                <div class="col-md-5 col-lg-4 col-xl-3 d-none d-md-flex align-items-center">
                    <img src="{{asset('img/discover-toyota/power_of_brand.png')}}" alt="" width="100%" data-aos="fade-left" data-aos-duration="1000">
                </div>
            </div>

            {{-- To Toyota --}}
            <h3 class="fw-bold py-sm-4 py-2" data-aos="fade-right" data-aos-duration="1000">From "TOYODA" to "TOYOTA"</h3>

            <div class="emblem-img d-flex d-md-none">
                <img src="{{asset('img/discover-toyota/toyoda_to_toyota.png')}}" alt="" width="100%" data-aos="fade-up" data-aos-duration="2000">
            </div>

            <div class="row">
                <p class="toyota-paragraph py-2 col-12 col-md-7 col-lg-8 col-xl-9 d-flex align-items-center" data-aos="fade-up" data-aos-duration="1500">
                    The "Toyota" name stems from the family name of the founder, Sakichi "Toyoda," with early vehicles produced by the company originally sold with a Toyoda emblem. In 1936, the company ran a public competition to design a new logo, leading to a change in the brand name to the current Toyota.
                </p>

                <div class="col-md-5 col-lg-4 col-xl-3 d-none d-md-flex align-items-center">
                    <img src="{{asset('img/discover-toyota/toyoda_to_toyota.png')}}" alt="" width="100%" data-aos="fade-left" data-aos-duration="1000">
                </div>
            </div>

            <div class="emblem-img d-flex d-md-none">
                <img src="{{asset('img/discover-toyota/toyoda_emblem.png')}}" alt="" width="100%" data-aos="fade-up" data-aos-duration="2000">
            </div>

            <div class="row">
                <p class="toyota-paragraph py-sm-4 py-2 col-12 col-md-7 col-lg-8 col-xl-9" data-aos="fade-up" data-aos-duration="2000">
                    Why the change? First, "Toyota" represents a voiceless consonant sound in Japanese, which is considered "clearer" than voiced consonants like in "Toyoda." The number of strokes to write Japanese characters, called jikaku, is also a factor. Eight strokes is believed to be connected to wealth and good fortune, and "Toyota" (トヨタ) contains exactly eight strokes. The change also signified the expansion of a small independent company to a larger corporate enterprise.
                </p>

                <div class="col-md-5 col-lg-4 col-xl-3 d-none d-md-flex align-items-center">
                    <img src="{{asset('img/discover-toyota/toyoda_emblem.png')}}" alt="" width="100%" data-aos="fade-left" data-aos-duration="1000">
                </div>
            </div>
        </div>
    </main>
@endsection
