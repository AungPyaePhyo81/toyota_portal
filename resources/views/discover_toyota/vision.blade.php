@extends('layouts.main')

@section('title', "Vision & Philosophy | About Toyota Myanmar")

@section('content')
    <div class="bg-toyota-light">
    <main class="container-fluid bg-toyota-light px-sm-5 px-3 pb-4">
        @include('components.breadcrumb',
                ['data' => [['route' => 'discover.about',
                            'name' => 'About Toyota Myanmar']]])
        <header class="py-4 container">
            <h2 class="fw-bold" data-aos="zoom-out" data-aos-duration="1000">Vision & Philosophy</h2>
        </header>
    </main>
    <article class="w-100 d-flex justify-content-center">
        <img src="{{asset('img/discover-toyota/vision_banner.png')}}" alt="" height="100%" data-aos="fade-up" data-aos-duration="1000">
    </article>
    <div class="container py-4">
        <h3 class="fw-bold" data-aos="zoom-out" data-aos-duration="1500">Founding Spirit</h3><br>
        <h3 data-aos="fade-up" data-aos-duration="1000">Toyoda Principles: Five Main Principles of Founder Sakichi Toyoda</h3><br>
        <div class="row">
            <div class="col-12 col-xl-3 d-flex flex-column align-items-center">
                <img src="{{asset('img/discover-toyota/toyoda.png')}}" alt=""  data-aos="fade-up" data-aos-duration="1500">
                <h5 data-aos="fade-up" data-aos-duration="1500">Sakichi Toyoda, Founder of Toyota</h5>
            </div>
            <div class="col-12 col-xl-9 p-5">
                <ul class="fs-3 d-flex flex-column justify-content-center h-100 gap-3" style="list-style: square">
                    <li data-aos="fade-up" data-aos-duration="1000">Always be faithful to your duties, thereby contributing to the company and to the overall good.</li>
                    <li data-aos="fade-up" data-aos-duration="1500">Always be studious and creative, striving to stay ahead of the times.</li>
                    <li data-aos="fade-up" data-aos-duration="2000">Always be practical and avoid frivolousness.</li>
                    <li data-aos="fade-up" data-aos-duration="2500">Always strive to build a homelike atmosphere at work that is warm and friendly.</li>
                    <li data-aos="fade-up" data-aos-duration="3000">Always have respect for spiritual matters, and remember to be grateful at all times.</li>
                </ul>
            </div>
        </div>
        <br>
        <hr>
        <h3 class="fw-bold mt-5" data-aos="zoom-out" data-aos-duration="1000">Mission</h3><br>
        <div class="w-100 d-flex align-items-center flex-column">
            {{-- <img src="{{asset('img/discover-toyota/toyota_mission.png')}}" alt="" height="100%"> --}}
            <h1 class="toyota-h1" data-aos="fade-up" data-aos-duration="1000">Producing Happiness for All</h1>
            <ul class="list-unstyled toyota-paragraph py-4">
                <li class="py-1" data-aos="fade-up" data-aos-duration="1300">We make the happiness of others our first priority.</li>
                <li class="py-1" data-aos="fade-up" data-aos-duration="1600">We make better products more affordable.</li>
                <li class="py-1" data-aos="fade-up" data-aos-duration="1900">We value every second and every cent.</li>
                <li class="py-1" data-aos="fade-up" data-aos-duration="2200">We give all our effort and offer all our ingenuity.</li>
                <li class="py-1" data-aos="fade-up" data-aos-duration="2500">We look forward, not backward.</li>
                <li class="py-1" data-aos="fade-up" data-aos-duration="2800">We believe the impossible is possible.</li>
            </ul>
        </div>
        <hr>
        <h3 class="fw-bold mt-5" data-aos="zoom-out" data-aos-duration="1000">Vision</h3><br>
        <div class="w-100 d-flex align-items-center flex-column">
            {{-- <img src="{{asset('img/discover-toyota/toyota_vision.png')}}" alt="" height="100%"> --}}
            <h1 class="toyota-h1" data-aos="fade-up" data-aos-duration="1000">Creating Mobility for All</h1>
            <ul class="list-unstyled toyota-paragraph py-4">
                <li class="py-1" data-aos="fade-up" data-aos-duration="1300">In a diverse and uncertain world,</li>
                <li class="py-1" data-aos="fade-up" data-aos-duration="1600">Toyota strives to raise the quality</li>
                <li class="py-1" data-aos="fade-up" data-aos-duration="1900">and availability of mobility. We wish</li>
                <li class="py-1" data-aos="fade-up" data-aos-duration="2200">to create new possibilities for all relationship</li>
                <li class="py-1" data-aos="fade-up" data-aos-duration="2500">with our planet.</li>
            </ul>
        </div>
        <hr>
        <h3 class="fw-bold mt-5" data-aos="zoom-out" data-aos-duration="1000">Value</h3><br>
        <div class="w-100 d-flex align-items-center flex-column">
            {{-- <img src="{{asset('img/discover-toyota/toyota_value.png')}}" alt="" height="100%"> --}}
            <h1 class="toyota-h1" data-aos="fade-up" data-aos-duration="1000">The Toyota Way</h1>
            <ul class="list-unstyled toyota-paragraph py-4">
                <li class="py-1" data-aos="fade-up" data-aos-duration="1300">Combining software, hardware and partnerships</li>
                <li class="py-1" data-aos="fade-up" data-aos-duration="1600">to create unique value that comes from the Toyota Way</li>
            </ul>
            <div class="row justify-content-center">
                <div class="col-12 col-sm-6 col-md-4 border-start border-end text-center">
                    <h2 data-aos="fade-up" data-aos-duration="1600">Software</h2>
                    <p data-aos="fade-up" data-aos-duration="1900" class="p-4">
                        Applying imagination to improve society
                        through a people-first design philosophy.
                        Paracticing Genchi Genbutsu
                        to understand operations at their essence
                    </p>
                </div>
                <div class="col-12 col-sm-6 col-md-4 border-start border-end text-center">
                    <h2 data-aos="fade-up" data-aos-duration="1900">Hardware</h2>
                    <p data-aos="fade-up" data-aos-duration="2200" class="p-4">
                        Creating a physical platform to enable
                        the mobility of people and things.
                        A flexible system that changes
                        with the software
                    </p>
                </div>
                <div class="col-12 col-sm-6 col-md-4 border-start border-end text-center">
                    <h2 data-aos="fade-up" data-aos-duration="2200">Partnerships</h2>
                    <p data-aos="fade-up" data-aos-duration="2500" class="p-4">
                        Expanding our abilities by uniting
                        the strength of partners, communities,
                        customers and employees to produce
                        mobility and happiness for all
                    </p>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
