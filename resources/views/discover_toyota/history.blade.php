@extends('layouts.main')

@section('title', "History of TOYOTA | About Toyota Myanmar")

@section('content')
    <div class="bg-toyota-light">
        <main class="container-fluid bg-toyota-light px-sm-5 px-3 pb-4">
            @include('components.breadcrumb',
                    ['data' => [['route' => 'discover.about',
                                'name' => 'About Toyota Myanmar']]])
            <header class="py-4">
                <h2 class="fw-bold" data-aos="zoom-out" data-aos-duration="1000">History of TOYOTA</h2>
            </header>
        </main>
        <article class="overflow-hidden">
            <img src="{{asset('img/discover-toyota/history_banner.png')}}" alt="" width="100%" data-aos="zoom-out" data-aos-duration="1500">
        </article>
        <div class="container py-4">
            <p class="toyota-paragraph lh-lg" data-aos="fade-up" data-aos-duration="1000">
                Ever since its founding, Toyota has sought to contribute to a more prosperous society through the manufacture of automobiles, operating its business with a focus on vehicle production and sales. <br>
                To celebrate its 75th anniversary, the company has compiled 75 Years of Toyota. Learn more about the company's progress over the last three-quarter century.
                <a href="#" class="animated-link-reverse text-danger" style="--line-color: var(--danger)">7 years of TOYOTA</a>
            </p>
            <img src="{{asset('img/discover-toyota/history_detial.png')}}" alt="" width="100%" class="my-5" data-aos="fade-up" data-aos-duration="1500">
        </div>
    </div>
@endsection
