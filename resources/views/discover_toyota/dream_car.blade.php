@extends('layouts.main')

@section('title', "Toyota Dream Car Art Contest")

@section('content')
    <main class="container-fluid px-sm-5 px-3 pb-4">
        @include('components.breadcrumb')
        <header class="py-4">
            <h2 class="fw-bold" data-aos="zoom-in" data-aos-duration="1000">Toyota Dream Car Art Contest</h2>
        </header>
    </main>

    <article class="overflow-hidden">
        <img src="{{asset('img/discover-toyota/toyota_dream_car.png')}}" alt="" width="100%" data-aos="zoom-out" data-aos-duration="1000">
    </article>

    <div class="container py-4">
        <br>
        <h3 class="fw-bold" data-aos="fade-right" data-aos-duration="1000">What Is TOYOTA DREAM CAR ART CONTEST ?</h3>
        <p class="toyota-paragraph" data-aos="fade-up" data-aos-duration="1000">Toyota Motor Corporation organizes 14th (2021) Toyota Dream Car Art Contest. This contest is a drawing contest which offers children 15 years of age and below to participate around the world. <br>
            The contest invites children from all around the world to share ideas about the future of mobility by drawing their dream cars. <br>
            Since 2004, Dream Car Art Contest has been conducted as a part of Toyota Motor Corporation’s social contribution initiatives. Now, more than 80 countries participate in this contest. In the previous contest in Myanmar, there were close to 3000 artwork submissions. <br>
            Toyota Dream Car Art Contest in Myanmar will be launched on 5th December 2020. ​Drawing entries can be submitted by 25th January 2021. Participants have a chance to win Apple iPad8 128GB (4G) as the Grand Prizes and there will be other interesting prizes. ​<br>
            <br>
            Toyota started the new ‘Start Your Impossible’ (SYI) initiative to support the creation of a more inclusive and sustainable society in which everyone can challenge their impossible and enjoy the freedom of mobility. SYI is also inspired by Toyota’s Worldwide partnership with the International Olympic Committee and the International Paralympic Committee and the fighting spirit of Olympic and Paralympic athletes who are constantly challenging their “impossible”. <br>
            <br>
            Toyota Dream Car Art Contest embodies the SYI spirit whereby it inspires children’s creativity and imagination to achieve their “impossible” goals especially in arts. Toyota also aims to support the creation of a more inclusive society by organizing DCAC drawing workshops in different types of schools. <br>
        </p>
        <br><br>
        <h3 class="fw-bold" data-aos="fade-right" data-aos-duration="1000">PRIZE FOR NATIONAL CONTEST WINNERS</h3>
        <div class="row bg-toyota-light px-2 px-sm-5 py-2 my-4" data-aos="fade-up" data-aos-duration="1000">
            <div class="col-12 col-sm-9 d-flex flex-column justify-content-center">
                <h4 class="text-danger fw-bold">Grand Prize</h4>
                <h4 class="text-dark fw-bold">Apple iPad8 128 GB (4G)</h4>
                <h4 class="text-dark fw-bold">Chance to win the other attractive prizes</h4>
                <p class="toyota-paragraph">Grand Prize( Apple iPad 8) ဆုအပြင် နောက်ထပ် အဖိုးတန်ဆုမဲပေါင်းများစွာ ပိုင်ဆိုင်နိုင်မည့်အခွင့်အရေး</p>
            </div>
            <div class="col-12 col-sm-3 d-flex justify-content-center">
                <img src="{{asset('img/discover-toyota/ipad.png')}}" alt="">
            </div>
        </div>
        <div class="row gap-3 gap-sm-5">
            <div class="col bg-toyota-light d-flex flex-column justify-content-center align-items-center" data-aos="fade-up" data-aos-duration="1500">
                <img src="{{asset('img/discover-toyota/laptop.png')}}" alt="1st prize" class="my-3">
                <h4 class="text-danger fw-bold">1<sup>st</sup> Prize</h4>
                <h5 class="text-dark">Lenovo V 130 </h5>
                <h5 class="text-dark">Celeron 14”</h5>
            </div>
            <div class="col bg-toyota-light d-flex flex-column justify-content-center align-items-center" data-aos="fade-up" data-aos-duration="2000">
                <img src="{{asset('img/discover-toyota/tablets.png')}}" alt="2nd prize" class="my-3">
                <h4 class="text-danger fw-bold">2<sup>nd</sup> Prize</h4>
                <h5 class="text-dark">Lenovo V 130 </h5>
                <h5 class="text-dark">Celeron 14”</h5>
            </div>
            <div class="col bg-toyota-light d-flex flex-column justify-content-center align-items-center" data-aos="fade-up" data-aos-duration="2500">
                <img src="{{asset('img/discover-toyota/speaker.png')}}" alt="3rd prize" class="my-3">
                <h4 class="text-danger fw-bold">3<sup>rd</sup> Prize</h4>
                <h5 class="text-dark">JBL Clip3</h5>
                <h5 class="text-dark">Portable Bluetooth Speaker</h5>
            </div>
        </div>
        <br>
        <p class="toyota-paragraph mb-5" data-aos="fade-up" data-aos-duration="1000">Awards will be given to top 3 winners from each of the 3 age categories. (Total 9 awards)</p>

        <div class="container">
            <div class="row">
            <div class="col">
                <p class="border-bottom border-secondary py-3">
                    <a href="{{route('dreamcart.2014Cart')}}" class="text-decoration-none link-dark">
                        <span class="text-danger">2014</span> Toyota Dream Car Art Content
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-right" viewBox="0 0 16 16" style="color: red;">
                        <path fill-rule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z"/>
                        </svg>
                    </a>
                </p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <p class="border-bottom border-secondary py-3">
                        <a href="{{route('dreamcart.2015Cart')}}" class="text-decoration-none link-dark">
                            <span class="text-danger">2015</span> Toyota Dream Car Art Content
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-right" viewBox="0 0 16 16" style="color: red;">
                            <path fill-rule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z"/>
                            </svg>
                        </a>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <p class="border-bottom border-secondary py-3">
                        <a href="{{route('dreamcart.2016Cart')}}" class="text-decoration-none link-dark">
                            <span class="text-danger">2016</span> Toyota Dream Car Art Content
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-right" viewBox="0 0 16 16" style="color: red;">
                            <path fill-rule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z"/>
                            </svg>
                        </a>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <p class="border-bottom border-secondary py-3">
                        <a href="{{route('dreamcart.2017Cart')}}" class="text-decoration-none link-dark">
                            <span class="text-danger">2017</span> Toyota Dream Car Art Content
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-right" viewBox="0 0 16 16" style="color: red;">
                            <path fill-rule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z"/>
                            </svg>
                        </a>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <p class="border-bottom border-secondary py-3">
                        <a href="{{route('dreamcart.2018Cart')}}" class="text-decoration-none link-dark">
                            <span class="text-danger">2018</span> Toyota Dream Car Art Content
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-right" viewBox="0 0 16 16" style="color: red;">
                            <path fill-rule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z"/>
                            </svg>
                        </a>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <p class="border-bottom border-secondary py-3">
                        <a href="{{route('dreamcart.2019Cart')}}" class="text-decoration-none link-dark">
                            <span class="text-danger">2019</span> Toyota Dream Car Art Content
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-right" viewBox="0 0 16 16" style="color: red;">
                            <path fill-rule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z"/>
                            </svg>
                        </a>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <p class="border-bottom border-secondary py-3">
                        <a href="{{route('dreamcart.2020Cart')}}" class="text-decoration-none link-dark">
                            <span class="text-danger">2020</span> Toyota Dream Car Art Content
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-right" viewBox="0 0 16 16" style="color: red;">
                            <path fill-rule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z"/>
                            </svg>
                        </a>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <p>
                        <a href="{{route('dreamcart.2021Cart')}}" class="text-decoration-none link-dark">
                            <span class="text-danger">2021</span> Toyota Dream Car Art Content
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-right" viewBox="0 0 16 16" style="color: red;">
                            <path fill-rule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z"/>
                            </svg>
                        </a>
                    </p>
                </div>
            </div>
        </div>

        <div class="row my-5" data-aos="fade-up" data-aos-duration="1000">
            <div class="col-6 col-sm-4 p-1">
                <img src="{{asset('img/temp/kid1.png')}}" alt="" width="100%">
            </div>
            <div class="col-6 col-sm-4 p-1">
                <img src="{{asset('img/temp/kid2.png')}}" alt="" width="100%">
            </div>
            <div class="col-6 col-sm-4 p-1">
                <img src="{{asset('img/temp/kid3.png')}}" alt="" width="100%">
            </div>
            <div class="col-6 col-sm-4 p-1">
                <img src="{{asset('img/temp/kid4.png')}}" alt="" width="100%">
            </div>
            <div class="col-6 col-sm-4 p-1">
                <img src="{{asset('img/temp/kid5.png')}}" alt="" width="100%">
            </div>
            <div class="col-6 col-sm-4 p-1">
                <img src="{{asset('img/temp/kid6.png')}}" alt="" width="100%">
            </div>
        </div>
    </div>
@endsection
