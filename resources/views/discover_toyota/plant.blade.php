@extends('layouts.main')

@section('title', "Toyota Myanmar Plant")

@section('content')
    <main class="container-fluid px-sm-5 px-3 pb-4">
        @include('components.breadcrumb')
        <header class="py-4">
            <h2 class="fw-bold zoom animation" style="--offset-scale: 1.2">Toyota Myanmar Plant</h2>
        </header>
    </main>

    <article class="toyota-plant-banner overflow-hidden">
        <img src="{{asset('img/discover-toyota/plant_banner.jpg')}}" alt="" width="100%" class="zoom animation" style="--offset-scale: 1.2; --offset-opacity: 1;">
        <div class="plant-title py-4 px-5 d-none d-sm-block">
            <h2 class="text-end fw-bold fade-x animation" style="--offset-x: 100px; --delay: 0;">MEET TOYOTA MYANMAR</h2>
            <p class="text-center fs-4 fade-x animation" style="--delay: 0;">Toyota Motor Asia Pacific Pte Ltd</p>
        </div>
    </article>

    <div class="d-block d-sm-none bg-white p-4">
        <h4 class="fw-bold text-center fade-x animation" style="--offset-x: 100px; --delay: 0;">MEET TOYOTA MYANMAR</h4>
        <h6 class="fade-x animation" style="--delay: 0;">Toyota Motor Asia Pacific Pte Ltd</h6>
    </div>

    <div class="container pb-4 toyota-plant-text">
        <p id="mm_paragraph" class="toyota-paragraph lh-lg fade-y animation pt-4"  style="display: none;">
            <b class="ls-1">ကုန်ထုတ်လုပ်မှုအခန်းကဏ္ဍတွင် လူ့စွမ်းအားအရင်းအမြစ်ဖွံ့ဖြိုးတိုးတက်လာစေခြင်းဖြင့် မြန်မာနိုင်ငံ ၌ မော်တော်ယာဥ်စက်မှုလုပ်ငန်း ပေါ်ပေါက်လာစေရန် ရည်ရွယ်ချက်ဖြင့် ဂျပန်နိုင်ငံ၊ တိုယိုတာမော်တော် ကော်ပိုရေးရှင်းသည် ရန်ကုန်တိုင်း‌‌‌ဒေသကြီး၊ ကျောက်တန်းမြို့နယ်၊ သီလဝါအထူးစီးပွားရေးဇုန်အတွင်း ဝင်ရောက်ရင်းနှီးမြုပ်နှံ၍ တိုယိုတာမြန်မာကုမ္ပဏီလီမီတက် ကို ၂၀၁၉ခုနှစ်၊ မေလ(၃၁)ရက် နေ့စွဲဖြင့် ဖွဲ့စည်းတည်ထောင်ကာ </b> Toyota Hilux Double Cab Adventure Grade
            <b class="ls-1"> နှင့် </b> Revo Grade
            <b class="ls-1"> အမျိုးအစား (၂)မျိုး အား </b>Semi Knock Down (SKD)
            <b class="ls-1"> စနစ်ဖြင့် တပ်ဆင်ထုတ်လုပ်၍ ပြည်တွင်း၌ဖြန့်ဖြူးရောင်းချ ပေးလျှက်ရှိပါတယ်ခင်ဗျာ။</b>
        </p>
        <br>
        <p  id="en_paragraph" class="toyota-paragraph lh-lg fade-y animation">
            With the objective of contributing automobile industry in Myanmar especially by developing human resources in manufacturing sector,Toyota Motor Corporation, Japan had formed Toyota Myanmar Co., Ltd with the date of 31st May 2019 and invested into Thilawa Special Economic Zone, located in Kyauk Tan Township, Yangon Region. Now, its subsidary company, Toyota Myanmar Co.,Ltd. is manufacturing Toyota Hilux Double Cab Adventure Grade and Revo Grade by Semi Knock Down system (SKD) and distributing those finished products inside the country.
        </p>
    </div>
@endsection
