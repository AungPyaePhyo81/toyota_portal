@extends('layouts.main')

@section('title', "About Toyota Myanmar")

@section('content')
    <main class="container-fluid px-sm-5 px-3 pb-4">
        @include('components.breadcrumb')
        <header class="container pt-4">
            <h2 class="fw-bold" data-aos="zoom-out" data-aos-duration="1000">About Toyota Myanmar</h2>
        </header>
        {{-- <article>
            <img src="{{asset('img/discover-toyota/about_banner.png')}}" alt="" width="100%">
        </article> --}}
        <div class="container py-1">
            <div class="row">
                <div class="col-12 col-xl-3 col-lg-4 col-md-6 p-2 position-relative" data-aos="fade-up" data-aos-duration="1000">
                    <a href="{{route('discover.about.vision')}}" class="btn btn-toyota-outline-light position-absolute bottom-0 m-4">Vision & Philosophy</a>
                    <img src="{{asset('img/discover-toyota/vision.png')}}" alt="" width="100%">
                </div>
                <div class="col-12 col-xl-3 col-lg-4 col-md-6 p-2 position-relative" data-aos="fade-up" data-aos-duration="1500">
                    <a href="{{route('discover.about.history')}}" class="btn btn-toyota-outline-light position-absolute bottom-0 m-4">History of TOYOTA</a>
                    <img src="{{asset('img/discover-toyota/history.png')}}" alt="" width="100%">
                </div>
                <div class="col-12 col-xl-3 col-lg-4 col-md-6 p-2 position-relative" data-aos="fade-up" data-aos-duration="2000">
                    <a href="{{route('discover.about.tradition')}}" class="btn btn-toyota-outline-light position-absolute bottom-0 m-4">TOYOTA Tradition</a>
                    {{-- <button class="btn btn-toyota-outline-light position-absolute bottom-0 m-4"></button> --}}
                    <img src="{{asset('img/discover-toyota/tradition.png')}}" alt="" width="100%">
                </div>
                <div class="col-12 col-xl-3 col-lg-4 col-md-6 p-2 position-relative" data-aos="fade-up" data-aos-duration="2500">
                    <a href="{{route('discover.about.design')}}" class="btn btn-toyota-outline-light position-absolute bottom-0 m-4">Design,R&D,Manufacturing
                        Facilities</a>
                    <img src="{{asset('img/discover-toyota/design.png')}}" alt="" width="100%">
                </div>
                <div class="col-12 col-xl-3 col-lg-4 col-md-6 p-2 position-relative" data-aos="fade-up" data-aos-duration="3000">
                    <a href="{{route('discover.about.emblem')}}" class="btn btn-toyota-outline-light position-absolute bottom-0 m-4">Emblem</a>
                    <img src="{{asset('img/discover-toyota/emblem.png')}}" alt="" width="100%">
                </div>
            </div>
        </div>
    </main>
@endsection
