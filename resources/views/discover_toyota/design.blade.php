@extends('layouts.main')

@section('title', "Design, R&D, Manufacturing Features | About Toyota Myanmar")

@section('content')
    <div class="bg-toyota-light">
        <main class="container-fluid bg-toyota-light px-sm-5 px-3 pb-4">
            @include('components.breadcrumb',
                    ['data' => [['route' => 'discover.about',
                                'name' => 'About Toyota Myanmar']]])
            <header class="py-4">
                <h2 class="fw-bold" data-aos="fade-right" data-aos-duration="1000">Design, R&D, Manufacturing features</h2>
            </header>
        </main>
        <article>
            <img src="{{asset('img/discover-toyota/design_banner.png')}}" alt="" width="100%" data-aos="zoom-out" data-aos-duration="1000">
        </article>
        <div class="container py-4">
            <br>
            <h3 class="fw-bold my-4" data-aos="fade-right" data-aos-duration="1000">Message</h3>
            <div class="row">
                <div class="col-sm-5 col-12">
                    <img src="{{asset('img/discover-toyota/designer.png')}}" alt="" width="100%" data-aos="fade-up" data-aos-duration="1000">
                </div>
                <div class="col-sm-7 col-12 px-2 px-sm-3 px-md-4 px-lg-5">
                    <h3 class="fw-bold" data-aos="fade-up" data-aos-duration="1500">Simon Humphries</h3>
                    <h4 class="fw-bold my-4" data-aos="fade-up" data-aos-duration="2000">Head of Design</h4>
                    <p class="toyota-paragraph" data-aos="fade-up" data-aos-duration="2500">Both the value and power of design lie in its ability to make people's lives better. <br><br>
                        Every day, at Toyota Design, together with our group companies arouboldnd the world, we challenge the boundaries of what new experiences we can create for our customers. Whether it is a car, a robot, a yacht or something yet to be named, our common goal is to have fun creating product experiences that will hopefully bring happiness to many.
                    </p>
                    <br>
                    <div class="d-flex flex-column flex-lg-row" data-aos="fade-up" data-aos-duration="3000">
                        <a href="https://toyotatimes.jp/en/series/design/"><img src="{{asset('img/discover-toyota/toyota_times.png')}}" alt=""></a>
                        <a href="https://toyotatimes.jp/en/series/design/" class="animated-link-reverse" style="--line-color:#000">Link to "Designing Dreams" article series Design Sketches</a>
                    </div>
                </div>
            </div>
            <br>
            <h3 class="fw-bold my-4" data-aos="fade-right" data-aos-duration="1000">Global Network</h3>
            <p class="toyota-paragraph" data-aos="fade-up" data-aos-duration="1500">Together with our satellite design centers and group companies, we develop our designs from a global perspective.</p>
            <br>
            <h3 class="fw-bold my-4" data-aos="fade-right" data-aos-duration="1000">Satellite Design Centers</h3>
            <div class="container d-flex justify-content-center">
                <img src="{{asset('img/discover-toyota/satellite_design_centers.png')}}" alt="" width="100%" data-aos="fade-up" data-aos-duration="1500">
            </div>
            <br>
            <h3 class="fw-bold my-4" data-aos="fade-right" data-aos-duration="1000">Design Partners within Toyota Group Companies</h3>
            <div class="container d-flex justify-content-center">
                <img src="{{asset('img/discover-toyota/design_partners_within_Toyota_gp_companies.png')}}" alt="" width="100%" data-aos="fade-up" data-aos-duration="1500">
            </div>
        </div>
    </div>
@endsection
