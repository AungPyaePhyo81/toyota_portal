@extends('layouts.main')

@section('title', 'Special Service Campaign')

@section('content')
    <main class="container-fluid px-sm-5 px-3 pb-4">
        @include('components.breadcrumb')
        <header class="py-4">
            <h2 class="fw-bold" data-aos="fade-right" data-aos-duration="1000">Special Service Campaign</h2>
        </header>
        <div class="d-flex justify-content-center align-items-center" style="min-height: 50vh">
            <h1 class="" style="color: #0005">
                @foreach (str_split('Coming Soon') as $i => $char)
                    <span class="fa-regular fa-bounce"
                        style="--fa-animation-duration: 2s;
                                --fa-animation-delay: {{$i*50}}ms;
                                font-family: 'ToyotaType', system-ui;
                                {{$char != ' ' ? 'margin:-5px' : 'margin:5px'}}">
                        {{$char}}
                    </span>
                @endforeach
            </h1>
        </div>
    </main>
@endsection
