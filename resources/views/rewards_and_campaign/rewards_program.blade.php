@extends('layouts.main')

@section('title', 'Rewards Program')

@section('content')
    <main class="container-fluid px-sm-5 px-3 pb-4">
        @include('components.breadcrumb')
        <header class="py-4">
            <h2 class="fw-bold" data-aos="fade-right" data-aos-duration="1000">TYMS Special Service Campaign</h2>
        </header>
    </main>

    <article class="overflow-hidden">
        <img src="{{asset('img/temp/fix_it.png')}}" alt="" width="100%" data-aos="zoom-out" data-aos-duration="1000">
    </article>

    <div class="container">
        <p class="toyota-paragraph py-sm-4 py-1 pt-4">
            Toyota Myanmar will be implementing the Takata Airbag Special Service Campaign, a nationwide initiative to replace the Takata airbag inflators in the 2006–2010 Yaris, Corolla, Echo, and RAV4 vehicles. These are the only Toyota models affected in Myanmar and Myanmar’s territories.
        </p>
        <p class="toyota-paragraph py-sm-4 py-1">
            The current airbags may malfunction, resulting in serious and possibly fatal injuries if not replaced. Please visit an authorized Toyota dealer immediately for this free replacement.
        </p>
        <p class="toyota-paragraph py-sm-4 py-1 fw-bold">
            Enter your Vehicle Identification Number (VIN) below to check if your vehicle is eligible for this free replacement service
        </p>
        <img src="{{asset('img/temp/VIN.png')}}" alt="" width="100%">
        <p class="toyota-paragraph py-sm-4 py-1 pt-4">
            The Vehicle Identification Number (VIN) can be found in these locations:
        </p>
            <ul class="toyota-paragraph"style="color: var(--danger)">
                <li><span style="color: #000">On the dashboard (usually nearest to the windshield on the driver's side)</span></li>
                <li><span style="color: #000">On the side of the driver’s door (example shown in the image above)</span></li>
                <li><span style="color: #000">In Page 9 of the vehicle bluebook (labelled “Chassis No.”)</span></li>
            </ul>
        <p class="toyota-paragraph py-sm-4 py-1 ">
            If you have been contacted about our airbag replacement service or have reason to believe your airbags need to be changed, enter your VIN to check if you are eligible.
        </p>
        <form action="">
            <input type="text" name="" id="" class="toyota-input w-100" placeholder="Enter your VIN here">
            <button type="submit" class="btn-toyota-danger btn py-2 px-4">SUBMIT</button>
        </form>
        <p class="toyota-paragraph pt-5 pb-3">DOWNLOAD MOBILE APP HERE</p>
        <img src="{{asset('img/temp/google_play.png')}}" alt="" class="pb-4">
        <img src="{{asset('img/temp/app_store.png')}}" alt="" class="px-3 pb-4">
    </div>
@endsection
