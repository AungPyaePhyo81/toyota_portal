@extends('layouts.main')

@section('title', 'Promotions')

@section('content')
    <main class="px-sm-5 px-3 pb-4">
        @include('components.breadcrumb')
        <header class="d-flex justify-content-between py-4 flex-md-row flex-column">
            <div>
                <h2 class="fw-bold">Promotion</h2>
            </div>
            <form class="d-flex align-items-center mt-3" action="{{ route('promotion.filter') }}" method="POST" id="sort_form">
                @csrf
                <label for="">Sort&nbsp;By&nbsp;:&nbsp;</label>
                <select name="sort" id="sort_select" class="form-select ms-2 me-4 wpx-200 toyota-input">
                    <option value="desc" {{ isset($sort) ? ($sort == 'desc' ? 'selected' : '') : '' }}>Newest</option>
                    <option value="asc" {{ isset($sort) ? ($sort == 'asc' ? 'selected' : '') : '' }}>Oldest</option>
                </select>
                <button type="button" data-bs-toggle="offcanvas" data-bs-target="#promoFilterOffcanvas"
                    class="btn btn-light border-dark rounded-0">FILTER</button>
            </form>
        </header>
        @if (isset($start_date) || isset($end_date) || isset($dealer))
            <p>Filters:
                {{ isset($start_date) ? date('d/m/Y', strtotime($start_date)) : '' }}
                {{ !isset($start_date) || !isset($end_date) ? '' : ' - ' }}
                {{ isset($end_date) ? date('d/m/Y', strtotime($end_date)) : '' }}
                {{ isset($dealer) ? $dealer : '' }}</p>
        @endif
    </main>
    <article>
        @foreach ($promotions as $idx => $promotion)
            <div class="row bg-white {{ $idx % 2 != 0 ? '' : 'dir-rtl' }}">
                <div class="col-md-6 col-12 p-0 overflow-hidden d-flex align-items-center bg-darker-white">
                    <img class="zoom animation" style="--offset-scale: 1.2"
                        src="{{ asset('storage/' . $promotion->promotion_thumbnail) }}" alt="" width="100%"
                        style="object-fit: cover;">
                </div>
                <div class="col-md-6 col-12 d-flex align-items-center justify-content-center">
                    <div class="w-75 py-5 text-start" style="direction: ltr !important;">
                        <h3 class="fw-bold lh-base fade-y animation">{{ $promotion->promotion_title }}</h3>
                        <p class="text-danger fade-y animation" style="--duration: 1.5s">Until
                            {{ date('M dS', strtotime($promotion->promotion_end_date)) }}</p>
                        <div class="fade-y animation" style="--duration: 2s;">
                            {!! $promotion->promotion_description !!}
                        </div>

                        @php $promo_dealers = json_decode($promotion->dealer_id, true); @endphp

                        @foreach ($promo_dealers as $dealer)
                            @php $promo_dealer = App\Models\Dealer::where('id', $dealer)->first(); @endphp

                            <a href="{{ route('dealer.detail', $dealer) }}"
                                class="btn btn-outline-dark px-4 rounded-0 me-3 my-2" data-aos="fade-up"
                                data-aos-duration="1500"
                                title="{{ $promo_dealer->name }}">{{ count($promo_dealers) > 1 ? $promo_dealer->name : 'Contact Dealer' }}</a>
                        @endforeach

                        <div class="py-5 fade-y animation">
                            <u>Share With Friends</u>
                            <br>
                            <div class="dir-rtl">
                                @php
                                    $shareButtons = \Share::page(route('promotion.share', $promotion->id))
                                        ->facebook()
                                        ->linkedin();
                                @endphp
                                {!! $shareButtons !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </article>
    @include('components.stay_informed')
    <div class="text-end p-4 m-4">
        <a href="#" class="animated-link-reverse" style="--line-color: #000">Back To Top</a>
    </div>
    @include('promotion.filter_form')
@endsection
