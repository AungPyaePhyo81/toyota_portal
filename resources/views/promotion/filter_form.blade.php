<div class="offcanvas offcanvas-start form-offcanvas" tabindex="-1" id="promoFilterOffcanvas">
    <div class="pt-4 pe-4">
        <button type="button" class="btn-close float-end" data-bs-dismiss="offcanvas" aria-label="Close"></button>
    </div>
    <div class="w-100 p-lg-5 p-3 overflow-auto">
        <form action="" class="px-sm-5 px-4" action="{{route('promotion.filter')}}" method="POST">
            @csrf
            <h2 class="fw-bold py-4">Filter Promotions</h2>
            <input type="hidden" name="sort" value="{{isset($sort) ? $sort : 'desc' }}">
            {{-- <input type="hidden" name=""> --}}
            {{-- <p class="toyota-paragraph">BY PROMOTION TYPE</p>
            <hr>
            <div class="form-check py-2">
                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                <label class="form-check-label" for="flexCheckDefault">
                    Servicing & Spares
                </label>
            </div>
            <div class="form-check py-2">
                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                <label class="form-check-label" for="flexCheckDefault">
                    Accessories
                </label>
            </div>
            <div class="form-check py-2">
                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                <label class="form-check-label" for="flexCheckDefault">
                    Financing
                </label>
            </div>
            <div class="form-check py-2">
                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                <label class="form-check-label" for="flexCheckDefault">
                    New Purchase
                </label>
            </div>
            <br> --}}

            <p class="toyota-paragraph">BY DATE RANGE</p>
            <hr>
            <div class="row">
                <div class="col-sm-6 col-12">
                    <input type="date" name="start_date" id="" class="form-control my-2 toyota-input">
                </div>

                <div class="col-sm-6 col-12">
                    <input type="date" name="end_date" id="" class="form-control my-2 toyota-input">
                </div>
            </div>
            <br>

            <p class="toyota-paragraph">By Dealer</p>
            <hr>
            <div class="row py-2">
                <div class="col-sm-6 col-12">
                    <select name="dealer_id" id="" class="form-select py-2 toyota-input">
                        <option value="" disabled selected>Name of dealer</option>
                        @foreach ($dealers as $dealer)
                            <option value="{{$dealer->id}}">{{$dealer->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="col-sm-6 col-12">
                </div>
            </div>
            <br>

            <div>
                <a href="{{route('promotion')}}" class="btn px-4" data-bs-dismiss="offcanvas" aria-label="Close">Clear filters</a>
                <button type="submit" class="btn btn-toyota-danger px-4">APPLY FILTERS</button>
            </div>
        </form>
    </div>
</div>


