<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="{!! $promotion->promotion_description !!}">
    <meta property="og:title" content="{{ $promotion->promotion_title }}">
    <meta property="og:description" content="{!! $promotion->promotion_description !!}">
    <meta name="image" property="og:image" content="{{ asset('storage/' . $promotion->promotion_thumbnail) }}">
    <title>Document</title>
</head>

<body>
    <h1>{{ $promotion->title }}</h1>
    <p>{!! $promotion->long_description !!}</p>
    <img src="{{ asset('storage/' . $promotion->promotion_thumbnail) }}" alt="promotion-share">
</body>

</html>
