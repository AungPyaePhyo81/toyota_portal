<!DOCTYPE html>

<html>

<head>

    <title>Toyota</title>

</head>

<body>

    <h3>New promotion!</h3>

    <p>
        Dear {{ $stayInformedMail }},
    </p>
    <p>
        New TOYOTA promotion has been posted
    </p>
    <p>
        Promotion is valid until {{ date('M dS', strtotime($data->promotion_deadline)) }}
    </p>
    <div>
        See more about promotion <a href="{{ $url }}">here.</a>
    </div>
    <p>Thank You</p>


</body>

</html>
