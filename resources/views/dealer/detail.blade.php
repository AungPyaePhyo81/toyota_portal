@extends('layouts.main')

@section('title', "$dealer->name | Find a Dealer")

@section('css')
    <link rel="stylesheet" href="{{ asset('css/intlTelInput.min.css') }}">
    <style>
        .iti--allow-dropdown {
            padding-top: 1%;
            width: 100%;
        }

        .error {
            display: none !important;
        }
    </style>
@endsection

@section('content')
    <main class="px-sm-5 px-3 pb-4 bg-darker-white">
        @include('components.breadcrumb', ['data' => [['route' => 'dealer', 'name' => 'Find A Dealer']]])
    </main>
    <article class="bg-white">
        <section class="container">
            <div class="row pt-4">
                <div class="col-12 col-md-7 p-4 p-sm-5">
                    @php
                        foreach ($dealer->dealerInfos as $info) {
                            $opening_hour = $info->opening_hour;
                            $opening_day = $info->opening_day;
                        }
                    @endphp
                    <h2 class="fw-bold">{{ $dealer->name }}</h2>
                    <p>{!! $dealer->description !!}</p>
                </div>
                <div class="col-12 col-md-5 p-4 p-sm-5 dealer-info-end">
                    <h4 class="fw-bold">{{ preg_replace('/\([^)]+\)/', '', $dealer->name) }}</h4>
                    <br>
                    <div class="row w-100 py-2">
                        <div class="col-12 col-sm-5 col-md-12 col-lg-5 fw-bold">Opening Hours</div>
                        <div class="col-12 col-sm-7 col-md-12 col-lg-7">: {{ $opening_hour ?? '' }}</div>
                    </div>
                    <div class="row w-100 py-2">
                        <div class="col-12 col-sm-5 col-md-12 col-lg-5 fw-bold">Opening Days</div>
                        <div class="col-12 col-sm-7 col-md-12 col-lg-7">: {{ $opening_day ?? '' }}</div>
                    </div>
                    @foreach ($dealer->dealerInfos as $info)
                        <div class="row w-100 py-2">
                            @php
                                $phone_nos = json_decode($info->phone_no, true);
                            @endphp
                            <div class="col-12 col-sm-5 col-md-12 col-lg-5 fw-bold">
                                {{ count($dealer->dealerInfos) > 1 ? $info->map->name : 'Phone' }}</div>
                            <div class="col-12 col-sm-7 col-md-12 col-lg-7">:
                                @foreach ((array) $phone_nos as $phone_no)
                                    <span>{{ $phone_no['phone_no'] . ($loop->last ? '' : ',') }}</span>
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                    <br>
                    <a href="{{ $dealer->website_link }}" class="btn btn-toyota-danger py-2" style="width: 210px;">DEALER
                        WEBSITE</a>



                    <button type="button" data-bs-toggle="offcanvas" data-bs-target="#inquiryOffcanvas"
                        class="btn btn-outline-dark rounded-0 py-2 my-3" style="width: 210px;">MAKE AN INQUIRY</button>
                    <u class="fw-bold pt-4 pb-2">Join With Us</u>
                    <div>
                        @php
                            $social_links = json_decode($dealer->social_link, true);
                            function social($link)
                            {
                                $host = parse_url($link, PHP_URL_HOST);
                                $platform = str_replace('.com', '', preg_replace('/^(www\.)?/', '', $host));
                                return $platform;
                            }
                        @endphp
                        <div class="d-flex">
                            @foreach ($social_links as $link)
                                @if (social($link['social_link']) == 't.me')
                                    <a href="{{ $link['social_link'] }}" target="_blank" class="icon-link text-dark mx-2">
                                        <i class="fa-solid fa-sm fa-paper-plane" title="telegram"></i>
                                    </a>
                                @else
                                    @php $social = social($link['social_link']); @endphp
                                    <a href="{{ $link['social_link'] }}" target="_blank" class="icon-link text-dark mx-2">
                                        <i class="fa-brands fa-sm fa-{{ $social != 'facebook' ? $social : 'square-' . $social }}"
                                            title="{{ social($link['social_link']) }}"></i>
                                    </a>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>

            @foreach ($dealer->dealerInfos as $idx => $info)
                @php
                    $services = json_decode($info->service, true);
                    $images = json_decode($info->image, true);
                    // dd($images);
                @endphp
                <hr>
                <div class="row pt-4">
                    @if ($idx == 0)
                        <h2 class="col-12 px-4 px-sm-5 pb-4 text-center text-md-start">Available Services</h2>
                    @endif
                    <div class="col-12 col-md-6 px-4 px-sm-5">
                        <p class="fw-bold">At {{ $info->map->name }}</p>
                        <ul class="list-unstyled">
                            @foreach ($services as $serviceId)
                                @php
                                    $service = App\Models\TypeOfService::where('id', $serviceId)->first();
                                @endphp
                                <li class="star-list">{{ $service->name ?? '' }}</li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-12 col-md-6 px-4 px-sm-5">
                        <div class="pb-4">
                            <span>Opening Hours</span><br>
                            <span>{{ $info->opening_hour }}</span>
                        </div>
                        <div class="pb-4">
                            <span>Opening Days</span><br>
                            <span>{{ $info->opening_day }}</span>
                        </div>
                        <div class="pb-4">
                            <span>Address</span><br>
                            <address>{{ $info->address }}</address>
                        </div>
                        <div class="pb-4">
                            <span>Call Us</span><br>
                            @php
                                $phone_nos = json_decode($info->phone_no, true);
                            @endphp
                            <span>
                                @foreach ((array) $phone_nos as $phone_no)
                                    {{ $phone_no['phone_no'] . ($loop->last ? '' : ',') }}
                                @endforeach
                            </span>
                        </div>
                        <div class="row">
                            @foreach ((array) $images as $img)
                                <div class="col-6 p-2">
                                    @if ($img['image'])
                                        <img src="{{ '/storage/' . $img['image'] }}" width="100%" height="100%">
                                    @endif
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endforeach
        </section>
        @include('dealer.inquiry_form')
        <div class="text-end p-4 m-4 mb-0">
            <a href="#" class="animated-link-reverse" style="--line-color: #000">Back To Top</a>
        </div>
    </article>
@endsection

@section('script')
    <script src="{{ asset('js/intlTelInput.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
    <script>
        $(document).ready(function() {

            const input = document.querySelector("#mobile_code");
            window.intlTelInput(input, {
                initialCountry: "mm",
                hiddenInput: "dial_code",
                utilsScript: "{{ asset('js/utils.js') }}",
            });

            $("#inquiryForm").validate({
                rules: {
                    name: "required",
                    email: {
                        required: true,
                        email: true
                    },
                    phone: "required",
                    type_of_inquiries: "required"
                },

                highlight: function(e) {
                    $(e).closest(".col-md-6").find("error").remove();
                    $(e).closest(".col-md-6").find(".form-error").removeClass("d-none");

                },

            });

        })
    </script>
@endsection
