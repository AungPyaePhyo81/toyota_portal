@extends('layouts.main')

@section('title', 'Find a Dealer')

@section('content')
    <main class="row">
        <div class="col-12 col-md-6 px-sm-5 px-3 pb-4 bg-white">
            @include('components.breadcrumb')
            <header class="container py-4">
                <h1 class="fw-bold">TOYOTA Myanmar Dealers & Service Center Locations</h2>
                    <p>Find the contact information of your preferred dealer or service center, and drop them an inquiry if
                        you have any questions</p>
            </header>
            <form class="container">
                @csrf
                <div class="input-group flex-column">
                    <label for="name" class="fw-bold toyota-paragraph">Dealer City</label>
                    <select name="city_id" id="city" class="toyota-input">
                        <option value="0" selected disabled>Select dealer city</option>
                        @foreach ($cities as $city)
                            <option value="{{ $city->id }}">{{ $city->city_name }}</option>
                        @endforeach
                    </select>
                </div>
                {{-- <div class="input-group flex-column mb-4">
                    <label for="name" class="fw-bold toyota-paragraph">Dealer/Service center</label>
                    <select name="dealer_id" id="dealer" class="toyota-input w-100">
                        <option value="0" selected disabled>Select type of service</option>
                        @foreach ($dealers as $dealer)
                            <option value="{{ $dealer->id }}">{{ $dealer->name }}</option>
                        @endforeach
                    </select>
                </div>
                <input type="submit" value="SEARCH" class="btn btn-outline-dark px-5 rounded-0" id="searchBtn"> --}}
            </form>
            <article class="container py-5">
                <div class="accordion dealer-accordion" id="dealerAccordion">
                    @foreach ($dealers as $dealer)
                        <div class="accordion-item" data-dealer-id="{{ $dealer->id }}">
                            <h2 class="accordion-header" id="heading{{ $dealer->id }}">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapse{{ $dealer->id }}" aria-expanded="false"
                                    aria-controls="collapse{{ $dealer->id }}">
                                    <i class="fa-solid fa-location-dot text-danger pe-3"></i> {{ $dealer->name }}
                                </button>
                            </h2>
                            <div id="collapse{{ $dealer->id }}" class="accordion-collapse collapse"
                                aria-labelledby="heading{{ $dealer->id }}" data-bs-parent="#dealerAccordion">
                                <div class="accordion-body">
                                    @foreach ($dealer->dealerInfos as $idx => $info)
                                        <a href="{{ route('dealer.detail', ['id' => $dealer->id]) }}"
                                            class="fw-bold animated-link"
                                            style="--line-color: #000">{{ $info->map->name }}</a>
                                        <p class="py-3">{{ $info->address }}</p>
                                        @php
                                            $phone_no = json_decode($info->phone_no, true);
                                        @endphp
                                        @if (count($phone_no) > 0)
                                            <div class="pb-4 d-flex flex-column align-items-start">
                                                <span>Sales Hotline</span>
                                                @foreach ($phone_no as $phone)
                                                    <a href="tel:{{ $phone['phone_no'] }}"
                                                        class="animated-link-reverse text-secondary p-0"
                                                        style="--line-color: var(--bs-gray)">{{ $phone['phone_no'] }}</a>
                                                @endforeach
                                            </div>
                                        @endif
                                        <span>{{ $info->opening_day }}</span><br>
                                        <p class="text-secondary">{{ $info->opening_hour }}</p>
                                        @if ($idx < count($dealer->dealerInfos) - 1)
                                            <hr>
                                        @endif
                                    @endforeach
                                    <a href="{{ route('dealer.detail', ['id' => $dealer->id]) }}"
                                        class="btn btn-toyota-danger px-4">CONTACT DEALER</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </article>
        </div>
        <div class="col-12 col-md-6 map-container">
            <div class="embed-responsive embed-responsive-16by9 map-wrapper" style="height:100%; width:100%;">
                <div id="map" class="map w-100 h-100"></div>
            </div>
        </div>
        </div>
    @endsection

    @section('script')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet.markercluster/1.5.3/leaflet.markercluster.js"></script>
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
        <script>
            $(document).ready(function() {

                var map = L.map('map').setView([{{ config('leaflet.map_center_latitude') }},
                    {{ config('leaflet.map_center_longitude') }}
                ], {{ config('leaflet.zoom_level') }});

                L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                }).addTo(map);


                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                var zoomedIn = true;
                var markers = L.markerClusterGroup();
                map.addLayer(markers);

                $('#city').on('change', function() {
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('ajax.dealers-by-city') }}",
                        data: {
                            'city_id': $('#city').val()
                        },
                        success: function(data) {
                            // map.setZoom(5);
                            showDealerAccordion(data)
                        }
                    });
                })

                $('#dealerAccordion .accordion-item').click(function() {
                    const id = $(this).attr('data-dealer-id');
                    showLocationByDealer(id)
                })

                const showLocationByDealer = (id) => {
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('ajax.get-dealer-by-id') }}",
                        data: {
                            'id': id
                        },
                        success: function(data) {
                            markers.clearLayers();
                            data?.dealer_infos?.map(item => {
                                map.setView([item.map?.latitude, item.map?.longitude], 12);
                            })
                            data?.dealer_infos?.map(item => {
                                markers.addLayer(
                                    L.marker([item.map?.latitude, item.map?.longitude])
                                    .bindPopup(item.map?.name)
                                );
                            });
                        }
                    });
                }

                const showDealerAccordion = (data, search = false) => {
                    let op = " ";
                    $('#dealerAccordion .accordion-item').hide();
                    for (var i = 0; i < data.length; i++) {
                        op += '<option value="' + data[i].id + '">' + data[i].name +
                            '</option>';
                        $(`#dealerAccordion .accordion-item[data-dealer-id='${data[i].id}']`)
                            .show();
                    }

                    if (search == false) {
                        $('#dealer').html(' ');
                        $('#dealer').append(
                            '<option value="0" selected disabled>Select type of </option>')
                        $('#dealer').append(op);
                    }


                    markers.clearLayers();


                    data.map(dealer => {
                        dealer?.dealer_infos.map(item => {
                            map.setView([item.map?.latitude, item.map?.longitude], 12);

                        })
                    })

                    data.map(dealer => {
                        dealer?.dealer_infos.map(item => {
                            markers.addLayer(L.marker([item?.map?.latitude,
                                    item?.map?.longitude
                                ])
                                .bindPopup(
                                    item?.map?.name));
                        })
                    })
                }

                const myCollapscible = document.getElementById('dealerAccordion')
                myCollapscible.addEventListener('show.bs.collapse', event => {
                    // alert('hiiii')
                    setTimeout(function() {
                        map.invalidateSize()
                    }, 400);

                    // map.invalidateSize();
                })

            })
        </script>
    @endsection
