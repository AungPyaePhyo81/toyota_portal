<div class="offcanvas offcanvas-start form-offcanvas" tabindex="-1" id="inquiryOffcanvas">
    <div class="pt-4 pe-4">
        <button type="button" class="btn-close float-end" data-bs-dismiss="offcanvas" aria-label="Close"></button>
    </div>
    <div class="w-100 p-lg-5 p-3 overflow-auto">
        <form class="px-sm-5 px-4" action="{{ route('online-inquiry.store') }}" method="POST" id="inquiryForm">
            @csrf
            <h2 class="fw-bold py-4">Online Inquiry Form</h2>
            <p class="text-secondary">Your satisfaction is of utmost importance to us.
                If you have any general questions, please provide the details below and we will contact you as soon as
                possible.</p>

            <div class="row">
                <div class="col-md-6 col-12 py-4">
                    <label for="name">Name</label>
                    <input type="text" name="name" id="name"
                        class="form-control my-2 toyota-input bg-darker-white rounded-0" placeholder="Eg. Ko Sai">
                    <div class="form-error pt-1 px-0 text-danger d-none">
                        Name field is required
                    </div>
                </div>

                <div class="col-md-6 col-12 py-4">
                    <label for="email">Email Address</label>
                    <input type="email" name="email" id="email"
                        class="form-control my-2 toyota-input bg-darker-white rounded-0"
                        placeholder="Eg. kosai@mail.com">
                    <div class="form-error pt-1 px-0 text-danger d-none">
                        Email field is required
                    </div>
                </div>

                <div class="col-md-6 col-12 py-4">
                    <label for="phone">Phone Number</label>
                    <input type="tel" class="form-control my-2 toyota-input bg-darker-white rounded-0 w-100"
                        placeholder="Phone number" id="mobile_code" name="phone">
                    {{-- <input type="tel" name="phone" id="phone"
                        class="form-control my-2 toyota-input bg-darker-white rounded-0" placeholder="Phone Number"> --}}
                    <div class="form-error pt-2 px-0 text-danger d-none">
                        Phone field is required
                    </div>
                </div>

                <div class="col-md-6 col-12 py-4">
                    <label for="email">Type of Inquiries</label>
                    <select name="type_of_inquiries" id="type_of_inquiries"
                        class="form-select py-2 toyota-input bg-darker-white rounded-0">
                        <option value="" disabled selected>Inquiry Type</option>
                        <option value="Sales">Sales</option>
                        <option value="Service">Service</option>
                        <option value="Spare Parts">Spare Parts</option>
                    </select>
                    <div class="form-error pt-1 px-0 text-danger d-none">
                        Type of Inquiries is required
                    </div>
                </div>

                <div class="col-lg-6 col-12 py-4">
                    <label for="description">Description</label>
                    <textarea name="description" id="" class="form-control my-2 toyota-input bg-darker-white rounded-0"
                        placeholder="Your Description"></textarea>
                </div>
            </div>
            <div>
                <button type="submit" class="btn btn-toyota-danger px-4">SUBMIT</button>
            </div>
        </form>
    </div>
</div>
