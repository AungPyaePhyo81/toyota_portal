<!DOCTYPE html>
<html lang="en">

<head>
    @yield('head')
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="{{ asset('img/toyotaFavi.png') }}" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>@yield('title') | Toyota Myanmar</title>

    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/scroll-animation.css') }}">
    <link rel="stylesheet" href="{{ asset('fontawesome-free-6.4.0-web/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('packages/line-awesome/css/line-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/intlTelInput.min.css') }}">
    <link rel="stylesheet" href="/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/css/owl.theme.default.min.css">

    {{-- CDN --}}
    <link rel="stylesheet" href="https://unpkg.com/aos@2.3.1/dist/aos.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.3/dist/leaflet.css"
        integrity="sha256-kLaT2GOSpHechhsozzB+flnD+zUyjE2LlfWPgU04xyI=" crossorigin="" />
    <script src="https://unpkg.com/leaflet@1.9.3/dist/leaflet.js"
        integrity="sha256-WBkoXOwTeyKclOHuWtc+i2uENFpDZ9YPdf5Hf+D7ewM=" crossorigin=""></script>
    @yield('css')
</head>

<body class="p-0">
    @include('layouts.nav')

    @yield('content')

    @include('layouts.footer')

    <script src="/js/scroll-animation.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/jquery.min.js"></script>
    <script src="/js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="{{ asset('js/alert.js') }}"></script>

    {{-- CDN --}}
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/masonry-layout@4.2.2/dist/masonry.pkgd.min.js"
        integrity="sha384-GNFwBvfVxBkLMJpYMOABq3c+d3KnQxudP/mGPkzpZSTYykLBNsZEnG2D9G/X/+7D" crossorigin="anonymous" async>
    </script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('js/intlTelInput.min.js') }}"></script>
    <script>
        AOS.init();
    </script>

    @if ($message = Session::get('success_message'))
        <script>
            successEvent(`{{ session()->get('success_message') }}`)
        </script>
    @endif
    @if ($message = Session::get('error_message'))
        <script>
            errorEvent(`{{ session()->get('error_message') }}`)
        </script>
    @endif

    {{-- script for pages --}}
    @yield('script')

    {{-- script for components --}}
    @yield('js')

    {{-- only for nav --}}
    @yield('nav-script')

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        lastScrollTop = 0
        $(window).scroll(function() {
            var scrollTop = $(window).scrollTop();
            // if (scrollTop > lastScrollTop) {
            //     $("#navbar").css("top", "-100px");
            // } else {
            //     $("#navbar").css("top", "0");
            // }
            lastScrollTop = scrollTop;
            if (lastScrollTop !== 0) {
                $(".scroller").css("opacity", "0");
            } else {
                $(".scroller").css("opacity", "1");
            }
        });
    </script>
</body>

</html>
