{{-- Large Nav --}}
<nav class="container-fluid">

    <div class="row custom-nav px-5 fade-y animation" style="--offset-y: -100%; --offset-opacity: 1; --delay:0;"
        id="navbar">
        <div class="col-1 d-flex d-lg-none justify-content-end align-items-center">
            <button class="btn" data-bs-toggle="offcanvas" data-bs-target="#offcanvasTop">
                <i class="fa-solid fa-bars-staggered text-danger fa-lg"></i>
            </button>
        </div>
        <div class="col-lg-2 col-10 d-flex justify-content-center align-items-center">
            <a href="{{ url('/') }}">
                <img src="{{ asset('img/toyota-logo.png') }}" alt="Toyota Logo" title="Home" class="nav-logo">
            </a>
        </div>
        <div class="col-10 d-none d-lg-block">
            <div class="top-nav py-2 d-flex justify-content-end gap-4">
                <a href="{{ route('download-brochures') }}" class="animated-link">Download Brochures</a>
                <a href="{{ route('inventory') }}" class="animated-link">Inventory</a>
                <a href="{{ route('blog') }}" class="animated-link">Blogs</a>
                <a href="{{ route('dealer') }}" class="animated-link">Find a Dealer</a>
                <a href="{{ route('coming-soon') }}" class="animated-link">Special Service Campaign</a>
                <div class="dropdown">
                    <div class="dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false"
                        id='lang-dropdown'>EN &nbsp;<i class="fas fa-chevron-down " id='lang-icon'></i></div>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="#" onclick="showParagraph('en')">EN</a></li>
                        <li><a class="dropdown-item" href="#" onclick="showParagraph('mm')">MM</a></li>
                    </ul>

                </div>
            </div>
            <div class="bot-nav py-2 d-flex justify-content-end gap-4">
                <span class="nav-dropdown" data-bs-target="#model" data-bs-toggle="offcanvas">Model&nbsp;<i
                        class="fas fa-chevron-down dropdown-icon" id='model-icon'></i></span>
                <span class="nav-dropdown" data-bs-target="#service"
                    data-bs-toggle="offcanvas">Service&nbsp;&&nbsp;Support&nbsp;<i
                        class="fas fa-chevron-down dropdown-icon" id='service-icon'> </i></span>
                <a href="{{ route('promotion') }}">Promotions</a>
                <a href="{{ route('coming-soon') }}">Rewards&nbsp;Program</a>
                <span class="nav-dropdown" data-bs-target="#discover"
                    data-bs-toggle="offcanvas">Discover&nbsp;TOYOTA&nbsp;<i class="fas fa-chevron-down dropdown-icon"
                        id='discover-icon'></i></span>
                <span class="nav-dropdown" data-bs-target="#electrified" data-bs-toggle="offcanvas">Electrified&nbsp;<i
                        class="fas fa-chevron-down dropdown-icon" id='electrified-icon'></i></span>
            </div>
        </div>
    </div>

    {{-- Small Nav --}}
    <div class="offcanvas offcanvas-top small-nav d-flex flex-column justify-content-around" id="offcanvasTop"
        aria-labelledby="offcanvasTopLabel" style="z-index: 1070">
        <div class="d-flex justify-content-center align-items-center">
            <img src="{{ asset('img/toyota-logo.png') }}" class="nav-logo" alt="Toyota Logo" title="Home">
        </div>
        <div class="row px-4 text-center">
            <div class="col-4"><a href="{{ route('promotion') }}" class="animated-link text-light">Promotions</a>
            </div>
            <div class="col-4"><a href="{{ route('blog') }}" class="animated-link text-light">Blog</a></div>
            <div class="col-4"><a href="{{ route('dealer') }}" class="animated-link text-light">Find a Dealer</a>
            </div>
        </div>
    </div>

    {{-- Dropdown Menu --}}
    <div class="offcanvas-wrap">
        {{-- Model --}}
        @php
            function modelRoute($subCategory)
            {
                $route =
                    $subCategory->status == 1
                        ? // status 1
                        route('model.car-detail', $subCategory->id)
                        : // status 0
                        (count($subCategory->carModel) == 1 && !is_null($subCategory->carModel[0]->specification)
                            ? // 1 model with spec
                            route('model.car.specification', $subCategory->carModel[0]->specification->id)
                            : // not 1 model
                            (count($subCategory->carModel) > 1
                                ? // 2 or more models
                                route('model.car-detail', $subCategory->id)
                                : // no model
                                '#'));
                return $route;
            }
        @endphp
        <div class="offcanvas offcanvas-top nav-offcanvas" id="model">

            <ul class="nav nav-tabs used-nav-tabs nav-justified py-2 px-md-5 mx-md-5 px-0 mx-0 @if (count($categories) < 5) w-50 @elseif(count($categories) < 8) w-75 @else w-90 @endif"
                role="tablist">
                {{-- All tag --}}
                <li class="nav-item" role="presentation">
                    <button class="nav-link used-car-tab active" data-bs-toggle="tab"
                        data-bs-target="#nav_sub_categoty_0" type="button" role="tab"
                        aria-selected="false">All</button>
                </li>
                {{-- Categories tag --}}
                @foreach ($categories as $category)
                    <li class="nav-item" role="presentation">
                        <button class="nav-link used-car-tab" data-bs-toggle="tab"
                            data-bs-target="#{{ 'nav_sub_categoty_' . $category->id }}" type="button"
                            role="tab" aria-controls="pickup-tab-pane"
                            aria-selected="false">{{ $category->name ?? '' }}</button>
                    </li>
                @endforeach
            </ul>
            <div class="tab-content p-3 mx-3 px-md-5 py-md-3 mx-md-5 w-100" id="carDisplayTabContent">
                {{-- All tab --}}
                <div class="tab-pane fade show active" id="nav_sub_categoty_0" role="tabpanel">
                    <div class="row">
                        @php
                            $limit = isset($_SERVER['HTTP_USER_AGENT']) && strpos($_SERVER['HTTP_USER_AGENT'], 'Mobi') !== false ? 3 : 7;
                            $subCategories = \App\Models\SubCategory::get();
                        @endphp
                        @foreach ($subCategories->take($limit) as $subCategory)
                            <div class="col-6 col-sm-4 col-md-3 px-4 pb-4">
                                <a href="{{ modelRoute($subCategory) }}"
                                    class="text-decoration-none text-dark d-flex flex-column justify-content-between h-100">
                                    <h5 class="fw-bold car-display-title mt-3 pb-4">{{ $subCategory->name ?? '' }}
                                    </h5>
                                    <img src="{{ asset('storage/' . $subCategory->image) }}" alt=""
                                        width="75%" class="car-display zoom animation">
                                </a>
                            </div>
                        @endforeach
                        {{-- See More --}}
                        @if (count($subCategories) > $limit)
                            <div class="col-6 col-sm-4 col-md-3 px-4 pb-4">
                                <a href="{{ route('inventory') }}" class="text-decoration-none text-dark">
                                    <h5 class="fw-bold car-display-title mt-3 pb-4">View More in Inventory</h5>
                                    <img src="{{ asset('img/compare/placeholder_right.png') }}" alt=""
                                        width="75%" class="car-display zoom animation">
                                </a>
                            </div>
                        @endif
                    </div>
                </div>

                {{-- Categories tab --}}
                @foreach ($categories as $category)
                    <div class="tab-pane fade" id="{{ 'nav_sub_categoty_' . $category->id }}" role="tabpanel"
                        aria-labelledby="home-tab" tabindex="{{ $category->id }}">
                        <div class="row">
                            @foreach ($category->subCategories->take(7) as $subCategory)
                                <div class="col-6 col-sm-4 col-md-3 px-4 pb-4">
                                    <a href="{{ modelRoute($subCategory) }}"
                                        class="text-decoration-none text-dark  d-flex flex-column justify-content-between h-100">
                                        <h5 class="fw-bold car-display-title mt-3 pb-4">{{ $subCategory->name ?? '' }}
                                        </h5>
                                        <img src="{{ asset('storage/' . $subCategory->image) }}" alt=""
                                            width="75%" class="car-display zoom animation">
                                    </a>
                                </div>
                            @endforeach
                            {{-- See More --}}
                            @if (count($category->subCategories) > 7)
                                <div class="col-6 col-sm-4 col-md-3 px-4 pb-4">
                                    <a href="{{ route('inventory') }}" class="text-decoration-none text-dark">
                                        <h5 class="fw-bold car-display-title mt-3 pb-4">View More
                                            {{ $category->name }} in
                                            Inventory</h5>
                                        <img src="{{ asset('img/compare/placeholder_right.png') }}" alt=""
                                            width="75%" class="car-display zoom animation">
                                    </a>
                                </div>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="col-12 border-top">
                <div class="d-flex justify-content-evenly px-5 py-3 text-secondary"
                    style="--line-color: var(--bs-gray)">
                    <a href="{{ route('model.compare-select') }}" class="animated-link">
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="30" height="30"
                            viewBox="0 0 34.9 48" class="text-dark">
                            <polyline points="32.7,41.4 32.7,4.5 13.6,4.5" fill="none" stroke="currentColor">
                            </polyline>
                            <rect x="30.9" y="41.4" width="3.4" height="3.2" fill="currentColor">
                            </rect>
                            <polyline points="17.4,1 13.6,4.5 17.4,8" fill="none" stroke="currentColor"
                                stroke-linecap="round" stroke-linejoin="round"></polyline>
                            <polyline points="2.2,6.6 2.2,43.5 21.3,43.5" fill="none" stroke="currentColor">
                            </polyline>
                            <rect x="0.5" y="4.2" width="3.4" height="3.2" fill="currentColor">
                            </rect>
                            <polyline points="17.5,47 21.3,43.5 17.5,40" fill="none" stroke="currentColor"
                                stroke-linecap="round" stroke-linejoin="round">
                            </polyline>
                        </svg>
                        <span class="ms-2">Compare Model</span>
                    </a>
                    <a href="{{ route('download-brochures') }}" class="animated-link d-flex align-items-center">
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="30" height="30"
                            viewBox="0 0 32.7 48" class="text-dark">
                            <path d="M4.4,47H1V9.5L19.7,1v37.5L1,47 M2.7,47h29V9.5h-12v29L1,47" fill="none"
                                stroke="currentColor"></path>
                            <line x1="16.5" y1="10.8" x2="2.6" y2="17.2" fill="none"
                                stroke="currentColor"></line>
                            <line x1="16.5" y1="19.1" x2="2.6" y2="25.5" fill="none"
                                stroke="currentColor"></line>
                        </svg>
                        <span class="ms-2">Download Brochures</span>
                    </a>

                    {{-- <a href="" class="animated-link"><i class="fa-solid fa-lg fa-calculator mx-2"></i>Payment Calculator</a> --}}
                    <a href="{{ route('inventory') }}" class="animated-link">
                        <img src="{{ asset('img/temp/inventorysearch.png') }}" alt="" width="17%">
                        Search Inventory
                    </a>
                </div>
            </div>

        </div>

        {{-- Service --}}
        <div class="offcanvas offcanvas-top nav-offcanvas" id="service">
            <div class="row w-100">
                <div class="col-8">
                    <div class="row">
                        <div class="col-6 p-5 d-flex justify-content-center">
                            <div>
                                <h4>Warranty & Support</h4>
                                <ul class="list-unstyled text-secondary" style="--line-color: var(--bs-gray)">
                                    <li class="py-1"><a href="{{ route('service-support.oem') }}"
                                            class="animated-link">OEM Warranty</a></li>
                                    <li class="py-1">
                                        <a class="animated-link" data-bs-toggle="collapse" href="#ownersManual"
                                            role="button" aria-expanded="false"
                                            aria-controls="ownersManual">Download
                                            Owners Manual</a>
                                        <div class="collapse my-2" id="ownersManual">
                                            <div class="card card-body">
                                                <a href="{{ url('pdf/20201224_OM_Hilux_OM0K404MM (Myanmar Version Book).pdf') }}"
                                                    class="animated-link om-link"
                                                    data-img="/img/owner-manuals/hilux_om.png"><i
                                                        class="fa-solid fa-download"></i> Hilux_OM (Myanmar
                                                    Version)</a>
                                                <a href="{{ url('pdf/22.01.2021_Navi_Hilux_OM0K430M_All Book_Check link.pdf') }}"
                                                    class="animated-link om-link"
                                                    data-img="/img/owner-manuals/hilux_navi.png"><i
                                                        class="fa-solid fa-download"></i> Hilux_Navi (Myanmar
                                                    Version)</a>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <br>
                                <h4>PARTS & ACCESSORIES</h4>
                                <ul class="list-unstyled text-secondary">
                                    <li class="py-1"><a href="{{ route('service-support.spare-part') }}"
                                            class="animated-link" style="--line-color: var(--bs-gray)">Spare Parts</a>
                                    </li>
                                    <li class="py-1"><a href="{{ route('service-support.accessories') }}"
                                            class="animated-link" style="--line-color: var(--bs-gray)">Accessories</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-6 p-5 d-flex justify-content-center">
                            <div>
                                <h4>MAINTENANCE & REPAIR</h4>
                                <ul class="list-unstyled text-secondary" style="--line-color: var(--bs-gray)">
                                    <li class="py-1"><a href="{{ route('service-support.maintenance') }}"
                                            class="animated-link">Maintenance Services</a></li>
                                    {{-- <li class="py-1"><a href="{{ route('service-support.support') }}"
                                            class="animated-link">Support Services</a></li> --}}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-4 m-0 p-0 service-support"
                    style='background: center / cover no-repeat url("/img/temp/service_support.jpg");'>
                    <img src="#" alt="" id="service_img" width="100%" height="100%">
                </div>
                <div class="col-12 border-top">
                    <div class="d-flex justify-content-evenly px-5 py-3 text-secondary"
                        style="--line-color: var(--bs-gray)">
                        <a href="{{ route('service-support.book-service') }}" class="animated-link">
                            <img src="{{ asset('img/temp/bookservice.png') }}" alt="" width="14%"
                                class="me-2">
                            Book A Service</a>
                        <a href="{{ route('dealer') }}" class="animated-link">
                            <img src="{{ asset('img/temp/finddealer.png') }}" alt="" width="16%"
                                class="me-2">
                            Service Centers</a>
                    </div>
                </div>
            </div>
        </div>

        {{-- Discover --}}
        @php
            $discovers = [[], ['route' => 'about', 'name' => 'About TOYOTA Myanmar'], ['route' => 'plant', 'name' => 'TOYOTA MYANMAR PLANT'], ['route' => 'dream-car', 'name' => 'TOYOTA DREAM CAR ART CONTEST'], ['route' => 'global-architecture', 'name' => 'TOYOTA NEW GLOBAL ARCHITECTURE'], ['route' => 'safety-sense', 'name' => 'TOYOTA SAFETY SENSE']];
        @endphp
        <div class="offcanvas offcanvas-top nav-offcanvas flex-row flex-wrap" id="discover">
            @for ($i = 1; $i < 6; $i++)
                <a href="{{ route('discover.' . $discovers[$i]['route']) }}"
                    class="{{ $i == 5 ? 'py-2' : 'pt-2' }} ps-2 w-25">
                    <div class="electrified-img-container w-100">
                        <span>{{ $discovers[$i]['name'] }}</span>
                        <img src='{{ asset("img/discover-toyota/discover_0$i.png") }}' alt=""
                            width="100%">
                    </div>
                </a>
            @endfor
        </div>

        {{-- Electrifieded --}}
        <div class="offcanvas offcanvas-top nav-offcanvas flex-row" id="electrified">
            <a href="{{ route('electrification') }}" class="electrified-img-container ms-2 my-2">
                <span>TOYOTA ELECTRIFICATION</span>
                <img src="{{ asset('img/electrification/electrification.jpg') }}" alt="" width="100%">
            </a>

            <a href="{{ route('hybrid-electric') }}" class="electrified-img-container m-2">
                <span>TOYOTA HYBRID ELECTRIC</span>
                <img src="{{ asset('img/electrification/hybrid_electric.jpg') }}" alt="" width="100%">
            </a>
        </div>
    </div>
</nav>

@section('nav-script')
    <script>
        //language icon
        $(document).click(function() {
            $('#lang-icon').css('transform', $('#lang-dropdown').hasClass('show') ? 'rotate(180deg)' :
                'rotate(0deg)');
        });
        function showParagraph(lang) {
            $("#en_paragraph").toggle(lang === "en");
            $("#mm_paragraph").toggle(lang !== "en");
            $('#lang-dropdown').html((lang === "en" ? "EN" : "MM") + ' &nbsp;<i class="fas fa-chevron-down" id="lang-icon"></i>');
        }

        $(document).ready(function() {
            //click dropdown toggle
            // $('.nav-dropdown').click(function(){
            //     target = $(this).attr('data-target')
            //     if($(this).hasClass('active')){
            //         $(this).removeClass('active')
            //         $(this).find(target).hide()
            //         $(this).find('.dropdown-icon').css('transform', 'rotate(0deg)')

            //         $(target).addClass('dropdown-animation-up')
            //         setTimeout(function() {
            //             $(target).css('display', 'none')
            //             $('.drop-down-bg').hide()

            //         }, 800)
            //     }
            //     else{
            //         $('.drop-down-bg').show()

            //         $('.nav-dropdown').removeClass('active')
            //         $(this).addClass('active')

            //         $('.nav-dropdown').find('.dropdown-icon').css('transform', 'rotate(0deg)')
            //         $(this).find('.dropdown-icon').css('transform', 'rotate(180deg)')

            //         $('.custom-drop-down').css('display', 'none')
            //         $(target).css('display', 'flex')

            //         $('.custom-drop-down').removeClass('dropdown-animation-down')
            //         $('.custom-drop-down').addClass('dropdown-animation-up')

            //         $(target).removeClass('dropdown-animation-up')
            //         $(target).addClass('dropdown-animation-down')
            //     }
            // })
            // cancel dropdown
            // $('.drop-down-bg').mouseup(function (e){
            //     var container = $(".custom-drop-down");

            //     if (!container.is(e.target) // if the target of the click isn't the container...
            //         && container.has(e.target).length === 0) // ... nor a descendant of the container
            //     {
            //         $('.custom-drop-down').addClass('dropdown-animation-up')
            //         $('.nav-dropdown').removeClass('active')
            //         $('.nav-dropdown').find('.dropdown-icon').css('transform', 'rotate(0deg)')
            //         setTimeout(function() {
            //             $('.drop-down-bg').hide()
            //             $('.custom-drop-down').css('display', 'none')
            //         }, 800)
            //     }
            // });

            // Owner Manual Img Preview
            $('#service_img').hide()
            $('.om-link').on('mouseenter', function() {
                $('#service_img').show()
                $('#service_img').attr('src', $(this).attr('data-img'))
            })
            $('.om-link').on('mouseout', function() {
                $('#service_img').hide()
            })

            // Dropdown Nav toggle
            nav_tags = ['model', 'service', 'discover', 'electrified']
            nav_tags.forEach(e => {
                $('#' + e)
                    .on('show.bs.offcanvas', function() {
                        $('.nav-dropdown[data-bs-target="#' + e + '"] i').css('rotate', '180deg')
                    })
                    .on('hide.bs.offcanvas', function() {
                        $('.nav-dropdown[data-bs-target="#' + e + '"] i').css('rotate', '0deg')
                    })
            })

        })
    </script>
@endsection
