<footer class="p-md-3 p-lg-5 pb-3">
    <div class="row text-light px-lg-4 mx-lg-4 px-sm-1 mx-sm-1">
        <div class="col-lg-4 col-sm-12 py-3 d-flex justify-content-lg-start justify-content-center">
            <div>
                <h5 class="text-center text-lg-start">Contact Us</h5>
                <ul class="footer-list mt-3 mt-sm-4 text-center text-lg-start">
                    <li class="footer-list-item my-1"><a href="{{route('dealer')}}" class="animated-link">Find a Dealer</a></li>
                    <li class="footer-list-item my-1"><a href="{{route('blog')}}" class="animated-link">Blogs</a></li>
                    <li class="footer-list-item my-1"><a href="{{route('inventory')}}" class="animated-link">Inventory</a></li>
                    <li class="footer-list-item my-1"><a href="{{route('used-car.index')}}" class="animated-link">Used Car for Sale</a></li>
                </ul>
            </div>
        </div>
        <div class="col-lg-4 col-sm-12 py-3 d-flex justify-content-center">
            <div>
                <h5 class="text-center">Discover <span class="text-toyota-danger">TOYOTA</span> Myanmar</h5>
                <ul class="footer-list mt-sm-4 mt-2 d-flex flex-lg-column flex-sm-row flex-column text-center text-lg-start">
                    <li class="footer-list-item my-1 mx-sm-4 mx-lg-0"><a href="{{route('discover.about')}}" class="animated-link">About TOYOTA</a></li>
                    <li class="footer-list-item my-1 mx-sm-4 mx-lg-0"><a href="{{route('discover.plant')}}" class="animated-link">TOYOTA Plant</a></li>
                    <li class="footer-list-item my-1 mx-sm-4 mx-lg-0"><a href="{{route('discover.dream-car')}}" class="animated-link"> Dream Car Art Contest</a></li>
                    <li class="footer-list-item my-1 mx-sm-4 mx-lg-0"><a href="{{route('discover.global-architecture')}}" class="animated-link"> New Global Architecture</a></li>
                    <li class="footer-list-item my-1 mx-sm-4 mx-lg-0"><a href="{{route('discover.safety-sense')}}" class="animated-link"> Safety Sense</a></li>
                    {{-- <li class="footer-list-item my-1 mx-sm-4 mx-lg-0"><a href="{{route('service-support.online-enquiry')}}" class="animated-link">Online Inquiry</a></li>
                    <li class="footer-list-item my-1 mx-sm-4 mx-lg-0"><a href="#" class="animated-link">Careers</a></li> --}}
                </ul>
            </div>
        </div>
        <div class="col-lg-4 col-sm-12 py-3 d-flex flex-column align-items-center">
            <h5>Join On Us</h5>
            <div class="d-flex justify-content-around">
                <a href="https://www.facebook.com/ToyotaMyanmar" class="icon-link mx-1"><i class="fa-brands fa-square-facebook"></i></a>
                <a href="https://www.linkedin.com/company/toyotamyanmar/" class="icon-link mx-1"><i class="fa-brands fa-linkedin"></i></a>
                <a href="https://t.me/toyotamyanmar" class="icon-link mx-1"><i class="fa-solid fa-paper-plane"></i></a>
                <a href="https://www.youtube.com/@toyotamyanmar3237" class="icon-link mx-1"><i class="fa-brands fa-youtube"></i></a>
            </div>
        </div>
        <div class="col mt-5 toyota-text-12 px-sm-3 text-center text-sm-start">
            Vechicle specifications may vary by market. <br>
            Please Contact your local dealer for more information.
        </div>
    </div>
</footer>
{{-- <footer class="bottom-footer px-5 px-sm-1">
    <div class="row px-lg-5 mx-lg-5 py-2 text-light px-sm-1 mx-sm-1 px-md-4 mx-md-4">
        <div class="col-sm-3 col-xs-12 py-1 toyota-text-14 text-center text-sm-start"><a href="#" class="animated-link">Legal Terms</a></div>
        <div class="col-sm-6 col-xs-12 py-1 toyota-text-14 text-center">© 2022. All rights reserved / TOYOTA Myanmar Corporation</div>
        <div class="col-sm-3 col-xs-12 py-1 toyota-text-14 text-center text-sm-end"><a href="#" class="animated-link">Privacy / Policy</a></div>
    </div>
</footer> --}}
