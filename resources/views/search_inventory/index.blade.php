@extends('layouts.main')

@section('title', "Search Inventory")

@section('content')
    <main class="container-fluid px-sm-5 px-3 pb-4">
        @include('components.breadcrumb')
        <article class="container">
            <header class="py-4">
                <h2 class="fw-bold text-center">Search Inventory</h2>
            </header>
            <ul class="nav nav-tabs used-nav-tabs nav-justified py-4 px-md-5 mx-md-5 px-0 mx-0" role="tablist">
                <li class="nav-item" role="presentation">
                    <button class="nav-link used-car-tab active" data-bs-toggle="tab" data-bs-target="#inventory_sub_category_0" type="button" role="tab" aria-selected="false">
                        All
                    </button>
                </li>
                @foreach ($categories as $category)
                    <li class="nav-item" role="presentation">
                        <button class="nav-link used-car-tab" data-bs-toggle="tab" data-bs-target="#{{ 'inventory_sub_category_' . $category->id }}" type="button" role="tab" aria-controls="all-tab-pane" aria-selected="false">
                            {{ $category->name ?? '' }}
                        </button>
                    </li>
                @endforeach
            </ul>
            <div class="tab-content custom-bg-dark">
                <div class="tab-pane fade show active"
                    id="inventory_sub_category_0" role="tabpanel">
                    <div class="row">
                        @foreach ($subCategories as $idx => $subCategory)
                            <div class="col-md-4 col-6 p-sm-3 p-2">
                            @include('search_inventory.card',
                                    ['img' => "storage/$subCategory->image",
                                    'name' => $subCategory->name,
                                    'route' => 'inventory.detail',
                                    'id' => $subCategory->id,
                                    'version' => 0])
                            </div>
                        @endforeach
                    </div>
                </div>
                @foreach ($categories as $category)
                        <div class="tab-pane fade"
                            id="{{ 'inventory_sub_category_' . $category->id }}" role="tabpanel">
                            <div class="row">
                                @foreach ($category->subCategories as $idx => $subCategory)
                                    <div class="col-md-4 col-6 p-sm-3 p-2">
                                    @include('search_inventory.card',
                                            ['img' => "storage/$subCategory->image",
                                            'name' => $subCategory->name,
                                            'route' => 'inventory.detail',
                                            'id' => $subCategory->id,
                                            'version' => 0])
                                    </div>
                                @endforeach
                            </div>
                        </div>
                @endforeach
            </div>
        </article>
    </main>

    <div class="w-100 bg-white d-flex flex-row justify-content-evenly align-items-center text-dark" style="--line-color: var(--bs-dark)">
        <div class="py-4 toyota-paragraph">
            <img src="{{asset('img/temp/onlineinquery.png')}}" alt="" width="14%" class="mt-1">
            <a href="{{route('service-support.online-enquiry')}}" class="animated-link-reverse">Online Enquiry</a>
        </div>
        <div class="py-4 toyota-paragraph">
            <img src="{{asset('img/temp/finddealer.png')}}" alt="" width="16%" class="mt-1">
            <a href="{{route('dealer')}}" class="animated-link-reverse">Find A Dealer</a>
        </div>
    </div>
@endsection
