@extends('layouts.main')

@section('title', "$sub_cate->name Inventory")

@section('content')
    <main class="container-fluid px-sm-5 px-3 pb-4">
        @include('components.breadcrumb', ['data' => [['route' => 'inventory', 'name' => 'Inventory']]])
        <article class="container">
            <header class="py-4">
                <h2 class="fw-bold text-center">{{ $sub_cate->name }} Inventory</h2>
                <div class="d-flex justify-content-center my-3">
                    <span class="px-3">Exact matches: <span id="exact_count"></span></span>
                    <span class="px-3">Similar matches: <span id="similar_count"></span></span>
                </div>
                <div class="d-flex justify-content-center" id="fliteredDealers">
                    {{-- <div class="alert alert-dismissible fade show py-2 px-3 rounded-0 bg-dark text-light" role="alert">
                        TOYOTA Aye And Son (Yangon)
                        <button type="button" class="fa-solid fa-xmark btn text-light pe-0 ps-2" data-bs-dismiss="alert"
                            aria-label="Close"></button>
                    </div> --}}
                </div>
            </header>
            <div class="row">
                <form class="col-sm-5 col-12" method="p">
                    <h1>Filters</h1>
                    <hr>
                    {{-- Dealer --}}
                    <div>
                        <a class="text-decoration-none text-dark w-100 d-flex justify-content-between inventory-list"
                            data-bs-toggle="collapse" href="#inventoryCollapseDealer" role="button" aria-expanded="false"
                            aria-controls="inventoryCollapseDealer">
                            DEALERS
                        </a>
                        <div class="collapse show" id="inventoryCollapseDealer">
                            <div class="card-body">
                                @foreach ($dealers as $i => $dealer)
                                    <div class="toyota-check my-3">
                                        <input class="toyota-checkbox" type="checkbox" value="{{ $dealer->id }}"
                                            id="dealerCheck{{ $i }}" name="dealer">
                                        <span class="toyota-checkmark"></span>
                                        <label class="form-check-label" for="dealerCheck{{ $i }}">
                                            {{ $dealer->name }}
                                            {{-- @php
                                                if (isset($dealer->dealerinfo)) {
                                                    foreach ($dealer->dealerinfo as $info) {
                                                        $city = $info->map->city->city_name;
                                                    }
                                                }
                                            @endphp

                                            ({{ $city ?? '' }}) --}}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <hr>
                    {{-- Model --}}
                    <div>
                        <a class="text-decoration-none text-dark w-100 d-flex justify-content-between inventory-list"
                            data-bs-toggle="collapse" href="#inventoryCollapseModel" role="button" aria-expanded="false"
                            aria-controls="inventoryCollapseDealer">
                            Model
                        </a>
                        <div class="collapse show" id="inventoryCollapseModel">
                            <div class="card-body">
                                @foreach ($models as $i => $model)
                                    <div class="toyota-check my-3">
                                        <input class="toyota-checkbox" type="checkbox" value="{{ $model->id }}"
                                            name="model" id="modelCheck{{ $i }}">
                                        <span class="toyota-checkmark"></span>
                                        <label class="form-check-label" for="modelCheck{{ $i }}">
                                            {{ $model->name }}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <hr>
                    {{-- Color --}}
                    <div>
                        <a class="text-decoration-none text-dark w-100 d-flex justify-content-between  inventory-list"
                            data-bs-toggle="collapse" href="#inventoryCollapseColor" role="button" aria-expanded="false"
                            aria-controls="inventoryCollapseDealer">
                            Color
                        </a>
                        <div class="collapse show" id="inventoryCollapseColor">
                            <div class="card-body row">
                                @foreach ($colors as $color)
                                    <div class="col-3 col-sm-4 col-md-3 col-lg-2 py-2">
                                        <div class="toyota-color-check">
                                            <input type="checkbox" value="{{ $color->id }}" name="color">
                                            {{-- <span class="circle shadow" style="--circle-color:{{sprintf('#%06X', mt_rand(0, 0xFFFFFF))}}"></span> --}}
                                            <span class="circle shadow" style="--circle-color:{{ $color->code }}"></span>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-outline-dark rounded-0 px-5 my-4" id="filterBtn">Apply
                        Filter</button>
                </form>
                <div class="col-sm-7 col-12 pt-5">
                    <div class="row" id="filteredCars">
                        {{-- @for ($i = 0; $i < 2; $i++)
                            <div class="col-lg-6 col-12 p-sm-3 p-2">
                                @include('search_inventory.card', [
                                    'img' => "img/temp/car_display$i.png",
                                    'name' => "Hilux Revo Rally $i WD",
                                    'route' => 'inventory.detail',
                                    'id' => $i,
                                    'dealer' => 'Dealer Name',
                                    'version' => 1,
                                ])
                            </div>
                        @endfor --}}
                        <div class="col-lg-6 col-12 p-sm-3 p-2" id="letStart">
                            <div class="bg-dark text-light shadow h-100 p-3">
                                <i class="las la-map-marked-alt h1"></i>
                                <h2>Let your local dealer find your Toyota.</h2>
                                <p>Whether it's currently being built or located at another dealership, your local dealer
                                    can help you get the Toyota you want.</p>
                                <div class="row">
                                    <div class="col-6">
                                        <b>TOYOTA Yangon</b>
                                        <ul class="list-unstyled">
                                            <li>(01) 546 546,</li>
                                            <li>(01) 540 740</li>
                                        </ul>
                                    </div>
                                    <div class="col-6 text-end">
                                        <i>Today’s Hour</i>
                                        <ul class="list-unstyled">
                                            <li>From: 09:00 AM</li>
                                            <li>To: 05:00 PM</li>
                                        </ul>
                                    </div>
                                </div>
                                <address>အမှတ် ၈၇(က)၊ ကမ္ဘာအေးဘုရားလမ်း၊ ဗဟန်းမြို့နယ်၊ ရန်ကုန်မြို့ ၊ ပုလဲကွန်ဒိုရှေ့
                                    ။87(A), Kaba Aye Pagoda Road, Bahan Township, Shwegondine, Yangon.</address>

                                <a href="{{ route('dealer') }}" class="btn btn-light w-100 rounded-0 py-2">Let's Get
                                    Started</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </main>
@endsection

@section('script')
    <script>
        $('#filterBtn').click(function(e) {
            e.preventDefault();

            const dealerIds = $('input[name="dealer"]:checked').map(function() {
                return this.value;
            }).get();

            const modelIds = $('input[name="model"]:checked').map(function() {
                return this.value;
            }).get();

            const colorIds = $('input[name="color"]:checked').map(function() {
                return this.value;
            }).get();
            getFliterData(dealerIds, modelIds, colorIds)
        });

        const getFliterData = (dealerIds = [], modelIds = [], colorIds = []) => {
            $.ajax({
                url: `{{ route('ajax.inventory.filter') }}`,
                type: 'POST',
                dataType: 'json',
                data: {
                    dealerIds: dealerIds,
                    modelIds: modelIds,
                    colorIds: colorIds
                },
                success: function(response) {
                    console.log(response)
                    const exact = response.exact;
                    const similar = response.similar;
                    if (exact.length == 0 && similar.length == 0) {
                        return errorEvent("There is no car")
                    }
                    $("#exact_count").html(exact.length);
                    $("#similar_count").html(similar.length);
                    showFilterDealers(exact, similar)
                    showFilterCars(exact, similar)

                },
                error: function(xhr, status, error) {
                    console.log(xhr.responseText);
                }
            });
        }

        const showFilterDealers = (exact, similar) => {
            $("#fliteredDealers").empty()

            let exactDealer = exact.map((item) => {
                return `<div class="alert alert-dismissible fade show py-2 px-3 rounded-0 bg-dark text-light" role="alert">
                         ${item.dealer?.name}
                    </div>`
            })
            $("#fliteredDealers").append(exactDealer)

            let smimilarDealer = similar.map((item) => {
                return `<div class="alert alert-dismissible fade show py-2 px-3 rounded-0 bg-dark text-light" role="alert">
                         ${item.dealer?.name}
                    </div>`
            })

            $("#fliteredDealers").append(smimilarDealer)
        }

        const showFilterCars = (exact, similar) => {
            $('#filteredCars').empty();
            // $('#filteredCars').children().not($('#letStart')).remove();
            // $('#letStart').appendTo($('#filteredCars')); // move #letStart to the end of #filteredCars container


            let exactCar = exact.map((item) => {
                buildCard(item.car_model?.name, item.dealer?.name, item.thumbnail)
            })

            let similarCar = similar.map((item) => {
                buildCard(item.car_model?.name, item.dealer?.name, item.thumbnail)
            })

            let letStart = `<div class="col-lg-6 col-12 p-sm-3 p-2" id="letStart">
                            <div class="bg-dark text-light shadow h-100 p-3">
                                <i class="las la-map-marked-alt h1"></i>
                                <h2>Let your local dealer find your Toyota.</h2>
                                <p>Whether it's currently being built or located at another dealership, your local dealer
                                    can help you get the Toyota you want.</p>
                                <div class="row">
                                    <div class="col-6">
                                        <b>TOYOTA Yangon</b>
                                        <ul class="list-unstyled">
                                            <li>(01) 546 546,</li>
                                            <li>(01) 540 740</li>
                                        </ul>
                                    </div>
                                    <div class="col-6 text-end">
                                        <i>Today’s Hour</i>
                                        <ul class="list-unstyled">
                                            <li>From: 09:00 AM</li>
                                            <li>To: 05:00 PM</li>
                                        </ul>
                                    </div>
                                </div>
                                <address>အမှတ် ၈၇(က)၊ ကမ္ဘာအေးဘုရားလမ်း၊ ဗဟန်းမြို့နယ်၊ ရန်ကုန်မြို့ ၊ ပုလဲကွန်ဒိုရှေ့
                                    ။87(A), Kaba Aye Pagoda Road, Bahan Township, Shwegondine, Yangon.</address>
                                <button class="btn btn-light w-100 rounded-0 py-2">Let's Get Started</button>
                            </div>
                        </div>`
            $("#filteredCars").append(letStart);

        }


        const buildCard = (model, dealer, img) => {
            let ele = `<div class="col-lg-6 col-12 p-sm-3 p-2">
                        <div class="bg-light shadow d-flex flex-column">
                            <div class="px-5 py-4">
                                <a href='' class="text-dark text-decoration-none">
                                    <img src="{{ asset('storage') }}/${img}" alt="" width="100%" height="100%" class="car-display">
                                </a>
                        </div>
                         <div class="p-3">
                                <button class="btn btn-toyota-outline-dark">${model}</button>
                                <hr>
                                <i class="fa-solid fa-circle-info"></i><span> ${dealer}</span>
                                <div class="text-center">
                                    <a href="#" class="btn btn-toyota-danger mt-3 px-5 py-2">View Details</a>
                                </div>
                         </div>
                        </div>
                        </div>`;
            $("#filteredCars").append(ele);
        }
    </script>
@endsection
