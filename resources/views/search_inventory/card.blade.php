<div class="bg-light shadow d-flex {{$version == 0 ? 'h-100 align-items-center' : 'flex-column'}}">
    <div class="px-1 px-sm-2 px-md-3 px-lg-4 px-xl-5 py-4">
        <a href='{{route($route, $id)}}' class="text-dark text-decoration-none">
            <img src="{{asset($img)}}" alt="" width="100%" height="100%" class="car-display">
            @if ($version == 0)
                <div class="toyota-paragraph text-center fw-bold py-2">
                    {{$name}}
                </div>
            @endif
        </a>
    </div>
    @if ($version == 1)
        <div class="p-3">
            <button class="btn btn-toyota-outline-dark">{{$name}}</button>
            <hr>
            <i class="fa-solid fa-circle-info"></i><span> {{$dealer}}</span>
            <div class="text-center">
                <a href="#" class="btn btn-toyota-danger mt-3 px-5 py-2">View Details</a>
            </div>
        </div>
    @endif
</div>
