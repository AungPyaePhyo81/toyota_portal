@extends('layouts.main')

@section('title', 'Comming Soon')

@section('css')
    <style>
        .cover-img-container {
            position: relative;
            max-height: 80vh;
            overflow: hidden;
        }

        .cover-text {
            position: absolute;
            bottom: 3%;
            right: 2%;
            padding: 10px 50px 0px 5px;
            border-top: solid 4px #FFF;
            border-right: solid 4px #FFF;
            transform-origin: left;
            transform: scaleX(0);
            transition: transform 1.5s ease;
        }

        .animate {
            transform: scaleX(1)
        }
    </style>
@endsection
@section('content')
    <main class="trans-page-main">
        <div class="transition-page-container">
            <div class="cover-img-container">
                <img src="{{ asset('img/coming_soon.jpg') }}" class="w-100 cover-img" alt="Coming Soon" style="object-fit: cover">
                <div class="cover-text d-none d-sm-block" id="animate-div">
                    <h1 class="text-light">
                        COMING SOON!
                    </h1>
                    {{-- <span class="text-light ">
                        Our website is under construction. <br>
                        We are sorry for your inconvenience.
                    </span> --}}
                </div>
            </div>
            <div class="d-block d-sm-none border-top border-3 border-end" style="background: #000">
                <h1 class="text-light text-center m-0 p-3" data-aos="fade-right" data-aos-duration="1500">
                    COMING SOON!
                </h1>
            </div>
        </div>
    </main>
@endsection
@section('js')
    <script>
        window.addEventListener('load', function() {
            var element = document.getElementById('animate-div');
            element.classList.add('animate');
        });
    </script>
@endsection
