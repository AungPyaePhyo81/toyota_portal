@extends('layouts.main')

@section('head')
    <title>Toyota Myanmar | Home</title>
@endsection

@section('content')
    <article>
        <a href="#scroll_target" class="scroller"></a>
        {{-- First Carousel --}}
        <section>
            <div id="homeCarousel" class="carousel slide home-caro carousel-fade" data-bs-ride="carousel">
                <div class="carousel-indicators">
                    <button type="button" data-bs-target="#homeCarousel" data-bs-slide-to="0" class="caro-indicator active"
                        aria-current="true" aria-label="Slide 1"></button>
                    <button type="button" data-bs-target="#homeCarousel" data-bs-slide-to="1" class="caro-indicator"
                        aria-label="Slide 2"></button>
                    <button type="button" data-bs-target="#homeCarousel" data-bs-slide-to="2" class="caro-indicator"
                        aria-label="Slide 3"></button>
                    <button type="button" data-bs-target="#homeCarousel" data-bs-slide-to="3" class="caro-indicator"
                        aria-label="Slide 4"></button>
                </div>
                <div class="carousel-inner">
                    <div class="carousel-item home-caro-item active">
                        {{-- <div class="discover-more d-block">
                            <h5 id="home_caro_title">Hilux Adventure</h5>
                            <p id="home_caro_text" class="discover-more-text">
                                Hilux Adventure's performance makes every road trip safe and secure!</p>
                        </div> --}}
                        @include('components.caro_parts', [
                            'data' => [
                                [
                                    'path' => 'first/Hilux Adventure Bed Liner & Deck Bar.jpg',
                                    'name' => 'Bed Liner & Deck Bar',
                                ],
                                [
                                    'path' => 'first/Hilux Adventure Interior Design.jpg',
                                    'name' => 'Interior Design',
                                ],
                                [
                                    'path' => 'first/Hilux Adventure LED Headlamps & Fog Lamps.jpg',
                                    'name' => 'LED Headlamps & Fog Lamps',
                                ],
                                ['path' => 'first/Hilux Adventure Wheel.jpg', 'name' => 'Wheel'],
                            ],
                            'idx' => 0,
                        ])
                        <div class="home-caro-img"
                            style="background: url('{{ asset('/img/slides/first/Hilux Adventure.jpg') }}') no-repeat center/cover">
                            <img src="{{ asset('img/slides/first/Hilux Adventure.jpg') }}" alt=""
                                class="zoom animation" id="caro_img0"
                                style="--offset-scale: 1.2; --delay: 0; --offset-opacity: 1;">
                        </div>
                    </div>
                    <div class="carousel-item home-caro-item">
                        {{-- <div class="discover-more d-block">
                            <h5 id="home_caro_title">Unbeatable Style</h5>
                            <p id="home_caro_text" class="discover-more-text">
                                Hilux Revo's performance makes every road trip safe and secure!</p>
                        </div> --}}
                        <div class="justify-content-center d-flex">
                            @include('components.caro_parts', [
                                'data' => [
                                    [
                                        'path' => 'second/Hilux Leather Interior Design.jpg',
                                        'name' => 'Interior Design',
                                    ],
                                    [
                                        'path' => 'second/Hilux Leather LED Headlamps and Fog Lamps.jpg',
                                        'name' => 'LED Headlamps and Fog Lamps',
                                    ],
                                    [
                                        'path' => 'second/Hilux Leather Rear Combination Lamps.jpg',
                                        'name' => 'Rear Combination Lamps',
                                    ],
                                    ['path' => 'second/Hilux Leather Wheel.jpg', 'name' => 'Wheel'],
                                ],
                                'idx' => 1,
                            ])
                        </div>
                        <div class="home-caro-img"
                            style="background: url('{{ asset('img/slides/second/Revo.jpg') }}') no-repeat center/cover">
                            <img src="{{ asset('img/slides/second/Revo.jpg') }}" alt="" class="zoom animation"
                                id="caro_img1" style="--offset-scale: 1.2; --delay: 0; --offset-opacity: 1;">
                        </div>
                    </div>
                    <div class="carousel-item home-caro-item">
                        {{-- <div class="discover-more d-block rv-50">
                            <h5 id="home_caro_title">Hilux Adventure</h5>
                            <p id="home_caro_text" class="discover-more-text" style="--text-color: #0009">
                                Hilux Adventure's performance makes every road trip safe and secure!</p>
                        </div> --}}
                        <div class="justify-content-center d-flex">
                            @include('components.caro_parts', [
                                'data' => [
                                    [
                                        'path' => 'third/Hilux Adventure Bed Liner & Deck Bar.jpg',
                                        'name' => 'Bed Liner & Deck Bar',
                                    ],
                                    [
                                        'path' => 'third/Hilux Adventure Interior Design.jpg',
                                        'name' => 'Interior Design',
                                    ],
                                    [
                                        'path' => 'third/Hilux Adventure LED Headlamps & Fog Lamps.jpg',
                                        'name' => 'LED Headlamps & Fog Lamps',
                                    ],
                                    [
                                        'path' => 'third/Hilux Adventure Wheel.jpg',
                                        'name' => 'Wheel',
                                    ],
                                ],
                                'idx' => 2,
                            ])
                        </div>
                        <div class="home-caro-img"
                            style="background: url('{{ asset('img/slides/third/Hilux-Adventure.jpg') }}') no-repeat center/cover">
                            <img src="{{ asset('img/slides/third/Hilux-Adventure.jpg') }}" alt=""
                                class="zoom animation" id="caro_img2"
                                style="--offset-scale: 1.2; --delay: 0; --offset-opacity: 1;">
                        </div>
                    </div>
                    <div class="carousel-item home-caro-item">
                        {{-- <div class="discover-more d-block">
                            <h5 id="home_caro_title">Hilux Revo</h5>
                            <p id="home_caro_text" class="discover-more-text" style="--text-color: #0009">
                                Hilux Revo's performance makes every road trip safe and secure!</p>
                        </div> --}}
                        <div class="justify-content-center d-flex">
                            @include('components.caro_parts', [
                                'data' => [
                                    [
                                        'path' => 'second/Hilux Leather Interior Design.jpg',
                                        'name' => 'Interior Design',
                                    ],
                                    [
                                        'path' => 'second/Hilux Leather LED Headlamps and Fog Lamps.jpg',
                                        'name' => 'LED Headlamps and Fog Lamps',
                                    ],
                                    [
                                        'path' => 'second/Hilux Leather Rear Combination Lamps.jpg',
                                        'name' => 'Rear Combination Lamps',
                                    ],
                                    [
                                        'path' => 'second/Hilux Leather Wheel.jpg',
                                        'name' => 'Wheel',
                                    ],
                                ],
                                'idx' => 3,
                            ])
                        </div>
                        <div class="home-caro-img"
                            style="background: url('{{ asset('img/slides/forth/Hilux Leather Grade.jpg') }}') no-repeat center/cover">
                            <img src="{{ asset('img/slides/forth/Hilux Leather Grade.jpg') }}" alt=""
                                class="zoom animation" id="caro_img3"
                                style="--offset-scale: 1.2; --delay: 0; --offset-opacity: 1;">
                        </div>
                    </div>
                    <button class="carousel-control-prev home-caro-control d-none d-sm-block" type="button"
                        data-bs-target="#homeCarousel" data-bs-slide="prev">
                        <i class="fa-solid fa-chevron-left home-caro-control-i" aria-hidden="true"></i>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next home-caro-control d-none d-sm-block" type="button"
                        data-bs-target="#homeCarousel" data-bs-slide="next">
                        <i class="fa-solid fa-chevron-right home-caro-control-i" aria-hidden="true"></i>
                        <span class="visually-hidden">Next</span>
                    </button>
                </div>
        </section>


        <div id="scroll_target"></div>

        {{-- Car Display --}}
        @php
            function carRoute($subCategory)
            {
                $route =
                    $subCategory->status == 1
                        ? // status 1
                        route('model.car-detail', $subCategory->id)
                        : // status 0
                        (count($subCategory->carModel) == 1 && !is_null($subCategory->carModel[0]->specification)
                            ? // 1 model with spec
                            route('model.car.specification', $subCategory->carModel[0]->specification->id)
                            : // not 1 model
                            (count($subCategory->carModel) > 1
                                ? // 2 or more models
                                route('model.car-detail', $subCategory->id)
                                : // no model
                                '#'));
                return $route;
            }
        @endphp
        @if (count($categories) > 0)
            <section>
                @php
                    $limit = isset($_SERVER['HTTP_USER_AGENT']) && strpos($_SERVER['HTTP_USER_AGENT'], 'Mobi') !== false ? 4 : 8;
                @endphp
                <div class="bg-white">
                    <ul class="nav nav-tabs nav-justified py-4 py-sm-4 car-display-nav container" id="carDisplayTab"
                        role="tablist">
                        {{-- All tag --}}
                        <li class="nav-item px-2 px-sm-4 border-end border-dark" role="presentation">
                            <button class="nav-link py-3 active" data-bs-toggle="tab" data-bs-target="#home_sub_category_0"
                                type="button" role="tab" aria-selected="true">All</button>
                        </li>
                        {{-- Category tag --}}
                        @foreach ($categories as $category)
                            @if (count($category->subCategories) > 0)
                                <li class="nav-item px-2 px-sm-4 @if (!$loop->last) border-end border-dark @endif"
                                    role="presentation">
                                    <button class="nav-link py-3" data-bs-toggle="tab"
                                        data-bs-target="#{{ 'home_sub_category_' . $category->id }}" type="button"
                                        role="tab" aria-controls="pickup-tab-pane"
                                        aria-selected="true">{{ $category->name ?? '' }}</button>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </div>
                <div class="tab-content pt-3 pt-md-3 pb-4 pb-md-4 container" id="carDisplayTabContent">
                    {{-- All tab --}}
                    <div class="tab-pane fade show active" id="home_sub_category_0" role="tabpanel">
                        <div class="row">
                            @foreach ($subCategories->take($limit) as $idx => $subCategory)
                                <div class="col-6 col-sm-4 col-md-3 p-4 p-xl-5 fade-y animation d-flex align-items-center"
                                    style="--delay: {{ 0.2 * $idx }}s">
                                    <a href="{{ carRoute($subCategory) }}"
                                        class="text-decoration-none text-dark d-flex flex-column justify-content-between h-100">
                                        <div class="d-flex flex-column justify-content-center h-100">
                                            <img src="{{ asset('storage/' . $subCategory->image) }}" alt=""
                                                width="100%" class="car-display">
                                        </div>
                                        <h6 class="car-display-title text-center mt-3">{{ $subCategory->name ?? '' }}</h6>
                                    </a>
                                </div>
                            @endforeach
                            {{-- See More --}}
                            @if (count($subCategories) > $limit)
                                <div class="collapse border-0 bg-color car-display-collapse" id="car_display_collapse0"
                                    data-span-tag="#more_less0">
                                    <div class="row">
                                        @for ($i = $limit; $i < count($subCategories); $i++)
                                            <div class="col-6 col-sm-4 col-md-3 p-4 p-xl-5 fade-y animation"
                                                style="--delay: {{ 0.2 * ($i - $limit) }}s">
                                                <a href="{{ carRoute($subCategories[$i]) }}"
                                                    class="text-decoration-none text-dark d-flex flex-column justify-content-between h-100">
                                                    <div class=" d-flex flex-column justify-content-center h-100">
                                                        <img src="{{ asset('storage/' . $subCategories[$i]->image) }}"
                                                            alt="" width="100%" class="car-display">
                                                    </div>
                                                    <h6 class="car-display-title text-center mt-3">
                                                        {{ $subCategories[$i]->name ?? '' }}
                                                    </h6>
                                                </a>
                                            </div>
                                        @endfor
                                    </div>
                                </div>
                                <div class="d-flex justify-content-center fade-y animation">
                                    <a class="btn btn-outline-dark rounded-0 px-5" data-bs-toggle="collapse"
                                        href="#car_display_collapse0" role="button" aria-expanded="false">
                                        View <span id="more_less0">More</span>
                                    </a>
                                </div>
                            @endif
                        </div>
                    </div>
                    {{-- Category tab --}}
                    @foreach ($categories as $category)
                        <div class="tab-pane fade" id="{{ 'home_sub_category_' . $category->id }}" role="tabpanel"
                            aria-labelledby="home-tab" tabindex="0">
                            <div class="row">
                                @foreach ($category->subCategories->take($limit) as $idx => $subCategory)
                                    <div class="col-6 col-sm-4 col-md-3 p-4 p-xl-5 fade-y animation"
                                        style="--delay: {{ 0.2 * $idx }}s">
                                        <a href="{{ carRoute($subCategory) }}"
                                            class="text-decoration-none text-dark d-flex flex-column justify-content-between h-100">
                                            <div class="d-flex flex-column justify-content-center h-100">
                                                <img src="{{ asset('storage/' . $subCategory->image) }}" alt=""
                                                    width="100%" class="car-display">
                                            </div>
                                            <h6 class="car-display-title text-center mt-3">{{ $subCategory->name ?? '' }}
                                            </h6>
                                        </a>
                                        {{-- <a href="" class="text-center car-display-link">Content Dealer</a> --}}
                                    </div>
                                @endforeach
                                {{-- See More --}}
                                @if (count($category->subCategories) > $limit)
                                    <div class="collapse border-0 bg-color car-display-collapse"
                                        id="car_display_collapse{{ $category->id }}"
                                        data-span-tag="#more_less{{ $category->id }}">
                                        <div class="row">
                                            @for ($i = $limit; $i < count($category->subCategories); $i++)
                                                <div class="col-6 col-sm-4 col-md-3 p-4 p-xl-5 fade-y animation"
                                                    style="--delay: {{ 0.2 * ($i - 7) }}s">
                                                    <a href="{{ carRoute($category->subCategories[$i]) }}"
                                                        class="text-decoration-none text-dark d-flex flex-column justify-content-between h-100">
                                                        <div class=" d-flex flex-column justify-content-between h-100">
                                                            <img src="{{ asset('storage/' . $category->subCategories[$i]->image) }}"
                                                                alt="" width="100%" class="car-display">
                                                        </div>
                                                        <h6 class="car-display-title text-center mt-3">
                                                            {{ $category->subCategories[$i]->name ?? '' }}
                                                        </h6>
                                                    </a>
                                                    {{-- <a href="" class="text-center car-display-link">Content Dealer</a> --}}
                                                </div>
                                            @endfor
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-center fade-y animation">
                                        <a class="btn btn-outline-dark rounded-0 px-5" data-bs-toggle="collapse"
                                            href="#car_display_collapse{{ $category->id }}" role="button"
                                            aria-expanded="false" aria-controls="collapseExample">
                                            View <span id="more_less{{ $category->id }}">More</span>
                                            {{ $category->name }}
                                        </a>
                                    </div>
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
            </section>
        @endif

        {{-- Video Carousel --}}
        <section>
            <div id="videoCarousel" class="carousel slide">
                <div class="carousel-indicators mb-3 mb-sm-4 m-0">
                    <button type="button" data-bs-target="#videoCarousel" data-bs-slide-to="0"
                        class="vd-caro-indicator active" aria-current="true" aria-label="Slide 1"></button>
                    <button type="button" data-bs-target="#videoCarousel" data-bs-slide-to="1"
                        class="vd-caro-indicator" aria-label="Slide 2"></button>
                    <button type="button" data-bs-target="#videoCarousel" data-bs-slide-to="2"
                        class="vd-caro-indicator" aria-label="Slide 3"></button>
                </div>
                <div class="carousel-inner">
                    <div class="carousel-item bg-darker-danger active">
                        <div class="vd-caro-item overflow-hidden">
                            <div class="vd-caro-explore-start position-absolute text-center">
                                <h5 class="text-light toyota-paragraph text-center fade-y animation">Once you’ve decided
                                    which pick-up meets your needs ! <br>
                                    It was very capable off road and had legendary reliability.</h5>
                                <div class="my-4 fade-y animation" style="--duration: 1.5s;">
                                    <a href="{{ route('inventory') }}"
                                        class="btn btn-outline-light px-4 rounded-0 toyota-paragraph">Explore Hilux</a>
                                </div>
                            </div>
                            <video height="100%" autoplay muted loop class="zoom animation"
                                style="--duration: 0.5s; --delay: 0.3s">
                                <source src="{{ asset('video/Toyota-Hilux-Overview-video.mp4') }}" type="video/mp4">
                                Your browser does not support the video tag.
                            </video>
                        </div>
                    </div>
                    <div class="carousel-item bg-darker-danger mid-caro">
                        <div class="vd-caro-item overflow-hidden">
                            <div class="vd-caro-text position-absolute text-center">
                                <h5 class="text-light toyota-paragraph text-center zoom animation"
                                    style="--offset-scale: 1.5"><b>Once you’ve decided which pick-up meets your needs !</b>
                                    <br>
                                    It was very capable off road and had legendary reliability.
                                </h5>
                            </div>
                            <div class="my-4 fade-y animation vd-caro-btn">
                                <a href="{{ route('inventory') }}"
                                    class="btn btn-outline-light px-4 rounded-0 toyota-paragraph">Explore Hilux</a>
                            </div>
                            <video autoplay muted loop class="py-5 my-5 zoom animation video-width"
                                style="--duration: 0.5s; --delay: 0.3s">
                                <source src="{{ asset('video/Toyota-Hilux-Overview-video.mp4') }}" type="video/mp4">
                                Your browser does not support the video tag.
                            </video>
                        </div>
                    </div>
                    <div class="carousel-item bg-darker-danger">
                        <div class="vd-caro-item overflow-hidden">
                            <div class="vd-caro-explore-end position-absolute">
                                <h5 class="text-light toyota-paragraph text-end"><span class="zoom-x animation d-block"
                                        style=" --offset-x: -25%">Once you’ve decided which pick-up meets your needs
                                        !</span> <br>
                                    <span class="text-dark zoom-x animation d-block"
                                        style="--duration: 1.5s; --offset-x: -25%">It was very capable off road and had
                                        legendary reliability.</span>
                                </h5>
                            </div>
                            <video width="100%" autoplay muted loop class="zoom animation"
                                style="--duration: 0.5s; --delay: 0.3s">
                                <source src="{{ asset('video/Toyota-Hilux-Overview-video.mp4') }}" type="video/mp4">
                                Your browser does not support the video tag.
                            </video>
                        </div>
                    </div>
                </div>
                <div class="d-none d-sm-block">
                    <button class="carousel-control-prev vd-caro-control" type="button" data-bs-target="#videoCarousel"
                        data-bs-slide="prev">
                        <i class="fa-solid fa-chevron-left vd-caro-control-i" aria-hidden="true"></i>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next vd-caro-control" type="button" data-bs-target="#videoCarousel"
                        data-bs-slide="next">
                        <i class="fa-solid fa-chevron-right vd-caro-control-i" aria-hidden="true"></i>
                        <span class="visually-hidden">Next</span>
                    </button>
                </div>
        </section>

        {{-- Middle Nav --}}
        <section class="container-fluid">
            <div class="row my-4 mx-2 mx-sm-0 mid-nav">
                <div class="col-6 col-sm-3 mid-nav-border border-end">
                    <a href="{{ route('inventory') }}"
                        class="text-decoration-none text-toyota-danger d-flex flex-column align-items-center py-3">

                        <img src="{{ asset('img/temp/search.png') }}" alt="Search Inventory" width="30px"
                            class="mb-2 zoom-x animation">

                        <span class="text-dark zoom-x animation" style="--duration: 1.5s;">Search&nbsp;Inventory</span>
                    </a>
                </div>
                <div class="col-6 col-sm-3 mid-nav-border border-sm-end">

                    <a href="{{ route('dealer') }}"
                        class="text-decoration-none text-toyota-danger d-flex flex-column align-items-center py-3">
                        <img src="{{ asset('img/temp/dealer.png') }}" alt="Find a Dealer" width="30px"
                            class="mb-2  zoom-x animation">

                        <span class="text-dark zoom-x animation" style="--duration: 1.5s;">Find a Dealer</span>
                    </a>
                </div>
                <div class="col-6 col-sm-3 mid-nav-border border-end border-xs-top">
                    <a href="{{ route('promotion') }}"
                        class="text-decoration-none text-toyota-danger d-flex flex-column align-items-center py-3">

                        <img src="{{ asset('img/temp/promotion.png') }}" alt="Promotion" width="35px"
                            class="mb-2 zoom-x animation" style="--offset-x: 100px">

                        <span class="text-dark zoom-x animation"
                            style="--duration: 1.5s; --offset-x: 100px">Promotion</span>
                    </a>
                </div>
                <div class="col-6 col-sm-3 mid-nav-border border-xs-top">
                    <a href="{{ route('discover.about') }}"
                        class="text-decoration-none text-toyota-danger d-flex flex-column align-items-center py-3">

                            <img src="{{ asset('img/temp/discover.png') }}" alt="Discover Toyota" width="30px"
                                class="mb-2 zoom-x animation" style="--offset-x: 100px">

                            <span class="text-dark zoom-x animation "
                                style="--duration: 1.5s; --offset-x: 100px">Discover&nbsp;Toyota</span>
                    </a>
                </div>
        </section>

        {{-- Web Banner --}}
        <section>
            <div class="row bg-darker-white">
                <div class="col-md-6 col-12 p-0 overflow-hidden">
                    <img src="{{ asset('img/temp/road_trip.jpg') }}" alt="" width="100%" height="100%"
                        class="zoom animation"
                        style="--offset-scale: 1.2; --delay: 0.5s; --offset-opacity: 1; object-fit: cover;">
                </div>
                <div class="col-md-6 col-12 p-0 d-flex align-items-center">
                    <div class="text-start p-5">
                        <h1 class="fade-y animation title-font-size">ROAD TRIP INTO THE ROUGH</h1>
                        <p class="fade-y animation toyota-paragraph my-4 my-sm-5" style="--duration: 1.5s">Experience the
                            beauty of Myanmar through this
                            documentary series with all new SKD Hilux Revo 2022.</p>
                        <div class="fade-y animation" style="--duration: 2s">
                            <a href="{{ route('inventory') }}" class="btn btn-outline-dark px-4 rounded-0">Discover
                                More</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row bg-dark dir-rtl">
                <div class="col-md-6 col-12 p-0 overflow-hidden">
                    <img src="{{ asset('img/temp/the_corolla.jpg') }}" alt="" width="100%" height="100%"
                        class="zoom animation"
                        style="--offset-scale: 1.2; --delay: 0.5s; --offset-opacity: 1; object-fit: cover;">
                </div>
                <div class="col-md-6 col-12 p-0 d-flex align-items-center">
                    <div class="text-end p-5 text-light" style="direction: ltr !important;">
                        <h1 class="fade-y animation title-font-size">THE COROLLA CROSS SERIES</h1>
                        <p class="fade-y animation toyota-paragraph my-4 my-sm-5 text-end" style="--duration: 1.5s">
                            Exuding strength and stability, the Corolla
                            Cross features a front grille with a wider base and curved corners, sleek Bi-Beam LED
                            headlights, and LED daytime running lights.
                        <p>
                        <div class=" fade-y animation" style="--duration: 2s">
                            <a href="{{ route('inventory') }}" class="btn btn-outline-light px-4 rounded-0">Discover
                                More</a>
                        </div>
                    </div>
                </div>

        </section>

        {{-- Used Car --}}
        @if ($usedCounts > 0)
            <section class="bg-darker-white">
                <div class="home-used-car container">
                    <h3 class="text-center pt-4 zoom animation">Used Car For Sale</h3>
                    <ul class="nav nav-tabs used-nav-tabs nav-justified py-2 px-md-5 mx-md-5 px-0 mx-0" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button class="nav-link used-car-tab active" data-bs-toggle="tab"
                                data-bs-target="#used_sub_categoty_0" type="button" role="tab">All</button>
                        </li>
                        @foreach ($usedCars as $used)
                            @if (count($used['available_cars']) > 0)
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link used-car-tab" data-bs-toggle="tab" type="button"
                                        data-bs-target="#{{ 'used_sub_categoty_' . $used['category_id'] }}"
                                        role="tab">{{ $used['category_name'] ?? '' }}</button>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                    <div class="tab-content w-100" id="carDisplayTabContent">
                        {{-- All --}}
                        <div class="tab-pane fade show active" id="used_sub_categoty_0" role="tabpanel">
                            <div class="row owl-carousel used-home-caro m-0">
                                @foreach ($usedCars as $category)
                                    @foreach ($category['available_cars'] as $usedCar)
                                        @include('used_car.card', [
                                            'image' => $usedCar->thumbnail,
                                            'name' => $usedCar->carModel->name,
                                            'engine' => $usedCar->engine,
                                            'transmission' => $usedCar->transmission,
                                            'fuel' => $usedCar->fuel,
                                            'price' => $usedCar->price,
                                            'id' => $usedCar->id,
                                            'col' => 'col-12',
                                        ])
                                    @endforeach
                                @endforeach
                            </div>
                        </div>
                        @foreach ($usedCars as $category)
                            <div class="tab-pane fade" id="{{ 'used_sub_categoty_' . $category['category_id'] }}"
                                role="tabpanel" aria-labelledby="home-tab" tabindex="{{ $category['category_id'] }}">
                                <div class="row owl-carousel used-home-caro m-0">
                                    @foreach ($category['available_cars'] as $usedCar)
                                        @include('used_car.card', [
                                            'image' => $usedCar->thumbnail,
                                            'name' => $usedCar->carModel->name,
                                            'engine' => $usedCar->engine,
                                            'transmission' => $usedCar->transmission,
                                            'fuel' => $usedCar->fuel,
                                            'price' => $usedCar->price,
                                            'id' => $usedCar->id,
                                            'col' => 'col-12',
                                        ])
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                        <div class="d-flex justify-content-end zoom-x animation" style="--offset-x: 100px; z-index:1;">
                            <a href="{{ route('used-car.index') }}" class="animated-link mb-4"
                                style="--line-color: #000;">View all <i class='la la-car fa-xl ms-2'></i></a>
                        </div>
                    </div>
                </div>
            </section>
        @endif

        {{-- Blog Carousel --}}
        @if (count($blogs) > 0)
            <section>
                <div class="container">
                    <h4 class="pb-2 pt-4" data-aos="fade-left" data-aos-duration="1000">Blogs</h4>
                    <div
                        class="owl-carousel owl-theme owl-loaded position-relative pt-2 pb-5 blog-carousel fade-y animation">
                        <div class="owl-stage-outer">
                            <div class="owl-stage">
                                @foreach ($blogs as $idx => $blog)
                                    <div class="owl-item blog-owl-item d-flex justify-content-center flex-column"
                                        style="--text-height: 45%">
                                        @include('blog.card', [
                                            $idx,
                                            'id' => $blog->id,
                                            'description' => $blog->slug,
                                            'title' => $blog->title,
                                            'blog_thumbnail' => $blog->blog_thumbnail,
                                            'no' => false,
                                        ])
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="d-flex justify-content-end zoom-x animation mt-3" style="--offset-x: 100px;">
                            <a href="{{ route('blog') }}" class="animated-link" style="--line-color: #000;">View all <i
                                    class='la la-book-open fa-xl ms-2'></i></a>
                        </div>
                    </div>
                </div>
            </section>
        @endif

    </article>
@endsection

@section('script')
    <script>
        $(document).ready(function() {

            // $('#homeCarousel').carousel({
            //     interval: false,
            // });

            // blog owl carousel
            var owl = $('.blog-carousel');
            owl.owlCarousel({
                loop: true,
                margin: 20,
                nav: true,
                dots: false,
                responsive: {
                    0: {
                        items: 1,
                    },
                    768: {
                        items: 2,
                    },
                    1134: {
                        items: 3,
                    }
                }
            });
            // owl.on('mousewheel', '.owl-stage', function(e) {
            //     if (e.originalEvent.deltaY > 0) {
            //         owl.trigger('next.owl');
            //     } else {
            //         owl.trigger('prev.owl');
            //     }
            //     e.preventDefault();
            // });
            $(".used-home-caro").owlCarousel({
                margin: 10,
                loop: false,
                dots: true,
                nav: true,
                responsive: {
                    0: {
                        items: 1,
                    },
                    1200: {
                        items: 2,
                    }
                }
            });

            org_imgs = ['first/Hilux Adventure.jpg', 'second/Revo.jpg', 'third/Hilux-Adventure.jpg',
                'forth/Hilux Leather Grade.jpg'
            ]

            $('.caro-part-img').click(function() {
                idx = $(this).attr('data-caro-target');
                console.log(idx)

                $('#caro_img' + idx).attr('src', $(this).attr('src'))
                // setTimeout(toOrg, 5000);
            })

            $('#homeCarousel').on('slide.bs.carousel', toOrg);

            function toOrg() {
                $.each(org_imgs, function(idx, ele) {
                    $('#caro_img' + idx).attr('src', 'img/slides/' + ele)
                });
            }

            $('.car-display-collapse').on('show.bs.collapse', function() {
                btn = $(this).attr('data-span-tag')
                $(btn).html('Less')
            })

            $('.car-display-collapse').on('hide.bs.collapse', function() {
                btn = $(this).attr('data-span-tag')
                $(btn).html('More')
            })
        })
    </script>
@endsection
