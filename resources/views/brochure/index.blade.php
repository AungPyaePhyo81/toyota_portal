@extends('layouts.main')

@section('title', 'Download Brochures')

@php
    function isActive($subCategories) {
        foreach ($subCategories as $subCategory) {
            $brochures = json_decode($subCategory->brochures, true);
            if (count($brochures) > 1 || !empty($brochures[0]['brochures'])) {
                return true;
            }
        }
        return false;
    }
@endphp

@section('content')
    <main class="px-sm-5 px-3 pb-4" style="min-height: 100vh">
        @include('components.breadcrumb')
        <header class="container py-4">
            <h2 class="fw-bold">Brochures</h2>
            <p>Get to know our vehicles better by downloading the brochures.</p>
        </header>
        <article class="container">
            <div class="row">
                <div class="col-3">
                    <ul class="toyota-paragraph nav nav-tab brochure-tab">
                        <li class="py-1">
                            <button type="button" class="w-100 text-start active" role="tab"
                                    data-bs-toggle="tab" data-bs-target="#brochure_sub_category">
                                All
                            </button>
                        </li>
                        @foreach ($categories as $category)
                            @if (isActive($category->subCategories))
                                <li class="py-1">
                                    <button type="button" class="w-100 text-start" role="tab"
                                            data-bs-toggle="tab" data-bs-target="#{{ 'brochure_sub_category'.$category->id}}">
                                        {{$category->name}}
                                    </button>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </div>

                <div class="col-9 tab-content">
                    {{-- All Tab --}}
                    <div class="tab-pane fade show active"
                        id="brochure_sub_category" role="tabpanel">
                        @foreach ($categories as $category)
                            @if (isActive($category->subCategories))
                                <h3 class="fw-bold py-3">{{$category->name}}</h3>
                            @endif
                            <div class="row">
                            @foreach ($category->subCategories as $subCategory)
                                @php
                                    $brochures = json_decode($subCategory->brochures, true);
                                @endphp
                                @foreach ($brochures as $brochure)
                                    @if ($brochure['brochures'] != '')
                                    <div class="col-12 col-sm-6 col-lg-4 pb-5">
                                        <div class="bg-white p-3 m-2">
                                            <img src="{{ asset('storage/' . $subCategory->image) }}" alt="" width="100%" class="my-5">
                                            <a href="{{ $subCategory->status == 1 ? route('model.car-detail', $subCategory->id) : route('model.car.specification', ['id' => $subCategory->id ?? 0]) }}" class="btn btn-toyota-outline-dark">{{ $subCategory->name ?? '' }}</a>
                                        </div>
                                        <h5 for="" class="mx-2">{{ basename($brochure['brochures']) ?? '' }}</h5>
                                        <a href="{{asset('storage/'.$brochure['brochures'])}}" target="_blank"
                                            class="animated-link-reverse text-danger mx-2"
                                            style="--line-color: var(--danger)">
                                            PDF {{round(filesize('storage/'.$brochure['brochures']) / (1024 * 1024), 2)}}MB
                                            <i class="fa-solid fa-chevron-right ms-1"></i>
                                        </a>
                                    </div>
                                    @endif
                                @endforeach
                            @endforeach
                            </div>
                        @endforeach
                    </div>
                    {{-- Category Tab --}}
                    @foreach ($categories as $category)
                        <div class="tab-pane fade"
                            id="{{ 'brochure_sub_category' . $category->id }}" role="tabpanel">
                            <h3 class="fw-bold py-3">{{$category->name}}</h3>
                            <div class="row">
                                @foreach ($category->subCategories as $subCategory)
                                    @php
                                        $brochures = json_decode($subCategory->brochures, true);
                                    @endphp
                                    @foreach ($brochures as $brochure)
                                        @if ($brochure['brochures'] != '')
                                        <div class="col-12 col-sm-6 col-lg-4 pb-5">
                                            <div class="bg-white p-3 m-2">
                                                <img src="{{ asset('storage/' . $subCategory->image) }}" alt="" width="100%" class="my-5">
                                                <button class="btn btn-toyota-outline-dark">{{ $subCategory->name ?? '' }}</button>
                                            </div>
                                            <h5 for="" class="mx-2">{{ basename($brochure['brochures']) ?? '' }}</h5>
                                            <a href="{{asset('storage/'.$brochure['brochures'])}}" target="_blank"
                                                class="animated-link-reverse text-danger mx-2"
                                                style="--line-color: var(--danger)">
                                                PDF {{round(filesize('storage/'.$brochure['brochures']) / (1024 * 1024), 2)}}MB
                                                <i class="fa-solid fa-chevron-right ms-1"></i>
                                            </a>
                                        </div>
                                        @endif
                                    @endforeach
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </article>
    </main>
@endsection
