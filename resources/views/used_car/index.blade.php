@extends('layouts.main')

@section('title', "Used Car for Sale")

@section('content')
    <main class="container-fluid px-sm-5 px-3 pb-4">
        @include('components.breadcrumb')
        <article class="container" id="container">
            <header class="d-flex justify-content-between py-4 flex-md-row flex-column">
                <div>
                    <h2 class="fw-bold">Used Car for Sale</h2>
                </div>
                <div id="search-form-container">
                    <form id="used-search-form" class="position-relative mt-3 mt-md-0" action="{{route('used-car.search')}}" method="GET">
                        <input type="search" name="term" id="used_search" value="{{ $search ?? '' }}" class="used-search" placeholder="Type here to search">
                        <button type="submit" class="search-icon"><i class="fa-solid fa-magnifying-glass"></i></button>
                    </form>
                </div>
            </header>
            @if (!isset($searchResult))
                <ul class="nav nav-tabs used-nav-tabs nav-justified py-2 px-md-5 mx-md-5 px-0 mx-0" navrole="tablist">
                    <li class="nav-item" role="presentation">
                        <button class="nav-link used-car-tab active"
                            data-bs-toggle="tab" data-bs-target="#used_sub_categoty_0"
                            type="button" role="tab">All</button>
                    </li>
                    @foreach ($usedCars as $used)
                        @if (count($used['available_cars']) > 0)
                            <li class="nav-item" role="presentation">
                                <button class="nav-link used-car-tab"
                                    data-bs-toggle="tab" id="home-tap"
                                    data-bs-target="#{{ 'used_sub_categoty_' . $used['category_id'] }}" type="button"
                                    role="tab" aria-controls="pickup-tab-pane"
                                    aria-selected="false">{{ $used['category_name'] ?? '' }}</button>
                            </li>
                        @endif
                    @endforeach
                </ul>
                <div class="tab-content" id="carDisplayTabContent1">
                    <div class="tab-pane fade show active"
                            id="used_sub_categoty_0" role="tabpanel">
                            <div class="row">
                            @foreach ($usedCars as $category)
                                @foreach ($category['available_cars'] as $usedCar)
                                    @include('used_car.card', [
                                        'image' => $usedCar->thumbnail,
                                        'name' => $usedCar->carModel->name,
                                        'engine' => $usedCar->engine,
                                        'transmission' => $usedCar->transmission,
                                        'fuel' => $usedCar->fuel,
                                        'price' => $usedCar->price,
                                        'id' => $usedCar->id,
                                    ])
                                @endforeach
                            @endforeach
                            </div>
                        </div>
                    @foreach ($usedCars as $category)
                        <div class="tab-pane fade"
                            id="{{ 'used_sub_categoty_' . $category['category_id'] }}" role="tabpanel"
                            aria-labelledby="home-tab" tabindex="{{ $category['category_id'] }}">
                            <div class="row">
                                @foreach ($category['available_cars'] as $usedCar)
                                    @include('used_car.card', [
                                        'image' => $usedCar->thumbnail,
                                        'name' => $usedCar->carModel->name,
                                        'engine' => $usedCar->engine,
                                        'transmission' => $usedCar->transmission,
                                        'fuel' => $usedCar->fuel,
                                        'price' => $usedCar->price,
                                        'id' => $usedCar->id,
                                    ])
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                </div>
            @else
                <h5 class="text-secondary">Search for: {{$search}}</h5>
                <div class="row">
                    @if (count($searchResult) > 0)
                        @foreach ($searchResult as $usedCar)
                            @include('used_car.card', [
                                'image' => $usedCar->thumbnail,
                                'name' => $usedCar->carModel->name,
                                'engine' => $usedCar->engine,
                                'transmission' => $usedCar->transmission,
                                'fuel' => $usedCar->fuel,
                                'price' => $usedCar->price,
                                'id' => $usedCar->id,
                            ])
                        @endforeach
                    @else
                    <div class="d-flex justify-content-center align-items-center" style="min-height: 50vh">
                        <h1 class="" style="color: #0005">
                            @foreach (str_split('No Results Found') as $i => $char)
                                <span class="fa-regular fa-bounce"
                                    style="--fa-animation-duration: 2s;
                                            --fa-animation-delay: {{$i*50}}ms;
                                            font-family: 'ToyotaType', system-ui;
                                            {{$char != ' ' ? 'margin:-5px' : 'margin:5px'}}">
                                    {{$char}}
                                </span>
                            @endforeach
                        </h1>
                    </div>
                    @endif

                </div>
            @endif

        </article>
    </main>
@endsection






