<div class="{{isset($col) ? $col : 'col-lg-6'}} col-12 py-3">
    <div class="used-card rounded row m-1 overflow-hidden">
        <a href="{{ route('used-car.detail', $id) }}" class="col-sm-7 col-12 d-flex justify-content-center overflow-hidden used-card-img p-0">
            <img src="{{asset('storage/' . $image)}}" alt="" class="zoom animation" style="--offset-scale: 1.2">
        </a>

        <div class="col-sm-5 col-12 p-4 d-flex flex-column justify-content-between">
            <div>
                <h5 class="text-light py-2 fade-y animation">{{$name}}</h5>
                <ul class="text-light used-card-list">
                    {!! "<li class='used-list-item engine-light fade-y animation' style='--delay: 0.3s'>$engine</li>" !!}
                    {!! "<li class='used-list-item gear-light fade-y animation' style='--delay: 0.4s'>$transmission</li>" !!}
                    {!! "<li class='used-list-item fuel-light fade-y animation' style='--delay: 0.5s'>$fuel</li>" !!}
                    {!! "<li class='used-list-item coin-light fade-y animation' style='--delay: 0.6s'>Starting From <br> $price Lakhs</li>" !!}
                </ul>
            </div>
            <div>
                <a href="{{ route('used-car.detail', $id) }}" class="btn btn-toyota-danger w-100 used-card-btn zoom animation"></a>
            </div>
        </div>
    </div>
</div>
