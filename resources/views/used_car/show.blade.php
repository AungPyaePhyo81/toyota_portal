@extends('layouts.main')

@php
    function infoList($label, $icon, $data){
        $result = isset($data) ? "<li class='used-list-item d-flex justify-content-between $icon'>
                        $label <span class='text-toyota-dark'>$data</span>
                    </li>" : '';
        return $result;
    }
    $name = $usedCar->carModel->name;
@endphp

@section('title', "$name for Sale")

@section('content')
    <main class="container-fluid px-sm-5 px-3 pb-4">
        @include('components.breadcrumb',
                ['data' => [['route' => 'used-car.index',
                            'name' => 'Used Car for Sale']]])
        <header class="py-4">
            <h1>Used Car for Sale</h1>
        </header>
        <article class="bg-light p-4">
            <div class="row">
                <div class="col-lg-7 col-sm-12">
                    <img src="{{asset('storage/' . $usedCar->thumbnail)}}" alt="view" width="100%" id="detail_view">
                    <div class="owl-carousel owl-theme owl-loaded used-carousel py-2">
                        @php
                            $images = json_decode($usedCar->image, true);
                    @endphp
                        <div class="owl-stage-outer">
                            <div class="owl-stage">
                                <div class="owl-item used-owl-item"
                                    style='background: center / cover no-repeat url("{{ asset('storage/'. $usedCar->thumbnail) }}");'
                                    data-src="{{ asset('storage/'. $usedCar->thumbnail) }}">
                                </div>
                                @foreach ($images as $img)
                                    <div class="owl-item used-owl-item"
                                        style='background: center / cover no-repeat url("{{asset('storage/'.$img['image'])}}");'
                                        data-src="{{asset('storage/'.$img['image'])}}">
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-sm-12">
                    <h4>{{$usedCar->carModel->name}}</h4>
                    <div class="used-list-item coin-danger text-toyota-danger fs-4 my-2">Start From {{$usedCar->price}} Lakhs</div>
                    <div class="text-secondary pt-3">Specification</div>
                    <ul class="used-card-list text-toyota-secondary">
                            {!! infoList("Engine Power", 'engine-dark', $usedCar->engine) !!}
                            {!! infoList("Transmission", 'gear-dark', $usedCar->transmission) !!}
                            {!! infoList("Fuel", 'fuel-dark', $usedCar->fuel) !!}
                            {!! infoList("Colour", 'color-dark', $usedCar->color ? $usedCar->color->name : null) !!}
                            {!! infoList("Car Number", 'license-dark', $usedCar->car_no) !!}
                            {!! infoList("Steering Position", 'steering-dark', $usedCar->steering_pos) !!}
                            {!! infoList("Trim/Grade", 'star-dark', $usedCar->trim_grade) !!}
                            {!! infoList("Driven Kilometer", 'dashboard-dark', $usedCar->driven_kilo) !!}
                            {!! infoList("Model Year", 'calender-dark', $usedCar->model_year) !!}
                        <li class="used-list-item d-flex justify-content-between phone-dark">
                            Contact Number <a href="tel:{{$usedCar->contact_no}}" class="text-toyota-dark animated-link" style="--line-color: #000">{{$usedCar->contact_no}}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </article>
    </main>
@endsection

@section('script')
    <script>
        $( document ).ready(function() {
            var owl = $('.owl-carousel');
            owl.owlCarousel({
                loop:false,
                margin:10,
                nav:true,
                dots: false,
                responsive:{
                    0:{
                        items:1,
                    },
                    300:{
                        items:2,
                    },
                    600:{
                        items:3,
                    },
                    900:{
                        items:4,
                    }
                }
            });
            owl.on('mousewheel', '.owl-stage', function (e) {
                if (e.originalEvent.deltaY > 0) {
                    owl.trigger('next.owl');
                } else {
                    owl.trigger('prev.owl');
                }
                e.preventDefault();
            });

            $('.used-owl-item').click(function(){
                $('#detail_view').attr('src', $(this).attr('data-src'))
            })
        });
    </script>
@endsection
