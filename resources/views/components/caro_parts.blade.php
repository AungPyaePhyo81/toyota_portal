<div class="home-caro-wrapper">
    <div class="home-caro-parts d-none d-md-flex">
        @foreach ($data as $val)
            <div class="p-0 m-0 d-flex flex-column align-items-center">
                <div class="caro-img-container w-100">
                    <img src='{{"img/slides/".$val['path']}}' class="caro-part-img" data-caro-target="{{$idx ?? ''}}" alt="">
                </div>
                <label class="text-light caro-part-label">{{$val['name']}}</label>
            </div>
        @endforeach
    </div>
</div>

<div class="home-caro-wrapper d-block d-md-none">
    <div class="home-caro-parts-sm row">
        @foreach ($data as $val)
            <div class="col-6 col-sm-3 p-0 m-0 d-flex flex-column align-items-center">
                <div class="caro-img-container" style="max-height: 50px">
                    <img src='{{"img/slides/".$val['path']}}' class="caro-part-img" data-caro-target="{{$idx ?? ''}}" alt="">
                </div>
                <label class="text-light caro-part-label">{{$val['name']}}</label>
            </div>
        @endforeach
    </div>
</div>
