<nav class="breadcrumb-nav pt-4">
    <a href="{{url('/')}}" class="breadcrumb-link">
        <i class="fa-solid fa-angle-left me-2"></i>
        <span>Back to Home Page</span>
    </a>
    @if (isset($data))
        @foreach ($data as $val)
            <a href="{{route($val['route'])}}" class="breadcrumb-link">
                <i class="fa-solid fa-angle-left mx-2"></i>
                <span>{{$val['name']}}</span>
            </a>
        @endforeach
    @endif
</nav>
