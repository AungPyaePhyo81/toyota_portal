<div class="bg-light px-sm-5 px-3 py-sm-5 py-4 stay-informed-form">
    <div class="container">
        <form action="{{ route('stay-informed.store') }}" id="stay_informed" method="POST">
            @csrf
            <h3 class="fw-bold">STAY INFORMED</h3>
            <p class="py-3">If you would like to stay up-to-date on the latest Toyota vehicles, news, and promotions
                please complete the form below.</p>

            <div class="input-group flex-column mb-5 overflow-hidden">
                <label for="name" class="fw-bold">Name</label>
                <input type="text" name="name" id="name" class="toyota-input" placeholder="Full Name">
                @error('name')
                    <div class="form-error pt-1 px-0 text-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="input-group flex-column mb-5 overflow-hidden">
                <label for="email" class="fw-bold">Email address</label>
                <input type="email" name="email" id="email" class="toyota-input" placeholder="Email Address">
                @error('email')
                    <div class="form-error pt-1 px-0 text-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="input-group flex-column mb-5">
                <label for="phone" class="fw-bold">Phone Number</label>
                <input style="width: 100%;" id="mobile_code" class="toyota-input" type="tel" name="phone"
                    placeholder="Phone Number">

                {{-- <input type="phone" name="phone" id="phone" class="toyota-input" placeholder="Phone Number"> --}}
                @error('phone')
                    <div class="form-error pt-1 px-0 text-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="mb-5 form-check">
                <input type="checkbox" class="form-check-input me-2" id="privacy" name="privacy_confirm"
                    value="1">
                <label class="form-check-label" for="privacy"> By submitting my information, I confirm that all
                    information and details of myself are true, correct and complete and acknowledge that I have read
                    and understood the Privacy Policy. I consent and authorise the Company to collect, use and disclose
                    my personal information and contact me via Mail/Email/Telephone/SMS-text.</label>
                @error('privacy_confirm')
                    <div class="form-error pt-1 px-0 text-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="mb-5 form-check">
                <input type="checkbox" class="form-check-input me-2" id="policy" name="notification" value="1">
                <label class="form-check-label" for="policy"> I would like to receive marketing information about
                    goods and services which may be provided by the Company, including (but not limited to) offers,
                    promotions and information about new goods and services by the Company, affiliates, insurance
                    companies or trusted third party vendors.</label>
            </div>
            <input type="submit" value="SUBMIT" class="btn btn-toyota-danger px-4" id="submitBtn">
        </form>
    </div>
</div>

@section('js')
    <script>
        // Intel Phone Number
        $(document).ready(function() {
            $('#sort_select').change(function() {
                $('#sort_form').submit();
            })
            const input = document.querySelector("#mobile_code");
            window.intlTelInput(input, {
                initialCountry: "mm",
                hiddenInput: "dial_code",
                utilsScript: "{{ asset('js/utils.js') }}",
            });
        })
    </script>
@endsection
