@extends('layouts.main')

@section('title', 'OEM Warranty')

@section('content')
    <main class="px-sm-5 px-3 pb-4">
        @include('components.breadcrumb')
        <header class="container py-4">
            <h2 class="fw-bold" data-aos="zoom-out" data-aos-duration='1000'>OEM Warranty</h2>
        </header>
        <article class="container">
            {{-- !-------------------Myanmar----------------------------------------------------------------------------------------! --}}
            <div id="mm_paragraph" style="display: none;">
                <h3 class="fw-bold" data-aos="zoom-out" data-aos-duration='1500'>Toyota Warranty ဆိုတာဘာလဲ?</h3>
                <p class="toyota-paragraph" data-aos="fade-up" data-aos-duration='1000'>Toyota ကားတွင်ထည့်သွင်းထားသော
                    အစိတ်အပိုင်းများတွင် ထုတ်လုပ်သူကြောင့်ဖြစ်သော တည်ဆောက်ပုံ/ထုတ်လုပ်မှုဆိုင်ရာ ချို့ယွင်းမှုများအတွက်
                    ပြင်ဆင်ခြင်း အစားထိုး လဲလှယ်ပေးခြင်းကို Warranty စာအုပ်ငယ်တွင် ဖော်ပြထားသော
                    စည်းကမ်းသတ်မှတ်ချက်များနှင့်အညီ Warranty ကာလအတွင်း လက်ခ၊ ပစ္စည်းခနှင့် အခြားလိုအပ်သော ပြင်ပ
                    ကုန်ကျစရိတ်များကို အခမဲ့ဆောင် ရွက် ပေးခြင်း ဖြစ်ပါသည်။ Warranty နှင့် ပြုလုပ်သော ပြုပြင်မှုများကို
                    Toyota မှအသိအမှတ်ပြုထားသော Service Centers များ တွင်သာ လုပ်ဆောင်ရပါမည်။</p>
                <br>
                <h3 class="fw-bold" data-aos="zoom-out" data-aos-duration='2000'>အာမခံ အခြေအနေ</h3>
                <p class="toyota-paragraph" data-aos="fade-up" data-aos-duration='1500'>Warranty ကာလသည် အချိန်ကာလ နှင့်
                    မောင်းနှင်လိုက်သည့် ခရီးအကွာအဝေးပေါ် မူတည်၍ သတ်မှတ်ထားခြင်း ဖြစ်သည်။ Toyota မှ အာမခံပေးထားသည်မှာ
                    သုံးနှစ်ကာ လ သို့မဟုတ် ကီလို မီတာ တစ်သိန်းဖြစ်ပြီး ကာလနှစ်ရပ်အနက် ဦးစွာရောက်ရှိသွားသည့် ကာလအထိ
                    အကျုံးဝင်သည်။ Warrantyအကျုံးဝင်သည့်ကာလနှင် ခရီးအကွာအ ဝေး သည် ယာဉ်ကို ဝယ်သူထံသို့ ပထမဆုံး
                    စတင်လွှဲအပ်သည့်နေ့မှ စတင်သည်။</p>
                <br>
                <h3 class="fw-bold" data-aos="zoom-out" data-aos-duration='2500'>အကျုံးဝင်သည့်အပိုင်း</h3>
                <div class="d-flex justify-content-center align-items-center" data-aos="fade-up" data-aos-duration='2000'>
                    <table class="table table-bordered toyota-table">
                        <thead>
                            <tr>
                                <th>Warranty Policy</th>
                                <th>Warranty Period​</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Vehicle</td>
                                <td>3 years / 100,000 kilometers​</td>
                            </tr>
                            <tr>
                                <td>Surface Rust, Paint Damage, Corrosion</td>
                                <td>3 years / 100,000 kilometers​</td>
                            </tr>
                            <tr>
                                <td>Surface Rust, Paint Damage on deck <br>
                                    panels of Pickup Trucks</td>
                                <td>1 year / 20,000 kilometers​</td>
                            </tr>
                            <tr>
                                <td>Battery</td>
                                <td>1 year / 20,000 kilometers​</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <br>
                <h3 class="fw-bold py-3" data-aos="zoom-out" data-aos-duration='1000'>အကျုံးမဝင်သည့်အပိုင်း</h3>
                <h4 class="fw-bold py-2" data-aos="fade-up" data-aos-duration='1200'>မတော်တဆမှု၊ အသုံးလွဲမှားမှု၊
                    လွန်ကဲစွာအသုံးပြုမှုနှင့် Toyota မှခွင့်ပြုထားသောပစ္စည်းများ အသုံးမပြုမှု</h4>
                <p class="toyota-paragraph" data-aos="fade-up" data-aos-duration='1400'>အရှိန်ပြင်းပြင်းမောင်းခြင်း၊
                    သတ်မှတ်ချက်ကျော်လွန်၍ဝန်တင်ခြင်းစသဖြင့် ပစ်စလက်ခတ်သုံးခြင်း၊ ထိန်းသိမ်းမှုမရှိခြင်း၊
                    နဂိုမူရင်းကိုပြင်ဆင်ပြောင်းလဲခြင်း၊ ကလိခြင်း၊ တစ်စုံတစ်ခုကိုဖြုတ်ပစ် ခြင်း ၊
                    ချိန်ညှိရာပြင်ဆင်ရာတို့တွင်စနစ်မကျခြင်း၊ ယာဉ်တိုက်မှုဖြစ်ခြင်း နှင့်
                    ခွင့်ပြုထားခြင်းမရှိသောအပိုဆောင်းပစ္စည်းများထည့်ခြင်း၊ ပြင်ပနေရာများတွင် ပြုပြင်ထားခြင်းတို့အတွက်
                    အကျုံးမ ဝင်ပါ။​</p>
                <br>
                <h4 class="fw-bold py-2" data-aos="fade-up" data-aos-duration='1600'>ပြုပြင်ထိန်းသိမ်းမှုမရှိခြင်း၊
                    လောင်စာဆီ၊ စက်ဆီ၊ ချောဆီများ မှားယွင်းစွာ သုံးစွဲခြင်း။</h4>
                <p class="toyota-paragraph" data-aos="fade-up" data-aos-duration='1800'>လိုအပ်သည့်ထိန်းသိမ်းမှုမရှိခြင်း
                    သို့မဟုတ် ထိန်းသိမ်းမှုနည်းစနစ်မကျခြင်း၊ ယာဉ်ပိုင်ရှင်လက်စွဲတွင် ထည့်သွင်းသတ်မှတ်ထားခြင်းမရှိသည့်
                    လောင်စာဆီစသည်တို့ကို အသုံးမပြုခြင်းကြောင့် လို အပ်လာသော ပြင်ဆင်ချိန်ညှိခြင်းများနှင့် အကျုံးမဝင်ပါ။</p>
                <br>
                <h4 class="fw-bold py-2" data-aos="fade-up" data-aos-duration='2000'>ပြင်ပအကြောင်းအရာများနှင့်
                    သဘာဝဘေးအန္တရာယ်</h4>
                <p class="toyota-paragraph" data-aos="fade-up" data-aos-duration='2200'>ကျောက်ခဲအစအနမှန်ခြင်း၊ အရောင်တင်ရာ၌
                    မျက်နှာပြင်ဆေးသားတိုက်စားမှုနှင့် ခြစ်ရာထင်ခြင်းများ အကျုံးမဝင်ပါ။ အက်စစ်မိုး၊ လေနှင့်ပါလာသော ဓာတုဗေဒ
                    ပစ္စည်း၊ သစ်စေး စသည်၊ ဆား၊ မိုးသီး၊ မုန်တိုင်း၊ မိုးကြိုး၊ ရေကြီးခြင်း စသော မမျှော်မှန်းနိုင်သည့်
                    သဘာဝဘေးဒဏ်ကျရောက်မှုများကြောင့် ပေါ်ပေါက်သည့် မျက်နှာပြင်၊ ဆေးသားတိုက်စားခြင်း၊ ပျက်စီးခြင်းတို့သည်
                    အာမခံအ ကျုံးမဝင်ပါ။</p>
                <br>
                <h4 class="fw-bold py-2" data-aos="fade-up" data-aos-duration='2400'>ကားတာယာ</h4>
                <p class="toyota-paragraph" data-aos="fade-up" data-aos-duration='2600'>ကားတာယာများအတွက် တာယာထုတ်လုပ်သူတို့၏
                    သီးခြားဝန်ဆောင်မှုဆိုင်ရာ အာမခံချက်ရှိသည်။​</p>
                <br>
            </div>
            {{-- --!-------------------english----------------------------------------------------------------------------------------! --}}
            <div id="en_paragraph">
                <h3 class="fw-bold" data-aos="zoom-out" data-aos-duration='1500'>What is Toyota Warranty?</h3>
                <p class="toyota-paragraph" data-aos="fade-up" data-aos-duration='1000'>Toyota warranty is written guarantee
                    for a quality of product and declared the manufacturer’s responsibility to repair or replace defective
                    components caused by any design or manufacturing fault, which are installed in our Toyota car during the
                    warranty period with certain terms and conditions specified in the warranty booklet. Warranty repair
                    will be made free of charge for labor cost, parts cost and sublet cost. Warranty repair shall be
                    performed only at Toyota authorized Service Shops.
                </p>
                <br>
                <h3 class="fw-bold" data-aos="zoom-out" data-aos-duration='2000'>Warranty Condition
                </h3>
                <p class="toyota-paragraph" data-aos="fade-up" data-aos-duration='1500'>The warranty period is specified as
                    the number of months elapsed or the distance that a vehicle has travelled since the delivery date. Here,
                    the warranty covers for a period of 36 months or 100,000 kilometers, whichever comes first. The warranty
                    period begins on the date that the vehicle is first delivered to the customer.
                </p>
                <br>
                <h3 class="fw-bold" data-aos="zoom-out" data-aos-duration='2500'>What is Covered
                </h3>
                <div class="d-flex justify-content-center align-items-center" data-aos="fade-up" data-aos-duration='2000'>
                    <table class="table table-bordered toyota-table">
                        <thead>
                            <tr>
                                <th>Warranty Policy</th>
                                <th>Warranty Period​</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Vehicle</td>
                                <td>3 years / 100,000 kilometers​</td>
                            </tr>
                            <tr>
                                <td>Surface Rust, Paint Damage, Corrosion</td>
                                <td>3 years / 100,000 kilometers​</td>
                            </tr>
                            <tr>
                                <td>Surface Rust, Paint Damage on deck <br>
                                    panels of Pickup Trucks</td>
                                <td>1 year / 20,000 kilometers​</td>
                            </tr>
                            <tr>
                                <td>Battery</td>
                                <td>1 year / 20,000 kilometers​</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <br>
                <h3 class="fw-bold py-3" data-aos="zoom-out" data-aos-duration='1000'>What is Not Covered
                </h3>
                <h4 class="fw-bold py-2" data-aos="fade-up" data-aos-duration='1200'>Accidents, Misuse, Abuse & Usage of
                    Toyota Non-Genuine Parts
                </h4>
                <p class="toyota-paragraph" data-aos="fade-up" data-aos-duration='1400'>Repairs and adjustments required as
                    a result of misuse (e.g., racing, overloading), negligence, modification, alteration, tempering,
                    disconnection, improper adjustments or repairs, accidents, and the use of aftermarket add-on parts or
                    materials, or replacement of outside parts rather than Toyota Genuine Parts and repair in with other
                    than Toyota authorized dealers shall not be covered.
                </p>
                <br>
                <h4 class="fw-bold py-2" data-aos="fade-up" data-aos-duration='1600'>Lack of Maintenance or Use of Wrong
                    Fuel, Oil or Lubes
                </h4>
                <p class="toyota-paragraph" data-aos="fade-up" data-aos-duration='1800'>Repairs and adjustments caused by
                    improper maintenance, lack of required maintenance or the use of fluids other than the fluids specified
                    in your Owner’s Manual are not covered.
                </p>
                <br>
                <h4 class="fw-bold py-2" data-aos="fade-up" data-aos-duration='2000'>External Factors & Natural Disaster
                </h4>
                <p class="toyota-paragraph" data-aos="fade-up" data-aos-duration='2200'>This warranty coverage is invalid
                    for factors beyond the manufacturer's control, such as cosmetic or surface corrosion from stone chips or
                    paint scratches. Damage or surface corrosion from the environment such as acid rain, airborne fall-out
                    (e.g., chemicals, tree sap), salt, hail, windstorms, lightning, and floods also shall not covered.
                </p>
                <br>
                <h4 class="fw-bold py-2" data-aos="fade-up" data-aos-duration='2400'>Tires</h4>
                <p class="toyota-paragraph" data-aos="fade-up" data-aos-duration='2600'>Tires are warranted under a
                    separate warranty provided by the tire manufacturer.
                    ​</p>
                <br>
            </div>
        </article>

    </main>

    <div class="w-100 bg-more-darker-white d-flex flex-column flex-sm-row justify-content-evenly align-items-center text-secondary"
        style="--line-color: var(--bs-gray)">
        <div class="py-4 toyota-paragraph">
            <img src="{{ asset('img/temp/bookservice.png') }}" alt="" width="14%">
            <a href="{{ route('service-support.book-service') }}" class="animated-link-reverse">Book A Service</a>
        </div>
        <div class="py-4 toyota-paragraph">
            <img src="{{ asset('img/temp/finddealer.png') }}" alt="" width="12%" class="mt-1">
            <a href="{{ route('dealer') }}" class="animated-link-reverse">Find A Service Centers</a>
        </div>
    </div>
@endsection
