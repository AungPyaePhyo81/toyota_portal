@extends('layouts.main')

@section('title', "Online Enquiry")

@section('content')
    <main style="background: center / cover no-repeat url('img/temp/enquiry_bg.png') fixed">
        <section class="bg-white form-offcanvas">
            <div class="container p-5">
                <h1 class="fw-bold py-3">ENQUIRY FOR SPARE PARTS</h1>
                <form action="{{route('enquiry.store')}}" method="Post">
                    @csrf
                    <div class="py-3">
                        <label for="" class="fw-bold toyota-paragraph">Name</label>
                        <input type="text" name="name" id="" class="form-control toyota-input" placeholder="Type your name">
                        @error('name')
                        <div class="form-error pt-1 px-0 text-danger">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="py-3">
                        <label for="" class="fw-bold toyota-paragraph">Address</label>
                        <textarea type="text" name="address" id="" class="form-control toyota-textarea" placeholder="Type your address" rows="10"></textarea>
                        @error('address')
                        <div class="form-error pt-1 px-0 text-danger">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="py-3">
                        <label for="" class="fw-bold toyota-paragraph">Email Address</label>
                        <input type="text" name="email" id="" class="form-control toyota-input" placeholder="Type your email">
                        @error('email')
                        <div class="form-error pt-1 px-0 text-danger">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <input type="submit" value="SUBMIT" class="btn-toyota-danger btn fw-bold px-4 my-3">
                </form>
            </div>
        </section>
    </main>
@endsection
