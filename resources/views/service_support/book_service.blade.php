@extends('layouts.main')

@section('title', "Book a Service")

@section('css')
    <link rel="stylesheet" href="{{ asset('css/intlTelInput.min.css') }}">
    <style>
        .iti--allow-dropdown {
            padding-top: 2%;
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <main style="background: center / cover no-repeat url('/img/service_support/service_book_bg.png') fixed">
        <section class="bg-white form-offcanvas">
            <div class="container p-5">
                <h1 class="fw-bold py-3">Book A Service</h1>
                <p class="toyota-paragraph w-75 text-secondary">
                    You will receive a confirmation email and a reminder for your service nearing the date. We look forward
                    to having you at our service center.
                </p>
                <form action="{{ route('service-booking.store') }}" method="POST">
                    @csrf
                    <div class="py-3">
                        <label for="" class="fw-bold toyota-paragraph">Type of service</label>
                        <select name="type_of_service_id" id="type_of_service_id" class="toyota-input form-select">
                            <option value="" disabled selected>Select Type of Service</option>
                            @foreach ($typeOfServices as $typeOfService)
                                <option value="{{ $typeOfService->id }}">{{ $typeOfService->name }}</option>
                            @endforeach
                            {{-- @for ($i = 0; $i < 4; $i++)
                                <option value="{{ $i }}">Type of Service 0{{ $i }}</option>
                            @endfor --}}
                        </select>
                        @error('type_of_service_id')
                            <div class="form-error pt-1 px-0 text-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="py-3">
                        <label for="" class="fw-bold toyota-paragraph">Province</label>
                        <select name="province_id" id="province_id" class="toyota-input form-select">
                            <option value="" disabled selected>Select a Province</option>
                            {{-- @for ($i = 0; $i < 4; $i++)
                                <option value="{{ $i }}">Province 0{{ $i }}</option>
                            @endfor --}}
                            @foreach ($provinces as $province)
                                <option value="{{ $province->id }}">{{ $province->name }}</option>
                            @endforeach
                        </select>
                        @error('province_id')
                            <div class="form-error pt-1 px-0 text-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="py-3">
                        <label for="" class="fw-bold toyota-paragraph">Service Center</label>
                        <select name="dealer_info_id" id="dealer_info_id" class="toyota-input form-select">
                            <option value="" disabled selected>Select Service Center</option>
                            {{-- @for ($i = 0; $i < 4; $i++)
                                <option value="{{ $i }}">Service Center 0{{ $i }}</option>
                            @endfor --}}
                            {{-- @foreach ($dealerInfos as $dealerInfo)
                                <option value="{{ $dealerInfo->id }}">{{ $dealerInfo->map->name }}</option>
                            @endforeach --}}
                        </select>
                        @error('dealer_info_id')
                            <div class="form-error pt-1 px-0 text-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="row py-3">
                        <div class="col-6">
                            <label for="date" class="fw-bold toyota-paragraph">Preferred date for
                                service</label>
                            <input type="date" class="toyota-input w-100" placeholder="Date" id="date"
                                name="date">
                            @error('date')
                                <div class="form-error pt-1 px-0 text-danger">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <div class="col-6">
                            <label for="time" class="fw-bold toyota-paragraph">Preferred time for
                                service</label>
                            <input type="time" class="toyota-input w-100" placeholder="time" id="time"
                                name="time">
                            @error('time')
                                <div class="form-error pt-1 px-0 text-danger">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                    </div>
                    <div class="py-3">
                        <label for="name" class="fw-bold toyota-paragraph">Name</label>
                        <input type="text" class="toyota-input w-100" placeholder="Name" id="name" name="name">
                        @error('name')
                            <div class="form-error pt-1 px-0 text-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                    <div class="row py-3">
                        <div class="col-6">
                            <label for="email" class="fw-bold toyota-paragraph">Email Address
                            </label>
                            <input type="email" class="toyota-input w-100" placeholder="Email address" id="email"
                                name="email">
                            @error('email')
                                <div class="form-error pt-1 px-0 text-danger">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <div class="col-6">
                            <label for="time" class="fw-bold toyota-paragraph">Phone Number</label>
                            <input type="tel" class="toyota-input w-100" placeholder="Phone number" id="mobile_code"
                                type="tel" name="phone">
                            {{-- <input style="width: 100%;" id="mobile_code" class="toyota-input" type="tel" name="phone"
                                placeholder="Phone Number"> --}}
                            @error('phone')
                                <div class="form-error pt-1 px-0 text-danger">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                    </div>
                    <div class="py-3 form-check">
                        <input type="checkbox" class="form-check-input me-2" id="privacy" name="notification"
                            value="1">
                        <label class="form-check-label" for="notification"> By submitting my information, I confirm that all
                            information and details of myself are true, correct and complete and acknowledge that I have
                            read
                            and understood the Privacy Policy. I consent and authorise the Company to collect, use and
                            disclose
                            my personal information and contact me via Mail/Email/Telephone/SMS-text.</label>
                        @error('notification')
                            <div class="form-error pt-1 px-0 text-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <input type="submit" value="SUBMIT" class="btn-toyota-danger btn fw-bold px-4 my-3"
                        onclick="deleteConfirm('hhhii')">
                </form>
            </div>
        </section>
    </main>
@endsection

@section('script')
    <script src="{{ asset('js/intlTelInput.min.js') }}"></script>

    <script>
        $(document).ready(function() {

            const input = document.querySelector("#mobile_code");
            window.intlTelInput(input, {
                initialCountry: "mm",
                hiddenInput: "dial_code",
                utilsScript: "{{ asset('js/utils.js') }}",
            });

            $('#dealer_info_id').on('focus', function() {
                let type_of_service_id = $('#type_of_service_id').val();
                let province_id = $('#province_id').val();
                //$('#state-dropdown').html('');

                $.ajax({
                    type: 'POST',
                    url: "{{ route('ajax.service-centers-by-province') }}",
                    data: {
                        type_of_service_id: type_of_service_id,
                        province_id: province_id,
                        _token: "{{ csrf_token() }}"
                    },
                    dataType: 'json',

                    success: function(response) {
                        console.table(response);
                        $('#dealer_info_id').html(
                            `<option value="" disabled selected>Select Service Center</option>`
                        );

                        $.each(response, function(key, val) {
                            $('#dealer_info_id').append(`
                                <option value="${val.id}">${val.map.name}</option>
                            `);
                        });
                    }
                })
            });
        })
    </script>
@endsection
