@extends('layouts.main')

@section('title', 'Spare Part')

@section('css')
    <style>
        .bg-dark {
            background-color: var(--black) !important;
        }

        .img-div {
            width: 180px;
            height: 230px
        }
    </style>
@endsection

@section('content')
    <main class="bg-toyota-light">
        <div class="container-fluid bg-toyota-light px-sm-5 px-3 pb-4">
            @include('components.breadcrumb')

        </div>
        <article class="overflow-hidden">
            <img src="{{ asset('img/spare_part/spare_part_banner.png') }}" alt="" width="100%" data-aos="zoom-out"
                data-aos-duration="1000">
        </article>
        <div class="container py-md-5 py-3 d-flex flex-column justify-content-between gap-5">
            <div>
                <header class="py-1">
                    <h5 class="fw-bold" data-aos="zoom-out" data-aos-duration="1000">Toyota Genuine Parts </h5>
                </header>
                <div class=" fw-bold">
                    Toyota သည် ၎င်း ထုတ်လုပ်ထားသည့် Genuine Parts တိုင်းကို အရည်းအသွေးမြင့်မားစေအောင် သေချာစမ်းသပ်ပြီးမှသာ
                    အကောင်းဆုံးဆိုသည့် အရည်အသွေးမြင့်သော အစိတ်အပိုင်းတို့ကို သုံးစွဲသူများ အတွက် ဦးစားပေးကာ ထုတ်လုပ်ပေးလျက်
                    ရှိပါသည်။
                </div>
                <div class="fw-bold">
                    Toyota ကားများကဲ့သို့ Toyota Genuine parts တွေသည်လည်း ကောင်းမွန်သည့် စွမ်းဆောင်ရည် ရှိသောကြောင့်
                    မောင်းနှင်သူ၊ စီးနှင်းလိုက်ပါသူ တွေအတွက်လည်း ဘေးကင်း လုံခြုံစိတ်ချမှုကို ပေးစွမ်း နိုင်သည့်အပြင် သက်တမ်း
                    ကြာရှည်စွာ အသုံးပြုနိုင်အောင် ထုတ်လုပ်ထားသည့် Toyota Genuine Parts များ ဖြစ်ပါသည်။
                </div>
            </div>

            <div>
                <header class="py-1">
                    <h5 class="fw-bold" data-aos="zoom-out" data-aos-duration="1000">Motor Oil</h5>
                </header>
                <div class=" fw-bold">
                    Toyota မှ ထုတ်လုပ်ပေးသည့် Motor Oil များသည် American Petroleum Institute (အမေရိကန် မော်တော်ယာဉ်
                    အင်ဂျင်နီယာများအသင်း) ၏ သတ်မှတ်ထားသည့် စံချိန်စံညွှန်း နှင့် အညီ ထုပ်လုပ်ထားသည့် အတွက် ကား၏ အင်ဂျင်ကို
                    ကောင်းမွန်သော အထောက်အပံ့များစွာ ပေးစွမ်းနိုင်ပါတယ်။ ထို့အပြင် ဆီစားနှုန်း သက်သာစေခြင်း အတွက်ပါထည့်သွင်း
                    ထုတ်လုပ်ထားပါသည်။
                </div>
                <div class="fw-bold">

                    ကား စက်နှိုးနေစဉ် အချိန်မှာလည်း အင်ဂျင်အတွင်းသို့ သန့်စင်သော ဆီများ ဝင်ရောက်ခြင်းကြောင့် အင် ဂျင်၏
                    သန့်ရှင်းမှုကို ထိန်းသိမ်း ပေးနိုင်ခြင်း၊ အိတ်ဇောမီးခိုးအထွက်များခြင်း အတွက်ကိုပါ လျော့ချ ပေးသည့် အတွက်
                    ပတ်ဝန်းကျင် ညစ်ညမ်းမှုကိုပါ ကာကွယ်ပေးနိုင်ပါသည်။ ထို့အပြင် ပူပြင်းသည့် ရာသီဥတု တွင်သာမက အေးမြသည့်
                    ရာသီဥတုများ မှာလည်း အင်ဂျင်ကို ချောမွေ့စွာဖြင့် စက်နှိုးပေးနိုင်အောင် ကူညီထောက်ပံ့ပေးပါသည်။
                    ထို့ကြောင့် ကားမောင်းနှင်နေသည့် သက်တမ်းတစ်လျှောက် အကောင်းဆုံးစွမ်းဆောင်ရည် ရရှိနိုင်အောင် Genuine Engine
                    Oil တွေသည် အမြင့်မားဆုံးသော စွမ်းဆောင်ရည်ကို လုပ်ဆောင်ပေးပါသည်။
                </div>
            </div>
            <div class="d-flex justify-content-center  justify-content-md-start  gap-4 flex-wrap">
                {{-- <div class="bg-dark img-div">
                    <img src="{{ asset('img/spare_part/spare_part8.png') }}" alt="" width="100%"
                        data-aos="zoom-out" data-aos-duration="1000" class="p-4">
                </div>
                <div class="bg-dark img-div">
                    <img src="{{ asset('img/spare_part/spare_part1.png') }}" alt="" width="100%" height="100%"
                        data-aos="zoom-out" data-aos-duration="1000" class="p-4">
                </div>
                <div class="bg-dark img-div text-center">
                    <img src="{{ asset('img/spare_part/spare_part2.png') }}" alt="" width="80%" height="100%"
                        data-aos="zoom-out" data-aos-duration="1000" class="p-4">
                </div>
                <div class="bg-dark img-div">
                    <img src="{{ asset('img/spare_part/spare_part3.png') }}" alt="" width="100%" height="100%"
                        data-aos="zoom-out" data-aos-duration="1000" class="p-4">
                </div> --}}
                <div class="img-div">
                    <img src="{{ asset('img/spare_part/spare_part4.png') }}" alt="" width="100%"
                        data-aos="zoom-out" data-aos-duration="1000" class="p-4">
                </div>
                <div class="img-div">
                    <img src="{{ asset('img/spare_part/spare_part5.png') }}" alt="" width="100%"
                        data-aos="zoom-out" data-aos-duration="1000" class="p-4">
                </div>
                <div class="img-div">
                    <img src="{{ asset('img/spare_part/spare_part6.png') }}" alt="" width="100%"
                        data-aos="zoom-out" data-aos-duration="1000" class="p-4 mt-2">
                </div>
                <div class="img-div">
                    <img src="{{ asset('img/spare_part/spare_part7.png') }}" alt="" width="100%"
                        data-aos="zoom-out" data-aos-duration="1000" class="p-4">
                </div>
            </div>

            <div>
                <header class="py-1">
                    <h5 class="fw-bold" data-aos="zoom-out" data-aos-duration="1000">Spare Parts</h5>
                </header>
                <div class=" fw-bold">
                    Toyota မှ ထုတ်လုပ်ထားသည့် Spare Parts များသည် ကားတစ်စီး၏ Design နှင့် တည်ဆောက်ထားသည့် Structure နှင့်
                    အညီ အံဝင်ခွင်ကျဖြစ်နေအောင် ဦးစားပေးထုတ်လုပ် ထားပါသည်။ Market အတွင်းသို့ မရောင်းချမီ စမ်းသပ်တပ်ဆင်ပြီး
                    အရည်အသွေး ပြည့်မပြည့် စစ်ဆေးပြီးမှသာ ထုတ်လုပ်ရောင်းချသည့် အတွက် အရည်အသွေးကောင်းမွန်သည့် Toyota Genuine
                    Parts များသည် မောင်းနှင် သူများအတွက် လုံခြုံစိတ်ချစေနိုင်သည့် စိတ်ခံစားမှု ကို ပေးစွမ်းနိုင်ပါသည်။
                </div>
                <div class=" fw-bold">

                    Toyota Myanmar မှ ရရှိနိုင်သည့် Genuine Motor Oil များ နှင့် Spare Parts များကို Toyota Official
                    Authorized Dist., Myanmar နှင့် Toyota Official Dealer’s Showrooms များတွင်
                    အသေးစိတ်ကိုဆက်သွယ်မေးမြန်းဝယ်ယူနိုင်ပါသည်
                </div>
            </div>

        </div>
    </main>
@endsection
