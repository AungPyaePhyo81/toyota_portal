@extends('layouts.main')

@section('title', 'Maintenance')

@section('content')
    <main class="bg-toyota-light">
        <div class="container-fluid bg-toyota-light px-sm-5 px-3 pb-4">
            @include('components.breadcrumb')
            <header class="py-4">
                <h2 class="fw-bold" data-aos="zoom-out" data-aos-duration="1000">Maintenance & Service</h2>
            </header>
            <div class="container toyota-paragraph">
                Keeping your Toyota up to date on maintenance ensures its safety and dependability. Schedule regular
                sessions with the skilled team of technicians at UTS, or opt for our express service to keep you moving on
                busy days.
            </div>
        </div>
        <article class="overflow-hidden">
            <img src="{{ asset('img/temp/maintenance.png') }}" alt="" width="100%" data-aos="zoom-out"
                data-aos-duration="1000">
        </article>
        <div class="container py-md-5 py-3">
            <div class="row" data-masonry='{"percentPosition": true }'>
                <div class="col-md-6 col-12">
                    <img src="{{ asset('img/temp/ms01.png') }}" alt="" width="100%" data-aos="zoom-out"
                        data-aos-duration="1000">
                </div>
                <div class="col-md-6 col-12 px-lg-5 py-md-0 p-3">
                    <h3 class="fw-bold">Toyota Express Maintenance</h3>
                    <p class="toyota-paragraph">
                        When your vehicle needs scheduled service, our pit crew-inspired approach helps get you in and out
                        quickly. And we do it all at the right price, with the quality you expect from your Toyota dealer.
                        In one hour or less, our experts will complete the following:
                    </p>
                    <ul class="toyota-paragraph not-justify">
                        <li>Oil change</li>
                        <li>Multi-point inspection</li>
                        <li>Engine air filter or A/C filter check and/or replacement</li>
                        <li>Tyre rotation</li>
                        <li>Tyre pressure adjustment</li>
                        <li>Wiper blade replacement</li>
                        <li>Light bulb replacement</li>
                        <li>Fluid top-offs</li>
                        <li>Battery check</li>
                    </ul>
                </div>
                <div class="col-md-6 col-12 p-lg-5 p-3">
                    <h3 class="fw-bold">Periodic Maintenance</h3>
                    <br>

                    <h5 class="fw-bold">Why choose UTS?</h5>
                    <p class="toyota-paragraph">
                        Only Toyota knows your Toyota. You are guaranteed of genuine parts and top-notch service when you
                        have your car serviced at UTS, an authorised Toyota dealer.
                    </p>
                    <h5 class="fw-bold">How often to service your vehicle</h5>
                    <p class="toyota-paragraph">
                        Only Toyota knows your Toyota. You are guaranteed of genuine parts and top-notch service when you
                        have your car serviced at UTS, an authorised Toyota dealer.
                    </p>
                    <h5 class="fw-bold">Why is it important?</h5>
                    <p class="toyota-paragraph">
                        Regular, proper servicing by qualified technicians can help with diagnosing and eliminating problems
                        such as transmission failure, malfunctioning brakes, and oil leaks.
                        Periodic vehicle maintenance can also offer a range of benefits, including:
                    </p>
                    <ul class="toyota-paragraph">
                        <li>Excellent running condition</li>
                        <li>Better fuel economy</li>
                        <li>Longer vehicle life</li>
                        <li>Higher resale value</li>
                        <li>Better pollution control</li>
                    </ul>
                </div>
                <div class="col-md-6 col-12">
                    <img src="{{ asset('img/temp/ms02.png') }}" alt="" width="100%" data-aos="zoom-out"
                        data-aos-duration="1000">
                </div>
            </div>
        </div>
    </main>

    <div class="w-100 bg-more-darker-white d-flex flex-column flex-sm-row justify-content-evenly align-items-center text-secondary"
        style="--line-color: var(--bs-gray)">
        <div class="py-sm-4 py-3 toyota-paragraph">
            <img src="{{ asset('img/temp/onlineinquery.png') }}" alt="" width="14%" class="mt-1">
            <a href="{{ route('service-support.online-enquiry') }}" class="animated-link-reverse">Online Enquiry</a>
        </div>
        <div class="py-sm-4 py-3 toyota-paragraph">
            {{-- <i class="fa-solid fa-lg fa-wrench mx-3"></i> --}}
            <img src="{{ asset('img/temp/bookservice.png') }}" alt="" width="14%">
            <a href="{{ route('service-support.book-service') }}" class="animated-link-reverse">
                Book A Service</a>
        </div>
        <div class="py-sm-4 py-3 toyota-paragraph">
            <img src="{{ asset('img/temp/finddealer.png') }}" alt="" width="12%">
            <a href="{{ route('dealer') }}" class="animated-link-reverse"> Find A Service Centers</a>
        </div>
    </div>
    @include('service_support.form_modals')
    </div>
@endsection
