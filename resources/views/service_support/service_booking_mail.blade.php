<!DOCTYPE html>

<html>

<head>

    <title>Toyota</title>

</head>

<body>
    <h3>Confirmation</h3>

    Hi {{ $data->name }},<br>

    Thanks for making your booking. <br>

    Your booking details are as follows: <br>

    Date: {{ $data->date }} <br>

    Time: {{ $data->time }} <br>
    Service Center : <b> {{ $data->dealerInfo->map->name }}</b> <br>
    Service Center Address : {{ $data->dealerInfo->address }}
    <p>Thank You</p>
</body>

</html>
