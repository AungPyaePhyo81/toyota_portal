<!DOCTYPE html>

<html>

<head>

    <title>Toyota</title>

</head>

<body>

    Dear {{ $data->name }},<br>

    We wanted to remind you that you have an appointment with {{ $data->dealerInfo->map->name }} on {{ $data->date }}
    at {{ $data->time }}.
    <p>Thank You</p>
</body>

</html>
