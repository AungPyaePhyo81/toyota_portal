@extends('layouts.main')

@section('title', "Accessories")

@php
    $accessories = App\Models\Accessory::orderBy('name')->get();
@endphp

@section('content')
    <main class="bg-toyota-light">
        <div class="container-fluid bg-toyota-light px-sm-5 px-3 pb-4">
            @include('components.breadcrumb')
            <div class="container">
                <p class="toyota-paragraph mt-5">
                    Hello Toyota Fan တို့ရေ.. <br><br>
                    Toyota Genuine Accessories တွေ ကို Clearance Sales အနေနဲ့ Special Discount – Up to 80 % ပေးနေပြီနော်။ ဒီ Promotion ကြီးမှာ Special Package တွေ အတွက် သာမဟုတ်ဘဲ အမျိုးအစား တစ်ခုချင်းစီ (Individual Items) တွေ အတွက်ပါ အထူး Discount တွေနဲ့ မိမိတို့ စိတ်ကြိုက် ဝယ်ယူနိုင်ဖို့ပါ စီစဉ်ပေး ထားပါတယ် ခင်ဗျာ။<br><br>

                        ဘယ်လို Toyota ကားအမျိုးအစားတွေ အတွက် ဒီ Promotion လေးကို ဖန်တီးပေးထားတာလဲ..?<br>
                    Corolla Cross, Rush, Vios, Avanza, Corolla Altis, Camry, Hilux ကားတွေရဲ့ Toyota Genuine Accessories တွေကို Special Discount ဖန်းတီး ပေးထား ပါတယ်။<br>
                    Decoration အတွက် သာမက Safety အတွက် အရေးကြီးတဲ့ Emergency Items တွေ လဲ ပါဝင်တာဖြစ်လို့ လက်လွတ် မခံသင့်တဲ့ အခွင့်အရေးကြီးပါ ခင်ဗျာ။<br><br>

                            ကားအမျိုးအစား တစ်ခုချင်းစီ အတွက် သင့်လျော်တဲ့ ဘယ်လို Special Package တွေကို ဖန်တီးပေးထားလဲ?<br>
                    ဘယ်လို ဝယ်ယူတပ်ဆင်နိုင်လဲဆိုတာ အသေးစိတ်သိရှိ လိုပါက အောက်တွင်ဖော်ပြထားသော Toyota Dealer များကို ဆက်သွယ် မေးမြန်းဝယ်ယူနိုင်ပါပြီ ခင်ဗျာ။<br><br>

                    လက်ကားဝယ်ယူလိုပါက ဖုန်းနံပါတ် 09972862685 နှင့် 09965805882 တို့ကို ဆက်သွယ်စုံစမ်းမေးမြန်းနိုင်ပါတယ်ခင်ဗျာ။<br><br>

                    Yangon<br>
                    Toyota Aye & Sons:<br>
                    87(A), Kaba Aye Pagoda Road, Bahan Township, Shwegondine, Yangon.<br>
                    09409824711, 09409824722, 09750142702<br><br>

                    Toyota Mingalar:<br>
                    No.(236),Ahlone Road,Ahlone Township,Yangon.<br>
                    09- 405 399993,09- 407314699<br><br>

                    Toyota Taw Win:<br>
                    No. 68, Taw Win Road, 9 Mile, Mayangon Township, Yangon, Myanmar.<br>
                    09-765443605, 09-765443607, 09-765443608, 09-770552000<br>

                    ---------------------------------<br>
                    Mandalay<br>
                    Toyota GW-Lion:<br>
                    Block [542], Upine – 10/339, Mandalay-Pyin Oo Lwin Road, Between 35th & 37th Street,Aung Pin Lae Quarter, Pathein Gyi T/S, Mandalay.<br>
                    09 969 700 002, 09 969 700 003<br>
                    ---------------------------------<br>

                    Naypyidaw<br>
                    Toyota MDMM: No. Z-01, Tawwin Yadanar Road and Corner of Padonmar 2nd Street, Shwe Kyar Pin Quarter, Zabuthiri Township, Naypyidaw.<br>
                    09444111197, 09444111198<br>
                    ---------------------------------<br>

                    Taunggyi<br>
                    Toyota Mingalar<br>
                    Pa/2 ၊ Bogyoke Aung San Road, Pyi Taw Thar Ward, Taunggyi, Shan State, Myanmar.<br>
                    09- 40779 9993<br>

                    ---------------------------------<br>
                    Lashio<br>
                    Toyota GW-Lion:<br>
                    No.(L/16/157), Quarter (12), Region (16), Mandalay – Muse Pyi Htaung Su Road, Near Kho Loung Traffic Light, Lashio.<br>
                    09 -8 999 444 25, 09- 8 999 444 35<br>
                </p>
                @foreach ($accessories as $accessory)
                    @php
                        $images = json_decode($accessory->images, true);
                    @endphp
                    <h3 class="fw-bold pt-5">{{$accessory->name}}'s Accessories</h3>
                    <div class="row">
                        @foreach ($images as $img)
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 p-4">
                                <button type="button" data-bs-img="{{ $img['image'] }}" class="btn-img-viewer overflow-hidden rounded"
                                data-bs-toggle="modal" data-bs-target="#imgViewerModal">
                                    <img src="{{asset("storage/".$img['image'])}}" alt="" width="100%">
                                </button>
                            </div>
                        @endforeach
                    </div>
                @endforeach
            </div>
        </div>
    </main>
    @include('blog.modal')

@endsection
