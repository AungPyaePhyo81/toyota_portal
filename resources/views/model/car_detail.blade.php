@extends('layouts.main')

@section('title', "$sub_cate->name")

@php
    $screen_sm = isset($_SERVER['HTTP_USER_AGENT']) && strpos($_SERVER['HTTP_USER_AGENT'], 'Mobi') !== false;

    function getName($path)
    {
        return ucfirst(basename($path, '.' . pathinfo($path, PATHINFO_EXTENSION)));
    }

    function designVer($title)
    {
        $mapping = [
            'design & interior' => 1,
            'design & comfort' => 1,
            'performance' => 2,
            'utility' => 2,
            'safety' => 3,
        ];

        return $mapping[strtolower($title)] ?? 0;
    }

    $color_count = $no_spec_count = 0;
    foreach($carModels as $model){
        $color_count += count($model->color) > 0 ? 1 : 0;
        $no_spec_count += !$model->specification ? 1 : 0;
    }
    if ($color_count > 0) {
        $first_car = \App\Models\CarModel::has('color')->first();
        // dd($first_car->color);
    }
@endphp

@section('content')
    <main class="bg-light">
        @if ($sub_cate->status == 1)
            {{-- <a href="#scroll_target" class="scroller"></a> --}}
            {{-- Web Banner --}}
            <div class="web-banner position-relative" style="background: url('{{ asset('storage/' . $sub_cate->banner) }}') no-repeat center/cover">
                @php
                    foreach($carModels as $model){
                        if($model->specification){
                            $spec_id = $model->specification->id;
                            break;
                        }
                    }
                @endphp
                @if (isset($spec_id))
                    <a href="{{ route('model.car.specification', $spec_id) }}" class="btn btn-dark rounded-0 view-spec-btn px-4">VIEW SPECIFICATION</a>
                @endif
                <img src="{{ asset('storage/' . $sub_cate->banner) }}" alt=""
                    class="zoom animation" style="--offset-opacity: 1; --offset-scale: 1.23; object-fit:cover">
            </div>
            {{-- <div id="scroll_target"></div> --}}

            {{-- Link Icons --}}
            <div class="col-12 border-top bg-toyota-white">
                <div class="row px-sm-5 px-3 text-secondary" style="--line-color: #6c757d">
                    {{-- Compare Model --}}
                    <div class="col-lg-3 col-6 py-sm-5 py-3 d-flex justify-content-center">
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 34.9 48" class="text-dark me-2 mt-1 zoom-x animation" style="--offset-x: -77px; --delay: .5s">
                            <polyline points="32.7,41.4 32.7,4.5 13.6,4.5" fill="none" stroke="currentColor">
                            </polyline>
                            <rect x="30.9" y="41.4" width="3.4" height="3.2" fill="currentColor">
                            </rect>
                            <polyline points="17.4,1 13.6,4.5 17.4,8" fill="none" stroke="currentColor"
                                stroke-linecap="round" stroke-linejoin="round"></polyline>
                            <polyline points="2.2,6.6 2.2,43.5 21.3,43.5" fill="none" stroke="currentColor">
                            </polyline>
                            <rect x="0.5" y="4.2" width="3.4" height="3.2" fill="currentColor">
                            </rect>
                            <polyline points="17.5,47 21.3,43.5 17.5,40" fill="none" stroke="currentColor"
                                stroke-linecap="round" stroke-linejoin="round">
                            </polyline>
                        </svg>
                        <a href="{{ route('model.compare-select') }}" class="animated-link   zoom-x animation"
                            style="--duration: 1.5s">Compare <span class=" d-none d-sm-inline">Model</span></a>
                    </div>

                    {{-- Download Brochures --}}
                    @php
                        $brochures = json_decode($sub_cate->brochures, true);
                    @endphp
                    @if (!isset($brochures))
                        <div class="col-lg-3 col-6 py-sm-5 py-3 d-flex justify-content-center">
                            {{-- <i class="fa-solid icon-xxl xs-icon fa-book mx-4 zoom-x animation" title="Download Brochures"></i> --}}
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="30" height="30" class="mx-3 zoom-x animation"
                            viewBox="0 0 32.7 48" class="text-dark">
                                <path d="M4.4,47H1V9.5L19.7,1v37.5L1,47 M2.7,47h29V9.5h-12v29L1,47" fill="none" stroke="currentColor"></path>
                                <line x1="16.5" y1="10.8" x2="2.6" y2="17.2" fill="none"
                                    stroke="currentColor"></line>
                                <line x1="16.5" y1="19.1" x2="2.6" y2="25.5" fill="none"
                                    stroke="currentColor"></line>
                            </svg>
                            <a href="{{ route('download-brochures') }}" class="animated-link zoom-x animation"
                                style="--duration: 1.5s"><span class=" d-none d-sm-inline">Download</span> Brochures</a>
                        </div>
                    @elseif (count($brochures) > 1)
                        <div class="dropdown col-lg-3 col-6 py-sm-5 py-3 d-flex justify-content-center">
                            <div class="dropdown-toggle d-flex justify-content-center" type="button" data-bs-toggle="dropdown"
                                aria-expanded="false">
                                {{-- <i class="fa-solid icon-xxl xs-icon fa-book mx-4 zoom-x animation" title="Download Brochures"></i> --}}
                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="30" height="30" class="mx-3 zoom-x animation"
                                    viewBox="0 0 32.7 48" class="text-dark">
                                    <path d="M4.4,47H1V9.5L19.7,1v37.5L1,47 M2.7,47h29V9.5h-12v29L1,47" fill="none"
                                        stroke="currentColor"></path>
                                    <line x1="16.5" y1="10.8" x2="2.6" y2="17.2" fill="none"
                                        stroke="currentColor"></line>
                                    <line x1="16.5" y1="19.1" x2="2.6" y2="25.5" fill="none"
                                        stroke="currentColor"></line>
                                </svg>

                                <a href="" class="animated-link   zoom-x animation"
                                    style="--duration: 1.5s"><span class=" d-none d-sm-inline">Download</span> Brochures</a>
                            </div>
                                <ul class="dropdown-menu">
                                    @foreach ($brochures as $brochure)
                                        <li><a class="dropdown-item" href="{{ url('storage/' . $brochure['brochures']) }}">
                                            <i class="fa-solid fa-download"></i> {{ basename($brochure['brochures']) }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                        </div>
                    @else
                        <div class="col-lg-3 col-6 py-sm-5 py-3 d-flex justify-content-center">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="30" height="30" class="mx-3 zoom-x animation"
                                viewBox="0 0 32.7 48" class="text-dark">
                                <path d="M4.4,47H1V9.5L19.7,1v37.5L1,47 M2.7,47h29V9.5h-12v29L1,47" fill="none"
                                    stroke="currentColor"></path>
                                <line x1="16.5" y1="10.8" x2="2.6" y2="17.2" fill="none"
                                    stroke="currentColor"></line>
                                <line x1="16.5" y1="19.1" x2="2.6" y2="25.5" fill="none"
                                    stroke="currentColor"></line>
                            </svg>

                            @foreach ($brochures as $brochure)
                                <a href="{{ url('storage/' . $brochure['brochures']) }}" class="animated-link   zoom-x animation"
                                    style="--duration: 1.5s" title="{{ basename($brochure['brochures']) }}"><span class=" d-none d-sm-inline">Download</span> Brochures</a>
                            @endforeach
                        </div>
                    @endif

                    {{-- Search Inventory --}}
                    <div class="col-lg-3 col-6 py-sm-5 py-3 d-flex justify-content-center">
                        <img src="{{ asset('img/temp/inventorysearch.png') }}" alt="" class="me-2 mt-1 zoom-x animation" style="--offset-x: 100px; filter: contrast(0%);" title="Search Inventory">

                        <a href="{{ route('inventory') }}" class="animated-link zoom-x animation"
                            style="--offset-x: 100px; --duration: 1.5s"><span class=" d-none d-sm-inline">Search</span>
                            Inventory</a>
                    </div>

                    {{-- Owner's Manual --}}
                    <div class="dropdown col-lg-3 col-6 py-sm-5 py-3 d-flex justify-content-center">
                        <div class="dropdown-toggle d-flex justify-content-center" type="button" data-bs-toggle="dropdown"
                            aria-expanded="false">
                            <span>
                                <img src="{{ asset('img/temp/owner-manual.png') }}" alt="" class="me-2 zoom-x animation" width="60%" style="--offset-x: 100px; filter: contrast(0%)">
                            </span>
                            <a href="" class="animated-link   zoom-x animation"
                                style="--offset-x: 90px; --duration: 1.5s"><span class=" d-none d-sm-inline">Owner</span>
                                Manual</a>
                        </div>
                        @php
                            $owner_manuals = json_decode($sub_cate->owner_manual, true);
                        @endphp
                        @if (isset($owner_manuals))
                            <ul class="dropdown-menu">
                                @foreach ($owner_manuals as $manual)
                                    <li><a class="dropdown-item" href="{{ url('storage/' . $manual['owner_manual']) }}"><i
                                                class="fa-solid fa-download"></i>
                                            {{ basename($manual['owner_manual']) }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                </div>
            </div>

            {{-- Specification --}}
            @if (count($carModels) > 0)
                <div class="container pb-5">
                    <h3 class="fw-bold mt-5 mb-4 fade-y animation">Explore Your {{ $sub_cate->name }}</h3>
                    @foreach ($carModels as $carModel)
                        @if ($carModel->specification)
                            <div class="row bg-white p-md-5 p-3 fade-y animation my-3 rounded" style="border: 1px solid #0002;">
                                <h4 class="fw-bold col-6 py-2">{{$carModel->name}}</h4>
                                <h4 class="text-end col-6 py-2 toyota-paragraph">{{$carModel->specification->price ? "from ".$carModel->specification->price. "MMK" : ''}}</h4>
                                <div class="col-lg-4 col-12 d-flex align-items-center flex-column">
                                    @if (count($carModel->color) > 0)
                                        @php
                                            $colors = json_decode($carModel->color, true);
                                            $random_color = $colors[array_rand($colors)];
                                        @endphp
                                        <img src="{{ asset('storage/' . $random_color['image']) }}" alt=""
                                            width="75%">
                                    @else
                                        <img src="{{ asset('storage/' . $carModel->subCategory->image) }}" alt=""
                                            width="75%">
                                    @endif
                                    @if ($carModel->specification)
                                        <div class="text-toyota-danger text-center fw-bold">
                                            <a href="{{ route('model.car.specification', $carModel->specification->id) }}"
                                                class="animated-link-reverse" style="--line-color: var(--danger)">View
                                                Specification</a>
                                        </div>
                                    @endif
                                </div>
                                <div class="col-lg-8 col-12 p-2">
                                    <div class="row">
                                        <div class="col-4 border-end d-flex flex-column align-items-center">
                                            <div class="toyota-paragraph py-sm-3 py-1">
                                                {{ $carModel->specification->horse_power ?? 0 }}</div>
                                            <div class="toyota-paragraph py-sm-3 py-1">Horsepower</div>
                                        </div>
                                        <div class="col-3 border-end d-flex flex-column align-items-center">
                                            <div class="toyota-paragraph py-sm-3 py-1">
                                                {{ $carModel->specification->torque ?? 0 }}</div>
                                            <div class="toyota-paragraph py-sm-3 py-1">Torque</div>
                                        </div>
                                        <div class="col-5 d-flex flex-column align-items-center">
                                            <div class="toyota-paragraph py-sm-3 py-1">
                                                {{ $carModel->specification->engine_name ?? 0 }}</div>
                                            <div class="toyota-paragraph py-sm-3 py-1">Engine</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            @endif

            {{-- Video Banner --}}
            @if ($sub_cate->video)
                <section class="mh-100-nav bg-darker-danger py-5">
                    <div class=" overflow-hidden d-flex justify-content-center align-items-center flex-column">
                        <h5 class="text-light toyota-paragraph text-center zoom animation pb-5"
                            style="--offset-scale: 1.5"><b>Once you’ve decided which pick-up meets your needs !</b>
                            <br>
                            It was very capable off road and had legendary reliability.
                        </h5>
                        <video autoplay controls muted class="rounded shadow zoom animation video-width" id="video" style="--duration: 0.5s; --delay: 0.3s">
                            <source src="{{ asset('storage/' . $sub_cate->video) }}" type="video/mp4">
                            Your browser does not support the video tag.
                        </video>
                    </div>
                    {{-- <h3 class="text-center zoom animation py-3 fw-bold">{{getName($sub_cate->video)}}</h3> --}}
                </section>
            @endif

            {{-- Information --}}
            <div class="py-2 py-sm-3 py-md-5 container">
                @foreach ($infos as $info)
                    @php
                        $descriptions = json_decode($info->description, true);
                        $images = json_decode($info->images, true);
                        $obj = count($descriptions) > count($images) ? $descriptions : $images;
                    @endphp
                    @switch(designVer($info->title))
                        {{-- Design --}}
                        @case(1)
                            <div class="bg-white p-sm-3 p-md-5 p-2">
                                <h3 class="fw-bold fade-y animation">{{ $info->title }}</h3>
                                @foreach ($obj as $key => $val)
                                    <div class="row {{ $key % 2 ? '' : 'dir-rtl' }}">
                                        <div class="col-12 col-lg-7 p-2">
                                            <img src="{{ asset('storage/' . $images[$key]['images']) }}" width="100%"
                                                class="rounded zoom animation" style="--delay: 0.5s">
                                        </div>
                                        <div class="col-12 col-lg-5 d-flex flex-column justify-content-center toyota-paragraph not-justify p-sm-5 p-3">
                                            <div class="fade-y animation dir-ltr" style="--delay: 0.5s">
                                                {!! $descriptions[$key]['description'] !!}
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @break

                        {{-- Performance & Utility --}}
                        @case(2)
                            <h3 class="fw-bold mt-5 mb-4 fade-y animation">{{ $info->title }}</h3>
                            <div class="row">
                                @foreach ($obj as $key => $val)
                                    <div class="col-12 {{ $loop->first ? (count($obj) % 2 ? '' : 'col-md-6') : 'col-md-6' }} p-3">
                                        <div class="bg-darker-white fade-y animation h-100 row">
                                            <img class="{{ $loop->first ? (count($obj) % 2 ? 'col-lg-7 col-12' : '') : '' }} p-0"
                                                src="{{ asset('storage/' . $images[$key]['images']) }}" width="100%"
                                                style="object-fit:cover">
                                            <div
                                                class="{{ $loop->first ? (count($obj) % 2 ? 'col-lg-5 col-12 d-flex justify-content-center flex-column' : '') : '' }} p-4">
                                                <div class="toyota-paragraph not-justify">
                                                    {!! $descriptions[$key]['description'] !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @break

                        {{-- Safety --}}
                        @case(3)
                            <h3 class="fw-bold mt-5 mb-4 fade-y animation">{{ $info->title }}</h3>
                            <div class="bg-darker-white row">
                                <div class="col-12 d-flex d-md-none p-4 fade-y animation">
                                        {!! $descriptions[0]['description'] !!}
                                </div>

                                <div class="col-12 col-lg-4 p-0">
                                    <div class="row w-100 h-100 m-0">
                                        @foreach (array_slice($images, 0, count($images) / 2) as $i => $img)
                                            <div data-bs-img="{{ $img['images'] }}" class="btn-img-viewer overflow-hidden col-12 col-md-6 col-lg-12 d-flex align-items-center justify-content-center p-3 {{$i%2?'pt-2':'pb-2'}}" data-bs-toggle="modal" data-bs-target="#imgViewerModal">
                                                <img src="{{ asset('storage/'. $img['images']) }}" width="100%" height="100%" class="rounded zoom animation" style="object-fit: cover;">
                                            </div>
                                            @if (count($descriptions) <= 1)
                                                <div class="col-12 d-flex d-md-none p-5 pb-5 pt-2 fade-y animation">
                                                    <span><i class="fa-solid fa-angles-right text-danger fa-lg"></i> {{getName($img['images'])}}</span>
                                                </div>
                                            @else
                                                <div class="col-12 d-flex d-md-none p-5 pb-5 pt-2 fade-y animation">
                                                    <span>{!! $descriptions[$i+1]['description'] !!}</span>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>

                                <div class="col-12 col-lg-4 justify-content-center align-items-center d-none d-md-flex">
                                    <div class="p-4">
                                        <div class="fade-y animation py-3">
                                            @foreach ($descriptions as $description)
                                                {!! $description['description'] !!}
                                            @endforeach
                                        </div>
                                        @if (count($descriptions) <= 1)
                                            <ul class="list-unstyled">
                                                @foreach ($images as $idx => $img)
                                                    <li class="fade-y animation py-2" style="--delay: {{($idx + 1) * 0.2}}s">
                                                        <i class="fa-solid fa-angles-right text-danger fa-lg"></i> {{getName($img['images'])}}
                                                    </li>
                                                @endforeach
                                            </ul>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-12 col-lg-4 d-flex justify-content-between">
                                    <div class="row">
                                        @foreach (array_slice($images, count($images) / 2) as $i => $img)
                                            <div data-bs-img="{{ $img['images'] }}" class="btn-img-viewer overflow-hidden col-12 col-md-6 col-lg-12 d-flex align-items-center justify-content-center p-3 {{$i%2?'pt-2':'pb-2'}}" data-bs-toggle="modal" data-bs-target="#imgViewerModal">
                                                <img src="{{ asset('storage/'. $img['images']) }}" width="100%" height="100%" class="rounded zoom animation" style="object-fit: cover;">
                                            </div>
                                            @if (count($descriptions) <= 1)
                                                <div class="col-12 d-flex d-md-none pb-5 pt-2 fade-y animation p-5">
                                                    <span><i class="fa-solid fa-angles-right text-danger fa-lg"></i> {{getName($img['images'])}}</span>
                                                </div>
                                            @else
                                                <div class="col-12 d-flex d-md-none pb-5 pt-2 fade-y animation p-5">
                                                    <span>{!! $descriptions[count($images) / 2 + $i +1]['description'] ?? '' !!}</span>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @break

                        {{-- Interior --}}
                        @default
                            <h3 class="fw-bold mt-5 mb-4 fade-y animation">{{$info->title}}</h3>
                            <div class="owl-carousel interior-caro">
                                @foreach ($obj as $key => $val)
                                    <div class="overflow-hidden interior-img-container" data-text="{{ trim(strip_tags($descriptions[$key]['description'])) }}">
                                        <img src="{{ asset('storage/'. $images[$key]['images']) }}" alt="" height="100%"
                                        class="zoom animation" style="--offset-scale: 1.2; object-fit: cover">
                                    </div>
                                @endforeach
                            </div>
                    @endswitch
                @endforeach
                @include('blog.modal')

                {{-- Exterior Color --}}
                @if ($color_count > 0)
                    <div class="bg-white p-sm-5 p-3 my-5 d-flex align-items-center flex-column fade-y animation">
                        <h3 class="fw-bold text-center fade-y animation">Exterior Colors</h3>
                        @if ($color_count > 1)
                            <div class="d-flex justify-content-end w-100">
                                <div>
                                    <select name="" id="model_color" class="form-select mb-4 mt-2">
                                        @foreach ($carModels as $model)
                                            @if (count($model->color) > 0)
                                                <option value="{{ $model->id }}">{{ $model->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                        <div id="colorCaroFade"
                            class="carousel slide carousel-fade carousel-dark d-flex align-items-center flex-column">
                            <div class="carousel-inner" id="color_caro_inner">
                                @foreach ($first_car->color as $color)
                                    <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                                        <img src="{{ asset("storage/$color->image") }}" class="d-block w-100"
                                            alt="{{ $color->name }}" title="{{ $color->name }}">
                                    </div>
                                @endforeach
                            </div>
                            <p class="toyota-paragraph customize-color pb-sm-5 pb-5 mb-sm-5 mb-3 zoom animation"> Customize
                                Your
                                Colour </p>
                            <div class="color-caro carousel-indicators" id="color_caro_indicator">
                                @foreach ($first_car->color as $idx => $color)
                                    <button type="button" data-bs-target="#colorCaroFade"
                                        data-bs-slide-to="{{ $idx }}" onclick="colorText(this)"
                                        data-color="{{ $color->name }}"
                                        class="color-caro-btn {{ $loop->first ? 'active' : '' }}"
                                        style="--color-picker-bg: {{ $color->code }}"></button>
                                @endforeach
                            </div>
                        </div>
                        <p class="toyota-paragraph" id="color_name">{{ $first_car->color[0]->name }}</p>
                    </div>
                @endif

            </div>

            {{-- Explore Other --}}
            @if ($no_spec_count > 0)
                <div class="container pb-5">
                    <h3 class="fw-bold mt-5 mb-4 fade-y animation">Explore Other {{$sub_cate->name}} Models</h3>
                    <div class="row">
                        @foreach ($carModels as $carModel)
                            @if (!$carModel->specification)
                                <div class="col-12 col-sm-6 p-3">
                                    <div class="bg-white p-md-5 p-3 fade-y animation my-3 rounded h-100 d-flex flex-column justify-content-between"
                                        style="border: 1px solid #0002;">
                                        <h4 class="fw-bold py-2">{{ $carModel->name }}</h4>
                                        <div class="d-flex align-items-center flex-column">
                                            @if (count($carModel->color) > 0)
                                                @php
                                                    $colors = json_decode($carModel->color, true);
                                                    $random_color = $colors[array_rand($colors)];
                                                @endphp
                                                <img src="{{ asset('storage/' . $random_color['image']) }}"
                                                    alt="" width="75%">
                                            @else
                                                <img src="{{ asset('storage/' . $carModel->subCategory->image) }}"
                                                    alt="" width="75%">
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            @endif
        @endif
    </main>
@endsection

@section('script')
    <script>
        function colorText(item) {
            console.log($(item).attr('data-color'));
            $('#color_name').text($(item).attr('data-color'));
        }
        $('#model_color').change(function() {
            $.ajax({
                type: 'POST',
                url: "{{ route('ajax.colors-model') }}",
                data: {
                    'model_id': $(this).val(),
                },
                success: function(data) {
                    var caro_inner = '';
                    var caro_indicator = '';
                    data.forEach(function(color, i) {
                        is_active = i == 0 ? 'active' : '';
                        caro_inner += `<div class="carousel-item ${is_active}">
                                            <img src="/storage/${color.image}" class="d-block w-100"
                                                alt="${color.name}" title="${color.name}">
                                        </div>`;
                        caro_indicator +=
                            `<button type="button" data-bs-target="#colorCaroFade" data-bs-slide-to="${i}"
                                            data-color="${color.name}" class="color-caro-btn ${is_active}"
                                            style="--color-picker-bg: ${color.code}" onclick="colorText(this)"></button>`;

                    })
                    $('#color_caro_inner').html(caro_inner);
                    $('#color_caro_indicator').html(caro_indicator);
                    $('#color_name').html(data['0'].name);
                }
            });
        })
        $(".interior-caro").owlCarousel({
            margin: 10,
            loop: false,
            dots: true,
            nav: true,
            responsive: {
                0: {
                    items: 1,
                },
                1200: {
                    items: 2,
                }
            }
        });
    </script>
@endsection
