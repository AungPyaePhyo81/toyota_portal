@extends('layouts.main')

@section('title', 'Compare Model')

@section('content')
    <form action="" class="d-none">
        <input type="hidden" name="first_model" value="" id="first_model">
        <input type="hidden" name="sec_model" value="" id="sec_model">
    </form>
    <main class="bg-white px-sm-5 px-3 pb-4">
        <header class="py-4">
            <h2 class="fw-bold">MODEL COMPARISON</h2>
            <h5>Select model grades and do a quick side-by-side comparison.</h5>
        </header>
        <div class="container">
            <div class="row">
                @for ($i = 1; $i < 3; $i++)
                    <div class="col-6 d-flex flex-column align-items-center justify-content-center">
                        <a type="button" data-bs-toggle="offcanvas" data-bs-target="#compareOffcanvas{{ $i }}"
                            class="w-100">
                            <img src="{{ asset('img/compare/car_placeholder_right.png') }}" alt="" width="100%"
                                class="car-placeholder" style="{{ $i == 1 ? 'transform: scaleX(-1)' : '' }}"
                                id="car_placeholder{{ $i }}">
                        </a>
                        <h5 class="fw-bold" id="model_label{{ $i }}">{{ $i }} {!! $i == 1 ? '<sup>st</sup>' : '<sup>nd</sup>' !!}
                            Model</h5>
                        <h5 class="fw-bold model-name" id="model_name{{ $i }}"></h5>
                        <a type="button" data-bs-toggle="offcanvas" data-bs-target="#compareOffcanvas{{ $i }}"
                            class="animated-link text-toyota-danger" style="--line-color: var(--danger)">
                            <span id="submit_btn{{ $i }}">ADD A MODEL</span>
                            <i class="fa-solid fa-angle-right"></i>
                        </a>
                    </div>
                @endfor
            </div>

            <form action="{{ route('model.compare-model') }}" method="post" class="text-center my-4">
                @csrf
                <input type="hidden" name="first_model_id" id="model_id1" value=" ">
                <input type="hidden" name="sec_model_id" id="model_id2" value=" ">
                <button type="submit" class="btn btn-toyota-danger" id="compare_btn">Compare Model</button>
            </form>

        </div>
        @include('model.compare_form', ['key' => 1])
    </main>

    @include('components.stay_informed')
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            const input = document.querySelector("#mobile_code");
            window.intlTelInput(input, {
                initialCountry: "mm",
                hiddenInput: "dial_code",
                utilsScript: "{{ asset('js/utils.js') }}",
            });
            $('.model-name').hide()
            $('#compare_btn').hide()
            for (var i = 1; i < 3; i++) {
                $("#submit_model" + i).on('click', function() {
                    var j = $(this).attr('data-sub-target')
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('ajax.get-model-by-id') }}",
                        data: {
                            'model_id': $("#model" + j).val()
                        },
                        success: function(data) {
                            if (jQuery.isEmptyObject(data)) {
                                $('#car_placeholder' + j).attr('src',
                                    'img/compare/car_placeholder_right.png');
                                $('#submit_btn' + j).html('ADD A MODEL')
                                $('#submit_model' + j).html('ADD MODEL')
                                $('#model_name' + j).hide()
                                $('#model_label' + j).show()
                                $('#model_id' + j).val(' ')
                            } else {
                                $('#car_placeholder' + j).attr('src', 'storage/' + data
                                    .sub_category?.image);
                                $('#submit_btn' + j).html('CHANGE MODEL')
                                $('#submit_model' + j).html('CHANGE')
                                $('#model_name' + j).html(data['name'])
                                $('#model_name' + j).show()
                                $('#model_label' + j).hide()
                                $('#model_id' + j).val($("#model" + j).val())
                            }

                            if ($('#model_id1').val() != ' ' && $('#model_id2').val() != ' ') {
                                $('#compare_btn').show()
                            } else {
                                $('#compare_btn').hide()
                            }
                        }
                    });

                });
            }
        })
    </script>
@endsection
