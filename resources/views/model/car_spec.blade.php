@extends('layouts.main')

@php
    $name = $specification->carModel->name;
    $safety_systems = json_decode($specification->safety_system, true)[0];
@endphp

@section('title', "$name")

@section('content')
    <main class="bg-white px-sm-5 px-3 pb-4">
        <header class="py-4">
            <h2 class="fw-bold">{{ $specification->carModel->name }} SPECIFICATIONS</h2>
        </header>
        <div class="container">
            <table class="table table-borderless table-hover">
                <tr>
                    <th>Grade</th>
                    <td>{{ $specification->grade }}</td>
                </tr>
                <tr>
                    <th>Drive train</th>
                    <td>{{ $specification->driven_train }}</td>
                </tr>
            </table>
            <div class="accordion" id="spec_accordion">
                <div class="accordion-item">
                    <h2 class="accordion-header" id="headingOne">
                        <button class="accordion-button" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Dimensions & Weight
                        </button>
                    </h2>
                    <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne"
                        data-bs-parent="#spec_accordion">
                        <div class="accordion-body p-2 p-sm-4">
                            <table class="table table-bordered">
                                <tr>
                                    <th>Overall</th>
                                    <td class="d-flex justify-content-between">LxWxH <span>mm</span></td>
                                    <td>{{ isset($specification->dimensions_weight) ? json_decode($specification->dimensions_weight, true)[0]['overall'] : '' }}
                                    </td>


                                </tr>
                                <tr>
                                    <th>Wheelbase</th>
                                    <td align="right">mm</td>
                                    <td>{{ isset($specification->dimensions_weight) ? json_decode($specification->dimensions_weight, true)[0]['wheel_base'] : '' }}
                                    </td>

                                </tr>
                                <tr>
                                    <th>Tread</th>
                                    <td class="d-flex justify-content-between">Front/Rear <span>mm</span></td>
                                    <td>{{ isset($specification->dimensions_weight) ? json_decode($specification->dimensions_weight, true)[0]['tread'] : '' }}
                                    </td>

                                </tr>
                                <tr>
                                    <th>Minimum running ground clearance</th>
                                    <td align="right">mm</td>
                                    <td>{{ isset($specification->dimensions_weight) ? json_decode($specification->dimensions_weight, true)[0]['minimum_running_ground_clearance'] : '' }}
                                    </td>

                                </tr>
                                <tr>
                                    <th>Minimum turning radius</th>
                                    <td class="d-flex justify-content-between">Tire <span>mm</span></td>
                                    <td>{{ isset($specification->dimensions_weight) ? json_decode($specification->dimensions_weight, true)[0]['minimum_turning_radius'] : '' }}
                                    </td>

                                </tr>
                                <tr>
                                    <th>Gross vehicles weight</th>
                                    <td class="d-flex justify-content-between">Total <span>kg</span></td>
                                    <td>{{ isset($specification->dimensions_weight) ? json_decode($specification->dimensions_weight, true)[0]['gross_vehicles_weight'] : '' }}
                                    </td>

                                </tr>
                                <tr>
                                    <th rowspan="2" class="align-middle">Curb weight</th>
                                    <td class="d-flex justify-content-between">Total min options <span>kg</span></td>
                                    <td>{{ isset($specification->dimensions_weight) ? json_decode($specification->dimensions_weight, true)[0]['total_min_options'] : '' }}
                                    </td>

                                </tr>
                                <tr>
                                    <td class="d-flex justify-content-between">Total max. option <span>kg</span></td>
                                    <td>{{ isset($specification->dimensions_weight) ? json_decode($specification->dimensions_weight, true)[0]['total_max_options'] : '' }}
                                    </td>

                                </tr>
                                <tr>
                                    <th>Seating capacity</th>
                                    <td align="right">Persons</td>
                                    <td>{{ isset($specification->dimensions_weight) ? json_decode($specification->dimensions_weight, true)[0]['seating_capacity'] : '' }}
                                    </td>

                                <tr>
                                    <th>Deck size</th>
                                    <td class="d-flex justify-content-between">LxWxH <span>mm</span></td>
                                    <td>{{ isset($specification->dimensions_weight) ? json_decode($specification->dimensions_weight, true)[0]['deck_size'] : '' }}
                                    </td>

                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="headingTwo">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            Engine
                        </button>
                    </h2>
                    <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo"
                        data-bs-parent="#spec_accordion">
                        <div class="accordion-body p-2 p-sm-4">
                            <table class="table table-bordered">
                                <tr>
                                    <th>Engine Model Code</th>
                                    <td></td>
                                    <td>{{ isset($specification->engine) ? json_decode($specification->engine, true)[0]['engine_model_code'] : '' }}
                                    </td>

                                </tr>
                                <tr>
                                    <th>Engine Type</th>
                                    <td></td>
                                    <td>{{ isset($specification->engine) ? json_decode($specification->engine, true)[0]['engine_type'] : '' }}
                                    </td>

                                </tr>
                                <tr>
                                    <th>Displacement</th>
                                    <td align="right">cc</td>
                                    <td>{{ isset($specification->engine) ? json_decode($specification->engine, true)[0]['displacement'] : '' }}
                                    </td>

                                </tr>
                                <tr>
                                    <th>Bore x stoke</th>
                                    <td align="right">mm</td>
                                    <td>{{ isset($specification->engine) ? json_decode($specification->engine, true)[0]['bore_x_stoke'] : '' }}
                                    </td>

                                </tr>
                                <tr>
                                    <th>Compression ratio</th>
                                    <td></td>
                                    <td>{{ isset($specification->engine) ? json_decode($specification->engine, true)[0]['compression_ratio'] : '' }}
                                    </td>

                                </tr>
                                <tr>
                                    <th>Max.output (SAE net)</th>
                                    <td align="right">Kw/rpm</td>
                                    <td>{{ isset($specification->engine) ? json_decode($specification->engine, true)[0]['max_output'] : '' }}
                                    </td>

                                </tr>
                                <tr>
                                    <th>Max.torque (SAE net)</th>
                                    <td align="right">Nm/rpm</td>
                                    <td>{{ isset($specification->engine) ? json_decode($specification->engine, true)[0]['max_torque'] : '' }}
                                    </td>

                                </tr>
                                <tr>
                                    <th>Fuel System</th>
                                    <td></td>
                                    <td>{{ isset($specification->engine) ? json_decode($specification->engine, true)[0]['fuel_system'] : '' }}
                                    </td>

                                </tr>
                                <tr>
                                    <th>Fuel Type</th>
                                    <td></td>
                                    <td>{{ isset($specification->engine) ? json_decode($specification->engine, true)[0]['fuel_type'] : '' }}
                                    </td>

                                </tr>
                                <tr>
                                    <th>Fuel Tank Capacity</th>
                                    <td align="right">Liters</td>
                                    <td>{{ isset($specification->engine) ? json_decode($specification->engine, true)[0]['fuel_tank_capacity'] : '' }}
                                    </td>

                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="headingThree">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            Chassis
                        </button>
                    </h2>
                    <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree"
                        data-bs-parent="#spec_accordion">
                        <div class="accordion-body p-2 p-sm-4">
                            <table class="table table-bordered">
                                <tr>
                                    <th class="d-flex justify-content-between">Transmission Type</th>
                                    <td align="center">
                                        {{ isset($specification->chassi) ? json_decode($specification->chassi, true)[0]['transmission_type'] : '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th class="d-flex justify-content-between">Suspension <span>(Front/Rear)</span></th>
                                    <td align="center">
                                        {{ isset($specification->chassi) ? json_decode($specification->chassi, true)[0]['suspension'] : '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th class="d-flex justify-content-between">Brake Type <span>(Front/Rear)</span></th>
                                    <td align="center">
                                        {{ isset($specification->chassi) ? json_decode($specification->chassi, true)[0]['brake_type'] : '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th class="d-flex justify-content-between">Tires</th>
                                    <td align="center">
                                        {{ isset($specification->chassi) ? json_decode($specification->chassi, true)[0]['tires'] : '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th class="d-flex justify-content-between">Wheels</th>
                                    <td align="center">
                                        {{ isset($specification->chassi) ? json_decode($specification->chassi, true)[0]['wheels'] : '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th class="d-flex justify-content-between">Steering gear type</th>
                                    <td align="center">
                                        {{ isset($specification->chassi) ? json_decode($specification->chassi, true)[0]['steering_gear_type'] : '' }}
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="accordion-item">
                    <h2 class="accordion-header" id="headingFour">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                            Standard equipment ( Exterior)
                        </button>
                    </h2>
                    <div id="collapseFour" class="accordion-collapse collapse" aria-labelledby="headingFour"
                        data-bs-parent="#spec_accordion">
                        <div class="accordion-body p-2 p-sm-4">
                            <table class="table table-bordered">
                                <tr>
                                    <th>Headlamps</th>
                                    <td align="center">
                                        {{ isset($specification->standard_equipment_exterior) ? json_decode($specification->standard_equipment_exterior, true)[0]['headlamps'] : '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Front fog & driving lamp</th>
                                    <td align="center">
                                        {{ isset($specification->standard_equipment_exterior) ? json_decode($specification->standard_equipment_exterior, true)[0]['front_fog_driving_lamp'] : '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Daytime running light system</th>
                                    <td align="center">
                                        {{ isset($specification->standard_equipment_exterior) ? json_decode($specification->standard_equipment_exterior, true)[0]['daytime_running_light_system'] : '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Rear combination lamps</th>
                                    <td align="center">
                                        {{ isset($specification->standard_equipment_exterior) ? json_decode($specification->standard_equipment_exterior, true)[0]['rear_combination_lamps'] : '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Outside rear view mirror</th>
                                    <td align="center">
                                        {{ isset($specification->standard_equipment_exterior) ? json_decode($specification->standard_equipment_exterior, true)[0]['outside_rear_view_mirror'] : '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Mudguard</th>
                                    <td align="center">
                                        {{ isset($specification->standard_equipment_exterior) ? json_decode($specification->standard_equipment_exterior, true)[0]['mudguard'] : '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Deck bar</th>
                                    <td align="center">
                                        {{ isset($specification->standard_equipment_exterior) ? json_decode($specification->standard_equipment_exterior, true)[0]['deck_bar'] : '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Bed liner</th>
                                    <td align="center">
                                        {{ isset($specification->standard_equipment_exterior) ? json_decode($specification->standard_equipment_exterior, true)[0]['bed_liner'] : '' }}
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="headingFive">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                            Standard equipment ( Interior)
                        </button>
                    </h2>
                    <div id="collapseFive" class="accordion-collapse collapse" aria-labelledby="headingFive"
                        data-bs-parent="#spec_accordion">
                        <div class="accordion-body p-2 p-sm-4">
                            <table class="table table-bordered">
                                <tr>
                                    <th>Steering switch</th>
                                    <td align="center">
                                        {{ isset($specification->standard_equipment_interior) ? json_decode($specification->standard_equipment_interior, true)[0]['steering_switch'] : '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Steering column</th>
                                    <td align="center">
                                        {{ isset($specification->standard_equipment_interior) ? json_decode($specification->standard_equipment_interior, true)[0]['steering_column'] : '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Part-time transfer switch</th>
                                    <td align="center">
                                        {{ isset($specification->standard_equipment_interior) ? json_decode($specification->standard_equipment_interior, true)[0]['part_time_transfer_switch'] : '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Drive mode switch</th>
                                    <td align="center">
                                        {{ isset($specification->standard_equipment_interior) ? json_decode($specification->standard_equipment_interior, true)[0]['drive_mode_switch'] : '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Cruise control</th>
                                    <td align="center">
                                        {{ isset($specification->standard_equipment_interior) ? json_decode($specification->standard_equipment_interior, true)[0]['cruise_control'] : '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Mutil-infromation display</th>
                                    <td align="center">
                                        {{ isset($specification->standard_equipment_interior) ? json_decode($specification->standard_equipment_interior, true)[0]['multi_information_display'] : '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Economy meter</th>
                                    <td align="center">
                                        {{ isset($specification->standard_equipment_interior) ? json_decode($specification->standard_equipment_interior, true)[0]['economy_meter'] : '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Front seat</th>
                                    <td align="center">
                                        {{ isset($specification->standard_equipment_interior) ? json_decode($specification->standard_equipment_interior, true)[0]['front_seat'] : '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Rear seat</th>
                                    <td align="center">
                                        {{ isset($specification->standard_equipment_interior) ? json_decode($specification->standard_equipment_interior, true)[0]['rear_seat'] : '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Seat cover material</th>
                                    <td align="center">
                                        {{ isset($specification->standard_equipment_interior) ? json_decode($specification->standard_equipment_interior, true)[0]['seat_cover_material'] : '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Audio system</th>
                                    <td align="center">
                                        {{ isset($specification->standard_equipment_interior) ? json_decode($specification->standard_equipment_interior, true)[0]['audio_system'] : '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Speaker system</th>
                                    <td align="center">
                                        {{ isset($specification->standard_equipment_interior) ? json_decode($specification->standard_equipment_interior, true)[0]['speaker_system'] : '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Air conditioning</th>
                                    <td align="center">
                                        {{ isset($specification->standard_equipment_interior) ? json_decode($specification->standard_equipment_interior, true)[0]['air_conditioning'] : '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Starting system</th>
                                    <td align="center">
                                        {{ isset($specification->standard_equipment_interior) ? json_decode($specification->standard_equipment_interior, true)[0]['starting_system'] : '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Wireless door lock</th>
                                    <td align="center">
                                        {{ isset($specification->standard_equipment_interior) ? json_decode($specification->standard_equipment_interior, true)[0]['wireless_door_lock'] : '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Accessory connector</th>
                                    <td align="center">
                                        {{ isset($specification->standard_equipment_interior) ? json_decode($specification->standard_equipment_interior, true)[0]['accessory_connector'] : '' }}
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="headingSix">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                            Safety System
                        </button>
                    </h2>
                    <div id="collapseSix" class="accordion-collapse collapse" aria-labelledby="headingSix"
                        data-bs-parent="#spec_accordion">
                        <div class="accordion-body p-2 p-sm-4">
                            <table class="table table-bordered">
                                @if ($safety_systems['pre_collision_system'] != "")
                                    <tr>
                                        <th>Pre-collision System (PCS)</th>
                                        <td align="center">
                                            {{ $safety_systems['pre_collision_system'] }}
                                        </td>
                                    </tr>
                                @endif

                                @if ($safety_systems['dynamic_radar_cruise_control'] != "")
                                    <tr>
                                        <th>Dynamic Radar Cruise Control (DRCC)</th>
                                        <td align="center">
                                            {{ $safety_systems['dynamic_radar_cruise_control'] }}
                                        </td>
                                    </tr>
                                @endif

                                @if ($safety_systems['lane_departure_alert'] != "")
                                    <tr>
                                        <th>Lane Departure Alert (LDA)</th>
                                        <td align="center">
                                            {{ $safety_systems['lane_departure_alert'] }}
                                        </td>
                                    </tr>
                                @endif

                                @if ($safety_systems['downhill_assist_control'] != "")
                                    <tr>
                                        <th>Downhill Assist Control (DAC)</th>
                                        <td align="center">
                                            {{ $safety_systems['downhill_assist_control'] }}
                                        </td>
                                    </tr>
                                @endif

                                @if ($safety_systems['limited_slip_differential'] != "")
                                    <tr>
                                        <th>Limited Slip Differential (LSD)</th>
                                        <td align="center">
                                            {{ $safety_systems['limited_slip_differential'] }}
                                        </td>
                                    </tr>
                                @endif

                                @if ($safety_systems['srs_airbags'] != "")
                                    <tr>
                                        <th>SRS airbags</th>
                                        <td align="center">
                                            {{ $safety_systems['srs_airbags'] }}
                                        </td>
                                    </tr>
                                @endif

                                @if ($safety_systems['anti_lock_brake_system'] != "")
                                    <tr>
                                        <th>Anti-lock brake system (ABS)</th>
                                        <td align="center">
                                            {{ $safety_systems['anti_lock_brake_system'] }}
                                        </td>
                                    </tr>
                                @endif

                                @if ($safety_systems['vehicles_stability_control'] != "")
                                    <tr>
                                        <th>Vehicles Stability Control (VSC)</th>
                                        <td align="center">
                                            {{ $safety_systems['vehicles_stability_control'] }}
                                        </td>
                                    </tr>
                                @endif

                                @if ($safety_systems['hill_start_assist_control'] != "")
                                    <tr>
                                        <th>Hill start assist control (HAC)</th>
                                        <td align="center">
                                            {{ $safety_systems['hill_start_assist_control'] }}
                                        </td>
                                    </tr>
                                @endif

                                @if ($safety_systems['trailer_sway_control'] != "")
                                    <tr>
                                        <th>Trailer Sway Control (TSC)</th>
                                        <td align="center">
                                            {{ $safety_systems['trailer_sway_control'] }}
                                        </td>
                                    </tr>
                                @endif

                                @if ($safety_systems['emergency_brake_signal'] != "")
                                    <tr>
                                        <th>Emergency brake signal</th>
                                        <td align="center">
                                            {{ $safety_systems['emergency_brake_signal'] }}
                                        </td>
                                    </tr>
                                @endif

                                @if ($safety_systems['clearance_back_sonar'] != "")
                                    <tr>
                                        <th>Clearance & back sonar</th>
                                        <td align="center">
                                            {{ $safety_systems['clearance_back_sonar'] }}
                                        </td>
                                    </tr>
                                @endif

                                @if ($safety_systems['back_monitor'] != "")
                                    <tr>
                                        <th>Back monitor</th>
                                        <td align="center">
                                            {{ $safety_systems['back_monitor'] }}
                                        </td>
                                    </tr>
                                @endif

                                @if ($safety_systems['child_restraint_system'] != "")
                                    <tr>
                                        <th>Child Restraint System</th>
                                        <td align="center">
                                            {{ $safety_systems['child_restraint_system'] }}
                                        </td>
                                    </tr>
                                @endif
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="d-flex justify-content-around align-items-center text-secondary py-4" style="--line-color: #6c757d">
                @php
                    $brochures = json_decode($specification->carModel->subCategory->brochures, true)
                @endphp
                @if (count($brochures) == 1)
                    <a href="{{url("storage/".$brochures[0]['brochures'])}}" class="animated-link toyota-paragraph">
                        <img src="{{asset('img/temp/download.png')}}" alt="" width="25px" class="me-2 mb-2"><span class="d-none d-sm-inline">Download</span> Brochures
                    </a>
                @elseif (count($brochures) > 1)
                    <div class="dropdown col-lg-3 col-6 py-sm-5 py-3 d-flex justify-content-center">
                        <div class="dropdown-toggle d-flex justify-content-center" type="button" data-bs-toggle="dropdown"
                            aria-expanded="false">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="30" height="30" class="mx-3 zoom-x animation"
                                viewBox="0 0 32.7 48" class="text-dark">
                                <path d="M4.4,47H1V9.5L19.7,1v37.5L1,47 M2.7,47h29V9.5h-12v29L1,47" fill="none"
                                    stroke="currentColor"></path>
                                <line x1="16.5" y1="10.8" x2="2.6" y2="17.2" fill="none"
                                    stroke="currentColor"></line>
                                <line x1="16.5" y1="19.1" x2="2.6" y2="25.5" fill="none"
                                    stroke="currentColor"></line>
                            </svg>

                            <a href="" class="animated-link   zoom-x animation"
                                style="--duration: 1.5s"><span class=" d-none d-sm-inline">Download</span> Brochures</a>
                        </div>
                            <ul class="dropdown-menu">
                                @foreach ($brochures as $brochure)
                                    <li><a class="dropdown-item" href="{{ url('storage/' . $brochure['brochures']) }}">
                                        <i class="fa-solid fa-download"></i> {{ basename($brochure['brochures']) }}</a>
                                    </li>
                                @endforeach
                            </ul>
                    </div>
                @endif
                <a href="{{ route('model.compare-select') }}" class="animated-link toyota-paragraph">
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="32" height="32"
                    viewBox="0 0 34.9 48" class="text-dark me-2">
                        <polyline points="32.7,41.4 32.7,4.5 13.6,4.5" fill="none" stroke="currentColor">
                        </polyline>
                        <rect x="30.9" y="41.4" width="3.4" height="3.2" fill="currentColor">
                        </rect>
                        <polyline points="17.4,1 13.6,4.5 17.4,8" fill="none" stroke="currentColor"
                            stroke-linecap="round" stroke-linejoin="round"></polyline>
                        <polyline points="2.2,6.6 2.2,43.5 21.3,43.5" fill="none" stroke="currentColor">
                        </polyline>
                        <rect x="0.5" y="4.2" width="3.4" height="3.2" fill="currentColor">
                        </rect>
                        <polyline points="17.5,47 21.3,43.5 17.5,40" fill="none" stroke="currentColor"
                            stroke-linecap="round" stroke-linejoin="round">
                        </polyline>
                    </svg>
                    Compare <span class="d-none d-sm-inline">Model</span>
                </a>
            </div>
        </div>
    </main>
    @include('components.stay_informed')
@endsection
