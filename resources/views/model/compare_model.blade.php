@extends('layouts.main')

@section('title', 'Model Comparison')

@section('css')
    <style>
        td,
        th {
            padding: 20px 10px !important;
        }

        td {
            border-left: 1px solid #0003;
        }

        .not-border-bottom {
            border-bottom-width: 0 !important;
        }
    </style>
@endsection

@section('content')
    <main class="bg-darker-white px-sm-5 px-3 pb-4">
        @include('components.breadcrumb', [
            'data' => [['route' => 'model.compare-select', 'name' => 'Select Model']],
        ])

        <form action="{{ route('model.compare-model') }}" method="post" class="d-none" id="compareForm">
            @csrf
            <input type="hidden" name="first_model_id" id="compare_model_id1" value="{{ $models[1]->id }}">
            <input type="hidden" name="sec_model_id" id="compare_model_id2" value="{{ $models[2]->id }}">
        </form>
        <article class="container">
            <header class="py-4">
                <h2 class="fw-bold">MODEL COMPARISON</h2>
                <h5>Select model grades and do a quick side-by-side comparison.</h5>
            </header>
            <div class="row">
                @for ($i = 1; $i < 3; $i++)
                    <div class="col-6 d-flex flex-column align-items-center justify-content-center position-relative">
                        <a href="{{ route('model.car-detail', ['id' => $models[$i]->id]) }}"
                            id="model_name{{ $i }}" class="animated-link-reverse fw-bold my-4"
                            style="--line-color: #000">
                            {{ $models[$i]->name }}
                        </a>
                        <img src="{{ asset('storage/' . $models[$i]->subCategory->image) }}"
                            id="car_placeholder{{ $i }}" alt="" width="100%"
                            style="{{ $i == 1 ? 'transform: scaleX(-1);' : '' }}">
                        <h5 class="fw-bold">{{ $i }} {!! $i == 1 ? '<sup>st</sup>' : '<sup>nd</sup>' !!} Model</h5>
                        <a type="button" data-bs-toggle="offcanvas" data-bs-target="#compareOffcanvas{{ $i }}"
                            class="animated-link text-toyota-danger" style="--line-color: var(--danger)">CHANGE MODEL <i
                                class="fa-solid fa-angle-right"></i></a>

                        @if ($i == 1)
                            <div class="vs d-flex align-items-center justify-content-center">
                                <span class="toyota-paragraph bg-darker-white py-2">vs</span>
                            </div>
                        @endif

                    </div>
                @endfor
            </div>

            <table class="table my-5">
                <tbody>
                    <tr>
                        <th>Price</th>
                        <td class="price">{{ 'From Ks ' . $specifications[1]['price'] ?? '' }}</td>
                        <td class="price">{{ 'From Ks ' . $specifications[2]['price'] ?? '' }}</td>
                    </tr>
                    <tr>

                        <th>Engine Type</th>
                        <td class="engine_type">{{ $specifications[1]['engine_type'] ?? '' }}</td>
                        <td class="engine_type">{{ $specifications[2]['engine_type'] ?? '' }}</td>
                    </tr>
                    <tr>
                        <th>Displacement Type</th>
                        {{-- <td>{{ $first_car['displacement_type'] }}</td>
                        <td>{{ $sec_car['displacement_type'] }}</td> --}}
                        <td class="displacement">{{ $specifications[1]['displacement'] ?? '' }}</td>
                        <td class="displacement">{{ $specifications[2]['displacement'] ?? '' }}</td>
                    </tr>
                    <tr>
                        <th>Max Output</th>
                        {{-- <td>{{ $first_car['max_output'] }}</td>
                        <td>{{ $sec_car['max_output'] }}</td> --}}
                        <td class="max_output">{{ $specifications[1]['max_output'] ?? '' }}</td>
                        <td class="max_output">{{ $specifications[2]['max_output'] ?? '' }}</td>
                    </tr>
                    <tr>
                        <th>Max Torque</th>
                        {{-- <td>{{ $first_car['max_torque'] }}</td>
                        <td>{{ $sec_car['max_torque'] }}</td> --}}

                        <td class="max_output">{{ $specifications[1]['max_torque'] ?? '' }}</td>
                        <td class="max_output">{{ $specifications[2]['max_torque'] ?? '' }}</td>
                    </tr>
                    <tr>
                        <th>Fuel Consumption</th>
                        {{-- <td>{{ $first_car['fuel_consumption'] }}</td>
                        <td>{{ $sec_car['fuel_consumption'] }}</td> --}}
                        <td class="fuel_consumption">{{ $specifications[1]['fuel_consumption'] ?? '' }}</td>
                        <td class="fuel_consumption">{{ $specifications[2]['fuel_consumption'] ?? '' }}</td>

                    </tr>
                    <tr>
                        <th class="not-border-bottom"></th>
                        <td class="not-border-bottom">
                            {{-- <a href="{{ $first_car['brochure'] }}" class="animated-link-reverse"
                                style="--line-color: #000">Download Brochure</a> --}}
                            <div class="dropdown-toggle d-flex justify-content-center" type="button"
                                data-bs-toggle="dropdown" aria-expanded="false">
                                <a href="#" class="animated-link-reverse" style="--line-color: #000">Download
                                    Brochure</a>
                            </div>
                            <ul class="dropdown-menu">
                                @php
                                    $brochures = json_decode($models[1]->subCategory?->brochures, true);
                                @endphp
                                @if (isset($brochures))
                                    @foreach ($brochures as $brochure)
                                        <li><a class="dropdown-item"
                                                href="{{ url('storage/' . $brochure['brochures']) }}"><i
                                                    class="fa-solid fa-download"></i>
                                                {{ basename($brochure['brochures']) }}</a>
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                        </td>
                        <td class="not-border-bottom">
                            {{-- <a href="{{ $sec_car['brochure'] }}" class="animated-link-reverse"
                                style="--line-color: #000">Download Brochure</a></td> --}}
                            <div class="dropdown-toggle d-flex justify-content-center" type="button"
                                data-bs-toggle="dropdown" aria-expanded="false">
                                <a href="#" class="animated-link-reverse" style="--line-color: #000">Download
                                    Brochure</a>
                            </div>
                            <ul class="dropdown-menu">
                                @php
                                    $brochures = json_decode($models[2]->subCategory?->brochures, true);
                                @endphp
                                @if (isset($brochures))
                                    @foreach ($brochures as $brochure)
                                        <li><a class="dropdown-item"
                                                href="{{ url('storage/' . $brochure['brochures']) }}"><i
                                                    class="fa-solid fa-download"></i>
                                                {{ basename($brochure['brochures']) }}</a>
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="accordion" id="spec_accordion">
                {{-- Measurements --}}
                <div class="accordion-item bg-darker-white">
                    <h2 class="accordion-header">
                        <button class="accordion-button collapsed bg-darker-white" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                            Dimensions weight
                        </button>
                    </h2>
                    <div id="collapseOne" class="accordion-collapse collapse" data-bs-parent="#spec_accordion">
                        <table class="table my-5">
                            <tbody>
                                <tr>
                                    <th>Overall</th>
                                    {{-- <td>{{ $first_car['price'] }}</td>
                                    <td>{{ $sec_car['price'] }}</td> --}}
                                    <td class="overall col-5">{{ $specifications[1]['overall'] ?? '' }}</td>
                                    <td class="overall col-5">{{ $specifications[2]['overall'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th>Wheelbase</th>
                                    <td class="wheel_base">{{ $specifications[1]['wheel_base'] ?? '' }}</td>
                                    <td class="wheel_base">{{ $specifications[2]['wheel_base'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th>Tread</th>
                                    {{-- <td>{{ $first_car['displacement_type'] }}</td>
                                    <td>{{ $sec_car['displacement_type'] }}</td> --}}
                                    <td class="tread">{{ $specifications[1]['tread'] ?? '' }}</td>
                                    <td class="tread">{{ $specifications[2]['tread'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th>Minimum running ground clearance</th>
                                    <td class="minimum_running_ground_clearance">
                                        {{ $specifications[1]['minimum_running_ground_clearance'] ?? '' }}</td>
                                    <td class="minimum_running_ground_clearance">
                                        {{ $specifications[2]['minimum_running_ground_clearance'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th>Minimum turning radius</th>
                                    <td class="minimum_turning_radius">
                                        {{ $specifications[1]['minimum_turning_radius'] ?? '' }}</td>
                                    <td class="minimum_turning_radius">
                                        {{ $specifications[2]['minimum_turning_radius'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th>Gross vehicles weight</th>
                                    <td class="gross_vehicles_weight">
                                        {{ $specifications[1]['gross_vehicles_weight'] ?? '' }}</td>
                                    <td class="gross_vehicles_weight">
                                        {{ $specifications[2]['gross_vehicles_weight'] ?? '' }}</td>
                                </tr>

                                <tr>
                                    <th>Total min options</th>
                                    <td class="total_min_options">{{ $specifications[1]['total_min_options'] ?? '' }}</td>
                                    <td class="total_min_options">{{ $specifications[2]['total_min_options'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th>Total max options</th>
                                    <td class="total_max_options">{{ $specifications[1]['total_max_options'] ?? '' }}</td>
                                    <td class="total_max_options">{{ $specifications[2]['total_max_options'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th>Seating capacity</th>
                                    <td class="seating_capacity">{{ $specifications[1]['seating_capacity'] ?? '' }}</td>
                                    <td class="seating_capacity">{{ $specifications[2]['seating_capacity'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th>Deck size</th>
                                    <td class="deck_size">{{ $specifications[1]['deck_size'] ?? '' }}
                                    </td>
                                    <td class="deck_size">{{ $specifications[2]['deck_size'] ?? '' }}
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
                {{-- Engine --}}
                <div class="accordion-item bg-darker-white">
                    <h2 class="accordion-header">
                        <button class="accordion-button collapsed bg-darker-white" type="button"
                            data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false"
                            aria-controls="collapseTwo">
                            Engine
                        </button>
                    </h2>
                    <div id="collapseTwo" class="accordion-collapse collapse" data-bs-parent="#spec_accordion">
                        <table class="table my-5">
                            <tbody>
                                <tr>
                                    <th>Engine Model Code</th>
                                    {{-- <td>{{ $first_car['price'] }}</td>
                                    <td>{{ $sec_car['price'] }}</td> --}}
                                    <td class="engine_model_code">{{ $specifications[1]['engine_model_code'] ?? '' }}</td>
                                    <td class="engine_model_code">{{ $specifications[2]['engine_model_code'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th>Engine Type</th>
                                    <td class="engine_type col-5">{{ $specifications[1]['engine_type'] ?? '' }}</td>
                                    <td class="engine_type col-5">{{ $specifications[2]['engine_type'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th>Displacement Type</th>
                                    {{-- <td>{{ $first_car['displacement_type'] }}</td>
                                    <td>{{ $sec_car['displacement_type'] }}</td> --}}
                                    <td class="displacement">{{ $specifications[1]['displacement'] ?? '' }}</td>
                                    <td class="displacement">{{ $specifications[2]['displacement'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th>Bore x stoke</th>
                                    <td class="bore_x_stoke">{{ $specifications[1]['bore_x_stoke'] ?? '' }}</td>
                                    <td class="bore_x_stoke">{{ $specifications[2]['bore_x_stoke'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th>Compression ratio</th>
                                    <td class="compression_ratio">{{ $specifications[1]['compression_ratio'] ?? '' }}</td>
                                    <td class="compression_ratio">{{ $specifications[2]['compression_ratio'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th>Max.output (SAE net)</th>
                                    <td class="max_output">{{ $specifications[1]['max_output'] ?? '' }}</td>
                                    <td class="max_output">{{ $specifications[2]['max_output'] ?? '' }}</td>
                                </tr>

                                <tr>
                                    <th>Max Torque</th>
                                    <td class="max_torque">{{ $specifications[1]['max_torque'] ?? '' }}</td>
                                    <td class="max_torque">{{ $specifications[2]['max_torque'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th>Fuel System</th>
                                    <td class="fuel_system">{{ $specifications[1]['fuel_system'] ?? '' }}</td>
                                    <td class="fuel_system">{{ $specifications[2]['fuel_system'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th>Fuel Type</th>
                                    <td class="fuel_type">{{ $specifications[1]['fuel_type'] ?? '' }}</td>
                                    <td class="fuel_type">{{ $specifications[2]['fuel_type'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th>Fuel Tank Capacity</th>
                                    <td class="fuel_tank_capacity">{{ $specifications[1]['fuel_tank_capacity'] ?? '' }}
                                    </td>
                                    <td class="fuel_tank_capacity">{{ $specifications[2]['fuel_tank_capacity'] ?? '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Fuel Consumption</th>
                                    <td class="fuel_consumption">{{ $specifications[1]['fuel_consumption'] ?? '' }}</td>
                                    <td class="fuel_consumption">{{ $specifications[2]['fuel_consumption'] ?? '' }}</td>
                                </tr>
                                {{-- <tr>
                                    <th class="not-border-bottom"></th>
                                    <td class="not-border-bottom"><a href="{{ $first_car['brochure'] }}"
                                            class="animated-link-reverse" style="--line-color: #000">Download Brochure</a>
                                    </td>
                                    <td class="not-border-bottom"><a href="{{ $sec_car['brochure'] }}"
                                            class="animated-link-reverse" style="--line-color: #000">Download Brochure</a>
                                    </td>
                                </tr> --}}
                            </tbody>
                        </table>
                    </div>
                </div>

                {{-- Chassi --}}
                <div class="accordion-item bg-darker-white">
                    <h2 class="accordion-header">
                        <button class="accordion-button collapsed bg-darker-white" type="button"
                            data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false"
                            aria-controls="collapseThree">
                            Chassi
                        </button>
                    </h2>
                    <div id="collapseThree" class="accordion-collapse collapse" data-bs-parent="#spec_accordion">
                        <table class="table my-5">
                            <tbody>
                                <tr>
                                    <th>Transmission Type</th>
                                    <td class="transmission_type col-5">
                                        {{ $specifications[1]['transmission_type'] ?? '' }}</td>
                                    <td class="transmission_type col-5">
                                        {{ $specifications[2]['transmission_type'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th>Suspension ( Front/Rear)</th>
                                    <td class="suspension">{{ $specifications[1]['suspension'] ?? '' }}</td>
                                    <td class="suspension">{{ $specifications[2]['suspension'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th>Brake Type (Front/Rear)</th>
                                    <td class="brake_type">{{ $specifications[1]['brake_type'] ?? '' }}</td>
                                    <td class="brake_type">{{ $specifications[2]['brake_type'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th>Rear combination lamps</th>
                                    <td class="rear_combination_lamps">
                                        {{ $specifications[1]['rear_combination_lamps'] ?? '' }}</td>
                                    <td class="rear_combination_lamps">
                                        {{ $specifications[2]['rear_combination_lamps'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th>Tires</th>
                                    <td class="tires">{{ $specifications[1]['tires'] ?? '' }}</td>
                                    <td class="tires">{{ $specifications[2]['tires'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th>Wheels</th>
                                    <td class="wheels">{{ $specifications[1]['wheels'] ?? '' }}</td>
                                    <td class="wheels">{{ $specifications[2]['wheels'] ?? '' }}</td>
                                </tr>

                                <tr>
                                    <th>Steering gear type</th>
                                    <td class="steering_gear_type">{{ $specifications[1]['steering_gear_type'] ?? '' }}
                                    </td>
                                    <td class="steering_gear_type">{{ $specifications[2]['steering_gear_type'] ?? '' }}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                {{-- Standard equipment exterior --}}
                <div class="accordion-item bg-darker-white">
                    <h2 class="accordion-header">
                        <button class="accordion-button collapsed bg-darker-white" type="button"
                            data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false"
                            aria-controls="collapseFour">
                            Standard equipment exterior
                        </button>
                    </h2>
                    <div id="collapseFour" class="accordion-collapse collapse" data-bs-parent="#spec_accordion">
                        <table class="table my-5">
                            <tbody>
                                <tr>
                                    <th>Headlamps</th>
                                    <td class="headlamps col-5">{{ $specifications[1]['headlamps'] ?? '' }}</td>
                                    <td class="headlamps col-5">{{ $specifications[2]['headlamps'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th>Front fog & driving lamp</th>
                                    <td class="front_fog_driving_lamp">
                                        {{ $specifications[1]['front_fog_driving_lamp'] ?? '' }}</td>
                                    <td class="front_fog_driving_lamp">
                                        {{ $specifications[2]['front_fog_driving_lamp'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th>Daytime running light system</th>
                                    <td class="daytime_running_light_system">
                                        {{ $specifications[1]['daytime_running_light_system'] ?? '' }}</td>
                                    <td class="daytime_running_light_system">
                                        {{ $specifications[2]['daytime_running_light_system'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th>Rear combination lamps</th>
                                    <td class="rear_combination_lamps">
                                        {{ $specifications[1]['rear_combination_lamps'] ?? '' }}</td>
                                    <td class="rear_combination_lamps">
                                        {{ $specifications[2]['rear_combination_lamps'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th>Outside rear view mirror</th>
                                    <td class="outside_rear_view_mirror">
                                        {{ $specifications[1]['outside_rear_view_mirror'] ?? '' }}</td>
                                    <td class="outside_rear_view_mirror">
                                        {{ $specifications[2]['outside_rear_view_mirror'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th>Mudguard</th>
                                    <td class="mudguard">{{ $specifications[1]['mudguard'] ?? '' }}</td>
                                    <td class="mudguard">{{ $specifications[2]['mudguard'] ?? '' }}</td>
                                </tr>

                                <tr>
                                    <th>Deck bar</th>
                                    <td class="deck_bar">{{ $specifications[1]['deck_bar'] ?? '' }}
                                    </td>
                                    <td class="deck_bar">{{ $specifications[2]['deck_bar'] ?? '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Bed liner</th>
                                    <td class="bed_liner">{{ $specifications[1]['bed_liner'] ?? '' }}
                                    </td>
                                    <td class="bed_liner">{{ $specifications[2]['bed_liner'] ?? '' }}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>


                {{-- Standard equipment interior --}}
                <div class="accordion-item bg-darker-white">
                    <h2 class="accordion-header">
                        <button class="accordion-button collapsed bg-darker-white" type="button"
                            data-bs-toggle="collapse" data-bs-target="#collapseFive" aria-expanded="false"
                            aria-controls="collapseFive">
                            Standard equipment interior
                        </button>
                    </h2>
                    <div id="collapseFive" class="accordion-collapse collapse" data-bs-parent="#spec_accordion">
                        <table class="table my-5">
                            <tbody>
                                <tr>
                                    <th>Steering switch</th>
                                    <td class="steering_switch col-5">{{ $specifications[1]['steering_switch'] ?? '' }}
                                    </td>
                                    <td class="steering_switch col-5">{{ $specifications[2]['steering_switch'] ?? '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Steering column</th>
                                    <td class="steering_column">
                                        {{ $specifications[1]['steering_column'] ?? '' }}</td>
                                    <td class="steering_column">
                                        {{ $specifications[2]['steering_column'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th>Part-time transfer switch</th>
                                    <td class="part_time_transfer_switch">
                                        {{ $specifications[1]['part_time_transfer_switch'] ?? '' }}</td>
                                    <td class="part_time_transfer_switch">
                                        {{ $specifications[2]['part_time_transfer_switch'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th>Drive mode switch</th>
                                    <td class="drive_mode_switch">
                                        {{ $specifications[1]['drive_mode_switch'] ?? '' }}</td>
                                    <td class="drive_mode_switch">
                                        {{ $specifications[2]['drive_mode_switch'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th>Cruise control</th>
                                    <td class="cruise_control">
                                        {{ $specifications[1]['cruise_control'] ?? '' }}</td>
                                    <td class="cruise_control">
                                        {{ $specifications[2]['cruise_control'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th>Multi-information display</th>
                                    <td class="multi_information_display">
                                        {{ $specifications[1]['multi_information_display'] ?? '' }}</td>
                                    <td class="multi_information_display">
                                        {{ $specifications[2]['multi_information_display'] ?? '' }}</td>
                                </tr>

                                <tr>
                                    <th>Economy meter</th>
                                    <td class="economy_meter">{{ $specifications[1]['economy_meter'] ?? '' }}
                                    </td>
                                    <td class="economy_meter">{{ $specifications[2]['economy_meter'] ?? '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Front seat</th>
                                    <td class="front_seat">{{ $specifications[1]['front_seat'] ?? '' }}
                                    </td>
                                    <td class="front_seat">{{ $specifications[2]['front_seat'] ?? '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Rear seat</th>
                                    <td class="rear_seat">{{ $specifications[1]['rear_seat'] ?? '' }}
                                    </td>
                                    <td class="rear_seat">{{ $specifications[2]['rear_seat'] ?? '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Seat cover material</th>
                                    <td class="seat_cover_material">{{ $specifications[1]['seat_cover_material'] ?? '' }}
                                    </td>
                                    <td class="seat_cover_material">{{ $specifications[2]['seat_cover_material'] ?? '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Audio system</th>
                                    <td class="audio_system">{{ $specifications[1]['audio_system'] ?? '' }}
                                    </td>
                                    <td class="audio_system">{{ $specifications[2]['audio_system'] ?? '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Speaker system</th>
                                    <td class="speaker_system">{{ $specifications[1]['speaker_system'] ?? '' }}
                                    </td>
                                    <td class="speaker_system">{{ $specifications[2]['speaker_system'] ?? '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Air conditioning</th>
                                    <td class="air_conditioning">{{ $specifications[1]['air_conditioning'] ?? '' }}
                                    </td>
                                    <td class="air_conditioning">{{ $specifications[2]['air_conditioning'] ?? '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Starting system</th>
                                    <td class="starting_system">{{ $specifications[1]['starting_system'] ?? '' }}
                                    </td>
                                    <td class="starting_system">{{ $specifications[2]['starting_system'] ?? '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Wireless door lock</th>
                                    <td class="wireless_door_lock">{{ $specifications[1]['wireless_door_lock'] ?? '' }}
                                    </td>
                                    <td class="wireless_door_lock">{{ $specifications[2]['wireless_door_lock'] ?? '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Accessory connector</th>
                                    <td class="accessory_connector">{{ $specifications[1]['accessory_connector'] ?? '' }}
                                    </td>
                                    <td class="accessory_connector">{{ $specifications[2]['accessory_connector'] ?? '' }}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>


                {{-- Safety system --}}
                <div class="accordion-item bg-darker-white">
                    <h2 class="accordion-header">
                        <button class="accordion-button collapsed bg-darker-white" type="button"
                            data-bs-toggle="collapse" data-bs-target="#collapseSix" aria-expanded="false"
                            aria-controls="collapseSix">
                            Safety system
                        </button>
                    </h2>
                    <div id="collapseSix" class="accordion-collapse collapse" data-bs-parent="#spec_accordion">
                        <table class="table my-5">
                            <tbody>
                                <tr>
                                    <th>Pre-collision System (PCS)</th>
                                    <td class="pre_collision_system col-5">
                                        {{ $specifications[1]['pre_collision_system'] ?? '' }}</td>
                                    <td class="pre_collision_system col-5">
                                        {{ $specifications[2]['pre_collision_system'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th>Dynamic Radar Cruise Control (DRCC)</th>
                                    <td class="dynamic_radar_cruise_control">
                                        {{ $specifications[1]['dynamic_radar_cruise_control'] ?? '' }}</td>
                                    <td class="dynamic_radar_cruise_control">
                                        {{ $specifications[2]['dynamic_radar_cruise_control'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th>Lane Departure Alert (LDA)</th>
                                    <td class="lane_departure_alert">
                                        {{ $specifications[1]['lane_departure_alert'] ?? '' }}</td>
                                    <td class="lane_departure_alert">
                                        {{ $specifications[2]['lane_departure_alert'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th>Downhill Assist Control (DAC)</th>
                                    <td class="downhill_assist_control">
                                        {{ $specifications[1]['downhill_assist_control'] ?? '' }}</td>
                                    <td class="downhill_assist_control">
                                        {{ $specifications[2]['downhill_assist_control'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th>Limited Slip Differential (LSD)</th>
                                    <td class="limited_slip_differential">
                                        {{ $specifications[1]['limited_slip_differential'] ?? '' }}</td>
                                    <td class="limited_slip_differential">
                                        {{ $specifications[2]['limited_slip_differential'] ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th>SRS airbags</th>
                                    <td class="srs_airbags">{{ $specifications[1]['srs_airbags'] ?? '' }}</td>
                                    <td class="srs_airbags">{{ $specifications[2]['srs_airbags'] ?? '' }}</td>
                                </tr>

                                <tr>
                                    <th>Anti-lock brake system (ABS)</th>
                                    <td class="anti_lock_brake_system">
                                        {{ $specifications[1]['anti_lock_brake_system'] ?? '' }}
                                    </td>
                                    <td class="anti_lock_brake_system">
                                        {{ $specifications[2]['anti_lock_brake_system'] ?? '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Vehicles Stability Control (VSC)</th>
                                    <td class="vehicle_stability_control">
                                        {{ $specifications[1]['vehicle_stability_control'] ?? '' }}
                                    </td>
                                    <td class="vehicle_stability_control">
                                        {{ $specifications[2]['vehicle_stability_control'] ?? '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Hill start assist control (HAC)</th>
                                    <td class="hill_start_assist_control">
                                        {{ $specifications[1]['hill_start_assist_control'] ?? '' }}
                                    </td>
                                    <td class="hill_start_assist_control">
                                        {{ $specifications[2]['hill_start_assist_control'] ?? '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Trailer Sway Control (TSC)</th>
                                    <td class="trailer_sway_control">
                                        {{ $specifications[1]['trailer_sway_control'] ?? '' }}
                                    </td>
                                    <td class="trailer_sway_control">
                                        {{ $specifications[2]['trailer_sway_control'] ?? '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Emergency brake signal</th>
                                    <td class="emergency_brake_signal">
                                        {{ $specifications[1]['emergency_brake_signal'] ?? '' }}
                                    </td>
                                    <td class="emergency_brake_signal">
                                        {{ $specifications[2]['emergency_brake_signal'] ?? '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Clearance & back sonar</th>
                                    <td class="clearance_back_sonar">
                                        {{ $specifications[1]['clearance_back_sonar'] ?? '' }}
                                    </td>
                                    <td class="clearance_back_sonar">
                                        {{ $specifications[2]['clearance_back_sonar'] ?? '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Back monitor</th>
                                    <td class="back_monitor">
                                        {{ $specifications[1]['back_monitor'] ?? '' }}
                                    </td>
                                    <td class="back_monitor">
                                        {{ $specifications[2]['back_monitor'] ?? '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Child Restraint System</th>
                                    <td class="child_restraint_system">
                                        {{ $specifications[1]['child_restraint_system'] ?? '' }}
                                    </td>
                                    <td class="child_restraint_system">
                                        {{ $specifications[2]['child_restraint_system'] ?? '' }}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </article>
        @include('model.compare_form', ['key' => 2])

    </main>
@endsection

@section('script')
    <script>
        const comparison = (className) => {
            const ele = $('.' + className);
            ele.addClass('text-muted');
        }
        $(document).ready(function() {
            for (var i = 1; i < 3; i++) {
                $("#submit_model" + i).on('click', function() {
                    let j = $(this).attr('data-sub-target')
                    let index = i
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('ajax.get-model-by-id') }}",
                        data: {
                            'model_id': $("#model" + j).val()
                        },
                        success: function(data) {
                            // console.log(data)
                            if (!jQuery.isEmptyObject(data)) {
                                $('#car_placeholder' + j).attr('src', '/storage/' + data
                                    .sub_category?.image)
                                $('#model_name' + j).html(data['name'])
                                $("#compare_model_id" + j).val(data.id)

                                // $("#compare_model_id" + j).val('shhhhh')
                                // const test = $("#compare_model_id" + j).val()
                                // console.log(test)
                                $("#compareForm").trigger("submit");
                            }
                        }
                    });

                });
            }

            const sameArrKeys = @json($sameArrKeys);

            sameArrKeys.map((item) => {
                comparison(item)
            })
        })
    </script>
@endsection
