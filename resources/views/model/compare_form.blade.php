@for ($i = 1; $i < 3; $i++)
    <div class="offcanvas offcanvas-start form-offcanvas" id="compareOffcanvas{{ $i }}">
        <div class="p-sm-5 p-4">
            <button type="button" class="btn-close float-end" data-bs-dismiss="offcanvas" aria-label="Close"></button>
        </div>
        <div class="d-flex align-items-center justify-content-center">
            <div class="row w-100 p-lg-5 p-3">
                <form method="POST" class="col-lg-7 col-12 px-sm-5 px-4">
                    <h2 class="fw-bold py-4">SELECT MODEL</h2>
                    <div class="input-gorup pb-2">
                        <label for="vehicle_type" class="fw-bold py-2">Vehicle type</label>
                        <select name="category_id" id="category{{ $i }}"
                            data-sub-target="#model{{ $i }}" class="form-select form-select-lg toyota-input">
                            <option value="0" disabled selected>Select vehicle type</option>
                            @foreach ($categories as $category)
                                <option value="{{ $category->id }}"
                                    {{ isset($models[$i]) ? ($category->id == $models[$i]->subCategory->category_id ? 'selected' : '') : '' }}>
                                    {{ $category->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="input-gorup pt-2">
                        <label for="model_grade" class="fw-bold py-2">Model grade</label>
                        <select name="model_id" id="model{{ $i }}"
                            data-img-target="#car_img{{ $i }}"
                            class="form-select form-select-lg toyota-input">
                            <option value="0" disabled selected>Select model grade</option>
                            @if (isset($models[$i]))
                                @foreach ($models as $model)
                                    <option value="{{ $model->id }}"
                                        {{ $model->id == $models[$i]->id ? 'selected' : '' }}>{{ $model->name }}
                                    </option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <button type="button" class="btn btn-outline-dark rounded-0 px-4 mt-5 py-2"
                        data-bs-dismiss="offcanvas">CANCLE</button>
                    <button type="button" class="btn btn-toyota-danger px-4 mt-5 ms-sm-4 py-2"
                        id="submit_model{{ $i }}" data-sub-target="{{ $i }}"
                        data-bs-dismiss="offcanvas">
                        @if (isset($models[$i]))
                            CHANGE
                        @else
                            ADD MODEL
                        @endif
                    </button>
                </form>
                <div class="col-lg-5 col-12 d-flex flex-column justify-content-center align-items-center py-5"
                    id="car_img{{ $i }}">
                    @if (isset($models[$i]))
                        <img src='{{ asset('storage/' . $models[$i]->image) }}' alt="" width="100%"
                            style="{{ $i == 1 ? 'transform: scaleX(-1)' : '' }}">
                        <label class="py-3 h5">{{ $models[$i]->name }}</label>
                    @else
                        <img src='{{ asset('img/compare/placeholder_right.png') }}' alt="" width="100%"
                            style="{{ $i == 1 ? 'transform: scaleX(-1)' : '' }}">
                        <label class="py-3 h5">{{ $i }} {!! $i == 1 ? '<sup>st</sup>' : '<sup>nd</sup>' !!} Model</label>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endfor

@section('js')
    <script>
        $(document).ready(function() {
            for (var i = 1; i < 3; i++) {
                $("#category" + i).on('change', function() {
                    var model = $(this).attr('data-sub-target')
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('ajax.models-by-category') }}",
                        data: {
                            'category_id': $(this).val()
                        },
                        success: function(data) {
                            var op = " ";
                            for (var j = 0; j < data.length; j++) {
                                op += '<option value="' + data[j].id + '">' + data[j].name +
                                    '</option>';
                            }
                            $(model).html(' ');
                            $(model).append(
                                '<option value="0" selected disabled>Select model grade</option>'
                            )
                            $(model).append(op);
                        }
                    });
                });

                $('#model' + i).on('change', function() {
                    var img = $(this).attr('data-img-target')
                    $.ajax({
                        type: 'POST',
                        url: "{{ route('ajax.sub-by-model') }}",
                        data: {
                            'model_id': $(this).val()
                        },

                        success: function(data) {
                            $(img + " img").attr('src', '/storage/' + data['image']);
                            $(img + " label").html(data['name']);
                        }
                    });
                });
            }
        });
    </script>
@endsection
