<?php

use App\Http\Controllers\Admin\CategoryCrudController;
use Illuminate\Support\Facades\Route;

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => array_merge(
        (array) config('backpack.base.web_middleware', 'web'),
        (array) config('backpack.base.middleware_key', 'admin')
    ),
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::crud('promotion', 'PromotionCrudController');
    Route::crud('dealer', 'DealerCrudController');
    Route::crud('map', 'MapCrudController');
    Route::crud('category', 'CategoryCrudController');
    Route::crud('subcategory', 'SubCategoryCrudController');
    Route::crud('carmodel', 'CarModelCrudController');
    Route::crud('specification', 'SpecificationCrudController');
    Route::crud('color', 'ColorCrudController');
    // Route::crud('carimage', 'CarImageCrudController');
    Route::crud('sub-info', 'SubCategoryInformationCrudController');
    Route::crud('blog', 'BlogCrudController');

    Route::crud('city', 'CityCrudController');

    Route::crud('info', 'DealersInfoCrudController');

    Route::crud('avaliablecar', 'AvaliableCarCrudController');
    Route::crud('enquiry', 'EnquiryCrudController');
    Route::crud('stayinformed', 'StayInformedCrudController');
    Route::crud('province', 'ProvinceCrudController');
    Route::crud('typeofservice', 'TypeOfServiceCrudController');
    Route::crud('servicebooking', 'ServiceBookingCrudController');
    Route::crud('onlineinquiry', 'OnlineInquiryCrudController');
    Route::crud('accessory', 'AccessoryCrudController');
}); // this should be the absolute last line of this file