<?php

use App\Http\Controllers\Admin\PromotionCrudController;
use App\Http\Controllers\AjaxController;
use App\Http\Controllers\FrontEndController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Dynamic Routes

Route::get('admin/servicebooking/{id}/confirm', [
    'as' => 'confirm',
    'uses' => 'App\Http\Controllers\Admin\ServiceBookingCrudController@confirm'
]);


Route::get('admin/servicebooking/{id}/unconfirm', [
    'as' => 'unconfirm',
    'uses' => 'App\Http\Controllers\Admin\ServiceBookingCrudController@unconfirm'
]);

Route::controller(FrontEndController::class)->group(function () {
    Route::get('/', 'home')->name('home');

    Route::get('/dealer', 'dealer')->name('dealer');
    Route::get('/dealer/{id}', 'dealerDetail')->name('dealer.detail');
    Route::post('/dealer/online-inquiry', 'onlineInquiryStore')->name('online-inquiry.store');

    Route::get('/car-detail/{id}', 'carDetail')->name('model.car-detail');
    Route::get('/model-comparison',  'model')->name('model.compare-select');
    Route::post('/model-comparison', 'modelImage')->name('model.compare-select-image');
    Route::post('/model-comparison/compare-model', 'compareModel')->name('model.compare-model');
    Route::get('/download-brochures', 'brochure')->name('download-brochures');

    Route::get('/search-inventory', 'inventory')->name('inventory');
    Route::get('/search-inventory-detail/{id}', 'inventoryDetail')->name('inventory.detail');


    Route::get('car-detail/{id}', 'carDetail')->name('model.car-detail');
    Route::get('car-detail/{id}/specification', 'carSpecification')->name('model.car.specification');

    Route::get('/promotion', 'promotion')->name('promotion');
    Route::post('/promotion', 'promotionFilter')->name('promotion.filter');
    Route::get('/promotion/{id}/share', 'promotionShare')->name('promotion.share');


    Route::get('/blogs', 'blog')->name('blog');
    Route::get('/blogs-search', 'blogSearch')->name('blog.search');
    Route::get('/blogs/{id}', 'blogDetail')->name('blog.detail');
    Route::get('/blogs/{id}/share', 'blogShare')->name('blog.share');

    Route::get('/used-car', 'usedcar')->name('used-car.index');
    Route::get('/used-car-search', 'usedCarSearch')->name('used-car.search');
    Route::get('/used-car-detail/{id}', 'usedCarDetail')->name('used-car.detail');

    Route::post('/stay-informed/store', 'stayInformedStore')->name('stay-informed.store');
    Route::post('/enquiry/store', 'enquiryStore')->name('enquiry.store');
    Route::get('/service-support/book-service', 'bookService')->name('service-support.book-service');
    Route::post('/service-support/service-booking', 'serviceBookingStore')->name('service-booking.store');
});

// ajax routes

Route::controller(AjaxController::class)->group(function () {
    Route::post('/sub-category-by-model', 'getSubCategoryByModel')->name('ajax.sub-by-model');
    Route::post('/dealers-by-city', 'getDealersByCity')->name('ajax.dealers-by-city');
    Route::post('/models-by-category', 'getModelsByCategory')->name('ajax.models-by-category');
    Route::post('/dealer-by-id', 'getDealerById')->name('ajax.get-dealer-by-id');
    Route::post('/dealers-by-city-and-dealer', 'getDealersByCityAndDealer')->name('ajax.dealers-by-city-and-dealer');
    Route::post('/inventory/filter', 'inventoryFilter')->name('ajax.inventory.filter');
    Route::post('/service-centers-by-province', 'getServiceCentersByProvince')->name('ajax.service-centers-by-province');
    Route::post('/model-by-id', 'getModelById')->name('ajax.get-model-by-id');
    Route::post('/colors-by-model', 'getColorsByModel')->name('ajax.colors-model');
});

// Static Routes (Fixed Information)

Route::view('/online-enquiry', 'service_support.online_enquiry')->name('service-support.online-enquiry');
Route::view('/oem-warranty', 'service_support.oem')->name('service-support.oem');
Route::view('/maintenance-services', 'service_support.maintenance')->name('service-support.maintenance');
Route::view('/accessories', 'service_support.accessories')->name('service-support.accessories');
// Route::view('/support-services', 'service_support.support')->name('service-support.support');

Route::view('/rewards-program', 'rewards_and_campaign.rewards_program')->name('rewards.program');
Route::view('/special-service-campaign', 'rewards_and_campaign.special_service_campaign')->name('special-service.campaign');

Route::view('/electrification', 'electrified.electrification')->name('electrification');
Route::view('/hybrid-electric', 'electrified.hybrid_electrification')->name('hybrid-electric');

Route::view('/coming-soon', 'coming_soon')->name('coming-soon');
Route::view('/spare-part', 'service_support.spare_part')->name('service-support.spare-part');


Route::name('discover.')->group(function () {
    Route::view('/about-toyota-myanmar', 'discover_toyota.about')->name('about');
    Route::view('/toyota-myanmar-plant', 'discover_toyota.plant')->name('plant');
    Route::view('/toyota-dream-car-art-contest', 'discover_toyota.dream_car')->name('dream-car');
    Route::view('/global-architecture', 'discover_toyota.global_architecture')->name('global-architecture');
    Route::view('/safety-sense', 'discover_toyota.safety_sense')->name('safety-sense');

    Route::prefix('/about-toyota-myanmar')->group(function () {
        Route::view('/history-of-toyota', 'discover_toyota.history')->name('about.history');
        Route::view('/traditon-of-toyota', 'discover_toyota.tradition')->name('about.tradition');
        Route::view('/vision-and-philosophy', 'discover_toyota.vision')->name('about.vision');
        Route::view('/design-r&d-manufacturing-features', 'discover_toyota.design')->name('about.design');
        Route::view('/emblem', 'discover_toyota.emblem')->name('about.emblem');
    });
});

Route::group(['prefix' => '/toyota-dream-car-art-contest'], function () {
    Route::view('/2014_cart_art_content', 'discover_toyota.2014_cart_art_content')->name('dreamcart.2014Cart');
    Route::view('/2015_cart_art_content', 'discover_toyota.2015_cart_art_content')->name('dreamcart.2015Cart');
    Route::view('/2016_cart_art_content', 'discover_toyota.2016_cart_art_content')->name('dreamcart.2016Cart');
    Route::view('/2017_cart_art_content', 'discover_toyota.2017_cart_art_content')->name('dreamcart.2017Cart');
    Route::view('/2018_cart_art_content', 'discover_toyota.2018_cart_art_content')->name('dreamcart.2018Cart');
    Route::view('/2019_cart_art_content', 'discover_toyota.2019_cart_art_content')->name('dreamcart.2019Cart');
    Route::view('/2020_cart_art_content', 'discover_toyota.2020_cart_art_content')->name('dreamcart.2020Cart');
    Route::view('/2021_cart_art_content', 'discover_toyota.2021_cart_art_content')->name('dreamcart.2021Cart');
});
