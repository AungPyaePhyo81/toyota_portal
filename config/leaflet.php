<?php

return [
    'zoom_level'           => 6,
    'detail_zoom_level'    => 10,
    'map_center_latitude'  => env('MAP_CENTER_LATITUDE', '19.7990'),
    'map_center_longitude' => env('MAP_CENTER_LONGITUDE', '95.9560'),
];
